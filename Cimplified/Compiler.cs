using Cimplified.Lexer;
using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cimplified
{
	public class Compiler
	{
		private Lexer<Terminal> _lexer;
		private Parser.Parser _parser;

		public Compiler(Lexer<Terminal> lexer, Parser.Parser parser)
		{
			_lexer = lexer;
			_parser = parser;
		}

		public DiagnosticCollector Compile(List<String> source)
		{
			var collector = new DiagnosticCollector(source);
			_lexer.Emit = collector.Add;
			_parser.Emit = collector.Add;
			var tokens = _lexer.Process(source);
			var whitespacekind = CimGrammarCreator.Whitespace;
			tokens = tokens.Where(t => t.Kind != whitespacekind);
			List<ParseTree> parseTrees = new List<ParseTree>();
			foreach (var token in tokens)
				parseTrees.Add(new ParseLeaf(token));
			_parser.Parse(parseTrees);
			return collector;
		}

	}

}
