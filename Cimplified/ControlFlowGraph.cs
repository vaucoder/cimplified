using System;
using System.Collections.Generic;
using Cimplified.Parser;

namespace Cimplified
{

	public class ControlFlowGraph
	{
		public readonly Dictionary<Label, CFGNode> Map;
		public readonly Label Start;

		public ControlFlowGraph(Dictionary<Label, CFGNode> map, Label start)
		{
			Map = map;
			Start = start;
		}
	}

	public abstract class CFGNode
	{
		public abstract Statement S { get; }

		public class Unconditional : CFGNode
		{
			public Statement _s;
			public Label Next;

			public override Statement S { get => _s; }

			public Unconditional(Statement s, Label next)
			{
				this._s = s;
				this.Next = next;
			}
		}

		public class Conditional : CFGNode
		{
			public Expression Expr;
			public Label True;
			public Label False;

			public override Statement S { get => Expr; }

			public Conditional(Expression e, Label t, Label f)
			{
				this.Expr = e;
				this.True = t;
				this.False = f;
			}
		}
	}

}



