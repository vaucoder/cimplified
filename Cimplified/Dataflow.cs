using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Cimplified.Generator;

namespace Cimplified
{

	public class Dataflow<T, R>
	{

		public void RunForward(
			Func<T, IEnumerable<R>> gen,
			Func<T, IEnumerable<R>> kill,
			Action<ISet<R>, ISet<R>> join,
			IEnumerable<T> startNodes,
			Func<T, IEnumerable<T>> succ
			)
		{
			var pred = Utils.GraphReverseLink(startNodes, succ);
			var orderedNodes = Utils.GraphOrderVertexes(startNodes, succ);
			Run(gen, kill, join, orderedNodes, pred);
		}

		public void RunBackward(
			Func<T, IEnumerable<R>> gen,
			Func<T, IEnumerable<R>> kill,
			Action<ISet<R>, ISet<R>> join,
			IEnumerable<T> startNodes,
			Func<T, IEnumerable<T>> succ,
			IEnumerable<T> allNodes // TODO: this is optional really
			)
		{
			var pred = Utils.GraphReverseLink(startNodes, succ);
			var finalNodes = allNodes.Where((n) => succ(n).Count() == 0);
			var orderedNodes = Utils.GraphOrderVertexes(finalNodes, pred);
			Run(gen, kill, join, orderedNodes, succ);
		}

		public Dictionary<T, HashSet<R>> In
			= new Dictionary<T, HashSet<R>>();
		public Dictionary<T, HashSet<R>> Out
			= new Dictionary<T, HashSet<R>>();

		public Func<T, HashSet<R>> InitIn =
			(i) => new HashSet<R>();

		public Func<T, HashSet<R>> InitOut =
			(i) => new HashSet<R>();

		public HashSet<R> GetIn(T n)
		{
			HashSet<R> ret;
			if (!In.TryGetValue(n, out ret)) {
				ret = InitIn(n);
				In[n] = ret;
			}

			return ret;
		}

		public HashSet<R> GetOut(T n)
		{
			HashSet<R> ret;
			if (!Out.TryGetValue(n, out ret)) {
				ret = InitOut(n);
				Out[n] = ret;
			}

			return ret;
		}

		public void SetIn(T n, HashSet<R> val)
		{
			In[n] = val;
		}

		public void SetOut(T n, HashSet<R> val)
		{
			Out[n] = val;
		}

		public void Run(
			Func<T, IEnumerable<R>> gen,
			Func<T, IEnumerable<R>> kill,
			Action<ISet<R>, ISet<R>> join,
			IEnumerable<T> orderedNodes,
			Func<T, IEnumerable<T>> next
			)
		{
			bool converged;
			do {
				converged = true;

				foreach (var n in orderedNodes) {

					var inSave = new HashSet<R>(GetIn(n));
					var outSave = new HashSet<R>(GetOut(n));

					var newOut = new HashSet<R>();
					foreach (var v in next(n)) {
						join(newOut, GetIn(v));
					}
					SetOut(n, newOut);

					var diff = new HashSet<R>(newOut);
					diff.ExceptWith(kill(n));
					var newIn = new HashSet<R>(gen(n));
					newIn.UnionWith(diff);
					SetIn(n, newIn);

					if (!newIn.SetEquals(inSave) || !newOut.SetEquals(outSave)) {
						converged = false;
					}
				}

			} while (!converged);

		}
	}

}

