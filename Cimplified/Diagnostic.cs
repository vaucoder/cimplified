using System;
using System.Collections.Generic;
using Cimplified.Lexer;
using Cimplified.Parser;

namespace Cimplified
{

	public class DiagnosticCollector
	{
		private readonly List<string> _source;
		private readonly List<Diagnostic> _diagnostics = new List<Diagnostic>();
		public DiagnosticCollector(List<string> source) {
			_source = source;
		}

		public void Add(Diagnostic d)
		{
			_diagnostics.Add(d);
		}

		public List<Diagnostic> Diagnostics { get => _diagnostics; }
	}


	public abstract class Diagnostic : Exception
	{
		public abstract override string ToString();
	}

	
	public class UnknownAction : Diagnostic
	{
		private string _errMsg;

		public UnknownAction(string errMsg)
		{
			_errMsg = errMsg;
		}
		public override string ToString()
		{
			return _errMsg;
		}
	}
	
	public class NoExpectedToken : Diagnostic
	{
		private string _errMsg;

		public NoExpectedToken(string errMsg)
		{
			_errMsg = errMsg;
		}
		public override string ToString()
		{
			return _errMsg;
		}
	}
	
}