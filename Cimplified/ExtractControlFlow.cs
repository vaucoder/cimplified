using System;
using System.Collections.Generic;
using System.Linq;
using Cimplified.Parser;

namespace Cimplified
{

	public class ExtractControlFlowGraph
	{
		// Extract ControlFlowGraph from given function Block
		public static ControlFlowGraph Extract1(Block block)
		{
			return new ExtractControlFlowGraph(block).Result;
		}

		public readonly Statement St;
		public Dictionary<Label, CFGNode> Map = new Dictionary<Label, CFGNode>();
		public Stack<Label> WhileStack = new Stack<Label>();

		public ExtractControlFlowGraph(Statement st)
		{
			St = st;
		}

		public ControlFlowGraph Result { get => GetResult(); }

		public ControlFlowGraph GetResult()
		{
			var first = Loop(null, St);
			return new ControlFlowGraph(Map, first);
		}

		public Label Loop(Label next, Statement s)
		{
			switch (s) {
				case Group x:
					return ExtractGroup(next, x);
				case IfClause x:
					return ExtractIf(next, x);
				case WhileClause x:
					return ExtractWhile(next, x);
				case BreakClause x:
					return ExtractBreak(next, x);
				case ContinueClause x:
					return ExtractContinue(next, x);
				case ReturnClause x:
					return ExtractReturn(next, x);
				case Statement x:
					return ExtractSimpleStatement(next, x);
				default:
					throw new Exception($"Unexpected type {Utils.Dump(s)}");
			}
		}

		public Label ExtractGroup(Label next, Group g)
		{
			foreach (var s2 in g.Statements.Reverse()) {
				next = Loop(next, s2);
			}
			return next;
		}

		public Label ExtractIf(Label next, IfClause cl)
		{
			// Note: simplified if does not have "elifs"
			var trueLabel = Loop(next, cl.Body);
			var falseLabel = next;
			if (cl.ElseBody != null) {
				falseLabel = Loop(next, cl.ElseBody);
			}
			var ifLabel = new UniqueLabel();
			var ifNode = new CFGNode.Conditional(cl.Condition, trueLabel, falseLabel);
			Map[ifLabel] = ifNode;
			return ifLabel;
		}

		public Label ExtractWhile(Label next, WhileClause cl)
		{
			var whileLabel = new UniqueLabel();
			WhileStack.Push(whileLabel);
			var prologLabel = Loop(whileLabel, cl.Prolog);
			var bodyLabel = Loop(prologLabel, cl.Body);
			var whileNode = new CFGNode.Conditional(cl.Condition, bodyLabel, next);
			Map[whileLabel] = whileNode;
			WhileStack.Pop();
			return prologLabel;
		}

		public Label ExtractSimpleStatement(Label next, Statement st)
		{
			var l = new UniqueLabel();
			var node = new CFGNode.Unconditional(st, next);
			Map[l] = node;
			return l;
		}

		public Label ExtractBreak(Label next, BreakClause br)
		{
			var num = br.N == null ? 1 : int.Parse(br.N.Number);

			if (num < 1)
				throw new Exception("Break number too low");
			if (WhileStack.Count < num)
				throw new Exception("Break number too high");

			var wlabel = WhileStack.ElementAt(num - 1);
			var node = new CFGNode.Unconditional(br, wlabel);
			var l = new UniqueLabel();
			Map[l] = node;
			return l;
		}

		public Label ExtractContinue(Label next, ContinueClause cont)
		{
			var wlabel = WhileStack.Peek();
			var node = new CFGNode.Unconditional(cont, wlabel);
			var l = new UniqueLabel();
			Map[l] = node;
			return l;
		}

		public Label ExtractReturn(Label next, ReturnClause ret)
		{
			var node = new CFGNode.Unconditional(ret, null);
			var l = new UniqueLabel();
			Map[l] = node;
			return l;
		}

	}
}


