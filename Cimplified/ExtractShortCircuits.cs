using System;
using System.Collections.Generic;
using Cimplified.Parser;
using System.Linq;

using ExprType = Cimplified.Parser.Type;

namespace Cimplified
{

	public static class ExtractShortCircuits
	{
		// Extract1 rewrites statements that depend on short-circuit evaluation
		// by adding some conditional statements
		// so that after the extraction
		// only IfClause and WhileClause
		// can have embedded short-circuit expressions.
		//
		// Example 1:
		// x := a || b
		//      ->
		// if (a || b) x := true
		// else x := false
		//
		// Example 2:
		// f(a || b)
		//      ->
		// if (a || b) x := true
		// else x := false
		// f(x)

		public static bool ContainsShortCircuit(Expression expression)
		{
			return (TypeChecking.GetExpressionType(expression) == ExprType.Bool && !(expression is BoolLiteral)) || expression is FunctionCall;
		}

		public static bool ConstainsShortCircuitNonFunCall(Expression expression)
		{
			return TypeChecking.GetExpressionType(expression) == ExprType.Bool && !(expression is FunctionCall);
		}

		public static (List<Statement>, Expression) ExtractExpression(Expression expression)
		{
			var subStatements = new List<Statement>();
			var subExpression = expression;

			if(expression is FunctionCall)
			{
				var functionCall = expression as FunctionCall;
				var newArguments = new List<Expression>();
				var anyChange = false;

				foreach(var argument in functionCall.Arguments)
				{
					if (ContainsShortCircuit(argument))
					{
						var newVariable = VarName.FreshTemporary();
						var newDeclaration = new DeclareAssignment(new TypeName("Bool"), newVariable, new BoolLiteral(true));

						newVariable.Declaration = newDeclaration;
						newDeclaration.VarName.Declaration = newDeclaration;

						var ifBlock = new Block(new List<Statement>() { new Assignment(newVariable, new BoolLiteral(true))});
						var elseBlock = new Block(new List<Statement>() { new Assignment(newVariable, new BoolLiteral(false)) });

						subStatements.Add(newDeclaration);
						subStatements.Add(new IfClause(argument, ifBlock, new List<ElIfClause>(), elseBlock));

						//todo: rewrite function call
						newArguments.Add(new VarUse(newVariable));

						anyChange = true;
					} else
					{
						newArguments.Add(argument);
					}
				}

				if(anyChange)
					return (subStatements, new FunctionCall(functionCall.FunctionName, newArguments));

				return (subStatements, functionCall);
			}

			return (subStatements, subExpression);
		}

		public static List<Statement> ExtractDeclare(DeclareAssignment declaration)
		{
			List<Statement> result = new List<Statement>();

			if (!ContainsShortCircuit(declaration.Value))
				return new List<Statement>() { declaration };

			var (subStatements, subExpression) = ExtractExpression(declaration.Value);

			if (ConstainsShortCircuitNonFunCall(declaration.Value))
			{
				var ifBlock = new Block(new List<Statement>() {
						new Assignment(declaration.VarName, new BoolLiteral(true))
					});

				var elseBlock = new Block(new List<Statement>()
					{
						new Assignment(declaration.VarName, new BoolLiteral(false))
					});

				declaration.Value = new BoolLiteral(true);

				result.Add(declaration);

				result.AddRange(subStatements);

				result.Add(new IfClause(subExpression, ifBlock, new List<ElIfClause>(), elseBlock));
			} else
			{
				result.AddRange(subStatements);

				declaration.Value = subExpression;

				result.Add(declaration);
			}

			return result;
		}

		public static List<Statement> ExtractAssign(Assignment assignment)
		{
			List<Statement> result = new List<Statement>();

			if (!ContainsShortCircuit(assignment.Value))
				return new List<Statement>() { assignment };

			var (subStatements, subExpression) = ExtractExpression(assignment.Value);

			if (ConstainsShortCircuitNonFunCall(assignment.Value))
			{
				var ifBlock = new Block(new List<Statement>() {
						new Assignment(assignment.VarName, new BoolLiteral(true))
					});

				var elseBlock = new Block(new List<Statement>()
					{
						new Assignment(assignment.VarName, new BoolLiteral(false))
					});

				result.AddRange(subStatements);
				result.Add(new IfClause(subExpression, ifBlock, new List<ElIfClause>(), elseBlock));
			} else
			{
				result.AddRange(subStatements);
				result.Add(new Assignment(assignment.VarName, subExpression));
			}

			return result;
		}

		public static List<Statement> ExtractReturnClause(ReturnClause clause)
		{
			List<Statement> result = new List<Statement>();

			if (clause.ReturnExpression == null || !ContainsShortCircuit(clause.ReturnExpression))
				return new List<Statement>() { clause };

			var (subStatements, subExpression) = ExtractExpression(clause.ReturnExpression);

			if (ConstainsShortCircuitNonFunCall(clause.ReturnExpression))
			{
				var newVariable = VarName.FreshTemporary();
				var newDeclaration = new DeclareAssignment(new TypeName("Bool"), newVariable, new BoolLiteral(true));

				newVariable.Declaration = newDeclaration;
				newDeclaration.VarName.Declaration = newDeclaration;

				var ifBlock = new Block(new List<Statement>() {
						new Assignment(newVariable, new BoolLiteral(true))
					});

				var elseBlock = new Block(new List<Statement>()
					{
						new Assignment(newVariable, new BoolLiteral(false))
					});

				result.Add(newDeclaration);

				result.AddRange(subStatements);
				result.Add(new IfClause(subExpression, ifBlock, new List<ElIfClause>(), elseBlock));

				result.Add(new ReturnClause(new VarUse(newVariable)));
			}
			else
			{
				result.AddRange(subStatements);
				result.Add(new ReturnClause(subExpression));
			}

			return result;
		}


		public static Statement Extract1(Statement s)
		{
			List<Statement> result = new List<Statement>();

			if(s is DeclareAssignment)
			{
				result = ExtractDeclare(s as DeclareAssignment);
			}

			if(s is Assignment)
			{
				result = ExtractAssign(s as Assignment);
			}

			if(s is Expression)
			{
				var (subStatements, subExpression) = ExtractExpression(s as Expression);
				result.AddRange(subStatements);
				result.Add(subExpression);
			}

			if(s is ReturnClause)
			{
				result = ExtractReturnClause(s as ReturnClause);
			}

			if(s is Block)
			{
				var block = s as Block;

				result = block.Statements.Select(statement => Extract1(statement)).ToList();
			}

			if(result.Count != 0)
			{
				if (result.Count == 1)
					return result.First();

				return new FlatGroup(result);
			}
				
			return s;
		}

	}

}
