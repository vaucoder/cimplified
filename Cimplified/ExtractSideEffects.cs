using System;
using System.Collections.Generic;
using Cimplified.Parser;
using System.Linq;

namespace Cimplified
{

	public static class ExtractSideEffects
	{

		// If "expr" contains function calls then Extract1 factors them out like so:
		// x + f() + x + g()
		//   ->
		// y := f()
		// z := g()
		// x + y + x + z
		//
		// Order of factored out function calls is unspecified
		public static (List<Statement>, Expression) Extract1(Expression expr)
		{
			if (expr is VarUse || expr is Literal)
			{
				return (new List<Statement>(), expr);
			}
			else if (expr is UnaryOperation)
			{
				var unaryOp = (UnaryOperation) expr;
				var (l, e) = Extract1(unaryOp.Operand);
				unaryOp.Operand = e;
				return (l, unaryOp);
			} 
			else if (expr is BinaryOperation)
			{
				var binaryOp = (BinaryOperation) expr;
				var (l1, e1) = Extract1(binaryOp.Left);
				var (l2, e2) = Extract1(binaryOp.Right);
				binaryOp.Left = e1;
				binaryOp.Right = e2;
				return (l1.Concat(l2).ToList(), binaryOp);
			} 
			else if (expr is FunctionCall)
			{
				var funcCall = (FunctionCall) expr;
				var lst = new List<Statement>();
				for (int i = 0; i < funcCall.Arguments.Count; i++)
				{
					var (l, e) = Extract1(funcCall.Arguments[i]);
					lst.AddRange(l);
					funcCall.Arguments[i] = e;
				}
				var v = VarName.FreshTemporary();
				var declaration = new DeclareAssignment(null, v, funcCall);
				var varUse = new VarUse(v);
				declaration.VarName.Declaration = declaration;
				v.Declaration = declaration;
				varUse.Name.Declaration = declaration;
				return (lst.Append(declaration).ToList(), varUse);
			} 
			else 
			{
				throw new NotImplementedException();
			} 
		}

	}

}
