using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Immutable;
using static Cimplified.Arithmetic;

namespace Cimplified
{
	public class Function
	{
		public readonly Function Parent;

		// static_link is a hidden variable that is always stored
		// at offset zero from the frame pointer
		public readonly Variable StaticLink;

		public readonly string Name;

		// Used to call this function
		public readonly Label Label;

		private readonly List<Variable> _variables = new List<Variable>();

		private readonly List<Variable> _arguments = new List<Variable>();

		public ImmutableList<Variable> Variables => _variables.ToImmutableList();

		public ImmutableList<Variable> Arguments => _arguments.ToImmutableList();

		// Variables that are used in children code and which are not saved by caller
		public IEnumerable<Variable> CalleeSavedVariables =>
			_arguments.Take(ArgumentRegisters.Length)
			.Union(_variables)
			.Where(v => v.UsedInFunctions.Count > 1 || v.Reg == null);

		public Function(Function parent, string name = null)
		{
			Parent = parent;
			if (Parent == null) {
				StaticLink = null;
			} else {
				StaticLink = new Variable(this);
			}

			Name = name;
			if (Name == null) {
				Label = new UniqueLabel();
			} else {
				Label = new NamedLabel(GetAbsoluteName());
			}
		}

		public string GetAbsoluteName()
		{
			string s = Name;
			for (var curr = this.Parent; curr != null; curr = curr.Parent) {
				s += "/" + curr.Name;
			}
			return s;
		}

		// Creates new variable, which is an Argument of this function,
		// and remembers it (mutates the state of the Function instance)
		public Variable CreateArgument()
		{
			var argument = new Variable(this);
			_arguments.Add(argument);
			return argument;
		}

		// Creates new variable, which is not an Argument of this function,
		// and remembers it (mutates the state of the Function instance)
		public Variable CreateVariable()
		{
			var variable = new Variable(this);
			_variables.Add(variable);
			return variable;
		}

		// Registers in which arguments get stored during function call
		// This are the same registers as per System V AMD64 ABI convention
		public static HwRegister[] ArgumentRegisters = new HwRegister[] {
			HwRegister.RDI,
			HwRegister.RSI,
			HwRegister.RDX,
			HwRegister.RCX,
			HwRegister.R8,
			HwRegister.R9,
		};

		// Calculates element offset in bytes from EBP register value
		public static int GetElementOffset(int elem_index)
		{
			return elem_index * Utils.ElementSize;
		}

		// For configuration like this:
		//
		//     _arguments = { arg0, ..., arg8 }
		//     _variables = { var0, var1, var2 }
		//     Sharedvariables = { arg2, arg5, arg7, var0, var2 }
		//
		// The stack looks like this:
		//
		//     static_link
		//     arg8                    { caller saved arguments that did not fit into registers
		//     arg7
		//     arg6                    }
		//     return address
		//     old RBP              <- RBP
		//     arg2
		//     arg5
		//     var0
		//     var2                 <- RSP
		//
		//     (Note: stack grows down, low addressed are down)

		public int? GetVariableOffset(Variable v)
		{
			var index = _variables.IndexOf(v);
			if (index < 0)
				return null;

			var sharedIndex = CalleeSavedVariables.IndexOf(v);
			if (sharedIndex < 0)
				return null;

			return - 1 - sharedIndex;
		}

		public int? GetArgumentOffset(Variable v)
		{
			var index = _arguments.IndexOf(v);
			if (index < 0)
				return null;

			if (index >= ArgumentRegisters.Length)
			{
				// caller saved
				return +2 + (index - ArgumentRegisters.Length);
			}
			else
			{
				// callee saved
				var sharedIndex = CalleeSavedVariables.IndexOf(v);
				if (sharedIndex < 0)
					return null;
				else
					return - 1 - sharedIndex;
			}
		}

		// Returns operation for accessing this Function's frame from its body's code
		public static ValueOperation GenerateFramePointerAcess()
		{
			return new RegisterAccess(HwRegister.RBP);
		}

		public static ValueOperation GenerateStackAccess(ValueOperation framePtr, int index)
		{
			var offset = GetElementOffset(index);
			return new MemoryAccess(new Arithmetic.Plus(framePtr, new ConstValue(offset)));
		}

		// Generate access for a variable that is used in the body of current Function
		public ValueOperation GenerateVariableAccess(Variable v)
		{
			// Register variables are special
			if (v.Reg != null &&
				(_arguments.Contains(v) || _variables.Contains(v)) &&
				!CalleeSavedVariables.Contains(v))
			{
				return new RegisterAccess(v.Reg);
			}

			// Other variables are accessed in a general way
			return GenerateRelativeVariableAccess(v, GenerateFramePointerAcess());
		}

		public ValueOperation GenerateRelativeVariableAccess(Variable v, ValueOperation framePtr)
		{
			var ao = GetArgumentOffset(v);
			var vo = GetVariableOffset(v);

			if (v == StaticLink) {
				return GenerateStackAccess(framePtr, +2 + Math.Max(0, _arguments.Count - ArgumentRegisters.Length));
			} if (ao != null) {
				return GenerateStackAccess(framePtr, ao.Value);
			} else if (vo != null) {
				return GenerateStackAccess(framePtr, vo.Value);
			} else if (Parent != null) {
				var sl = GenerateRelativeVariableAccess(StaticLink, framePtr);
				return Parent.GenerateRelativeVariableAccess(v, sl);
			} else {
				throw new Exception($"Not my variable {v}");
			}
		}

		public List<Operation> GenerateCall(List<ValueOperation> args, ValueOperation result, Function caller)
		{
			List<Operation> ret = new List<Operation>();

			ValueOperation findStaticLink(Function current, ValueOperation framePtr)
			{
				if (current == null) {
					throw new Exception($"Function {this.GetAbsoluteName()} is not callable from {caller.GetAbsoluteName()}");
				} else if (current == this.Parent) {
					return framePtr;
				} else {
					var parentFramePtr =
						current.GenerateRelativeVariableAccess(current.StaticLink, framePtr);
					return findStaticLink(current.Parent, parentFramePtr);
				}
			}

			var registerArgs = args.Take(ArgumentRegisters.Length);
			var stackArgs = args.Skip(ArgumentRegisters.Length);

			// Save register arguments
			foreach (var (reg, arg) in ArgumentRegisters.Zip(registerArgs, (a, b) => (a, b))) {
				ret.Add(new RegisterWrite(reg, arg));
			}

			// Now goes StaticLink, but note that global functions
			// never access their static link so no need to push in their case
			if (this.Parent != null) {
				var staticLink = findStaticLink(caller, GenerateFramePointerAcess());
				ret.Add(new Push(staticLink));
			}

			// Push stack arguments
			foreach (var arg in stackArgs.Reverse()) {
				ret.Add(new Push(arg));
			}

			ret.Add(new Call(Label));

			// Save result to "result" after "this" return
			ret.Add(Utils.WriteToMemory(result, Utils.GetReturnAddress()));

			// Restore stack pointer
			// Callee left RSP before the return address
			//   so we need to add what is left to RPS
			//   namely the space for StaticLink + stackArgs
			var rspOffset = stackArgs.Count();
			if (this.Parent != null) {
				rspOffset += 1;
			}

			if (rspOffset != 0) {
				ret.Add(new RegisterWrite(HwRegister.RSP,
					new Arithmetic.Plus(
							new RegisterAccess(HwRegister.RSP),
							new ConstValue(rspOffset * Utils.ElementSize)
						)
					)
				);
			}

			return ret;
		}

		private long GetRequiredStack()
		{
			return (CalleeSavedVariables.Count() + 1) * Utils.ElementSize;
		}


		//stack layout |static_link|args_not_in_register(arg.Count > 6)|return_adr|<RSP
		//post genPrologue RBP>|static_link|args_not_in_register|return_adr|prev_rbp|args_from_register_space|variables|<RSP
		public List<Operation> GeneratePrologue()
		{
			var operations = new List<Operation>();

			var requiredStack = GetRequiredStack();


			operations.Add(new MemoryWrite() {
				Address =
					new Plus(
						new RegisterAccess(HwRegister.RSP),
						new ConstValue(-Utils.ElementSize)
					),
				Value = new RegisterAccess(HwRegister.RBP)
			});

			operations.Add(new RegisterWrite() {
				Reg = HwRegister.RBP,
				Value =
					new Plus(
						new RegisterAccess(HwRegister.RSP),
						new ConstValue(-Utils.ElementSize)
					)
			});

			operations.Add(new RegisterWrite() {
				Reg = HwRegister.RSP, 
				Value = new Plus(new RegisterAccess(HwRegister.RSP), new ConstValue(-1 * requiredStack)) });

			for(int i = 0; i < _arguments.Count; i++)
			{
				var opAccess = GenerateVariableAccess(_arguments[i]);

				if(opAccess is RegisterAccess)
				{
					var regAccess = opAccess as RegisterAccess;
					if(i < ArgumentRegisters.Length)
					{
						operations.Add(new RegisterWrite(regAccess.Register, new RegisterAccess(ArgumentRegisters[i])));
					} else
					{
						operations.Add(new RegisterWrite(regAccess.Register, GenerateStackAccess(GenerateFramePointerAcess(), GetArgumentOffset(_arguments[i]).Value)));
					}
				} else if (opAccess is MemoryAccess)
				{
					var memAccess = opAccess as MemoryAccess;
					if(i < ArgumentRegisters.Length)
					{
						operations.Add(new MemoryWrite(memAccess.Address, new RegisterAccess(ArgumentRegisters[i])));
					}
				}
			}

			return operations;
		}

		public List<Operation> GenerateEpilogue()
		{
			var operations = new List<Operation>();

			var requiredStack =  GetRequiredStack();

			operations.Add(new RegisterWrite() {
				Reg = HwRegister.RSP,
				Value = new Plus(new RegisterAccess(HwRegister.RSP), new ConstValue(requiredStack))
			});

			operations.Add(new RegisterWrite() {
				Reg = HwRegister.RBP,
				Value = new MemoryAccess(
						new Plus(new RegisterAccess(HwRegister.RSP),
						new ConstValue(-Utils.ElementSize)
					)
				)
			});

			operations.Add(new OpReturn());

			return operations;
		}
	}
}
