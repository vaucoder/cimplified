﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Cimplified.Generator
{
    public abstract class AsmInstruction
    {
        public virtual IReadOnlyList<Register> RegistersUsed => new List<Register>();
        public virtual IReadOnlyList<Register> RegistersDefined => new List<Register>();
        public virtual IReadOnlyList<Label> PossibleTargets => new List<Label> { null };
        public abstract string ToAsmString();

        public override string ToString()
        {
            return ToAsmString();
        }

        protected static string ToAsmString(IInstructionParameter param)
        {
            if (param is Constant c)
            {
                return c.Value.ToString();
            }
            else if (param is HwRegister r)
            {
                return r.Name.ToLower();
            }
            else if (param is NamedLabel l)
            {
               return l.Name;
            }
            else
            {
                throw new ArgumentException("Argument `param` must be a `Constant`, a `HwRegister` or a `NamedLabel`.");
            }
        }
        protected static List<Register> FilterRegisters(params IInstructionParameter[] parameters)
        {
            return parameters.Where(x => x is Register).Cast<Register>().Distinct().ToList();
        }
    }
    public abstract class NullaryAsmInstruction : AsmInstruction
    {

    }
    public abstract class UnaryAsmInstruction<A> : AsmInstruction where A : IInstructionParameter
    {
        protected A a;
    }
    public abstract class BinaryAsmInstruction<A, B> : AsmInstruction where A : IInstructionParameter where B : IInstructionParameter
    {
        protected A a;
        protected B b;
    }
    public class AddInstruction : BinaryAsmInstruction<Register, IInstructionParameter>
    {
        // add a b
        public AddInstruction(Register a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            return $"add {ToAsmString(a)} {ToAsmString(b)}";
        }

        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a, b);
    }
    public class MulInstruction : UnaryAsmInstruction<Register>
    {
        // mul a
        public MulInstruction(Register a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"mul {ToAsmString(a)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(HwRegister.RAX);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(HwRegister.RAX, a);
    }
    public class DivInstruction : UnaryAsmInstruction<Register>
    {
        // div a
        public DivInstruction(Register a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"div {ToAsmString(a)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(HwRegister.RAX);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(HwRegister.RAX, a);
    }
    public class SubInstruction : BinaryAsmInstruction<Register, IInstructionParameter>
    {
        // sub a b
        public SubInstruction(Register a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            return $"sub {ToAsmString(a)} {ToAsmString(b)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a, b);
    }
    public class SeteInstruction : UnaryAsmInstruction<Register>
    {
        // sete a
        public SeteInstruction(Register a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"sete {ToAsmString(a)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
    }
    public class SetbInstruction : UnaryAsmInstruction<Register>
    {
        // setb a
        public SetbInstruction(Register a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"setb {ToAsmString(a)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
    }
    public class AndInstruction : BinaryAsmInstruction<Register, IInstructionParameter>
    {
        // and a b
        public AndInstruction(Register a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            return $"and {ToAsmString(a)} {ToAsmString(b)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a, b);
    }
    public class OrInstruction : BinaryAsmInstruction<Register, IInstructionParameter>
    {
        // or a b
        public OrInstruction(Register a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            return $"or {ToAsmString(a)} {ToAsmString(b)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a, b);
    }
    public class NotInstruction : UnaryAsmInstruction<Register>
    {
        // not a
        public NotInstruction(Register a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"not {ToAsmString(a)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a);
    }
    public class MovInstruction : BinaryAsmInstruction<Register, IInstructionParameter>
    {
        // mov a b
        public MovInstruction(Register a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            if (a.Equals(b))
            {
                return "";
            }
            return $"mov {ToAsmString(a)} {ToAsmString(b)}";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(b);

    }
    public class MovFromMemoryInstruction : BinaryAsmInstruction<Register, IInstructionParameter>
    {
        // mov a [b]
        public MovFromMemoryInstruction(Register a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            return $"mov {ToAsmString(a)} [{ToAsmString(b)}]";
        }
        public override IReadOnlyList<Register> RegistersDefined => FilterRegisters(a);
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(b);

    }
    public class MovToMemoryInstruction : BinaryAsmInstruction<IInstructionParameter, IInstructionParameter>
    {
        // mov [a] b
        public MovToMemoryInstruction(IInstructionParameter a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            return $"mov [{ToAsmString(a)}] {ToAsmString(b)}";
        }
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a, b);

    }
    public class PushInstruction : UnaryAsmInstruction<IInstructionParameter>
    {
        // push a
        public PushInstruction(IInstructionParameter a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"push {ToAsmString(a)}";
        }
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a);
    }

    public class CallInstruction : UnaryAsmInstruction<Label>
    {
        // call a
        public CallInstruction(Label a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"call {ToAsmString(a)}";
        }
        public override IReadOnlyList<Label> PossibleTargets => new List<Label> { a };

    }
    public class RetInstruction : NullaryAsmInstruction
    {
        // ret
        public override string ToAsmString()
        {
            return "ret";
        }
        public override IReadOnlyList<Label> PossibleTargets => new List<Label>();
    }
    public class JmpInstruction : UnaryAsmInstruction<Label>
    {
        // jmp a
        public JmpInstruction(Label a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"jmp {ToAsmString(a)}";
        }
        public override IReadOnlyList<Label> PossibleTargets => new List<Label> { a };
    }
    public class CmpInstruction : BinaryAsmInstruction<IInstructionParameter, IInstructionParameter>
    {
        // cmp a b
        public CmpInstruction(IInstructionParameter a, IInstructionParameter b)
        {
            this.a = a;
            this.b = b;
        }
        public override string ToAsmString()
        {
            return $"cmp {ToAsmString(a)} {ToAsmString(b)}";
        }
        public override IReadOnlyList<Register> RegistersUsed => FilterRegisters(a, b);
    }
    public class JzInstruction : UnaryAsmInstruction<Label>
    {
        // jz a
        public JzInstruction(Label a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"jz {ToAsmString(a)}";
        }
        public override IReadOnlyList<Label> PossibleTargets => new List<Label> { null, a };
    }
    public class JnsInstruction : UnaryAsmInstruction<Label>
    {
        // jns a
        public JnsInstruction(Label a)
        {
            this.a = a;
        }
        public override string ToAsmString()
        {
            return $"jns {ToAsmString(a)}";
        }
        public override IReadOnlyList<Label> PossibleTargets => new List<Label> { null, a };
    }
}
