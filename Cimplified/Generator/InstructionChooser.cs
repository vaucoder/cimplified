﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimplified.Generator
{
	using InstructionFunc = Func<Register, List<IInstructionParameter>, List<AsmInstruction>>;
	
	public class InstructionChooser
	{
		private class OpSizeComparer : IComparer<(Operation, InstructionFunc)>
		{
			private static int RecursiveSize(Operation op)
			{
				if (op is null)
					return 0;
				int size = 1;
				Type type = op.GetType();
				foreach (var field in type.GetFields())
					if (typeof(Operation).IsAssignableFrom(field.FieldType))
						size += RecursiveSize((Operation)field.GetValue(op));
				return size;
			}

			public int Compare((Operation, InstructionFunc) x, (Operation, InstructionFunc) y)
			{
				var sizeX = RecursiveSize(x.Item1);
				var sizeY = RecursiveSize(y.Item1);
				if (sizeX < sizeY)
					return 1;
				if (sizeX > sizeY)
					return -1;
				return 0;
			}

		}

		public readonly InstructionSet InstructionSet;

		public InstructionChooser(InstructionSet instructionSet)
		{
			instructionSet.Sort(new OpSizeComparer());
			InstructionSet = instructionSet;
		}

		private static void RecursiveCmp(Operation template, Operation opTree, List<object> subs)
		{
			if (template is null || opTree is null)
				throw new ArgumentException("Doesn't match!");
			Type type = template.GetType();
			if (opTree.GetType() != type)
				throw new ArgumentException("Doesn't match!");
			foreach (var field in type.GetFields())
			{
				if (typeof(Operation).IsAssignableFrom(field.FieldType))
				{
					if (field.GetValue(template) is null && field.GetValue(opTree) != null)
						subs.Add(field.GetValue(opTree));
					else
						RecursiveCmp((Operation)field.GetValue(template), (Operation)field.GetValue(opTree), subs);
				}
				else if (typeof(IInstructionParameter).IsAssignableFrom(field.FieldType))
				{
					if (field.GetValue(template) is null && field.GetValue(opTree) != null)
						subs.Add(field.GetValue(opTree));
				}
			}
		}

		private static IEnumerable<object> FindMatch(Operation template, Operation opTree)
		{
			List<object> subs = new List<object>();
			try
			{
				RecursiveCmp(template, opTree, subs);
				return subs;
			}
			catch (ArgumentException e)
			{
				return null;
			}
		}

		public IEnumerable<AsmInstruction> ChooseInstruction(Operation operation, Register returnRegister=null)
		{
			if (returnRegister is null)
				returnRegister = HwRegister.RAX;
			foreach (var (template, f) in InstructionSet)
			{
				var subs = FindMatch(template, operation);
				if (subs is null)
					continue;
				var parameters = new List<IInstructionParameter>();
				var subtrees = new List<(Register, Operation)>();
				foreach (var sub in subs)
				{
					if (sub is IInstructionParameter param)
						parameters.Add(param);
					else if (sub is Operation op)
					{
						var virtualReg = new VirtualRegister();
						parameters.Add(virtualReg);
						subtrees.Add((virtualReg, op));
					}
					else
						throw new ArgumentException("Operation template for unknown object type");
				}
				var asmInstructions = new List<AsmInstruction>();
				foreach (var (res, subtree) in subtrees)
					asmInstructions.AddRange(ChooseInstruction(subtree, res));
				asmInstructions.AddRange(f(returnRegister, parameters));
				return asmInstructions;
			}
			throw new KeyNotFoundException("Couldn't find operation template for giver operation tree");
		}
	}
}
