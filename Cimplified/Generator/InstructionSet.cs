﻿using System;
using System.Collections.Generic;
using System.Text;
using Cimplified;

namespace Cimplified.Generator
{ 
	using InstructionFunc = Func<Register, List<IInstructionParameter>, List<AsmInstruction>>;
	
	public interface IInstructionParameter
	{

    }

	public class Constant : IInstructionParameter
	{
		public long Value { get; set; }

		public Constant(Int64 value)
		{
			Value = value;
		}
	}

	public class InstructionSet : List<(Operation, InstructionFunc)>
	{

    }

    public class InstructionSetCreator
    {
        (Operation, InstructionFunc) createPlusInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.Plus(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(HwRegister.RAX, parameters[0]),
                    new AddInstruction(HwRegister.RAX, parameters[1]),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createMultInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.Mult(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(HwRegister.RAX, parameters[0]),
                    new MovInstruction(HwRegister.RBX, parameters[0]),
                    new MulInstruction(HwRegister.RBX),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createDivInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.Div(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(HwRegister.RAX, parameters[0]),
                    new MovInstruction(HwRegister.RBX, parameters[0]),
                    new DivInstruction(HwRegister.RBX),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createSubInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.Plus(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(HwRegister.RAX, parameters[0]),
                    new SubInstruction(HwRegister.RAX, parameters[1]),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createEqInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.Eq(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new CmpInstruction(parameters[0], parameters[1]),
                    new SeteInstruction(HwRegister.RAX),
                    new AndInstruction(HwRegister.RAX, new Constant(1)),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createLeqInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.Leq(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new CmpInstruction(parameters[0], parameters[1]),
                    new SetbInstruction(HwRegister.RAX),
                    new AndInstruction(HwRegister.RAX, new Constant(1)),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createAndInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.And(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(HwRegister.RAX, parameters[0]),
                    new AndInstruction(HwRegister.RAX, parameters[1]),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createOrInstruction()
        {
            ValueOperation left = null;
            ValueOperation right = null;
            var operation = new Arithmetic.Or(left, right);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(HwRegister.RAX, parameters[0]),
                    new OrInstruction(HwRegister.RAX, parameters[1]),
                    new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createNegationInstruction()
        {
            ValueOperation operand = null;
            var operation = new Arithmetic.Negation(operand);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(HwRegister.RAX, parameters[0]),
                    new NotInstruction(HwRegister.RAX),
                    new AndInstruction(HwRegister.RAX, new Constant(1)), // true = 0...01
					new MovInstruction(register, HwRegister.RAX)
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createPushInstruction()
        {
            ValueOperation value = null;
            var operation = new Push(value);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new PushInstruction(parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createMemoryAccessInstruction()
        {
            ValueOperation adress = null;
            var operation = new MemoryAccess(adress);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovFromMemoryInstruction(register, parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createRegisterAccessInstruction()
        {
            Register sourceRegister = null;
            var operation = new RegisterAccess(sourceRegister);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(register, parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createRegisterWriteInstruction()
        {
            Register targetRegister = null;
            ValueOperation value = null;
            var operation = new RegisterWrite(targetRegister, value);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction((Register) parameters[0], parameters[1])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createMemoryWriteInstruction()
        {
            ValueOperation targetAdress = null;
            ValueOperation value = null;
            var operation = new MemoryWrite(targetAdress, value);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovToMemoryInstruction(parameters[0], parameters[1])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createNoopInstruction()
        {
            var operation = new Noop();
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction> { };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createConstValueInstruction()
        {
            Constant constant = null;
            var operation = new ConstValue(constant);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new MovInstruction(register, (Constant)parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createCallInstruction()
        {
            Label target = null;
            var operation = new Call(target);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new CallInstruction((Label)parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createReturnInstruction()
        {
            var operation = new OpReturn();
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new RetInstruction()
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createUnconditionalJumpInstruction()
        {
            Label target = null;
            var operation = new Jump.Unconditional(target);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new JmpInstruction((Label) parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createIfZeroJumpInstruction()
        {
            Label target = null;
            ValueOperation value = null;
            var operation = new Jump.IfZero(target, value);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new CmpInstruction(parameters[1], new Constant(0)),
                    new JzInstruction((Label) parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        (Operation, InstructionFunc) createIfPositiveJumpInstruction()
        {
            Label target = null;
            ValueOperation value = null;
            var operation = new Jump.IfPositive(target, value);
            InstructionFunc func = (register, parameters) =>
            {
                return new List<AsmInstruction>{
                    new CmpInstruction(parameters[1], new Constant(0)),
                    new JnsInstruction((Label) parameters[0])
                };
            };
            return ((Operation)operation, func);
        }
        public InstructionSet Create()
        {
            return new InstructionSet{
                createPlusInstruction(),
                createMultInstruction(),
                createDivInstruction(),
                createSubInstruction(),
                createEqInstruction(),
                createLeqInstruction(),
                createAndInstruction(),
                createOrInstruction(),
                createNegationInstruction(),
                createPushInstruction(),
                createMemoryAccessInstruction(),
                createRegisterAccessInstruction(),
                createRegisterWriteInstruction(),
                createMemoryWriteInstruction(),
                createNoopInstruction(),
                createConstValueInstruction(),
                createCallInstruction(),
                createReturnInstruction(),
                createUnconditionalJumpInstruction(),
                createIfZeroJumpInstruction(),
                createIfPositiveJumpInstruction()
            };
        }
    }
}
