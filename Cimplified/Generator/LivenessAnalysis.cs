﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Cimplified.Generator
{
	public class LivenessAnalysis
	{
		public static ISet<(Register, Register)> Analyse(IReadOnlyList<(Label, AsmInstruction)> instructionList)
		{
			var df = new Dataflow<AsmInstruction, Register>();

			var first = instructionList.First().Item2;
			var start = new List<AsmInstruction>() { first };
			var all = instructionList.Select((i) => i.Item2);
			var graph = Utils.InstructionListToGraph(instructionList);
			df.RunBackward(
				(i) => i.RegistersUsed, // gen
				(i) => i.RegistersDefined, // kill
				(o, x) => o.UnionWith(x), // join
				start,
				(i) => graph[i], // succ
				all);

			var interference = new HashSet<(Register, Register)>();
			void addPairs(IEnumerable<Register> conflicts)
			{
				foreach (var x in conflicts) {
					foreach (var y in conflicts) {
						if (x != y) {
							interference.Add((x, y));
						}
					}
				}
			}

			var H = new HashSet<AsmInstruction>();
			void loop(AsmInstruction current)
			{
				if (H.Contains(current)) {
					return;
				}

				H.Add(current);

				var outs = df.Out[current];

				foreach (var v in graph[current]) {
					var intersection = new HashSet<Register>(outs);
					intersection.IntersectWith(df.In[v]);
					addPairs(intersection);
					loop(v);
				}
			}

			addPairs(df.In[first]);
			loop(first);

			return interference;
		}
	}
}
