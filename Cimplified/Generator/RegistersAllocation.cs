using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Cimplified.Generator
{
	public class RegistersAllocation
	{
		private class RegisterGraph
		{
			private class RegisterEdge
			{
				private readonly RegisterNode _first;
				private readonly RegisterNode _second;

				public RegisterEdge(RegisterNode dest1, RegisterNode dest2)
				{
					_first = dest1;
					_second = dest2;
				}


				public RegisterNode Destination(RegisterNode source)
				{
					if (source != _first && source != _second)
						throw new Exception("Edge register error");

					return _first == source ? _second : _first;
				}
			}

			private class RegisterNode : IComparable<RegisterNode>
			{
				private readonly Register _nodeValue;
				private ISet<RegisterEdge> _edges;
				private ISet<RegisterNode> _neighbours;
				private readonly bool _isSpecial;

				private int _registerCount;
				private ISet<RegisterNode> _copies;

				private bool _needsCheck;
				private RegisterNode _mergeCandidate;

				private bool _removed;

				private readonly ISet<Register> _mergedWith;

				public Register Register { get => _nodeValue; }
				public int Degree { get => _edges.Count; }
				public ISet<RegisterNode> Copies { get => _copies; }
				public ISet<RegisterEdge> Edges { get => _edges; }
				public ISet<RegisterNode> Neighbours { get => _neighbours; }
				public ISet<Register> MergedWith { get => _mergedWith; }
				public bool Special { get => _isSpecial; }

				public RegisterNode(Register node, int registerCount, bool isSpecial)
				{
					_nodeValue = node;
					_registerCount = registerCount;
					_needsCheck = true;
					_isSpecial = isSpecial;

					_mergedWith = new HashSet<Register>();

					_removed = false;
				}

				public RegisterNode(Register node, int registerCount, bool isSpecial, ISet<Register> mergedWith)
				{
					_nodeValue = node;
					_registerCount = registerCount;
					_needsCheck = true;
					_isSpecial = isSpecial;

					_mergedWith = mergedWith;

					_removed = false;
				}

				public void InitEdges(ISet<RegisterEdge> edges, ISet<RegisterNode> copies)
				{
					_edges = edges;
					_copies = copies;
					_neighbours = edges.Select(x => x.Destination(this)).ToHashSet();
				}

				public void MarkRemoved()
				{
					_removed = true;
				}

				//CheckCoalesce -> checks if vertex can merge with any of copy nodes

				private void CheckMergeCandidates()
				{
					if (!_needsCheck)
						return;

					_mergeCandidate = null;
					_needsCheck = false;

					if (_isSpecial || _removed)
						return;

					foreach (var node in _copies)
					{
						if (node.Neighbours.Contains(this) || node._isSpecial || node._removed)
							continue;

						var commonNeighbourset = Neighbours.Where(x => x.Degree >= _registerCount).ToHashSet();

						commonNeighbourset.UnionWith(node.Neighbours.Where(x => x.Degree >= _registerCount).ToHashSet());

						if (commonNeighbourset.Count < _registerCount)
						{
							_mergeCandidate = node;
							return;
						}
					}
				}

				public RegisterNode GetMergeCandidate()
				{
					CheckMergeCandidates();

					return _mergeCandidate;
				}

				public bool IsMergeable()
				{
					CheckMergeCandidates();

					return _mergeCandidate != null;
				}

				public void ResetMergeCheck()
				{
					_needsCheck = true;
				}

				public IEnumerable<RegisterNode> RemoveEdge(RegisterEdge edge)
				{
					var destination = edge.Destination(this);
					IEnumerable<RegisterNode> result = new List<RegisterNode>();

					if (destination.Degree >= _registerCount)
					{
						result = _neighbours.Where(x => x != destination).Append(this);
					}

					_neighbours.Remove(destination);
					_edges.Remove(edge);

					return result;
				}

				public IEnumerable<RegisterNode> ReplaceEdge(RegisterEdge newEdge, RegisterEdge prevEdge)
				{
					var newDestination = newEdge.Destination(this);
					var prevDest = prevEdge.Destination(this);

					_edges.Add(newEdge);
					_neighbours.Add(newDestination);

					_neighbours.Remove(prevDest);
					_edges.Remove(prevEdge);

					IEnumerable<RegisterNode> result = new List<RegisterNode>();

					if (newDestination.Degree >= _registerCount)
					{
						result = _neighbours.Where(x => x != newDestination).Append(this);
					}

					return result;
				}

				// sort by degree, if is HwRegister it should land at the end of sort, if equal degree return some order e.g from GetHashCode
				public int CompareTo(RegisterNode other)
				{
					if (ReferenceEquals(this, other))
						return 0;

					if (other is null)
						return 1;

					if (_isSpecial && other._isSpecial)
						return this.GetHashCode() - other.GetHashCode();

					if (_isSpecial)
						return 1;

					if (other._isSpecial)
						return -1;

					if (other.Degree - Degree == 0)
						return this.GetHashCode() - other.GetHashCode();

					return Degree - other.Degree;
				}

				public void AppendMerge(ISet<Register> newNodes, ISet<RegisterEdge> newEdges, ISet<RegisterNode> newNeighbours)
				{
					_mergedWith.UnionWith(newNodes);

					_edges.UnionWith(newEdges);
					_neighbours.UnionWith(newNeighbours);
				}
			}

			private int _registerCount;
			private List<RegisterNode> _order;
			private ISet<RegisterNode> _mergeCandidates;
			private SortedSet<RegisterNode> _unprocessedRegisters;

			private RegisterGraph(int registerCount, ISet<RegisterNode> nodes)
			{
				_order = null;
				_mergeCandidates = nodes;
				_unprocessedRegisters = new SortedSet<RegisterNode>(nodes);
				_registerCount = registerCount;
			}


			private void Coalesce(RegisterNode mergedNode, RegisterNode mergeWith)
			{
				//reset _byDeg and _coalescable

				var neihbourSet = mergedNode.Neighbours;
				neihbourSet.UnionWith(mergeWith.Neighbours);

				_unprocessedRegisters.Remove(mergeWith);
				_unprocessedRegisters.Remove(mergedNode);

				mergeWith.MarkRemoved();

				_mergeCandidates.Remove(mergeWith);
				_mergeCandidates.Remove(mergedNode);

				foreach (var common in neihbourSet)
				{
					_unprocessedRegisters.Remove(common);
				}

				var addMergedWith = mergeWith.MergedWith;
				addMergedWith.Add(mergeWith.Register);

				var edgesSecond = mergeWith.Edges.Select(x => (x, x.Destination(mergeWith))).ToDictionary(x => x.Item2, x => x.Item1);

				var setOfNewEdges = new HashSet<RegisterEdge>();
				var setOfNewNeighbours = new HashSet<RegisterNode>();


				foreach(var node in edgesSecond.Keys)
				{
					if (mergedNode.Neighbours.Contains(node))
					{
						var candidates = node.RemoveEdge(edgesSecond[node]);

						UpdateCoalescable(candidates);
					} else
					{
						var newEdge = new RegisterEdge(mergedNode, node);

						var candidates = node.ReplaceEdge(newEdge, edgesSecond[node]);

						UpdateCoalescable(candidates);

						setOfNewEdges.Add(newEdge);
						setOfNewNeighbours.Add(node);
					}
				}


				mergedNode.AppendMerge(addMergedWith, setOfNewEdges, setOfNewNeighbours);

				//append _byDeg and _coalescable

				mergedNode.ResetMergeCheck();

				_mergeCandidates.Add(mergedNode);
				_unprocessedRegisters.Add(mergedNode);

				foreach (var common in neihbourSet)
				{
					_unprocessedRegisters.Add(common);
				}
			}

			private void DeleteNode(RegisterNode toDelete)
			{
				var edges = toDelete.Edges;

				toDelete.MarkRemoved();

				foreach (var edge in edges)
				{
					var destination = edge.Destination(toDelete);

					var candidates = destination.RemoveEdge(edge);

					UpdateCoalescable(candidates);
				}
			}

			private (List<Register>, Dictionary<Register, List<Register>>) FromOrder()
			{
				return (_order.Select(x => x.Register).ToList(), _order.ToDictionary(x => x.Register, x => x.MergedWith.ToList()));
			}

			private void UpdateCoalescable(IEnumerable<RegisterNode> nodes)
			{
				foreach (var node in nodes)
				{
					node.ResetMergeCheck();
				}

				_mergeCandidates.UnionWith(nodes);
			}


			public bool HandleMerging()
			{
				var anyDone = false;

				while (_mergeCandidates.Count > 0)
				{
					var node = _mergeCandidates.Take(1).Single();
					_mergeCandidates.Remove(node);

					if (node.IsMergeable())
					{
						var with = node.GetMergeCandidate();

						Coalesce(node, with);

						anyDone = true;
					}
				}

				return anyDone;
			}

			private void AddToSortedStack(IList<RegisterNode> nodes, Stack<RegisterNode> revOrder)
			{
				var commonNeighbours = new HashSet<RegisterNode>();

				foreach (var node in nodes)
				{
					commonNeighbours.UnionWith(node.Neighbours);
					_unprocessedRegisters.Remove(node);
				}

				commonNeighbours.ExceptWith(nodes);

				foreach (var common in commonNeighbours)
				{
					_unprocessedRegisters.Remove(common);
				}


				foreach (var node in nodes)
				{
					DeleteNode(node);

					revOrder.Push(node);
				}

				foreach (var common in commonNeighbours)
				{
					_unprocessedRegisters.Add(common);
				}
			}

			private bool HandleOrdering(Stack<RegisterNode> revOrder)
			{
				int i = 0;
				var iter = _unprocessedRegisters.GetEnumerator();

				var toRemove = new List<RegisterNode>();

				while (iter.MoveNext() && iter.Current.Degree < _registerCount && !iter.Current.Special)
				{

					if (!iter.Current.IsMergeable())
					{
						toRemove.Add(iter.Current);
					}
				}

				AddToSortedStack(toRemove, revOrder);

				return toRemove.Count > 0;
			}

			public (List<Register>, Dictionary<Register, List<Register>>) OrderRegisters()
			{
				if (_order != null)
					return FromOrder();

				var vertexStack = new Stack<RegisterNode>();

				_order = new List<RegisterNode>();

				while (_unprocessedRegisters.Count > 0)
				{
					if (HandleOrdering(vertexStack))
						continue;

					if (!HandleMerging())
					{
						var minNode = _unprocessedRegisters.Min;

						AddToSortedStack(new List<RegisterNode>() { minNode }, vertexStack);
					}
				}

				while (vertexStack.Count > 0)
				{
					_order.Add(vertexStack.Pop());
				}

				return FromOrder();
			}

			public static RegisterGraph CreateOrderGraph(int registerCount, IEnumerable<Register> toAllocate, IEnumerable<(Register, Register)> conflicts, IEnumerable<(Register, Register)> copies)
			{
				var copyDict = copies.GroupBy(x => x.Item1).ToDictionary(x => x.Key, x => x.Select(y => y.Item2).ToHashSet());

				foreach (var node in toAllocate)
				{
					if (!copyDict.ContainsKey(node))
						copyDict[node] = new HashSet<Register>();
				}

				var nodes = toAllocate.ToDictionary(x => x, x => new RegisterNode(x, registerCount, x is HwRegister));

				var edges = new Dictionary<RegisterNode, IList<RegisterEdge>>();

				foreach (var node in nodes.Values)
				{
					edges[node] = new List<RegisterEdge>();
				}

				foreach (var conflict in conflicts)
				{
					var node1 = nodes[conflict.Item1];
					var node2 = nodes[conflict.Item2];

					if (node1.Register.GetHashCode() < node2.Register.GetHashCode())
					{
						var edge = new RegisterEdge(node1, node2);

						edges[node1].Add(edge);
						edges[node2].Add(edge);
					}
				}

				foreach (var node in nodes.Values)
				{
					node.InitEdges(edges[node].ToHashSet(), copyDict[node.Register].Select(x => nodes[x]).ToHashSet());
				}

				return new RegisterGraph(registerCount, nodes.Values.ToHashSet());
			}
		}

		public class ColorGraph<V>
		{
			private class Vertex<V>
			{
				private int? _color;
				private readonly V _node;
				private ISet<Vertex<V>> _neigbhours;
				private int _colorCount;

				public Vertex(V node, int colorCount, int? preColored)
				{
					_node = node;
					_color = preColored;
					_colorCount = colorCount;
				}

				public void AddNeigbhours(ISet<Vertex<V>> neigbhours)
				{
					_neigbhours = neigbhours;
				}

				public V Node { get => _node; }

				public bool IsColored { get => _color.HasValue; }
				public int Color { get => _color.Value; }

				public bool TryColor()
				{
					if (IsColored)
						return _color < _colorCount;

					var neighbourColors = _neigbhours.Where(n => n.IsColored).Select(n => n.Color).ToHashSet();

					int color = 0;

					while (neighbourColors.Contains(color))
					{
						color++;
					}

					_color = color;

					return _color < _colorCount;
				}

				public void AddEdges(ISet<Vertex<V>> neighbours)
				{
					_neigbhours = neighbours;
				}
			}

			private readonly IDictionary<V, Vertex<V>> _graph;
			private readonly int _colorCount;

			private ColorGraph(IDictionary<V, Vertex<V>> graph, int colorCount)
			{
				_graph = graph;
				_colorCount = colorCount;
			}

			public (Dictionary<V, int>, List<V>) ColorByOrder(IEnumerable<V> order)
			{
				var spilled = new List<V>();

				foreach (var vertex in order)
				{
					var node = _graph[vertex];

					if (!node.TryColor())
					{
						spilled.Add(node.Node);
					}
				}

				var coloredMap = new Dictionary<V, int>();

				foreach (var vertex in order)
				{
					var node = _graph[vertex];

					if (node.Color < _colorCount)
						coloredMap[vertex] = node.Color;
				}

				return (coloredMap, spilled);
			}

			public static ColorGraph<V> CreateColorGraph(IEnumerable<V> vertexes, IEnumerable<(V, V)> edges, int colorCount, Func<V, int?> _precolorFunc, Dictionary<V, List<V>> mergedWith)
			{
				var nodes = vertexes.ToDictionary(x => x, x => new Vertex<V>(x, colorCount, _precolorFunc(x)));

				var edgeSets = new Dictionary<V, ISet<Vertex<V>>>();

				foreach (var node in nodes.Keys)
				{
					edgeSets[node] = new HashSet<Vertex<V>>();
				}

				foreach (var edge in edges)
				{
					edgeSets[edge.Item1].Add(nodes[edge.Item2]);
				}

				foreach (var node in nodes)
				{
					var vertex = node.Key;

					if (mergedWith.ContainsKey(vertex))
					{
						foreach (var mergeVertex in mergedWith[vertex])
						{
							edgeSets[vertex].UnionWith(edgeSets[mergeVertex]);
						}
					}

					node.Value.AddNeigbhours(edgeSets[vertex]);
				}

				return new ColorGraph<V>(nodes, colorCount);
			}
		}

		public static HwRegister GetHwRegisterFromColor(int color, IList<HwRegister> hwRegisters)
		{
			if (color < hwRegisters.Count)
				return hwRegisters.ElementAt(color);

			return null;
		}

		public static Func<Register, int?> GeneratePreColorFunc(IList<HwRegister> registers) 
		{
			Dictionary<HwRegister, int> hwRegisterMap = new Dictionary<HwRegister, int>();

			for (int i = 0; i < registers.Count; i++)
			{
				hwRegisterMap[registers[i]] = i;
			}

			int? precolorFunc(Register register)
			{
				if (register is HwRegister)
					return hwRegisterMap[register as HwRegister];

				return null;
			}

			return precolorFunc;
		}

		public static (Dictionary<Register, HwRegister>, List<Register>) Allocate(IEnumerable<Register> toAllocate, IEnumerable<(Register, Register)> conflicts, IEnumerable<(Register, Register)> copies)
		{
			var hwRegisters = toAllocate.Where(reg => reg is HwRegister).Select(reg => reg as HwRegister).ToList();

			int registerCount = hwRegisters.Count();

			var registerGraph = RegisterGraph.CreateOrderGraph(registerCount, toAllocate, conflicts, copies);

			var (order, merge) = registerGraph.OrderRegisters();

			var colorGraph = ColorGraph<Register>.CreateColorGraph(toAllocate, conflicts, registerCount, GeneratePreColorFunc(hwRegisters), merge);

			var (colorDict, spilled) = colorGraph.ColorByOrder(order);

			foreach(var register in merge)
			{
				foreach(var merged in register.Value)
				{
					colorDict[merged] = colorDict[register.Key];
				}
			}

			return (colorDict.ToDictionary(x => x.Key, x => GetHwRegisterFromColor(x.Value, hwRegisters)), spilled);
		}
	}
}
