using Cimplified.Generator;

namespace Cimplified
{

	public abstract class Label : IInstructionParameter
	{

	}

	public class NamedLabel : Label
	{
		public readonly string Name;

		public NamedLabel(string name)
		{
			Name = name;
		}

		public override bool Equals(object other)
		{
			return other is NamedLabel o && this.Name == o.Name;
		}

		public override int GetHashCode()
		{
			return Name.GetHashCode();
		}
	}

	public class UniqueLabel : Label
	{
		private static int counter = 0;
		public readonly int Id;

		public UniqueLabel()
		{
			Id = counter++;
		}

		public UniqueLabel(int x)
		{
			Id = x;
		}

		public override bool Equals(object other)
		{
			return other is UniqueLabel o && this.Id == o.Id;
		}

		public override int GetHashCode()
		{
			return this.Id;
		}
	}

}


