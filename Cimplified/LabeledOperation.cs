namespace Cimplified
{

	public class LabeledOperation
	{
		public Operation Operation;
		public Label Label;
		public LabeledOperation(Operation o, Label l = null)
		{
			Operation = o;
			if (l == null) {
				Label = new UniqueLabel();
			} else {
				Label = l;
			}
		}
	}

}

