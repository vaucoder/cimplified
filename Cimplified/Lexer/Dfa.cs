using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Cimplified.Lexer
{
	public abstract class Dfa<TSymbol, TCat, TState>
		where TSymbol : IEquatable<TSymbol> where TState : IEquatable<TState>
	{
		private ImmutableDictionary<TState, ImmutableDictionary<TSymbol, TState>> _transitionsTable;

		public ImmutableDictionary<TState, ImmutableDictionary<TSymbol, TState>> TransitionsTable
		{
			get
			{
				if (_transitionsTable != null)
					return _transitionsTable;
				var transitionsTable = new Dictionary<TState, ImmutableDictionary<TSymbol, TState>>();
				var stateQueue = new Queue<TState>();
				stateQueue.Enqueue(Start());
				while (stateQueue.Count > 0)
				{
					var state = stateQueue.Dequeue();
					if (transitionsTable.ContainsKey(state))
						continue;
					var transitions = Transitions(state);
					foreach (var t in transitions.Values)
						stateQueue.Enqueue(t);

					transitionsTable[state] = transitions;
				}

				_transitionsTable = transitionsTable.ToImmutableDictionary();
				return _transitionsTable;
			}
		}

		private ImmutableDictionary<TState, ImmutableDictionary<TSymbol, ImmutableHashSet<TState>>>
			_reverseTransitionsTable;

		public ImmutableDictionary<TState, ImmutableDictionary<TSymbol, ImmutableHashSet<TState>>>
			ReverseTransitionsTable
		{
			get
			{
				if (_reverseTransitionsTable != null)
					return _reverseTransitionsTable;

				var reverseTransitionsTable = new Dictionary<TState, Dictionary<TSymbol, HashSet<TState>>>();
				foreach (var (startState, transitions) in TransitionsTable)
				{
					if (!reverseTransitionsTable.ContainsKey(startState))
						reverseTransitionsTable[startState] = new Dictionary<TSymbol, HashSet<TState>>();
					foreach (var (symbol, state) in transitions)
					{
						if (!reverseTransitionsTable.ContainsKey(state))
							reverseTransitionsTable[state] = new Dictionary<TSymbol, HashSet<TState>>();

						if (!reverseTransitionsTable[state].ContainsKey(symbol))
							reverseTransitionsTable[state][symbol] = new HashSet<TState>();

						reverseTransitionsTable[state][symbol].Add(startState);
					}
				}

				_reverseTransitionsTable = reverseTransitionsTable.ToImmutableDictionary(e => e.Key,
					e => e.Value.ToImmutableDictionary(x => x.Key, x => x.Value.ToImmutableHashSet()));
				return _reverseTransitionsTable;
			}
		}

		private ImmutableHashSet<TState> _states;

		public ImmutableHashSet<TState> States
		{
			get
			{
				if (_states != null) return _states;
				_states = new HashSet<TState>(TransitionsTable.Keys).ToImmutableHashSet();
				return _states;
			}
		}

		private ImmutableHashSet<TState> _acceptingStates;

		public ImmutableHashSet<TState> AcceptingStates
		{
			get
			{
				if (_acceptingStates != null)
					return _acceptingStates;

				var acceptingStates = new HashSet<TState>();

				foreach (var state in States.Where(state => Category(state) != null))
				{
					acceptingStates.Add(state);
				}

				_acceptingStates = acceptingStates.ToImmutableHashSet();
				return _acceptingStates;
			}
		}

		public abstract TState Start();

		public abstract TState Dead();

		public abstract TCat Category(TState state);

		public abstract ImmutableDictionary<TSymbol, TState> Transitions(TState state);

		public abstract bool IsStable(TState state);
	}


	class RegexDfa<TSymbol> : Dfa<TSymbol, bool, Regex<TSymbol>> where TSymbol : IEquatable<TSymbol>
	{
		private readonly Regex<TSymbol> _startState;

		public RegexDfa(Regex<TSymbol> startState)
		{
			_startState = startState;
		}

		//isAccept
		override
			public bool Category(Regex<TSymbol> state)
		{
			return state.ContainsEpsilon();
		}

		//isDead
		override
			public bool IsStable(Regex<TSymbol> state)
		{
			return state.IsEmpty();
		}

		override
			public Regex<TSymbol> Start()
		{
			return _startState;
		}

		override
			public Regex<TSymbol> Dead()
		{
			return new EmptyRegex<TSymbol>();
		}

		override
			public ImmutableDictionary<TSymbol, Regex<TSymbol>> Transitions(Regex<TSymbol> state)
		{
			var transitions = new Dictionary<TSymbol, Regex<TSymbol>>();

			foreach (var symbol in state.Heads())
			{
				transitions[symbol] = state.Derivative(symbol).Normalize();
			}

			return transitions.ToImmutableDictionary();
		}
	}

	public class HashableList<T> : IEquatable<HashableList<T>>
	{
		private ImmutableList<T> list;

		public ImmutableList<T> List
		{
			get { return list; }
		}

		public HashableList(List<T> list)
		{
			this.list = list.ToImmutableList();
		}

		public HashableList(params T[] elements)
		{
			var list = new List<T>();
			list.AddRange(elements);
			this.list = list.ToImmutableList();
		}

		public bool Equals(HashableList<T> other)
		{
			return List.SequenceEqual(other.List);
		}

		public override int GetHashCode()
		{
			var hash = 1;
			unchecked
			{
				List.ForEach(o => hash = 31 * hash + o.GetHashCode());
			}

			return hash;
		}
	}

	public class CombinedDfa<TSymbol, TLabel, TState, TCat> : Dfa<TSymbol, TCat, HashableList<TState>>
		where TSymbol : IEquatable<TSymbol> where TState : IEquatable<TState>
	{
		private readonly Func<List<TLabel>, TCat> _combine;
		private readonly List<(TLabel, Dfa<TSymbol, bool, TState>)> _dfas;
		private readonly List<TState> _startingStates;
		private readonly List<TState> _deadStates;

		public CombinedDfa(List<(TLabel, Dfa<TSymbol, bool, TState>)> dfas,
			Func<List<TLabel>, TCat> combine)
		{
			_combine = combine;
			_dfas = dfas;
			_startingStates = _dfas.Select(dfa => dfa.Item2.Start()).ToList();
			_deadStates = _dfas.Select(dfa => dfa.Item2.Dead()).ToList();
		}

		override
			public HashableList<TState> Start()
		{
			return new HashableList<TState>(_startingStates);
		}

		override
			public HashableList<TState> Dead()
		{
			return new HashableList<TState>(_deadStates);
		}

		override
			public TCat Category(HashableList<TState> states)
		{
			if (_dfas.Count != states.List.Count)
			{
				throw new ArgumentException();
			}

			var acceptingDfas = new List<TLabel>();
			for (var i = 0; i < _dfas.Count; i++)
			{
				if (_dfas[i].Item2.Category(states.List[i]))
				{
					acceptingDfas.Add(_dfas[i].Item1);
				}
			}

			return _combine.Invoke(acceptingDfas);
		}

		override
			public ImmutableDictionary<TSymbol, HashableList<TState>> Transitions(HashableList<TState> states)
		{
			if (_dfas.Count != states.List.Count)
			{
				throw new ArgumentException();
			}

			var allTransitions = new List<ImmutableDictionary<TSymbol, TState>>();
			for (var i = 0; i < _dfas.Count; i++)
			{
				allTransitions.Add(_dfas[i].Item2.Transitions(states.List[i]));
			}

			var alphabet = allTransitions.SelectMany(dict => dict.Keys).Distinct();
			var stateTransitions = new Dictionary<TSymbol, List<TState>>();
			foreach (var c in alphabet)
			{
				var transitionsOverLetter = new List<TState>();
				for (var i = 0; i < _dfas.Count; i++)
				{
					transitionsOverLetter.Add(allTransitions[i].TryGetValue(c, out var x)
						? x
						: _dfas[i].Item2.Dead());
				}

				stateTransitions.Add(c, transitionsOverLetter);
			}

			return stateTransitions.ToImmutableDictionary(e => e.Key, e => new HashableList<TState>(e.Value));
		}

		override
			public bool IsStable(HashableList<TState> states)
		{
			if (_dfas.Count != states.List.Count)
			{
				throw new ArgumentException();
			}

			var isStable = true;
			for (var i = 0; i < _dfas.Count; i++)
			{
				isStable &= _dfas[i].Item2.IsStable(states.List[i]);
			}

			return isStable;
		}
	}

	public static class CombinedDfa
	{
		public static Dfa<TSymbol, TCat, HashableList<TState>>
			Create<TSymbol, TLabel, TState, TCat>(List<(TLabel, Dfa<TSymbol, bool, TState>)> dfas,
				Func<List<TLabel>, TCat> combine) where TSymbol : IEquatable<TSymbol> where TState : IEquatable<TState>
		{
			return new CombinedDfa<TSymbol, TLabel, TState, TCat>(dfas, combine);
		}
	}
}