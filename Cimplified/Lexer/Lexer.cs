using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;

namespace Cimplified.Lexer
{

	public class UnknownTokenError : Diagnostic
	{
		public Location Loc;
		public string Line;

		public override string ToString()
		{
			return $"Unknown token at {Loc.Line}:{Loc.Pos}";
		}
	}

	public class Lexer<TKind>
	{
		public Action<Diagnostic> Emit;

		private readonly Dfa<char, int, HashableList<Regex<char>>> _combined;
		private readonly TKind[] _tokens;

		public Lexer(List<(TKind, Regex<char>)> lst)
		{
			// Labels are indexes of corresponding regexes
			var labeled_dfas = lst.Select((p, i) => (i, Utils.RegexToDfa(p.Item2))).ToList();

			// Combine prefers first regex
			int combine(List<int> accepted)
			{
				return accepted.Count > 0 ? accepted.First() : -1;
			}

			_combined = CombinedDfa.Create(labeled_dfas, combine);
			_tokens = lst.Select((p) => p.Item1).ToArray();
		}

		static List<string> RemoveComments(List<string> source)
		{
			for (int i = 0; i < source.Count; i++)
			{
				int idx = source[i].IndexOf('#');
				if (idx < 0)
					continue;
				source[i] = source[i].Remove(idx, source[i].Length - idx);
			}
			return source;
		}

		public IEnumerable<Token<TKind>> Process(List<String> source)
		{
			source = RemoveComments(source);
			var start = _combined.Start();
			var state = start;
			var pos = (0, -1); // (Line number, Column number)
			var str = ""; // Current token string
			var acceptingPos = (-1, -1); // Last accepting position
			var acceptingCategory = -1; // Last accepting token index
			var acceptingNl = false; // Last accepting newline state
			var acceptingStr = ""; // Last accepting token string
			var eof = false; // End of file?
			var nl = false; // Newline?
			var canReset = false; // Not-commited accepting state exists?

			void incrementPos()
			{
				// Increment column number
				pos = (pos.Item1, pos.Item2 + 1);

				if (source.Count <= pos.Item1)
				{
					// Reached the end of the source
					eof = true;
				} else if (source[pos.Item1].Length <= pos.Item2)
				{
					// Increment line number
					if (source.Count - 1 <= pos.Item1)
					{
						// If last line -> eof
						eof = true;
					} else if (nl)
					{
						// Jump to new line
						nl = false;
						pos = (pos.Item1 + 1, -1);
						incrementPos();
					} else
					{
						// Make newline character
						nl = true;
					}
				}
			}

			char readChar()
			{
				if (eof)
				{
					return '\0';
				} else if (nl)
				{
					return '\n';
				} else
				{
					return source[pos.Item1][pos.Item2];
				}
			}

			void checkCategory()
			{
				var cat = _combined.Category(state);
				if (cat >= 0)
				{
					// Remember accepting state
					acceptingPos = pos;
					acceptingCategory = cat;
					acceptingNl = nl;
					acceptingStr = str;
					canReset = true;
				}
			}

			bool transition(char c)
			{
				// Check if DFAs are dead
				var dict = _combined.Transitions(state);
				return dict.TryGetValue(c, out state) && !_combined.IsStable(state);
			}

			Location currentLocation() {
				return new Location {
					Line = pos.Item1,
					Pos = pos.Item2 - str.Length + 1,
				};
			}

			// Returns true if reset succeded
			bool tryReset()
			{
				if (canReset)
				{
					pos = acceptingPos;
					nl = acceptingNl;
					str = acceptingStr;
					canReset = false;
					return true;
				} else
				{
					var message = new UnknownTokenError {
							Loc = currentLocation(),
							Line = str,
						};
					if (Emit != null) {
						Emit(message);
					} else {
						throw message;
					}
					return false;
				}
			}

			Token<TKind> acceptingToken()
			{
				return new Token<TKind>
				{
					Kind = _tokens[acceptingCategory],
					Text = str,
					Loc = currentLocation(),
				};
			}

			/* Empty string can be accepted */
			checkCategory();

			while (true)
			{
				incrementPos();
				var c = readChar();
				str += c;
				if (!transition(c))
				{
					if (tryReset()) {
						yield return acceptingToken();
					}
					state = start;
					str = "";
					if (eof)
					{
						break;
					} else
					{
						continue;
					}
				}

				checkCategory();
			}
		}
	}


}
