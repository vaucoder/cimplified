using System;
using System.Collections.Generic;
using System.Linq;


namespace Cimplified.Lexer
{
	public abstract class Regex<TSymbol> : IEquatable<Regex<TSymbol>> where TSymbol : IEquatable<TSymbol>
	{
		public abstract bool ContainsEpsilon();

		public abstract Regex<TSymbol> Derivative(TSymbol c);

		public abstract HashSet<TSymbol> Heads();

		public abstract bool IsEmpty();

		public abstract bool Equals(Regex<TSymbol> other);

		public abstract Regex<TSymbol> Normalize();

		override
		public abstract int GetHashCode();
	}


	public class StarRegex<TSymbol> : Regex<TSymbol> where TSymbol : IEquatable<TSymbol>
	{
		private Regex<TSymbol> _regex;

		public StarRegex(Regex<TSymbol> regex)
		{
			this._regex = regex;
		}

		override
		public bool ContainsEpsilon()
		{
			return true;
		}

		override
		public bool IsEmpty()
		{
			return false;
		}

		override
		public Regex<TSymbol> Derivative(TSymbol c)
		{
			return new ConcatRegex<TSymbol>(this._regex.Derivative(c), new StarRegex<TSymbol>(this._regex));
		}

		override
		public HashSet<TSymbol> Heads()
		{
			return this._regex.Heads();
		}

		override
		public bool Equals(Regex<TSymbol> other)
		{
			if (other == null || other.GetType() != typeof(StarRegex<TSymbol>))
				return false;
			return _regex.Equals(((StarRegex<TSymbol>)other)._regex);
		}

		override
		public Regex<TSymbol> Normalize()
		{
			if (_regex == null)
				return this;
			if (_regex.GetType() == typeof(StarRegex<TSymbol>))
			{
				if (_regex.Heads().Count == 0 && _regex.ContainsEpsilon()) // _Regex = \epsilon
					return _regex; // \epsilon* -> \epsilon
				return this._regex.Normalize(); // X** -> X*
			}
			return new StarRegex<TSymbol>(_regex.Normalize());
		}

		override
		public int GetHashCode()
		{
			return _regex.GetHashCode();
		}
	}


	public class UnionRegex<TSymbol> : Regex<TSymbol> where TSymbol : IEquatable<TSymbol>
	{
		private HashSet<Regex<TSymbol>> _regexSet
		{
			get;
			set;
		}

		public UnionRegex(params Regex<TSymbol>[] regexes)
		{
			this._regexSet = new HashSet<Regex<TSymbol>>(regexes);
		}
		public UnionRegex(TSymbol first, params TSymbol[] symbols)
		{
			var all = new[] {first}.Concat(symbols);
			this._regexSet = all.Select(x => new AtomicRegex<TSymbol>(x)).ToHashSet<Regex<TSymbol>>();
		}

		public UnionRegex(HashSet<Regex<TSymbol>> regexSet)
		{
			_regexSet = regexSet;
		}

		override
		public bool ContainsEpsilon()
		{
			return this._regexSet.Count > 0 && this._regexSet.Any(x => x.ContainsEpsilon());
		}

		override
		public Regex<TSymbol> Derivative(TSymbol c)
		{
			var derivatives = this._regexSet.ToList().Select(x => x.Derivative(c)).ToArray();
			return new UnionRegex<TSymbol>(derivatives);
		}

		override
		public HashSet<TSymbol> Heads()
		{
			var set = new HashSet<TSymbol>();
			foreach (var regex in this._regexSet)
			{
				set.UnionWith(regex.Heads());
			}
			return set;
		}

		override
		public bool IsEmpty()
		{
			return this._regexSet.Count == 0 || this._regexSet.All(x => x.IsEmpty());
		}

		override
		public bool Equals(Regex<TSymbol> other)
		{
			if (other == null || other.GetType() != typeof(UnionRegex<TSymbol>))
				return false;
			return _regexSet.SetEquals(((UnionRegex<TSymbol>)other)._regexSet);
		}

		override
		public Regex<TSymbol> Normalize()
		{
			// (X \cup X -> X) and (X \cup Y -> Y \cup X) already done by HashSet
			HashSet<Regex<TSymbol>> regexSet = new HashSet<Regex<TSymbol>>();
			foreach (var reg in _regexSet)
			{
				var regNormalized = reg.Normalize();
				if (regNormalized.GetType() == typeof(UnionRegex<TSymbol>))
					// union tree flattening ; equivalent to (X \cup Y) \cup Z -> X \cup (Y \cup Z)
					regexSet.UnionWith(((UnionRegex<TSymbol>)regNormalized)._regexSet);
				else if (regNormalized.GetType() != typeof(EmptyRegex<TSymbol>))
					regexSet.Add(regNormalized);
			}
			if (regexSet.Count > 1)
				return new UnionRegex<TSymbol>(regexSet);
			if (regexSet.Count == 1)
				return regexSet.First(); // union(X) -> X
			return new EmptyRegex<TSymbol>(); // union() -> EmptyRegex
		}

		override
		public int GetHashCode()
		{
			int hashCode = 0;
			foreach (var r in _regexSet)
				hashCode += 29 * r.GetHashCode();
			return hashCode;
		}
	}


	public class ConcatRegex<TSymbol> : Regex<TSymbol> where TSymbol : IEquatable<TSymbol>
	{
		private List<Regex<TSymbol>> _regexList
		{
			get;
			set;
		}

		public ConcatRegex(params Regex<TSymbol>[] regexes)
		{
			this._regexList = new List<Regex<TSymbol>>(regexes);
		}

		public ConcatRegex(List<Regex<TSymbol>> regexList)
		{
			_regexList = regexList;
		}
		public ConcatRegex(TSymbol first, params TSymbol[] symbols)
		{
			var all = new[] {first}.Concat(symbols);
			this._regexList = all.Select(x => new AtomicRegex<TSymbol>(x)).ToList<Regex<TSymbol>>();
		}

		override
		public bool ContainsEpsilon()
		{
			return this._regexList.Count > 0 && this._regexList.All(x => x.ContainsEpsilon());
		}

		override
		public Regex<TSymbol> Derivative(TSymbol c)
		{
			if (this.IsEmpty())
			{
				return new UnionRegex<TSymbol>();
			} else if (this._regexList.Count == 1)
			{
				return this._regexList.First().Derivative(c);
			}
			var first = this._regexList.First();
			var rest = new ConcatRegex<TSymbol>(this._regexList.Skip(1).ToArray());
			var baseDerivative = new ConcatRegex<TSymbol>(first.Derivative(c), rest);
			if (first.ContainsEpsilon())
			{
				return new UnionRegex<TSymbol>(baseDerivative, rest.Derivative(c));
			} else
			{
				return baseDerivative;
			}
		}

		override
		public HashSet<TSymbol> Heads()
		{
			var set = new HashSet<TSymbol>();
			if (this.IsEmpty())
			{
				return set;
			}
			foreach (var regex in this._regexList)
			{
				set.UnionWith(regex.Heads());
				if (!regex.ContainsEpsilon())
				{
					break;
				}
			}
			return set;
		}

		override
		public bool IsEmpty()
		{
			return this._regexList.Count == 0 || this._regexList.Any(x => x.IsEmpty());
		}

		override
		public bool Equals(Regex<TSymbol> other)
		{
			if (other == null || other.GetType() != typeof(ConcatRegex<TSymbol>))
				return false;
			return _regexList.SequenceEqual(((ConcatRegex<TSymbol>)other)._regexList);
		}

		override
		public Regex<TSymbol> Normalize()
		{
			List<Regex<TSymbol>> regexList = new List<Regex<TSymbol>>(_regexList);
			List<Regex<TSymbol>> finalRegexList = new List<Regex<TSymbol>>();
			// remove all \epsilons; X \epsilon -> \epsilon X -> X
			regexList.RemoveAll(r => r.Heads().Count == 0 && r.ContainsEpsilon());
			foreach (var reg in regexList)
			{
				if (reg.IsEmpty())
					return new EmptyRegex<TSymbol>(); // \emptyset X -> X \emptyset -> \emptyset
			}
			foreach (var reg in regexList)
			{
				var regNormalized = reg.Normalize();
				if (regNormalized.GetType() == typeof(ConcatRegex<TSymbol>))
				// concat tree flattening ; equivalent to (XY)Z -> X(YZ) for binary regexes
					finalRegexList.AddRange(((ConcatRegex<TSymbol>)regNormalized)._regexList);
				else
					finalRegexList.Add(regNormalized);
			}
			if (finalRegexList.Count > 1)
				return new ConcatRegex<TSymbol>(finalRegexList);
			if (finalRegexList.Count == 1)
				return finalRegexList.First(); // concat(X) -> X
			return new EmptyRegex<TSymbol>(); // concat() -> EmptyRegex
		}

		override
		public int GetHashCode()
		{
			int hashCode = 0;
			foreach (var r in _regexList)
				hashCode += 29 * r.GetHashCode();
			return hashCode;
		}
	}


	public class AtomicRegex<TSymbol> : Regex<TSymbol> where TSymbol : IEquatable<TSymbol>
	{
		private TSymbol _c
		{
			get;
			set;
		}

		public AtomicRegex(TSymbol c)
		{
			this._c = c;
		}

		override
		public bool ContainsEpsilon()
		{
			return false;
		}

		override
		public Regex<TSymbol> Derivative(TSymbol c)
		{
			if (this._c.Equals(c))
			{
				return new StarRegex<TSymbol>(new UnionRegex<TSymbol>());
			}
			return new UnionRegex<TSymbol>();
		}

		override
		public HashSet<TSymbol> Heads()
		{
			return new HashSet<TSymbol> {this._c};
		}

		override
		public bool IsEmpty()
		{
			return false;
		}

		override
		public bool Equals(Regex<TSymbol> other)
		{
			if (other == null || other.GetType() != typeof(AtomicRegex<TSymbol>))
				return false;
			return _c.Equals(((AtomicRegex<TSymbol>)other)._c);
		}

		override
		public Regex<TSymbol> Normalize()
		{
			return this;
		}

		override
		public int GetHashCode()
		{
			return _c.GetHashCode();
		}
	}


	public class EmptyRegex<TSymbol> : Regex<TSymbol> where TSymbol : IEquatable<TSymbol>
	{
		override
		public Regex<TSymbol> Normalize()
		{
			return this;
		}

		override
		public HashSet<TSymbol> Heads()
		{
			return new HashSet<TSymbol>();
		}

		override
		public bool ContainsEpsilon()
		{
			return false;
		}

		override
		public Regex<TSymbol> Derivative(TSymbol c)
		{
			return this;
		}

		override
		public bool IsEmpty()
		{
			return true;
		}

		override
		public bool Equals(Regex<TSymbol> other)
		{
			return other.GetType() == typeof(EmptyRegex<TSymbol>);
		}

		override
		public int GetHashCode()
		{
			return 0;
		}
	}

}
