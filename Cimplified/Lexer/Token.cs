﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Cimplified.Lexer
{
	public struct Location
	{
		public int Line;
		public int Pos;
	}
	
	public struct Token<TKind>
	{
		public Token(TKind kind, string text = "", Location loc = default)
		{
			Kind = kind;
			Text = text;
			Loc = default;
		}

		public TKind Kind;
		public string Text;
		public Location Loc;
	}
}
