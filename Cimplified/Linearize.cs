using System;
using System.Collections.Generic;
using System.Linq;
using Cimplified.Parser;

namespace Cimplified
{

	public class Linearize
	{

		public static List<LabeledOperation> Process(FunctionDeclaration fn)
		{
			return new Linearize(fn).Result;
		}

		public List<LabeledOperation> Ret = new List<LabeledOperation>();
		public HashSet<Label> H = new HashSet<Label>();
		public Label Current;
		public readonly FunctionDeclaration Decl;
		public readonly Function Fn;
		public readonly ControlFlowGraph CFG;

		public Linearize(FunctionDeclaration decl)
		{
			Decl = decl;
			Fn = Decl.Fun;
			CFG = ExtractControlFlowGraph.Extract1(Decl.Body);
		}

		public List<LabeledOperation> Result { get => GetResult(); }

		public List<LabeledOperation> GetResult()
		{
			Fn.GeneratePrologue().ForEach(Push);
			Loop(CFG.Start);
			return Ret;
		}

		public void Push(Operation op)
		{
			if (Current is object) {
				Ret.Add(new LabeledOperation(op, Current));
				Current = null;
			} else {
				Ret.Add(new LabeledOperation(op));
			}
		}

		public void Exit()
		{
			Fn.GenerateEpilogue().ForEach(Push);
		}

		public void Loop(Label current)
		{
			if (current == null) {
				Exit();
				return;
			} else if (H.Contains(current)) {
				Push(new Jump.Unconditional(current));
				return;
			} else {
				H.Add(current);
			}

			var old_current = Current;
			Current = current;

			var node = CFG.Map[current];
			if (node is CFGNode.Unconditional u) {
				HandleUnconditional(u);
			} else if (node is CFGNode.Conditional c) {
				HandleConditional(c);
			} else {
				throw new Exception($"Unexpected type {Utils.Dump(current)}");
			}

			Current = old_current;
		}

		public void HandleUnconditional(CFGNode.Unconditional u)
		{
			switch (u.S) {
				case DeclareAssignment x:
					HandleAssignment(x.Var, x.Value);
					Loop(u.Next);
					break;
				case Assignment x:
					HandleAssignment(x.VarName.Declaration.Var, x.Value);
					Loop(u.Next);
					break;
				case Expression x:
					Utils.GeneralExpressionToOperation(Fn, x).ForEach(Push);
					Loop(u.Next);
					break;
				case ReturnClause x:
					HandleReturn(x.ReturnExpression);
					return;
				case BreakClause x:
					Push(new Jump.Unconditional(u.Next));
					Loop(u.Next);
					break;
				case ContinueClause x:
					Push(new Jump.Unconditional(u.Next));
					Loop(u.Next);
					break;
				case FunctionDeclaration x:
					Loop(u.Next);
					break;
				default:
					throw new Exception($"Not expected type {Utils.Dump(u.S)}");
			}
		}

		public void HandleConditional(CFGNode.Conditional c)
		{
			Push(new Jump.IfZero(c.False, Utils.SimpleExpressionToOperation(Fn, (SimpleExpression)c.Expr)));
			Loop(c.True);
			Loop(c.False);
		}

		public void HandleAssignment(Variable v, Expression val)
		{
			var access = Fn.GenerateVariableAccess(v);
			if (val is FunctionCall x) {
				Utils.FunctionCallToOperation(Fn, x, access).ForEach(Push);
			} else if (val is SimpleExpression se) {
				var op = Utils.SimpleExpressionToOperation(Fn, se);
				Push(Utils.WriteToMemory(access, op));
			} else {
				throw new Exception($"Unexpected type {Utils.Dump(val)}");
			}
		}

		public void HandleReturn(Expression expr)
		{
			if (expr != null) {
				var op = Utils.SimpleExpressionToOperation(Fn, (SimpleExpression)expr);
				var r = Utils.GetReturnAddress();
				Push(Utils.WriteToMemory(r, op));
			}

			Exit();
		}
	}

}
