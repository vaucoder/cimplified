﻿using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Cimplified.Lexer;

namespace Cimplified
{
    public class Cimplified
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                System.Console.WriteLine("No arguments provided");
                return;
            }
            if (!args[0].EndsWith(".cim"))
            {
                System.Console.WriteLine("Incorrect file extension");
                return;
            }
            var inputFileName = args[0];
            var source = new List<string>(File.ReadAllLines(inputFileName));
            var lexer = new Lexer.Lexer<Terminal>(CimGrammarCreator.CreateTerminalToRegex());
            var parser = new Parser.Parser(CimGrammarCreator.CreateGrammar());
            var compiler = new Compiler(lexer, parser);
            compiler.Compile(source);
        }
    }
}
