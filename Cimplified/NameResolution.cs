using System;
using System.Collections.Generic;
using Cimplified.Parser;

namespace Cimplified
{
	public static class NameResolution
	{
		public static void Process(AST mutableAST)
		{
            Dictionary<string, Stack<IDeclaration>> dictionary = new Dictionary<string, Stack<IDeclaration>>();
            Stack<List<string>> blocksStack = new Stack<List<string>>();
            DeclarationNodeVisitor declarationNodeVisitor = new DeclarationNodeVisitor();
            declarationNodeVisitor.Visit((Program)mutableAST, (dictionary, blocksStack));
		}
	}

    public class DeclarationNodeVisitor : RecursiveASTNodeVisitor<(Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>)>
    {
        public override void Visit(AST n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter = default)
        {
            return;
        }

        public override void Visit(Program n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter)
        {
            open(parameter.Item2);
            base.Visit(n, parameter);
            close(parameter.Item1, parameter.Item2);
        }

        private void define(string name, IDeclaration declaration, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter)
        {
            var dictionary = parameter.Item1;
            var blocksStack = parameter.Item2;
            if (!dictionary.ContainsKey(name))
                dictionary.Add(name, new Stack<IDeclaration>());

            dictionary[name].Push(declaration);
            blocksStack.Peek().Add(name);
        }

        private void open(Stack<List<string>> blocksStack)
        {
            blocksStack.Push(new List<string>());
        }

        private void close(Dictionary<string, Stack<IDeclaration>> dictionary, Stack<List<string>> blocksStack)
        {
            var names = blocksStack.Pop();
            foreach (var name in names)
                dictionary[name].Pop();
        }

        private IDeclaration find(Dictionary<string, Stack<IDeclaration>> dictionary, string name)
        {
            return dictionary[name].Peek();
        }

        public override void Visit(FunctionDeclaration n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter)
        {
            var dictionary = parameter.Item1;
            var blocksStack = parameter.Item2;

            define(n.FunName.Name, n, parameter);
            n.FunName.Declaration = n;

            open(blocksStack);

            foreach (var argument in n.ArgumentList)
            {
                define(argument.VarName.Name, argument, parameter);
                argument.VarName.Declaration = argument;
            }             

            base.Visit(n, parameter);

            close(dictionary, blocksStack);
        }

        public override void Visit(DeclareAssignment n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter)
        {
            define(n.VarName.Name, n, parameter);
            n.VarName.Declaration = n;
            base.Visit(n, parameter);
        }

        public override void Visit(Block n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter)
        {
            open(parameter.Item2);
            base.Visit(n, parameter);
            close(parameter.Item1, parameter.Item2);
        }

        public override void Visit(Argument n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter = default)
        {
            define(n.VarName.Name, n, parameter);
            n.VarName.Declaration = n;
            base.Visit(n, parameter);
        }

        public override void Visit(FunctionName n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter = default)
        {
            n.Declaration = (FunctionDeclaration)find(parameter.Item1, n.Name);
            base.Visit(n, parameter);
        }

        public override void Visit(VarName n, (Dictionary<string, Stack<IDeclaration>>, Stack<List<string>>) parameter = default)
        {
            n.Declaration = (IVariableDeclaration)find(parameter.Item1, n.Name);
            base.Visit(n, parameter);
        }
    }
}
