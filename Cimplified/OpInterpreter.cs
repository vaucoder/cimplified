using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Immutable;
using Cimplified.Parser;

namespace Cimplified
{
	public class OpInterpreter
	{
		public static Int64 InterpretFunctions(List<FunctionDeclaration> lst)
		{
			var interpreter = new OpInterpreter();
			Int64 ip = 0;

			foreach (var fn in lst) {
				ip = interpreter.LoadProgram(fn.Fun.Label, Linearize.Process(fn));
			}

			return interpreter.Enter(ip);
		}

		// Data
		public List<byte?> Memory = new List<byte?>(); // Infinite memory
		public Dictionary<Register, Int64> Registers = new Dictionary<Register, Int64>();
		public static readonly Int64 InitialRSP = 40000;

		// Code
		public List<LabeledOperation> CodeMemory = new List<LabeledOperation>();
		public Dictionary<Label, Int64> JumpTable = new Dictionary<Label, Int64>();
		public Int64 IP = -1;

		public static Label HaltProgramLabel = new UniqueLabel();
		public static List<LabeledOperation> HaltProgram =
			new List<LabeledOperation>() { new LabeledOperation(new Halt()) };

		public OpInterpreter()
		{
			RegisterWrite(HwRegister.RBP, InitialRSP);
			RegisterWrite(HwRegister.RSP, InitialRSP);
			LoadProgram(HaltProgramLabel, HaltProgram);
		}

		public void RegisterWrite(Register r, Int64 val)
		{
			Registers[r] = val;
		}

		public Int64? RegisterAccessNothrow(Register r)
		{
			Int64 ret;
			if (Registers.TryGetValue(r, out ret)) {
				return ret;
			} else {
				return null;
			}
		}

		public Int64 RegisterAccess(Register r)
		{
			var x = RegisterAccessNothrow(r);
			if (x is null) {
				throw new Exception($"Accessing uninitialized register {Utils.Dump(r)}");
			} else {
				return x.Value;
			}
		}

		public void MemoryEnlarge(Int64 to)
		{
			while (Memory.Count <= to) {
				Memory.Add(null);
			}
		}

		public Int64? MemoryAccessNothrow(Int64 address)
		{
			if (address < 0) {
				throw new Exception($"Negative memory access {address}");
			} else if (address == 0) {
				throw new Exception($"Null pointer dereference");
			}

			MemoryEnlarge(address + sizeof(Int64));
			var x = 0;
			for (int i = 0; i < sizeof(Int64); i++) {
				var b = Memory[(int)address + i];
				if (b is null) {
					return null;
				}

				x += b.Value << (8 * i);
			}

			return x;
		}

		public Int64 MemoryAccess(Int64 address)
		{
			var x = MemoryAccessNothrow(address);
			if (x is null) {
				throw new Exception($"Accessing uninitialized memory at {address}");
			} else {
				return x.Value;
			}
		}

		public void MemoryWrite(Int64 address, Int64 val)
		{
			MemoryEnlarge(address + sizeof(Int64));
			for (int i = 0; i < sizeof(Int64); i++) {
				Memory[(int)address + i] = (byte)((val >> (8 * i)) & 0xff);
			}
		}

		public void Push(Int64 val)
		{
			RegisterWrite(HwRegister.RSP, RegisterAccess(HwRegister.RSP) - sizeof(Int64));
			MemoryWrite(RegisterAccess(HwRegister.RSP), val);
		}

		public Int64 Pop()
		{
			var ret = MemoryAccess(RegisterAccess(HwRegister.RSP));
			RegisterWrite(HwRegister.RSP, RegisterAccess(HwRegister.RSP) + sizeof(Int64));
			return ret;
		}

		public Int64 Eval(ValueOperation op)
		{
			Int64 toint(bool x) {
				if (x) return 1;
				else return 0;
			}

			switch (op) {

				case Arithmetic.Plus x:
					return Eval(x.Left) + Eval(x.Right);
				case Arithmetic.Mult x:
					return Eval(x.Left) * Eval(x.Right);
				case Arithmetic.Div x:
					return Eval(x.Left) / Eval(x.Right);
				case Arithmetic.Sub x:
					return Eval(x.Left) - Eval(x.Right);
				case Arithmetic.Eq x:
					return toint(Eval(x.Left) == Eval(x.Right));
				case Arithmetic.Leq x:
					return toint(Eval(x.Left) < Eval(x.Right));
				case Arithmetic.And x:
					return Eval(x.Left) & Eval(x.Right);
				case Arithmetic.Or x:
					return Eval(x.Left) | Eval(x.Right);
				case Arithmetic.Negation x:
					if (Eval(x.Operand) == 0) return 1;
					else return 0;

				case MemoryAccess x:
					return MemoryAccess(Eval(x.Address));
				case RegisterAccess x:
					return RegisterAccess(x.Register);
				case ConstValue x:
					return x.Value;

				default:
					throw new Exception($"Unexpected type {Utils.Dump(op)}");
			}
		}

		Int64 CurrentResult()
		{
			return Eval(Utils.GetReturnAddress());
		}

		// Loads program and returns pointer to it
		public Int64 LoadProgram(Label label, List<LabeledOperation> instructions)
		{
			var top = CodeMemory.Count;
			JumpTable[label] = top;

			for (int i = 0; i < instructions.Count; i++) {
				JumpTable[instructions[i].Label] = top + i;
				CodeMemory.Add(instructions[i]);
			}

			CodeMemory.Add(new LabeledOperation(new Halt(), new UniqueLabel()));

			return top;
		}

		public Int64 Enter(Int64 ip)
		{
			// Return address is the address of Halt program
			Push(JumpTable[HaltProgramLabel]);

			IP = ip;
			Operation op;
			do {
				op = CodeMemory[(int)IP].Operation;
			} while (Interpret(op));

			return CurrentResult();
		}

		public bool Interpret(Operation op)
		{

			switch (op) {

				case ValueOperation x:
					Eval(x); // no effect
					break;

				case Noop x:
					break;

				case MemoryWrite x:
					MemoryWrite(Eval(x.Address), Eval(x.Value));
					break;

				case RegisterWrite x:
					RegisterWrite(x.Reg, Eval(x.Value));
					break;

				case Push x:
					Push(Eval(x.Value));
					break;

				case Call x:
					Push(IP + 1);
					IP = JumpTable[x.Target];
					return true;

				case OpReturn x:
					IP = Pop();
					return true;

				case Jump.Unconditional x:
					IP = JumpTable[x.Label];
					return true;

				case Jump.IfZero x:
					if (Eval(x.Value) == 0) {
						IP = JumpTable[x.Label];
						return true;
					}
					break;

				case Jump.IfPositive x:
					if (Eval(x.Value) > 0) {
						IP = JumpTable[x.Label];
						return true;
					}
					break;

				case Halt x:
					return false;

				default:
					throw new Exception($"Unexpected type {Utils.Dump(op)}");

			}

			IP++;
			return true;
		}
	}
}
