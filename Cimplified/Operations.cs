using System;
using Cimplified.Generator;

namespace Cimplified
{
	public abstract class Operation
	{


	}

	public abstract class ValueOperation : Operation
	{

	}

	public abstract class ControlOperation : Operation
	{

	}

	public class Push : ControlOperation
	{
		public ValueOperation Value;
		public Push(ValueOperation val)
		{
			Value = val;
		}
	}

	public abstract class Arithmetic : ValueOperation
	{
		public abstract class Binary : Arithmetic
		{
			public ValueOperation Left;
			public ValueOperation Right;
		}

		public abstract class Unary : Arithmetic
		{
			public ValueOperation Operand;
		}

		public class Plus : Binary
		{
			public Plus(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class Mult : Binary
		{
			public Mult(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class Div : Binary
		{
			public Div(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class Sub : Binary
		{
			public Sub(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class Eq : Binary
		{
			public Eq(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class Leq : Binary
		{
			public Leq(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class And : Binary
		{
			public And(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class Or : Binary
		{
			public Or(ValueOperation left, ValueOperation right)
			{
				Left = left;
				Right = right;
			}
		}

		public class Negation : Unary
		{
			public Negation(ValueOperation operand)
			{
				Operand = operand;
			}
		}

	}


	public class MemoryAccess : ValueOperation
	{
		public ValueOperation Address;
		public MemoryAccess(ValueOperation a)
		{
			Address = a;
		}
	}

	public class RegisterAccess : ValueOperation
	{
		public Register Register;
		public RegisterAccess(Register r)
		{
			Register = r;
		}
	}

	public class RegisterWrite : ControlOperation
	{
		public Register Reg;
		public ValueOperation Value;
		public RegisterWrite(Register reg, ValueOperation val)
		{
			Reg = reg;
			Value = val;
		}

		public RegisterWrite()
		{

		}
	}

	public class MemoryWrite : ControlOperation
	{
		public ValueOperation Address;
		public ValueOperation Value;
		public MemoryWrite(ValueOperation addr, ValueOperation val)
		{
			Address = addr;
			Value = val;
		}

		public MemoryWrite()
		{

		}
	}

	public class Noop : ControlOperation
	{

	}
	
	public class ConstValue : ValueOperation
	{
		public Constant Constant;
		public Int64 Value { get => Constant.Value; }
		public ConstValue(Int64 v)
		{
			this.Constant = new Constant(v);
		}
		public ConstValue(Constant c)
		{
			this.Constant = c;
		}
	}

	public class Call : ControlOperation
	{
		public Label Target;
		public Call(Label target)
		{
			Target = target;
		}
	}

	public class OpReturn : ControlOperation
	{

	}

	public abstract class Jump : ControlOperation
	{
		public Label Label;

		public class Unconditional : Jump
		{
			public Unconditional(Label label)
			{
				Label = label;
			}
		}

		public abstract class Conditional : Jump
		{
			public ValueOperation Value;
		}

		public class IfZero : Conditional
		{
			public IfZero(Label label, ValueOperation val)
			{
				Label = label;
				Value = val;
			}
		}

		public class IfPositive : Conditional
		{
			public IfPositive(Label label, ValueOperation val)
			{
				Label = label;
				Value = val;
			}
		}
	}

	public class Halt : ControlOperation
	{

	}

}
