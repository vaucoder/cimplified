using System;
using System.Collections.Generic;

namespace Cimplified.Parser
{

	public abstract class ASTNodeVisitor<TParameter>
	{
		public abstract void Visit(AST n, TParameter parameter = default);

		public virtual void Visit(Program n, TParameter parameter = default)
		{
			Visit((AST)n, parameter);
		}

		public virtual void Visit(Statement n, TParameter parameter = default)
		{
			Visit((AST)n, parameter);
		}

		public virtual void Visit(Expression n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(SimpleExpression n, TParameter parameter = default)
		{
			Visit((Expression)n, parameter);
		}

		public virtual void Visit(Literal n, TParameter parameter = default)
		{
			Visit((SimpleExpression)n, parameter);
		}

		public virtual void Visit(BoolLiteral n, TParameter parameter = default)
		{
			Visit((Literal)n, parameter);
		}

		public virtual void Visit(IntLiteral n, TParameter parameter = default)
		{
			Visit((Literal)n, parameter);
		}

		public virtual void Visit(VarName n, TParameter parameter = default)
		{
			Visit((AST)n, parameter);
		}

		public virtual void Visit(VarUse n, TParameter parameter = default)
		{
			Visit((SimpleExpression)n, parameter);
		}

		public virtual void Visit(FunctionName n, TParameter parameter = default)
		{
			Visit((AST)n, parameter);
		}

		public virtual void Visit(TypeName n, TParameter parameter = default)
		{
			Visit((AST)n, parameter);
		}

		public virtual void Visit(BinaryOperation n, TParameter parameter = default)
		{
			Visit((SimpleExpression)n, parameter);
		}

		public virtual void Visit(Plus n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(Mult n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(Div n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(Sub n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(Eq n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(Leq n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(And n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(Or n, TParameter parameter = default)
		{
			Visit((BinaryOperation)n, parameter);
		}

		public virtual void Visit(UnaryOperation n, TParameter parameter = default)
		{
			Visit((SimpleExpression)n, parameter);
		}

		public virtual void Visit(Negation n, TParameter parameter = default)
		{
			Visit((UnaryOperation)n, parameter);
		}

		public virtual void Visit(FunctionCall n, TParameter parameter = default)
		{
			Visit((Expression)n, parameter);
		}

		public virtual void Visit(Assignment n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(DeclareAssignment n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(IfClause n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(ElIfClause n, TParameter parameter = default)
		{
			Visit((AST)n, parameter);
		}

		public virtual void Visit(WhileClause n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(FunctionDeclaration n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(Argument n, TParameter parameter = default)
		{
			Visit((AST)n, parameter);
		}

		public virtual void Visit(ReturnClause n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(BreakClause n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(ContinueClause n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(Group n, TParameter parameter = default)
		{
			Visit((Statement)n, parameter);
		}

		public virtual void Visit(FlatGroup n, TParameter parameter = default)
		{
			Visit((Group)n, parameter);
		}

		public virtual void Visit(Block n, TParameter parameter = default)
		{
			Visit((Group)n, parameter);
		}
	}
}
