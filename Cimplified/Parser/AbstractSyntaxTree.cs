using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;

namespace Cimplified.Parser
{

	public interface IDeclaration
	{
		TypeName TypeName {get;}
	}

	public interface IVariableDeclaration : IDeclaration
	{
		Variable Var { get; set; }
	}

	public enum Type
	{
		Unknown,
		Bool,
		Int,
		Void,
	}


	public abstract class AST
	{
		public void Accept<TParameter>(ASTNodeVisitor<TParameter> v, TParameter parameter = default)
		{
			v.Visit((dynamic)this, parameter);
		}

		protected static void Verify(ParseTree parseTree, Symbol symbol)
		{
			if (!parseTree.Symbol.Equals(symbol))
				throw new ArgumentException($"Bad ParseTree given. Expected symbol:" +
				                            $"'{symbol}', but got '{parseTree.Symbol}'.");
		}

		protected static void Verify(ParseTree parseTree, params Symbol[] symbols)
		{
			bool isOneOf = false;
			foreach (var symbol in symbols)
				isOneOf |= parseTree.Symbol.Equals(symbol);
			if (!isOneOf)
			{
				string str = "{";
				foreach (var symbol in symbols)
					str += $"{symbol}, ";
				str = str.Remove(str.Length - 2, 2) + "} ";
				throw new ArgumentException($"Bad ParseTree given. Expected one of:" +
				                            $"{str}but got '{parseTree.Symbol}'.");
			}
		}

		protected static void Verify(ParseBranch parseBranch, int n)
		{
			if (parseBranch.Children.Count != n)
				throw new ArgumentException($"{parseBranch.Symbol} tree should have exactly {n} children");
		}

		// Returns null if node does not have children
		public virtual IEnumerable<AST> Children { get { return null; } }
	}

	// Root of the AST
	public class Program : AST
	{
		public IList<FunctionDeclaration> Declarations { get; set; }

		public Program()
		{

		}

		public Program(IList<FunctionDeclaration> declarations)
		{
			Declarations = declarations;
		}

		public static Program Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.start);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			var declarations = new List<FunctionDeclaration>();
			foreach (var subtree in parseBranch.Children)
				declarations.Add( FunctionDeclaration.Create(subtree));
			return new Program(declarations);
		}

		public override IEnumerable<AST> Children {
			get {
				return Declarations;
			}
		}
	}

	public abstract class Statement : AST
	{
		private static readonly Dictionary<Production, Func<ParseTree, Statement>> Statements =
		new Dictionary<Production, Func<ParseTree, Statement>>
		{
			{CimGrammarCreator.statementToCallOrAssign, GetCallOrAssign},
			{CimGrammarCreator.statementToDeclaration, GetDeclaration},
			{CimGrammarCreator.statementToIfClause, IfClause.Create},
			{CimGrammarCreator.statementToWhileClause, WhileClause.Create},
			{CimGrammarCreator.statementToReturn, ReturnClause.Create},
			{CimGrammarCreator.statementToBreak, BreakClause.Create},
			{CimGrammarCreator.statementToContinue, ContinueClause.Create},
		};

		public static Statement GetDeclaration(ParseTree parseTree)
		{
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (parseBranch.Prod.Equals(CimGrammarCreator.declarationToAssign))
				return DeclareAssignment.Create(parseBranch);
			if (parseBranch.Prod.Equals(CimGrammarCreator.declarationToVFunc) ||
				parseBranch.Prod.Equals(CimGrammarCreator.declarationToBFunc))
				return FunctionDeclaration.Create(parseBranch);
			throw new ArgumentException(
				$"Unknown production from {CimGrammarCreator.declaration}: {parseBranch.Prod}.");
		}
		
		public static Statement GetCallOrAssign(ParseTree parseTree)
		{
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (parseBranch.Prod.Equals(CimGrammarCreator.callOrAssignToAssign))
				return Assignment.Create(parseBranch);
			if (parseBranch.Prod.Equals(CimGrammarCreator.callOrAssignToCall) ||
				parseBranch.Prod.Equals(CimGrammarCreator.callOrAssignToEmptyCall))
				return FunctionCall.Create(parseBranch);
			throw new ArgumentException(
				$"Unknown production from {CimGrammarCreator.callOrAssign}: {parseBranch.Prod}.");
		}
	
		public static Statement Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.statement);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			var create = Statements[parseBranch.Prod];
			return create(parseBranch.Children.First());
		}

	}

	public abstract class Expression : Statement
	{
		private static readonly Dictionary<Production, Func<ParseTree, Expression>> Expressions =
		new Dictionary<Production, Func<ParseTree, Expression>>
		{
			{CimGrammarCreator.expToExpPrime, GetSubexpression},
			{CimGrammarCreator.expToBinaryOp, BinaryOperation.Create},
			{CimGrammarCreator.expToUnaryOp, UnaryOperation.Create},
			{CimGrammarCreator.expPrimeToNameOrCall, GetNameOrCall},
			{CimGrammarCreator.expPrimeToLiteral, Literal.Create},
			{CimGrammarCreator.expPrimeToParentedExp, GetSubexpression},
			{CimGrammarCreator.parentedExpProd, GetParentedExp},
		};

		public static Expression GetNameOrCall(ParseTree parseTree)
		{
			ParseBranch child = (ParseBranch)((ParseBranch)parseTree).Children.First();
			if (child.Prod.Equals(CimGrammarCreator.nameOrCallToName))
				return VarUse.Create(child.Children.First());
			if (child.Prod.Equals(CimGrammarCreator.nameOrCallToCall) ||
				child.Prod.Equals(CimGrammarCreator.nameOrCallToEmptyCall))
				return FunctionCall.Create(child);
			throw new ArgumentException(
				$"Unknown production from {CimGrammarCreator.callOrAssign}: {child.Prod}.");
		}
		public static new Expression Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.expression,
				CimGrammarCreator.expression_1, CimGrammarCreator.parentedExpression);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			var create = Expressions[parseBranch.Prod];
			return create(parseBranch);
		}

		public static Expression GetSubexpression(ParseTree parseTree)
		{
			ParseBranch parseBranch = (ParseBranch)parseTree;
			return Create(parseBranch.Children.First());
		}

		public static Expression GetParentedExp(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.parentedExpression);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			Verify(parseBranch, 3);
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.lparen))
				throw new ArgumentException("\'(\' expected");
			if (!parseBranch.Children.Last().Symbol.Equals(CimGrammarCreator.rparen))
				throw new ArgumentException("\')\' expected");
			return Create(parseBranch.Children[1]);
		}

		public Type Type;

	}

	public abstract class SimpleExpression : Expression
	{
		
	}

	public abstract class Literal : SimpleExpression
	{

		public static new Expression Create(ParseTree parseTree)
		{
			while (parseTree.Symbol.Equals(CimGrammarCreator.expression) ||
			       parseTree.Symbol.Equals(CimGrammarCreator.expression_1))
				parseTree = ((ParseBranch)parseTree).Children.First();
			AST.Verify(parseTree, CimGrammarCreator.literal);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			Verify(parseBranch, 1);
			ParseLeaf leaf = (ParseLeaf)parseBranch.Children.First(); 
			if (leaf.Symbol.Equals(CimGrammarCreator.boolLiteral))
				return BoolLiteral.Create(leaf);
			else if (leaf.Symbol.Equals(CimGrammarCreator.intLiteral))
				return IntLiteral.Create(leaf);
			else
				throw new ArgumentException($"Unrecognized literal {leaf.Symbol.ToString()}");
		}
	}

	public class BoolLiteral : Literal
	{
		public bool Value { get; set; }
		public BoolLiteral(bool value)
		{
			Value = value;
		}

		public static new BoolLiteral Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.boolLiteral);
			ParseLeaf leaf = (ParseLeaf)parseTree;
			return new BoolLiteral(leaf.Token.Text == "true");
		}
		public BoolLiteral()
		{

		}
	}

	public class IntLiteral : Literal
	{
		public string Number { get; set; }

		public IntLiteral(string number)
		{
			Number = number;
		}

		public static new IntLiteral Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.intLiteral);
			ParseLeaf leaf = (ParseLeaf)parseTree;
			return new IntLiteral(leaf.Token.Text);
		}
		public IntLiteral()
		{

		}
	}

	public class VarName : AST
	{
		public readonly bool IsTemporary;
		public IVariableDeclaration Declaration;
		public string Name { get; set; }

		public VarName(string name, bool temp = false)
		{
			Name = name;
			IsTemporary = temp;
		}

		static int tempCounter = 0;
		public static VarName FreshTemporary(string name = null)
		{
			var tname = name is null ? $".x{tempCounter++}" : name;
			return new VarName(tname, true);
		}

		public static VarName Create(ParseTree parseTree)
		{
			while (parseTree.Symbol.Equals(CimGrammarCreator.expression) ||
			       parseTree.Symbol.Equals(CimGrammarCreator.expression_1))
				parseTree = ((ParseBranch)parseTree).Children.First();
			AST.Verify(parseTree, CimGrammarCreator.name);
			ParseLeaf leaf = (ParseLeaf)parseTree;
			return new VarName(leaf.Token.Text);
		}
		public VarName()
		{

		}
	}

	public class VarUse : SimpleExpression
	{
		public VarName Name { get; set; }

		public VarUse(VarName name)
		{
			Name = name;
		}
		public VarUse()
		{

		}

		public static new VarUse Create(ParseTree parseTree)
		{
			return new VarUse(VarName.Create(parseTree));
		}

		public override IEnumerable<AST> Children {
			get {
				yield return Name;
			}
		}
	}

	public class FunctionName : AST
	{
		public FunctionDeclaration Declaration;
		public Function Fun;
		public string Name { get; set; }

		public FunctionName(string name)
		{
			Name = name;
		}

		public static FunctionName Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.name);
			ParseLeaf leaf = (ParseLeaf)parseTree;
			return new FunctionName(leaf.Token.Text);
		}
		public FunctionName()
		{

		}
	}

	public class TypeName : AST
	{
		public string Name { get; set; }

		public Type Type
		{
			get {
				if (Name == "Bool"){
					return Type.Bool;
				} else if (Name == "Int"){
					return Type.Int;
				} else if (Name == "Void"){
					return Type.Void;
				} else {
					throw new NotImplementedException();
				}
			}
		}

		public TypeName(string name)
		{
			Name = name;
		}

		public static TypeName Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.type,
				CimGrammarCreator.baseType, CimGrammarCreator.voidType);
			ParseLeaf leaf;
			if (parseTree.Symbol.Equals(CimGrammarCreator.voidType))
				leaf = (ParseLeaf)parseTree;
			else
			{
				ParseBranch parseBranch = (ParseBranch)parseTree;
				while (!(parseBranch.Children.First() is ParseLeaf))
					parseBranch = (ParseBranch)parseBranch.Children.First();
				leaf = (ParseLeaf)parseBranch.Children.First();
			}
			return new TypeName(leaf.Symbol.ToString());
		}
		public TypeName()
		{

		}
	}

	public class BinaryOperation : SimpleExpression
	{
		public Expression Left { get; set; }
		public Expression Right { get; set; }

		public override IEnumerable<AST> Children {
			get {
				yield return Left;
				yield return Right;
			}
		}

		public static new BinaryOperation Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.expression);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			Verify(parseBranch, 3);
			var left = Expression.Create(parseBranch.Children.First());
			var right = Expression.Create(parseBranch.Children.Last());
			ParseLeaf leaf = (ParseLeaf)parseBranch.Children[1]; 
			switch (leaf.Token.Text)
			{
				case ("+"):
					return new Plus {Left = left, Right = right};
				case ("/"):
					return new Div {Left = left, Right = right};
				case ("-"):
					return new Sub {Left = left, Right = right};
				case ("=="):
					return new Eq {Left = left, Right = right};
				case ("<"):
					return new Leq {Left = left, Right = right};
				case ("and"):
					return new And {Left = left, Right = right};
				case ("or"):
					return new Or {Left = left, Right = right};
				default:
					throw new AggregateException($"Unknown binary operator: \"{leaf.Token.Text}\"");
			}
		}
	}

	public class Plus : BinaryOperation
	{

	}

	public class Mult : BinaryOperation
	{

	}

	public class Div : BinaryOperation
	{

	}

	public class Sub : BinaryOperation
	{

	}

	public class Eq : BinaryOperation
	{

	}

	public class Leq : BinaryOperation
	{

	}

	public class And : BinaryOperation
	{

	}

	public class Or : BinaryOperation
	{
	}

	public class UnaryOperation : SimpleExpression
	{
		public Expression Operand { get; set; }
		public override IEnumerable<AST> Children {
			get {
				yield return Operand;
			}
		}

		public static new UnaryOperation Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.expression);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			Verify(parseBranch, 2);
			var exp = Expression.Create(parseBranch.Children.Last());
			ParseLeaf leaf = (ParseLeaf)parseBranch.Children.First(); 
			switch (leaf.Token.Text)
			{
				case ("not"):
					return new Negation {Operand = exp};
				default:
					throw new AggregateException($"Unknown unary operator: \"{leaf.Token.Text}\"");
			}
		}
	}

	public class Negation : UnaryOperation
	{
	}

	public class FunctionCall : Expression
	{
		public FunctionName FunctionName { get; set; }
		public IList<Expression> Arguments { get; set; }

		public FunctionCall(FunctionName functionName, IList<Expression> arguments)
		{
			FunctionName = functionName;
			Arguments = arguments;
		}

		public FunctionCall()
		{

		}

		public static new Expression Create(ParseTree parseTree)
		{
			if (parseTree.Symbol.Equals(CimGrammarCreator.expression))
				parseTree = ((ParseBranch)parseTree).Children.First();
			AST.Verify(parseTree, CimGrammarCreator.callOrAssign,
				CimGrammarCreator.nameOrCall, CimGrammarCreator.callOrAssign);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			FunctionName fName = FunctionName.Create(parseBranch.Children.First());
			if (!parseBranch.Children[1].Symbol.Equals(CimGrammarCreator.lparen))
				throw new ArgumentException("\'(\' expected");
			List<Expression> arguments = new List<Expression>();
			for (int i = 2; i < parseBranch.Children.Count - 1; i++)
				if (i % 2 == 0)
					arguments.Add(Expression.Create(parseBranch.Children[i]));
				else if (!parseBranch.Children[i].Symbol.Equals(CimGrammarCreator.comma))
					throw new ArgumentException("Separator expected between arguments");
			if (!parseBranch.Children.Last().Symbol.Equals(CimGrammarCreator.rparen))
				throw new ArgumentException("\')\' expected");
			return new FunctionCall(fName, arguments);
		}

		public override IEnumerable<AST> Children {
			get {
				yield return FunctionName;
				foreach (var c in Arguments) { yield return c; }
			}
		}
	}

	public class Assignment : Statement
	{
		public VarName VarName { get; set; }
		public Expression Value { get; set; }

		public Assignment(VarName varName, Expression value)
		{
			Value = value;
			VarName = varName;
		}

		public static new Assignment Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.assignment, CimGrammarCreator.callOrAssign);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			Verify(parseBranch, 3);
			VarName varName = VarName.Create(parseBranch.Children.First());
			if (!parseBranch.Children[1].Symbol.Equals(CimGrammarCreator.assignmentSymbol))
				throw new ArgumentException("Assignment operator expected");
			Expression value = Expression.Create(parseBranch.Children[2]);
			return new Assignment(varName, value);
		}

		public Assignment()
		{

		}

		public override IEnumerable<AST> Children {
			get {
				yield return VarName;
				yield return Value;
			}
		}
	}

	public class DeclareAssignment : Statement, IVariableDeclaration
	{
		public TypeName TypeName { get; set; }
		public VarName VarName { get; set; }
		public Expression Value { get; set; }
		public Variable Var { get; set; }

		public DeclareAssignment(TypeName typeName, VarName varName, Expression value)
		{
			TypeName = typeName;
			Value = value;
			VarName = varName;
		}

		public static new DeclareAssignment Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.declaration);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			Verify(parseBranch, 5);
			TypeName typeName = TypeName.Create(parseBranch.Children[0]);
			VarName varName = VarName.Create(parseBranch.Children[1]);
			if (!parseBranch.Children[2].Symbol.Equals(CimGrammarCreator.assignmentSymbol))
				throw new ArgumentException("Assignment operator expected");
			Expression value = Expression.Create(parseBranch.Children[3]);
			if (!parseBranch.Children[2].Symbol.Equals(CimGrammarCreator.assignmentSymbol))
				throw new ArgumentException("Semicolon expected at the end of declaration");
			return new DeclareAssignment(typeName, varName, value);
		}

		public DeclareAssignment()
		{

		}

		public override IEnumerable<AST> Children {
			get {
				yield return TypeName;
				yield return VarName;
				yield return Value;
			}
		}
	}

	public class IfClause : Statement
	{
		public Expression Condition { get; set; }
		public Block Body { get; set; }
		public IList<ElIfClause> ElIfs { get; set; }
		public Block ElseBody{ get; set; }

		public IfClause(Expression condition, Block body, IList<ElIfClause> elIfs, Block elseBody)
		{
			Condition = condition;
			Body = body;
			ElIfs = elIfs;
			ElseBody = elseBody;
		}

		public static new IfClause Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.ifClause);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.ifKeyword))
				throw new ArgumentException("\"if\" keyword expected");
			var condition = Expression.Create(parseBranch.Children[1]);
			var body = Block.Create(parseBranch.Children[2]);
			var elIfs = new List<ElIfClause>();
			Block elseBody = null;
			int i = 3;
			while (parseBranch.Children[i].Symbol.Equals(CimGrammarCreator.elifClause))
				elIfs.Add(new ElIfClause(parseBranch.Children[i++]));
			ParseBranch elseBranch = (ParseBranch)parseBranch.Children.Last();
			AST.Verify(elseBranch, CimGrammarCreator.elseClause);
			if (elseBranch.Children.Any()) {
				Verify(elseBranch, 2);
				if (!elseBranch.Children.First().Symbol.Equals(CimGrammarCreator.elseKeyword))
					throw new ArgumentException("\"else\" keyword expected");
				AST.Verify(elseBranch.Children.Last(), CimGrammarCreator.block);
				elseBody = Block.Create(elseBranch.Children.Last());
			}
			return new IfClause(condition, body, elIfs, elseBody);
		}

		public IfClause()
		{

		}

		public override IEnumerable<AST> Children {
			get {
				yield return Condition;
				yield return Body;
				if (ElIfs != null)
				{
					foreach (var c in ElIfs) { yield return c; }
				}
				if (ElseBody != null)
				{
					yield return ElseBody;
				}
			}
		}
	}

	public class ElIfClause : AST
	{
		public Expression Condition { get; set; }
		public Block Body { get; set; }

		public ElIfClause(Expression condition, Block body)
		{
			Condition = condition;
			Body = body;
		}

		public ElIfClause(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.elifClause);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.elifKeyword))
				throw new ArgumentException("\"elif\" keyword expected");
			Condition = Expression.Create(parseBranch.Children[1]);
			Body = Block.Create(parseBranch.Children[2]);
		}

		public ElIfClause()
		{

		}

		public override IEnumerable<AST> Children {
			get {
				yield return Condition;
				yield return Body;
			}
		}
	}

	public class WhileClause : Statement
	{
		public readonly FlatGroup Prolog;
		public Expression Condition { get; set; }
		public Block Body { get; set; }

		public WhileClause(Expression condition, Block body)
		{
			Prolog = new FlatGroup(new List<Statement>());
			Condition = condition;
			Body = body;
		}

		public WhileClause(FlatGroup prolog, Expression condition, Block body)
		{
			Prolog = prolog;
			Condition = condition;
			Body = body;
		}

		public static new WhileClause Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.whileClause);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.whileKeyword))
				throw new ArgumentException("\"while\" keyword expected");
			var condition = Expression.Create(parseBranch.Children[1]);
			var body = Block.Create(parseBranch.Children[2]);
			return new WhileClause(condition, body);
		}

		public WhileClause()
		{

		}

		public override IEnumerable<AST> Children {
			get {
				yield return Prolog;
				yield return Condition;
				yield return Body;
			}
		}
	}

	public class FunctionDeclaration : Statement, IDeclaration
	{
		public Function Fun { get; set; }
		public TypeName TypeName{ get; set; }
		public FunctionName FunName { get; set; }
		public IList<Argument> ArgumentList { get; set; }
		public Block Body { get; set; }

		public FunctionDeclaration(TypeName typeName, FunctionName funName, IList<Argument> arguments, Block body)
		{
			TypeName = typeName;
			FunName = funName;
			ArgumentList = arguments;
			Body = body;
		}

		public FunctionDeclaration()
		{

		}
		public static new FunctionDeclaration Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.funcDeclaration, CimGrammarCreator.declaration);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			Verify(parseBranch, 4);
			var typeName = TypeName.Create(parseBranch.Children[0]);
			var funName = FunctionName.Create(parseBranch.Children[1]);
			var argumentList = Argument.CreateList(parseBranch.Children[2]);
			var body = Block.Create(parseBranch.Children.Last());
			return new FunctionDeclaration(typeName, funName, argumentList, body);
		}

		public override IEnumerable<AST> Children
		{
			get
			{
				yield return TypeName;
				yield return FunName;
				foreach (var c in ArgumentList) { yield return c; }
				yield return Body;
			}
		}

	}

	public class Argument : AST, IVariableDeclaration
	{
		public TypeName TypeName{ get; set; }
		public VarName VarName { get; set; }
		public Variable Var { get; set; }

		public Argument(TypeName typeName, VarName varName)
		{
			TypeName = typeName;
			VarName = varName;
		}

		public Argument()
		{

		}

		public override IEnumerable<AST> Children {
			get {
				yield return TypeName;
				yield return VarName;
			}
		}

		public static IList<Argument> CreateList(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.parametersList);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.lparen))
				throw new ArgumentException("Left paren expected.");
			var types = new List<TypeName>();
			var vars = new List<VarName>();
			for (int i = 1; i < parseBranch.Children.Count - 1; i++)
			{
				if (i % 3 == 1)
					types.Add( TypeName.Create(parseBranch.Children[i]));
				else if (i % 3 == 2)
					vars.Add(VarName.Create(parseBranch.Children[i]));
				else if (!parseBranch.Children[i].Symbol.Equals(CimGrammarCreator.comma))
					throw new ArgumentException("Separator expected between arguments");
			}
			if (!parseBranch.Children.Last().Symbol.Equals(CimGrammarCreator.rparen))
				throw new ArgumentException("Right paren expected.");
			if (types.Count != vars.Count)
				throw new ArgumentException(
					"Provided parse branch has unequal number of types and variables.");
			var args = new List<Argument>();
			foreach (var (t, v) in Enumerable.Zip(types, vars, (t, v) => (t, v)))
				args.Add(new Argument(t, v));
			return args;
		}
	}

	public class ReturnClause : Statement
	{
		public Expression ReturnExpression { get; set; }

		public ReturnClause(Expression returnExpression)
		{
			ReturnExpression = returnExpression;
		}

		public ReturnClause()
		{

		}

		public static new ReturnClause Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.returnClause);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.returnKeyword))
				throw new ArgumentException("\"return\" keyword expected");
			Expression returnExpression = null;
			if (parseBranch.Children.Count > 1)
				returnExpression = Expression.Create(parseBranch.Children[1]);
			return new ReturnClause(returnExpression);
		}

		public override IEnumerable<AST> Children { get { yield return ReturnExpression; } }
	}

	public class BreakClause : Statement
	{
		public IntLiteral N { get; set; }

		public BreakClause(IntLiteral literal = null)
		{
			N = literal;
		}

		public BreakClause()
		{

		}

		public static new BreakClause Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.breakClause);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.breakKeyword))
				throw new ArgumentException("\"break\" keyword expected");
			IntLiteral literal = null;
			if (parseBranch.Children.Count == 2)
				literal = IntLiteral.Create(parseBranch.Children[1]);
			return new BreakClause(literal);
		}

		public override IEnumerable<AST> Children { get { yield return N; } }
	}

	public class ContinueClause : Statement
	{
		public IntLiteral Literal { get; set; }

		public ContinueClause(IntLiteral literal = null)
		{
			Literal = literal;
		}

		public ContinueClause()
		{

		}

		public static new ContinueClause Create(ParseTree parseTree = null)
		{
			AST.Verify(parseTree, CimGrammarCreator.continueClause);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.continueKeyword))
				throw new ArgumentException("\"continue\" keyword expected");
			IntLiteral literal = null;
			if (parseBranch.Children.Count == 2)
				literal = IntLiteral.Create(parseBranch.Children[1]);
			return new ContinueClause(literal);
		}

		public override IEnumerable<AST> Children { get { yield return Literal; } }
	}

	public abstract class Group : Statement
	{
		public IList<Statement> Statements { get; set; }
		public override IEnumerable<AST> Children { get { return Statements; } }
	}

	// FlatGroup is a artificial grouping of statements
	// It is different from block in that it does not introduce new scope
	// and that is needed in two places: in prolog of WhileClause and in ExtractShortCircuits.
	// If WhileClause had Block as it's prolog, then NameResolution would consider variables introduced
	// in prolog as visible only in the prolog because Block introduces new scope, but that is not the intent with prolog.
	// ExtractShortCircuits often needs to return multiple statements and for the same reason we don't want to pack them into a Block.
	public class FlatGroup : Group
	{
		public FlatGroup()
		{

		}
		void addFlatten(Statement st)
		{
			if (st is FlatGroup g) {
				foreach (var x in g.Statements) {
					addFlatten(x);
				}
			} else {
				Statements.Add(st);
			}
		}

		public FlatGroup(IList<Statement> statements)
		{
			Statements = new List<Statement>();
			foreach (var st in statements) {
				addFlatten(st);
			}
		}
	}

	public class Block : Group
	{
		public Block(params Statement[] statements)
		{
			Statements = statements.ToList();
		}

		public Block(IList<Statement> statements)
		{
			Statements = statements;
		}

		public Block()
		{

		}
		public static new Block Create(ParseTree parseTree)
		{
			AST.Verify(parseTree, CimGrammarCreator.block);
			ParseBranch parseBranch = (ParseBranch)parseTree;
			if (parseBranch.Children.Count < 2)
				throw new ArgumentException("Block tree should have at least 2 children");
			if (!parseBranch.Children.First().Symbol.Equals(CimGrammarCreator.lbracket))
				throw new ArgumentException("\'{\' expected");
			var statements = new List<Statement>();
			for (int i = 1; i < parseBranch.Children.Count - 1; i++)
				statements.Add(Statement.Create(parseBranch.Children[i]));
			if (!parseBranch.Children.Last().Symbol.Equals(CimGrammarCreator.rbracket))
				throw new ArgumentException("\'}\' expected");
			return new Block(statements);
		}

	}

}
