﻿﻿using System;
using Cimplified.Lexer;
using System.Collections.Generic;

namespace Cimplified.Parser
{
    using Atomic = AtomicRegex<Symbol>;
	using Union = UnionRegex<Symbol>;
	using Star = StarRegex<Symbol>;
	using Concat = ConcatRegex<Symbol>;
	using Empty = EmptyRegex<Symbol>;

	using AtomicChar = AtomicRegex<char>;
	using UnionChar = UnionRegex<char>;
	using StarChar = StarRegex<char>;
	using ConcatChar = ConcatRegex<char>;
	using EmptyChar = EmptyRegex<char>;
    public static class CimGrammarCreator
	{
		// NonTerminals
		public static NonTerminal start = new NonTerminal("Start");
		public static NonTerminal expression = new NonTerminal("Expression");
		public static NonTerminal expression_1 = new NonTerminal("Expression_1");
		public static NonTerminal parentedExpression = new NonTerminal("ParentedExpression");
		public static NonTerminal literal = new NonTerminal("Literal");
		public static NonTerminal baseType = new NonTerminal("BaseType");
		public static NonTerminal type = new NonTerminal("Type");
		public static NonTerminal callOrAssign = new NonTerminal("CallOrAssign");
		public static NonTerminal nameOrCall = new NonTerminal("NameOrCall");
		public static NonTerminal assignment = new NonTerminal("Assignment");
		public static NonTerminal declaration = new NonTerminal("Declaration");
		public static NonTerminal ifClause = new NonTerminal("IfClause");
		public static NonTerminal elseClause = new NonTerminal("ElseClause");
		public static NonTerminal elifClause = new NonTerminal("ElifClause");
		public static NonTerminal whileClause = new NonTerminal("WhileClause");
		public static NonTerminal statement = new NonTerminal("Statement");
		public static NonTerminal block = new NonTerminal("Block");
		public static NonTerminal returnClause = new NonTerminal("ReturnClause");
		public static NonTerminal funcDeclaration = new NonTerminal("FuncDeclaration");
		public static NonTerminal parametersList = new NonTerminal("ParametersList");
		public static NonTerminal breakClause = new NonTerminal("BreakClause");
		public static NonTerminal continueClause = new NonTerminal("ContinueClause");
		
		// Terminals
		public static Terminal whitespace = new Terminal("WHITESPACE");
		public static Terminal binaryOp = new Terminal("binaryOp");
		public static Terminal unaryOp = new Terminal("unaryOp");
		public static Terminal assignmentSymbol = new Terminal("assignmentSymbol");
		public static Terminal lbracket = new Terminal("lbracket");
		public static Terminal rbracket = new Terminal("rbracket");
		public static Terminal lparen = new Terminal("lparen");
		public static Terminal rparen = new Terminal("rparen");
		public static Terminal semicolon = new Terminal("semicolon");
		public static Terminal comma = new Terminal("comma");
		public static Terminal name = new Terminal("name");
		public static Terminal intLiteral = new Terminal("intLiteral");
		public static Terminal boolLiteral = new Terminal("boolLiteral");
		public static Terminal intType = new Terminal("intType");
		public static Terminal boolType = new Terminal("boolType");
		public static Terminal voidType = new Terminal("voidType");
		public static Terminal returnKeyword = new Terminal("returnKeyword");
		public static Terminal whileKeyword = new Terminal("whileKeyword");
		public static Terminal ifKeyword = new Terminal("ifKeyword");
		public static Terminal elifKeyword = new Terminal("elifKeyword");
		public static Terminal elseKeyword = new Terminal("elseKeyword");
		public static Terminal breakKeyword = new Terminal("breakKeyword");
		public static Terminal continueKeyword = new Terminal("continueKeyword");
		public static Terminal Whitespace 
		{
			get{
				return whitespace;
			}
		}
		public static HashSet<Terminal> Terminals
		{
			get{
				return new HashSet<Terminal>
				{
					whitespace,
					unaryOp,
					binaryOp,
					assignmentSymbol,
					lbracket,
					rbracket,
					lparen,
					rparen,
					semicolon,
					comma,
					name,
					intLiteral,
					boolLiteral,
					intType,
					boolType,
					voidType,
					returnKeyword,
					whileKeyword,
					ifKeyword,
					elifKeyword, 
					breakKeyword,
					continueKeyword,
					elseKeyword
				};
			}
		}

		// Productions

		// S -> funcDeclr*
		public static Production startToFuncDecl = new Production(start, new Star(new Atomic(funcDeclaration)));
		// funcDeclr -> type name argList block
		public static Production funcDeclProd = new Production(funcDeclaration, new Concat(new Atomic(type), new Atomic(name), new Atomic(parametersList), new Atomic(block)));
		// type -> baseType | Void
		public static Production typeToBaseTypesOrVoid = new Production(type, new Union(new Atomic(baseType), new Atomic(voidType)));
		// baseType -> Bool | Int
		public static Production baseTypesToIntOrBool = new Production(baseType, new Union(new Atomic(boolType), new Atomic(intType)));
		// parametersList -> () | (baseType name <, BaseType name>*)
		private static Regex<Symbol> _parametersRest = new Star(new Concat(new Atomic(comma), new Atomic(baseType), new Atomic(name)));
		public static Production parametersListToEmpty = new Production(parametersList, new Concat(new Atomic(lparen), new Atomic(rparen)));
		public static Production parametersListToNonEmpty = new Production(parametersList, new Concat(new Atomic(lparen), new Atomic(baseType), new Atomic(name), _parametersRest, new Atomic(rparen)));
		// returnClause -> return exp | return
		public static Production returnClauseProd = new Production(returnClause, new Union(new Concat(new Atomic(returnKeyword), new Atomic(expression)), new Atomic(returnKeyword)));
		// statement -> callOrAssign; | declaration | ifClause | whileClause | funcDeclr | returnClause; | breakClause; | continueClause;
		public static Production statementToCallOrAssign = new Production(statement, new Concat(new Atomic(callOrAssign), new Atomic(semicolon)));
		public static Production statementToDeclaration = new Production(statement, new Atomic(declaration));
		public static Production statementToIfClause = new Production(statement, new Atomic(ifClause));
		public static Production statementToWhileClause = new Production(statement, new Atomic(whileClause));
		public static Production statementToReturn = new Production(statement, new Concat(new Atomic(returnClause), new Atomic(semicolon)));
		public static Production statementToBreak = new Production(statement, new Concat(new Atomic(breakClause), new Atomic(semicolon)));
		public static Production statementToContinue = new Production(statement, new Concat(new Atomic(continueClause), new Atomic(semicolon)));
		// declaration -> baseType name = expr ; | baseType name parametersList block | voidType name parametersList block
		public static Production declarationToAssign = new Production(declaration, new Concat(new Atomic(baseType), new Atomic(name), new Atomic(assignmentSymbol), new Atomic(expression), new Atomic(semicolon)));
		public static Production declarationToBFunc = new Production(declaration, new Concat(new Atomic(baseType), new Atomic(name), new Atomic(parametersList), new Atomic(block)));
		public static Production declarationToVFunc = new Production(declaration, new Concat(new Atomic(voidType), new Atomic(name), new Atomic(parametersList), new Atomic(block)));
		// callOrAssign -> name = exp | name lp rp | name lp exp (comma exp)* rp
		public static Production callOrAssignToAssign = new Production(callOrAssign, new Concat(new Atomic(name), new Atomic(assignmentSymbol), new Atomic(expression)));
		public static Production callOrAssignToEmptyCall = new Production(callOrAssign, new Concat(new Atomic(name), new Atomic(lparen), new Atomic(rparen)));
		private static Regex<Symbol> _argumentsRest = new Star(new Concat(new Atomic(comma), new Atomic(expression)));
		public static Production callOrAssignToCall = new Production(callOrAssign, new Concat(new Atomic(name), new Atomic(lparen), new Atomic(expression), _argumentsRest, new Atomic(rparen)));
		// exp -> exp' | exp' op exp' | unaryOp exp'
		public static Production expToExpPrime = new Production(expression, new Atomic(expression_1));
		public static Production expToBinaryOp = new Production(expression, new Concat(new Atomic(expression_1), new Atomic(binaryOp), new Atomic(expression_1)));
		public static Production expToUnaryOp = new Production(expression, new Concat(new Atomic(unaryOp), new Atomic(expression_1)));
		// exp' -> name | funcCall | parentedExp | term
		public static Production expPrimeToNameOrCall = new Production(expression_1, new Atomic(nameOrCall));
		public static Production expPrimeToParentedExp = new Production(expression_1, new Atomic(parentedExpression));
		public static Production expPrimeToLiteral = new Production(expression_1, new Atomic(literal));
		// parentedExp -> (exp)
		public static Production parentedExpProd = new Production(parentedExpression, new Concat(new Atomic(lparen), new Atomic(expression), new Atomic(rparen)));

		public static Production nameOrCallToName = new Production(nameOrCall, new Atomic(name));
		// funcCall -> name() | name(exp argumentsRest*)
		public static Production nameOrCallToEmptyCall = new Production(nameOrCall, new Concat(new Atomic(name), new Atomic(lparen), new Atomic(rparen)));
		public static Production nameOrCallToCall = new Production(nameOrCall, new Concat(new Atomic(name), new Atomic(lparen), new Atomic(expression), _argumentsRest, new Atomic(rparen)));
		// literal -> bool | int
		public static Production literalToBoolOrInt = new Production(literal, new Union(new Atomic(boolLiteral), new Atomic(intLiteral)));
		// assign ->  name = exp
		public static Production assignmentProd = new Production(assignment, new Concat(new Atomic(name), new Atomic(assignmentSymbol), new Atomic(expression)));
		// ifClause -> if parentedExp block elifClause* elseClause
		public static Production ifProduction = new Production(ifClause, new Concat(new Atomic(ifKeyword), new Atomic(parentedExpression), new Atomic(block), new Star(new Atomic(elifClause)), new Atomic(elseClause)));
		// elseClause -> \epsilon | else block
		public static Production elseProduction = new Production(elseClause, new Union(new Star(new Empty()), new Concat(new Atomic(elseKeyword), new Atomic(block)))); 
		// elifClause -> elif parentedExp block
		public static Production elifClauseProd = new Production(elifClause, new Concat(new Atomic(elifKeyword), new Atomic(parentedExpression), new Atomic(block)));
		// whileClause -> while parentedExp block
		public static Production whileClauseProd = new Production(whileClause, new Concat(new Atomic(whileKeyword), new Atomic(parentedExpression), new Atomic(block)));
		// block -> {statement*}
		public static Production blockToStatements = new Production(block, new Concat(new Atomic(lbracket), new Star(new Atomic(statement)), new Atomic(rbracket)));
		// breakClause -> break | break intLiteral
		public static Production breakClauseProd = new Production(breakClause, new Union(new Atomic(breakKeyword), new Concat(new Atomic(breakKeyword), new Atomic(intLiteral))));
		public static Production continueClauseProd = new Production(continueClause, new Union(new Atomic(continueKeyword), new Concat(new Atomic(continueKeyword), new Atomic(intLiteral))));

		public static List<Production> Productions
		{
			get{
				return new List<Production>
				{
					startToFuncDecl,
					funcDeclProd,
					typeToBaseTypesOrVoid,
					baseTypesToIntOrBool,
					parametersListToEmpty,
					parametersListToNonEmpty,
					returnClauseProd,
					statementToCallOrAssign,
					statementToDeclaration,
					statementToIfClause,
					statementToWhileClause,
					statementToReturn,
					statementToBreak,
					declarationToAssign,
					declarationToVFunc,
					declarationToBFunc,
					callOrAssignToAssign,
					callOrAssignToEmptyCall,
					callOrAssignToCall,
					expToExpPrime,
					expToBinaryOp,
					expToUnaryOp,
					expPrimeToNameOrCall,
					expPrimeToParentedExp,
					expPrimeToLiteral,
					nameOrCallToName,
					nameOrCallToEmptyCall,
					nameOrCallToCall,
					parentedExpProd,
					literalToBoolOrInt, 
					assignmentProd, 
					ifProduction, 
					elseProduction, 
					elifClauseProd, 
					whileClauseProd, 
					blockToStatements, 
					breakClauseProd,
					statementToContinue,
					continueClauseProd,
				};
			}
		}

        static Regex<char> StringUnion(string value)
        {
            return new UnionChar(value[0], value.Substring(1).ToCharArray());
        }

        static Regex<char> StringConcat(string value)
        {
            return new ConcatChar(value[0], value.Substring(1).ToCharArray());
        }

        public static List<(Terminal, Regex<char>)> CreateTerminalToRegex()
		{
			var digit = StringUnion("0123456789");
            var lowerCase = StringUnion("_abcdefghijklmnopqrstuvwxyz");
            var upperCase = StringUnion("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
            var whitespace = StringUnion(" \t\n");
            var alpha = new UnionChar(lowerCase, upperCase);
            var alphaNumerical = new UnionChar(alpha, digit);

			return new List<(Terminal, Regex<char>)>
			{

				{(Whitespace, whitespace)},
				{(binaryOp, new UnionChar(StringUnion("+-/*<"), StringConcat("=="), StringConcat("and"), StringConcat("or")))},
				{(unaryOp, StringConcat("not"))},
				{(assignmentSymbol, new AtomicChar('='))},
				{(lbracket, new AtomicChar('{'))},
				{(rbracket, new AtomicChar('}'))},
				{(lparen, new AtomicChar('('))},
				{(rparen, new AtomicChar(')'))},
				{(semicolon, new AtomicChar(';'))},
				{(comma, new AtomicChar(','))},
				{(intLiteral, new ConcatChar(digit, new StarChar(digit)))},
				{(boolLiteral, new UnionChar(StringConcat("true"), StringConcat("false")))},
				{(intType, StringConcat("Int"))},
				{(boolType, StringConcat("Bool"))},
				{(voidType, StringConcat("Void"))},
				{(returnKeyword, StringConcat("return"))},
				{(whileKeyword, StringConcat("while"))},
				{(ifKeyword, StringConcat("if"))},
				{(elifKeyword, StringConcat("elif"))},
				{(elseKeyword, StringConcat("else"))},
				{(breakKeyword, StringConcat("break"))},
				{(continueKeyword, StringConcat("continue"))},
				{(name, new ConcatChar(lowerCase, new StarChar(alphaNumerical)))},
			};
		}
        public static Grammar CreateGrammar()
		{
			return new Grammar(start, Productions, Terminals);
        }
    }

}
