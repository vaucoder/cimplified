﻿using Cimplified.Lexer;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Cimplified.Parser
{
	public class GDfas<TState> : Dictionary<NonTerminal, Dfa<Symbol, Production, TState>>
		where TState : IEquatable<TState>
	{
	}

	public class Grammar
	{
		public NonTerminal Start { get; private set; }
		public IEnumerable<Production> Productions { get; private set; }

		public IEnumerable<Terminal> Terminals { get; private set; }

		public Grammar(NonTerminal start, IEnumerable<Production> productions, IEnumerable<Terminal> terminals)
		{
			Start = start;
			Productions = productions;
			Terminals = terminals;
		}
	}


	public static class GrammarAnalysis
	{
		public static GDfas<HashableList<Regex<Symbol>>> ComputeGdfas(Grammar grammar)
		{
			var gDfas = new GDfas<HashableList<Regex<Symbol>>>();
			var dict = new Dictionary<NonTerminal, List<(Production, Dfa<Symbol, bool, Regex<Symbol>>)>>();
			foreach (var production in grammar.Productions)
			{
				Dfa<Symbol, bool, Regex<Symbol>> dfa = Utils.RegexToDfa(production.Rhs);
				if (dict.ContainsKey(production.Lhs))
					dict[production.Lhs].Add((production, dfa));
				else
					dict.Add(production.Lhs,
						new List<(Production, Dfa<Symbol, bool, Regex<Symbol>>)> {(production, dfa)});
			}

			Func<List<Production>, Production> combine = (x) => x.Count > 0 ? x.First() : null;
			foreach (var (nt, lst) in dict)
			{
				var cdfa = CombinedDfa.Create(lst, combine);
				gDfas.Add(nt, cdfa);
			}

			return gDfas;
		}

		public static HashSet<NonTerminal> ComputeNullable<TState>(GDfas<TState> gdfas)
			where TState : IEquatable<TState>
		{
			var stateQueue = new Queue<Tuple<NonTerminal, Dfa<Symbol, Production, TState>, TState>>();
			var visitedStates = new HashSet<Tuple<NonTerminal, Dfa<Symbol, Production, TState>, TState>>();
			var nullable = new HashSet<NonTerminal>();
			var conditioned =
				new Dictionary<NonTerminal, HashSet<Tuple<NonTerminal, Dfa<Symbol, Production, TState>, TState>>>();
			foreach (var (nonterminal, cdfa) in gdfas)
			{
				foreach (var state in cdfa.TransitionsTable.Keys)
				{
					var production = cdfa.Category(state);
					if (production is object)
					{
						stateQueue.Enqueue(Tuple.Create(nonterminal, cdfa, state));
					}
				}
			}

			while (stateQueue.Count > 0)
			{
				var tuple = stateQueue.Dequeue();
				if (visitedStates.Contains(tuple))
				{
					continue;
				}

				visitedStates.Add(tuple);
				var (nonterminal, cdfa, state) = tuple;
				if (state.Equals(cdfa.Start()))
				{
					nullable.Add(nonterminal);
					if (conditioned.ContainsKey(nonterminal))
					{
						conditioned[nonterminal].ToList().ForEach(s => stateQueue.Enqueue(s));
					}

					continue;
				}

				foreach (var (symbol, predecessors) in cdfa.ReverseTransitionsTable[state])
				{
					if (symbol is NonTerminal)
					{
						var castedSymbol = (NonTerminal) symbol;
						if (nullable.Contains(symbol))
						{
							predecessors.ToList().ForEach(p => stateQueue.Enqueue(Tuple.Create(nonterminal, cdfa, p)));
						}
						else
						{
							if (!conditioned.ContainsKey(castedSymbol))
							{
								conditioned[castedSymbol] =
									new HashSet<Tuple<NonTerminal, Dfa<Symbol, Production, TState>, TState>>();
							}

							predecessors.ToList().ForEach(p =>
								conditioned[castedSymbol].Add(Tuple.Create(nonterminal, cdfa, p)));
						}
					}
				}
			}

			return nullable;
		}

		public static IDictionary<NonTerminal, HashSet<Symbol>> ComputeFirst<TState>(GDfas<TState> gdfas,
			HashSet<NonTerminal> nullable) where TState : IEquatable<TState>
		{
			var ret = new Dictionary<NonTerminal, HashSet<Symbol>>();

			foreach (var (key, value) in gdfas)
			{
				var bag = new HashSet<Symbol>();

				void add(Symbol s)
				{
					if (bag.Contains(s))
					{
						return;
					}

					bag.Add(s);
					switch (s)
					{
						case NonTerminal nt:
							add_recursively(gdfas[nt], gdfas[nt].Start());
							break;
					}
				}

				void add_recursively(Dfa<Symbol, Production, TState> D, TState state)
				{
					var transitions = D.TransitionsTable[state];
					foreach (var (symbol, t) in transitions)
					{
						add(symbol);
						if (nullable.Contains(symbol))
						{
							var v = D.Transitions(state)[symbol];
							add_recursively(D, v);
						}
					}
				}

				add(key);
				ret[key] = bag;
			}

			return ret;
		}

		public static IDictionary<NonTerminal, HashSet<Symbol>> ComputeFollow<TState>(GDfas<TState> gdfas,
			ISet<NonTerminal> nullable, IDictionary<NonTerminal, HashSet<Symbol>> first)
			where TState : IEquatable<TState>
		{
			var follow = new Dictionary<NonTerminal, HashSet<Symbol>>();
			var statesToProceed =
				new Queue<(NonTerminal, Symbol, bool, Dfa<Symbol, Production, TState>, TState, HashSet<Symbol>)>();
			var visitedStates = new HashSet<(NonTerminal, Symbol, bool, Dfa<Symbol, Production, TState>, TState)>();
			var followContainsSubset = new Dictionary<NonTerminal, HashSet<NonTerminal>>();

			foreach (var (dfaNonTerminal, dfa) in gdfas)
			{
				follow[dfaNonTerminal] = new HashSet<Symbol>();
				followContainsSubset[dfaNonTerminal] = new HashSet<NonTerminal>();
				foreach (var state in dfa.States)
				{
					foreach (var (symbol, predecessors) in dfa.ReverseTransitionsTable[state])
					{
						predecessors.ToList().ForEach(predecessorState => statesToProceed.Enqueue((dfaNonTerminal,
							symbol, dfa.AcceptingStates.Contains(state), dfa, predecessorState,
							new HashSet<Symbol>())));
					}
				}
			}

			while (statesToProceed.Count > 0)
			{
				var (dfaNonTerminal, symbolToAddToFollow, isAccepting, dfa, state, previousFollowInCurrentDfa) =
					statesToProceed.Dequeue();
				if (visitedStates.Contains((dfaNonTerminal, symbolToAddToFollow, isAccepting, dfa, state))) continue;
				visitedStates.Add((dfaNonTerminal, symbolToAddToFollow, isAccepting, dfa, state));

				if (isAccepting && symbolToAddToFollow is NonTerminal nonTerminal)
					followContainsSubset[nonTerminal].Add(dfaNonTerminal);

				foreach (var (symbol, predecessors) in dfa.ReverseTransitionsTable[state])
				{
					var followForSymbolInCurrentDfa = new HashSet<Symbol>();
					predecessors.ToList().ForEach(predecessor =>
						statesToProceed.Enqueue((dfaNonTerminal, symbol, isAccepting && nullable.Contains(symbol), dfa,
							predecessor, followForSymbolInCurrentDfa))
					);

					if (!(symbol is NonTerminal previousStateNonTerminal)) continue;

					if (nullable.Contains(symbolToAddToFollow) && symbolToAddToFollow is NonTerminal nt)
					{
						followForSymbolInCurrentDfa.UnionWith(previousFollowInCurrentDfa);
						follow[previousStateNonTerminal].UnionWith(followForSymbolInCurrentDfa);
					}

					if (symbolToAddToFollow is NonTerminal nonTerminalToAddToFollow)
					{
						followForSymbolInCurrentDfa.UnionWith(first[nonTerminalToAddToFollow]);
						follow[previousStateNonTerminal].UnionWith(first[nonTerminalToAddToFollow]);
					}
					else
					{
						followForSymbolInCurrentDfa.Add(symbolToAddToFollow);
						follow[previousStateNonTerminal].Add(symbolToAddToFollow);
					}
				}
			}

			// add if follow(B) \subset follow(A)
			foreach (var (nonTerminal, containedSubsets) in followContainsSubset)
			{
				foreach (var followSubsetNonTerminal in containedSubsets)
				{
					follow[nonTerminal].UnionWith(follow[followSubsetNonTerminal]);
				}
			}

			// Brute fix. Should be replaced somehow
			foreach (var nonTerminal in follow.Keys)
			{
				var intersect = follow[nonTerminal].Intersect(nullable).ToHashSet();
				foreach (var symbol in intersect)
				{
					if (symbol is NonTerminal)
					{
						follow[nonTerminal].UnionWith(follow[(NonTerminal) symbol]);
					}
				}
			}

			return follow.ToImmutableDictionary();
		}

		public static IDictionary<NonTerminal, HashSet<Symbol>> ComputeFirstPlus<TState>(ISet<NonTerminal> nullable,
			IDictionary<NonTerminal, HashSet<Symbol>> first, IDictionary<NonTerminal, HashSet<Symbol>> follow)
			where TState : IEquatable<TState>
		{
			var firstPlus = new Dictionary<NonTerminal, HashSet<Symbol>>();
			foreach (var A in first.Keys) // iterate through all NonTerminals
			{
				if (!nullable.Contains(A))
					firstPlus[A] = first[A];
				else
					firstPlus[A] = new HashSet<Symbol>(first[A].Union(follow[A]));
			}

			return firstPlus;
		}
	}
}