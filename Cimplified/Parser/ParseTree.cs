﻿using Cimplified.Lexer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cimplified.Parser
{
	public abstract class ParseTree
	{
		public abstract Symbol Symbol
		{ get; }

		public static AST ParseTreeToAST(ParseTree parseTree)
		{
			return Program.Create(parseTree);
		}

	}

	public class ParseLeaf : ParseTree
	{
		public ParseLeaf(Token<Terminal> token)
		{
			Token = token;
		}

		public Token<Terminal> Token
		{ get; private set; }

		public override Symbol Symbol => Token.Kind;
	}

	public class ParseBranch : ParseTree
	{
		public ParseBranch(Production prod, IReadOnlyList<ParseTree> children)
		{
			Prod = prod;
			Children = children;
		}
		public Production Prod
		{ get; private set; }

		public IReadOnlyList<ParseTree> Children
		{ get; private set; }
		
		public override Symbol Symbol => Prod.Lhs;
	}
}
