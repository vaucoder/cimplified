using System;
using Cimplified.Lexer;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Cimplified.Parser
{
	public class Parser<TState> where TState : IEquatable<TState>
	{
		public Action<Diagnostic> Emit;

		public int RunNext(ParseTree input, Stack<List<ParseTree>> gatheredTrees, Stack<(NonTerminal, TState)> state)
		{
			try
			{
				var (nonTerminal, stateOfDfa) = state.Peek();
				Action nextAction = this.ActionTable[(nonTerminal, stateOfDfa, input.Symbol)];
				nextAction.Run(this, input, gatheredTrees, state);
			}
			catch (KeyNotFoundException)
			{
				var diagnostic = new UnknownAction($"No action for given Symbol x State combination." +
				                                   $"({input.Symbol}, {state.Peek().ToString()}) Skipping token.");
				if (!(Emit is null))
					Emit(diagnostic);
				else
					throw diagnostic;
				return -1;
			}
			return 0;
		}
		
		public abstract class Action : IEquatable<Action>
		{
			public abstract void Run(Parser<TState> parser, ParseTree input,
									 Stack<List<ParseTree>> gatheredTrees, Stack<(NonTerminal, TState)> state);

			public abstract bool Equals(Action other);
		}

		public class Shift : Action
		{
			public Shift(TState nextState)
			{
				NextState = nextState;
			}

			public override bool Equals(Action other)
			{
				return (other is Shift shift && shift.NextState.Equals(NextState));
			}

			public TState NextState
			{ get; private set; }

			public override void Run(Parser<TState> parser, ParseTree input,
									 Stack<List<ParseTree>> gatheredTrees, Stack<(NonTerminal, TState)> state)
			{
				gatheredTrees.Peek().Add(input);
				var (nonTerminal, _) = state.Pop();
				state.Push((nonTerminal, NextState));
			}
		}

		public class Call : Action
		{
			public Call(TState nextState, TState childState, NonTerminal childNonTerminal)
			{
				NextState = nextState;
				ChildState = childState;
				ChildNonTerminal = childNonTerminal;
			}

			public override bool Equals(Action other)
			{
				return (other is Call call && ChildState.Equals(call.ChildState)
				                           && NextState.Equals(call.NextState)
				                           && ChildNonTerminal.Equals(call.ChildNonTerminal));
			}
			
			public TState ChildState
			{ get; private set; }

			public TState NextState
			{ get; private set; }

			public NonTerminal ChildNonTerminal
			{ get; private set; }

			public override void Run(Parser<TState> parser, ParseTree input,
									 Stack<List<ParseTree>> gatheredTrees, Stack<(NonTerminal, TState)> state)
			{
				gatheredTrees.Push(new List<ParseTree>());
				var (nonTerminal, _) = state.Pop();
				state.Push((nonTerminal, NextState));
				state.Push((ChildNonTerminal, ChildState));
				parser.RunNext(input, gatheredTrees, state);
			}
		}

		public class Reduce : Action
		{
			public Reduce(Production prod)
			{
				Prod = prod;
			}

			public override bool Equals(Action other)
			{
				return (other is Reduce reduce && Prod.Equals(reduce.Prod));
			}
			
			public Production Prod
			{ get; private set; }
			
			public override void Run(Parser<TState> parser, ParseTree input,
									 Stack<List<ParseTree>> gatheredTrees, Stack<(NonTerminal, TState)> state)
			{
				var children = gatheredTrees.Pop();
				ParseTree newTree = new ParseBranch(Prod, children);
				gatheredTrees.Peek().Add(newTree);
				state.Pop();
				if (state.Any())
					parser.RunNext(input, gatheredTrees, state);
			}
		}

		public Dictionary<(NonTerminal, TState, Symbol), Action> ActionTable;
		
		public (NonTerminal, TState) StartState;

		public Parser()
		{
		}

		public ParseTree Parse(IEnumerable<ParseTree> parseTrees)
		{
			var gatheredTrees = new Stack<List<ParseTree>>();
			gatheredTrees.Push(new List<ParseTree>());
			gatheredTrees.Push(new List<ParseTree>());
			var state = new Stack<(NonTerminal, TState)>();
			state.Push(StartState);
			foreach (var inpTree in parseTrees)
				RunNext(inpTree, gatheredTrees, state);
			while (state.Any())
				if (0 != RunNext(new ParseLeaf(new Token<Terminal>(null)), gatheredTrees, state))
					break;
			var finalTrees = gatheredTrees.Pop();
			if (gatheredTrees.Any() || finalTrees.Count != 1)
				throw new NoExpectedToken("Unexpected end of code.");
			return finalTrees.First();
		}
	}

	public class Parser : Parser<HashableList<Regex<Symbol>>>
	{
		private void AddActionTableEntry(NonTerminal nonTerminal, HashableList<Regex<Symbol>> state, Symbol symbol, Action action)
        {
	        if (ActionTable.ContainsKey((nonTerminal, state, symbol)))
	        {
		        if (ActionTable[(nonTerminal, state, symbol)].Equals(action))
			        return;
				throw new Exception("Not LL1");
	        }
			ActionTable[(nonTerminal, state, symbol)] = action;
        }

		public Parser(Grammar grammar)
		{
			ActionTable = new Dictionary<(NonTerminal, HashableList<Regex<Symbol>>, Symbol), Action>();
			var gdfas = GrammarAnalysis.ComputeGdfas(grammar);
			StartState = (CimGrammarCreator.start, gdfas[grammar.Start].Start());
			var nullable = GrammarAnalysis.ComputeNullable<HashableList<Regex<Symbol>>>(gdfas);
			var first = GrammarAnalysis.ComputeFirst<HashableList<Regex<Symbol>>>(gdfas, nullable);
			var follow = GrammarAnalysis.ComputeFollow<HashableList<Regex<Symbol>>>(gdfas, nullable, first);
			var firstPlus = GrammarAnalysis.ComputeFirstPlus<HashableList<Regex<Symbol>>>(nullable, first, follow);

			var statesQueue = new Queue<HashableList<Regex<Symbol>>>();
			foreach (var entry in gdfas)
            {
				var nonTerminal = entry.Key;
				var dfa = entry.Value;
				var visited = new HashSet<HashableList<Regex<Symbol>>>();
				statesQueue.Enqueue(dfa.Start());
				var transitionsTable = dfa.TransitionsTable;
				while (statesQueue.Count > 0)
                {
					var currentState = statesQueue.Dequeue();					
					visited.Add(currentState);

					var terminals = grammar.Terminals;

					foreach (var transition in transitionsTable[currentState])
                    {
						if (!visited.Contains(transition.Value))
							statesQueue.Enqueue(transition.Value);
                    }

					Production prod = dfa.Category(currentState);
					if (prod != null) // current state is accepting
						AddActionTableEntry(nonTerminal, currentState, null, new Reduce(prod));

					foreach (var terminal in terminals)
                    {
						Action action;

						if (prod != null && follow[prod.Lhs].Contains(terminal))
						{
							action = new Reduce(prod);
							AddActionTableEntry(nonTerminal, currentState, terminal, action);
						}

						foreach (var transition in transitionsTable[currentState])
						{
							if (transition.Key.GetType() == typeof(Terminal) && transition.Key == terminal)
							{
								action = new Shift(transition.Value);
								AddActionTableEntry(nonTerminal, currentState, terminal, action);
							}
							if (transition.Key.GetType() == typeof(NonTerminal) && firstPlus[(NonTerminal)transition.Key].Contains(terminal))
							{
								action = new Call(transition.Value, gdfas[(NonTerminal) transition.Key].Start(), (NonTerminal)transition.Key);
								AddActionTableEntry(nonTerminal, currentState, terminal, action);
							}
						}
                    }
                }
            }
		}
	}
}
