using System;
using System.Collections.Generic;

namespace Cimplified.Parser
{

	public abstract class RecursiveASTNodeVisitor<TParameter> : ASTNodeVisitor<TParameter>
	{
		public static void VisitChildren(ASTNodeVisitor<TParameter> v, AST n, TParameter p = default)
		{
			foreach (var c in n.Children)
			{
				if (c != null)
					c.Accept(v, p);
			}
		}

		public override void Visit(Program n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(VarUse n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(BinaryOperation n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(UnaryOperation n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(FunctionCall n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(Assignment n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(DeclareAssignment n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(IfClause n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(ElIfClause n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(WhileClause n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(FunctionDeclaration n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(Argument n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(ReturnClause n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(BreakClause n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(Group n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(FlatGroup n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}

		public override void Visit(Block n, TParameter parameter = default)
		{
			VisitChildren(this, n, parameter);
		}
	}
}
