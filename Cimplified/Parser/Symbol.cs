﻿using Cimplified.Lexer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cimplified.Parser
{
    public class Symbol : IEquatable<Symbol>
    {
        public string symbol
        {get;}

        public Symbol()
        {

		}

        public Symbol(string symbol)
        {
            this.symbol = symbol;
		}

		public bool Equals(Symbol other)
		{
			if (other is null)
				return false;
			if (symbol is null)
				return ReferenceEquals(this, other);
			return symbol.Equals(other.symbol);
		}

		public override string ToString()
		{
			return symbol;
		}
    }

    public class Terminal : Symbol
    {
        public Terminal() : base()
        {

		}

                public Terminal(char symbol) : base(new string(new char[] { symbol }))
                {

                }

                public Terminal(string symbol) : base(symbol)
                { 
            
		}
    }

    public class NonTerminal : Symbol
    {
        public NonTerminal() : base()
        {

        }

                public NonTerminal(char symbol) : base(new string(new char[] { symbol }))
                {

                }

                public NonTerminal(string symbol) : base(symbol)
                {

		}
    }

    public class Production : IEquatable<Production>
    {
	public NonTerminal Lhs { get; private set; }

	public Regex<Symbol> Rhs { get; private set; }

	public Production(NonTerminal lhs, Regex<Symbol> rhs)
	{
	    Lhs = lhs;
	    Rhs = rhs;
	}

	public bool Equals(Production other)
	{
		return !(other is null) && Lhs.Equals(other.Lhs) && Rhs.Equals(other.Rhs);
	}

    }
}
