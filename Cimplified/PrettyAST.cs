using System;
using System.Collections.Generic;
using Cimplified.Parser;

namespace Cimplified
{

	// TODO: make output pretty
	public class PrettyNodeVisitor : ASTNodeVisitor<bool>
	{

		public static void Print(AST n, bool omitTemporaries = false)
		{
			n.Accept(new PrettyNodeVisitor(omitTemporaries));
		}

		public readonly bool OmitTemporary;
		public PrettyNodeVisitor(bool omitTemporaries = false)
		{
			OmitTemporary = omitTemporaries;
		}

		public override void Visit(AST n, bool parameter = default)
		{
			var children = n.Children;
			if (children != null) {
				Writer.Write($"({n.GetType().Name}");
				foreach (var c in children) {
					if (c is object) {
						Writer.Write(" ");
						c.Accept(this);
					}
				}
				Writer.Write(")");
			} else {
				Writer.Write(n.GetType().Name);
			}
		}

		public override void Visit(BoolLiteral n, bool parameter = default)
		{
			Writer.Write($"{n.Value}");
		}

		public override void Visit(IntLiteral n, bool parameter = default)
		{
			Writer.Write($"{n.Number}");
		}

		public override void Visit(VarName n, bool parameter = default)
		{
			if (OmitTemporary && n.IsTemporary) {
				Writer.Write($"({n.GetType().Name} $tmp)");
			} else {
				Writer.Write($"({n.GetType().Name} {n.Name})");
			}
		}

		public override void Visit(FunctionName n, bool parameter = default)
		{
			Writer.Write($"({n.GetType().Name} {n.Name})");
		}

		public override void Visit(TypeName n, bool parameter = default)
		{
			Writer.Write($"({n.GetType().Name} {n.Name})");
		}
	}
}
