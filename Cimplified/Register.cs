

using Cimplified.Generator;

namespace Cimplified
{
	public abstract class Register : IInstructionParameter
	{

	}

	public class HwRegister : Register
	{
		public readonly string Name;

		private HwRegister(string name)
		{
			Name = name;
		}

		public static HwRegister RAX = new HwRegister("RAX");
		public static HwRegister RBX = new HwRegister("RBX");
		public static HwRegister RCX = new HwRegister("RCX");
		public static HwRegister RDX = new HwRegister("RDX");
		public static HwRegister RBP = new HwRegister("RBP");
		public static HwRegister RSP = new HwRegister("RSP");
		public static HwRegister RDI = new HwRegister("RDI");
		public static HwRegister RSI = new HwRegister("RSI");
		public static HwRegister R8 = new HwRegister("R8");
		public static HwRegister R9 = new HwRegister("R9");
		public static HwRegister R10 = new HwRegister("R10");
		public static HwRegister R11 = new HwRegister("R11");
		public static HwRegister R12 = new HwRegister("R12");
		public static HwRegister R13 = new HwRegister("R13");
		public static HwRegister R14 = new HwRegister("R14");
		public static HwRegister R15 = new HwRegister("R15");
	}

	public class VirtualRegister : Register
	{
		// Use for comparisons
		public UniqueLabel Id;

		public VirtualRegister()
		{
			Id = new UniqueLabel();
		}
	}
}
