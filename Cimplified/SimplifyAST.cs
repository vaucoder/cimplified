using System;
using System.Collections.Generic;
using System.Linq;
using Cimplified.Parser;

namespace Cimplified
{
	public static class SimplifyAST
	{

		public static AST Simplify(AST ast)
		{
			switch (ast) {
				case Statement x:
					return SimplifyStatement(x);
				case Program x:
					return new Program(x.Declarations.Select(SimplifyFunctionDeclaration).ToList());
				default:
					throw new Exception($"Unexpected type {ast}");
			}
		}

		public static FunctionDeclaration SimplifyFunctionDeclaration(FunctionDeclaration decl)
		{
			// FunctionName references a declaration by a reference.
			// It means that we cannot create a new declaration
			// and therefore have to mutate the referenced one.
			decl.Body = SimplifyBlock(decl.Body);
			return decl;
		}

		public static Statement SimplifyStatement(Statement st)
		{
			switch (st) {
				case Block x: {
						return new Block(x.Statements.Select(SimplifyStatement).ToList());
					}

				case FlatGroup x: {
						return new FlatGroup(x.Statements.Select(SimplifyStatement).ToList());
					}

				case DeclareAssignment x: {
						// VarName references a declaration by a reference.
						// It means that we cannot create a new declaration
						// and therefore have to mutate the referenced one.
						var (sideEffects, expr) = ExtractSideEffects.Extract1(x.Value);
						x.Value = expr;
						var plain = ExtractShortCircuits.Extract1(x);
						sideEffects.Add(plain);
						return new FlatGroup(sideEffects);
					}

				case Assignment x: {
						var (sideEffects, expr) = ExtractSideEffects.Extract1(x.Value);
						var newAss = new Assignment(x.VarName, expr);
						var plain = ExtractShortCircuits.Extract1(newAss);
						sideEffects.Add(plain);
						return new FlatGroup(sideEffects);
					}

				case Expression x: {
						var (sideEffects, expr) = ExtractSideEffects.Extract1(x);
						var plain = ExtractShortCircuits.Extract1(expr);
						sideEffects.Add(plain);
						return new FlatGroup(sideEffects);
					}

				case ReturnClause x: {
						if (x.ReturnExpression is null) {
							return x;
						}

						var (sideEffects, expr) = ExtractSideEffects.Extract1(x.ReturnExpression);
						var newAss = new ReturnClause(expr);
						var plain = ExtractShortCircuits.Extract1(newAss);
						sideEffects.Add(plain);
						return new FlatGroup(sideEffects);
					}

				case IfClause x: {
						var (sideEffects, expr) = ExtractSideEffects.Extract1(x.Condition);
						var simpleBody = SimplifyBlock(x.Body);
						var simpleElseBody = SimplifyElifs(x.ElIfs, x.ElseBody);
						var newIf = new IfClause(expr, simpleBody, null, simpleElseBody);
						sideEffects = sideEffects.Select(ExtractShortCircuits.Extract1).Select(SimplifyStatement).ToList();
						sideEffects.Add(newIf);
						return new FlatGroup(sideEffects);
					}

				case WhileClause x: {
						var (sideEffects, expr) = ExtractSideEffects.Extract1(x.Condition);
						var simpleProlog = new FlatGroup(x.Prolog.Statements.Select(SimplifyStatement).ToList());
						var simpleBody = SimplifyBlock(x.Body);
						sideEffects.Add(simpleProlog);
						sideEffects = sideEffects.Select(ExtractShortCircuits.Extract1).Select(SimplifyStatement).ToList();
						var newProlog = new FlatGroup(sideEffects);
						return new WhileClause(newProlog, expr, simpleBody);
					}

				case FunctionDeclaration x:
					return SimplifyFunctionDeclaration(x);

				case BreakClause x:
					return x;

				case ContinueClause x:
					return x;

				default:
					throw new Exception($"Not expected statement of type {Utils.Dump(st)}");
			}

		}

		public static Block SimplifyBlock(Statement st)
		{
			return new Block(new List<Statement>() { SimplifyStatement(st) });
		}

		public static Block SimplifyElifs(IList<ElIfClause> elifs, Block elseBlock)
		{
			if (elifs == null || elifs.Count == 0) {
				return elseBlock;
			}

			var head = elifs[0];
			var tail = elifs.Skip(1).ToList();
			var newIf = new IfClause(head.Condition, head.Body, tail, elseBlock);
			return SimplifyBlock(newIf);
		}

	}
}
