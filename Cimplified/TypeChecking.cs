using System;
using Cimplified.Parser;
using System.Linq;

namespace Cimplified
{
    using Type = Parser.Type;
    public static class TypeChecking
    {
        public static void Process(AST mutableAST)
        {
            var endOfBlockChecker = new ReturnBreakContinueEndOfBlockNodeVisitor();
            var allPathReturns = new ReturnInFunctionNodeVisitor();
            var depthChecker = new BreakContinueDepthNodeVisitor();
            var typeChecker = new TypeNodeVisitor();
            endOfBlockChecker.Visit((Program)mutableAST);
            allPathReturns.Visit((Program)mutableAST);
            depthChecker.Visit((Program)mutableAST);
            typeChecker.Visit((Program)mutableAST);
        }

        public static Type GetExpressionType(Expression expr)
        {
            switch (expr) {
                case And n: return Type.Bool;
                case Or n: return Type.Bool;
                case Negation n: return Type.Bool;
                case Plus n: return Type.Int;
                case Sub n: return Type.Int;
                case Div n: return Type.Int;
                case Mult n: return Type.Int;
                case Eq n: return Type.Bool;
                case Leq n: return Type.Bool;
                case BoolLiteral n: return Type.Bool;
                case IntLiteral n: return Type.Int;
                case VarUse n: {
                        var decl = n.Name.Declaration;
                        if (decl is object && decl.TypeName is TypeName tn)
                            return tn.Type;
                        else
                            return Type.Unknown;
                    }
                case FunctionCall n: {
                        var decl = n.FunctionName.Declaration;
                        if (decl is object && decl.TypeName is TypeName tn)
                            return tn.Type;
                        else
                            return Type.Unknown;
                    }
                default: throw new Exception($"Unexpected type {expr}");
            }
        }

    }

    public abstract class ASTDiagnostic : Diagnostic
    {
        public AST Context;
    }

    public class TypeNodeVisitor : RecursiveASTNodeVisitor<Type>
    {
        public class TypeMismatchError : ASTDiagnostic
        {
            public Type Expected;
            public Type Actual;
            public string Info = "Type mismatch";
            public override string ToString()
            {
                return $"{Info}. Expected {Expected}, got {Actual}.";
            }
        }

        public class ArgumentNumberError : ASTDiagnostic
        {
            public FunctionDeclaration Declaration;
            public FunctionCall FunctionCall;
            public Type Actual;
            public override string ToString()
            {
                return $"Function '{Declaration.FunName.Name}' takes {Declaration.ArgumentList.Count} arguments "
                + $"({FunctionCall.Arguments.Count} was given).";
            }
        }

        public override void Visit(AST n, Type parameter = default)
        {
            return;
        }

        public override void Visit(DeclareAssignment n, Type parameter = default)
        {
            base.Visit(n);

            if (n.TypeName == null) {
                if (n.Value.Type == Type.Unknown) {
                    throw new Exception("Value expected to have type");
                }

                n.TypeName = new TypeName(n.Value.Type.ToString());
                return;
            }

            if (n.TypeName.Type != n.Value.Type)
                throw new TypeMismatchError
                {
                    Context = n,
                    Expected = n.TypeName.Type,
                    Actual = n.Value.Type
                };
        }

        public override void Visit(Assignment n, Type parameter = default)
        {
            base.Visit(n);
            var declareAssigment = (DeclareAssignment)n.VarName.Declaration;
            if (declareAssigment.TypeName.Type != n.Value.Type)
                throw new TypeMismatchError
                {
                    Context = n,
                    Expected = declareAssigment.TypeName.Type,
                    Actual = n.Value.Type
                };
        }

        void binaryOperatorVisit(BinaryOperation op, Type opType)
        {
            base.Visit(op);
            if (op.Left.Type != opType)
                throw new TypeMismatchError
                {
                    Context = op,
                    Expected = opType,
                    Actual = op.Left.Type
                };
            if (op.Right.Type != opType)
                throw new TypeMismatchError
                {
                    Context = op,
                    Expected = opType,
                    Actual = op.Left.Type
                };
            op.Type = opType;
        }

        void unaryOperatorVisit(UnaryOperation op, Type opType)
        {
            base.Visit(op);
            if (op.Operand.Type != opType)
                throw new TypeMismatchError
                {
                    Context = op,
                    Expected = opType,
                    Actual = op.Operand.Type
                };
            op.Type = opType;
        }

        public override void Visit(Eq n, Type parameter = default)
        {
            binaryOperatorVisit(n, Type.Int);
            n.Type = Type.Bool;
        }
        public override void Visit(Leq n, Type parameter = default)
        {
            binaryOperatorVisit(n, Type.Int);
            n.Type = Type.Bool;
        }

        public override void Visit(BoolLiteral n, Type parameter = default)
        {
            n.Type = Type.Bool;
        }

        public override void Visit(IntLiteral n, Type parameter = default)
        {
            n.Type = Type.Int;
        }

        public override void Visit(VarUse n, Type parameter = default)
        {
            n.Type = n.Name.Declaration.TypeName.Type;
        }

        public override void Visit(FunctionCall n, Type parameter = default)
        {
            base.Visit(n);
            var declaration = (FunctionDeclaration)n.FunctionName.Declaration;
            if (declaration.ArgumentList.Count != n.Arguments.Count)
                throw new ArgumentNumberError
                {
                    Context = n,
                    Declaration = declaration,
                    FunctionCall = n
                };
            for (int i = 0; i < declaration.ArgumentList.Count; i++)
                if (declaration.ArgumentList[i].TypeName.Type != n.Arguments[i].Type)
                    throw new TypeMismatchError
                    {
                        Context = n,
                        Expected = declaration.ArgumentList[i].TypeName.Type,
                        Actual = n.Arguments[i].Type,
                        Info = $"Type mismatch for the argument '{declaration.ArgumentList[i].VarName.Name}' of the '{declaration.FunName.Name}' function"
                    };
            n.Type = declaration.TypeName.Type;
        }

        public override void Visit(BinaryOperation n, Type parameter = default)
        {
            binaryOperatorVisit(n, TypeChecking.GetExpressionType(n));
        }

        public override void Visit(UnaryOperation n, Type parameter = default)
        {
            unaryOperatorVisit(n, TypeChecking.GetExpressionType(n));
        }

        public override void Visit(FunctionDeclaration n, Type parameter = default)
        {
            base.Visit(n, n.TypeName.Type);
        }

        public override void Visit(IfClause n, Type parameter = default)
        {
            base.Visit(n, parameter);
            if (n.Condition.Type != Type.Bool)
                throw new TypeMismatchError
                {
                    Context = n,
                    Expected = Type.Bool,
                    Actual = n.Condition.Type
                };
        }

        public override void Visit(ElIfClause n, Type parameter = default)
        {
            base.Visit(n, parameter);
            if (n.Condition.Type != Type.Bool)
                throw new TypeMismatchError
                {
                    Context = n,
                    Expected = Type.Bool,
                    Actual = n.Condition.Type
                };
        }

        public override void Visit(WhileClause n, Type parameter = default)
        {
            base.Visit(n, parameter);
            if (n.Condition.Type != Type.Bool)
                throw new TypeMismatchError
                {
                    Context = n,
                    Expected = Type.Bool,
                    Actual = n.Condition.Type
                };
        }

        public override void Visit(ReturnClause n, Type parameter = default)
        {
            base.Visit(n);
            if (n.ReturnExpression is null && parameter != Type.Void)
                throw new TypeMismatchError
                {
                    Context = n,
                    Expected = parameter,
                    Actual = Type.Void
                };
            if (n.ReturnExpression is object && n.ReturnExpression.Type != parameter)
                throw new TypeMismatchError
                {
                    Context = n,
                    Expected = parameter,
                    Actual = n.ReturnExpression.Type
                };
        }

    }

    public class BreakContinueDepthNodeVisitor : RecursiveASTNodeVisitor<int>
    {
        public class BreakNotEnoughNestedError : ASTDiagnostic
        {
            public int realDepth;
            public int breakDepth;

            public override string ToString()
            {
                return $"Break-clause is nested in while-clauses with depth={realDepth}. Breaking {breakDepth} nested while-clauses is not possible.";
            }
        }

        public class ClauseNotNestedError : ASTDiagnostic
        {
            public override string ToString()
            {
                return "Break/continue clauses must be nested in at least one while-clause.";
            }
        }

        public override void Visit(AST n, int parameter = 0)
        {
            return;
        }

        public override void Visit(FunctionDeclaration n, int parameter = 0)
        {
            base.Visit(n, 0);
        }

        public override void Visit(WhileClause n, int parameter = 0)
        {
            base.Visit(n, parameter + 1);
        }

        public override void Visit(BreakClause n, int parameter = 0)
        {
            if (parameter == 0)
                throw new ClauseNotNestedError { Context = n };
            int breakDepth = n.N is null ? 1 : Int32.Parse(n.N.Number);
            if (breakDepth > parameter)
                throw new BreakNotEnoughNestedError
                {
                    Context = n,
                    breakDepth = breakDepth,
                    realDepth = parameter
                };
        }

        public override void Visit(ContinueClause n, int parameter = 0)
        {
            if (parameter == 0)
                throw new ClauseNotNestedError { Context = n };
        }
    }
    public class ReturnInFunctionNodeVisitor : RecursiveASTNodeVisitor<int>
    {
        private bool _returns = false;
        public class NotAllPathReturnError : ASTDiagnostic
        {
            public FunctionDeclaration FunctionDeclaration;
            public override string ToString()
            {
                return $"Not all paths in the `{FunctionDeclaration.FunName.Name}` return a value.";
            }
        }
        public override void Visit(AST n, int parameter = 0)
        {
            return;
        }

        public override void Visit(FunctionDeclaration n, int parameter = 0)
        {
            base.Visit(n);
            if (n.TypeName.Type == Type.Void)
                return;
            var statements = n.Body.Statements.ToList();
            statements.RemoveAll(x => x is null);
            if (statements.Count == 0)
                throw new NotAllPathReturnError
                {
                    Context = n,
                    FunctionDeclaration = n
                };
            _returns = false;
            statements.Last().Accept(this);
            if (!_returns)
                throw new NotAllPathReturnError
                {
                    Context = n,
                    FunctionDeclaration = n
                };
            _returns = false;
        }

        public override void Visit(ReturnClause n, int parameter = 0)
        {
            _returns = true;
        }
        public override void Visit(WhileClause n, int parameter = 0)
        {
            if (n.Body.Statements.Count > 0)
                n.Body.Statements.Last().Accept(this);
        }

        public override void Visit(IfClause n, int parameter = 0)
        {
            if (n.ElseBody is null)
                return;
            foreach (var c in n.Children.Skip(1))
            {
                _returns = false;
                c.Accept(this);
                if (!_returns)
                    return;
            }
        }

    }

    public class ReturnBreakContinueEndOfBlockNodeVisitor : RecursiveASTNodeVisitor<bool>
    {
        public class ClauseNotInTheEndOfBlockError : ASTDiagnostic
        {
            public override string ToString()
            {
                return "Return/break/continue clauses must be in the end of the block.";
            }
        }

        public override void Visit(AST n, bool parameter = false)
        {
            return;
        }

        public void VisitGroup(Group n)
        {
            var children = n.Children.ToList();
            children.RemoveAll(x => x is null);
            if (children.Count == 0)
                return;
            children.SkipLast(1).ToList().ForEach(x => x.Accept(this, false));
            children.Last().Accept(this, true);
        }

        public override void Visit(FlatGroup n, bool parameter = false)
        {
            VisitGroup(n);
        }

        public override void Visit(Block n, bool parameter = false)
        {
            VisitGroup(n);
        }

        public override void Visit(BreakClause n, bool parameter)
        {
            if (!parameter)
                throw new ClauseNotInTheEndOfBlockError { Context = n };
        }

        public override void Visit(ReturnClause n, bool parameter)
        {
            if (!parameter)
                throw new ClauseNotInTheEndOfBlockError { Context = n };
        }

        public override void Visit(ContinueClause n, bool parameter)
        {
            if (!parameter)
                throw new ClauseNotInTheEndOfBlockError { Context = n };
        }

    }

}
