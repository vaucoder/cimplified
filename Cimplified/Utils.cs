using Cimplified.Lexer;
using Cimplified.Parser;
using Cimplified.Generator;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace Cimplified
{
	public static class Utils
	{

		public static int IndexOf<T>(this IEnumerable<T> source, T value)
		{
			int index = 0;
			var comparer = EqualityComparer<T>.Default;
			foreach (T item in source)
			{
				if (comparer.Equals(item, value))
				{
					return index;
				}
				index++;
			}
			return -1;
		}

		// Converts object to string recursively
		// Use for debugging
		public static string Dump(Object o_0)
		{
			var H = new HashSet<object>();

			string Loop(Object o) {
				if (o is null)
				{
					return "null";
				}

				if (H.Contains(o))
				{
					return o.ToString();
				} else
				{
					H.Add(o);
				}

				System.Type type = o.GetType();
				if (type.IsValueType)
				{
					return o.ToString();
				}

				var s = "";
				s += type.Name;

				if (o is System.Collections.IEnumerable ie) {
					s += " [";
					foreach (var e in ie) {
						s += " ";
						s += Loop(e);
					}
					s += " ]";
				}

				s += " { ";

				var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
				foreach (var f in fields) {
					s += f.Name;
					s += " = ";
					s += Loop(f.GetValue(o));
					s += " ";
				}

				s += "}";
				return s;
			}

			return Loop(o_0);
		}

		private class DisjointSet<T> where T : IEquatable<T>
		{
			private readonly IDictionary<T, T> _parent;
			private readonly IDictionary<T, int> _rank;

			public DisjointSet()
			{
				_parent = new Dictionary<T, T>();
				_rank = new Dictionary<T, int>();
			}

			public T Find(T node)
			{
				if(_parent[node].Equals(node))
					return _parent[node];

				_parent[node] = Find(_parent[node]);
				return _parent[node];
			}

			public void Union(T x, T y)
			{
				T xRoot = Find(x);
				T yRoot = Find(y);

				if (_rank[xRoot] < _rank[yRoot])
					_parent[xRoot] = yRoot;
				else if (_rank[xRoot] > _rank[yRoot])
					_parent[yRoot] = xRoot;
				else
				{
					_parent[xRoot] = yRoot;
					_rank[yRoot]++;
				}
			}

			public void Make(T r)
			{

				_parent[r] = r;
				_rank[r] = 0;
			}
		}


		private static (int, TStateA, TStateB) CreateTupleFirst<TStateA, TStateB>(TStateA p)
		{
			return (0, p, default(TStateB));
		}

		private static (int, TStateA, TStateB) CreateTupleSecond<TStateA, TStateB>(TStateB q)
		{
			return (1, default(TStateA), q);
		}

		private static TCat GetCategory<TSymbol, TStateA, TStateB, TCat>(Dfa<TSymbol, TCat, TStateA> first, Dfa<TSymbol, TCat, TStateB> second, (int, TStateA, TStateB) combinedState)
			where TSymbol : IEquatable<TSymbol>
			where TStateA : IEquatable<TStateA>
			where TStateB : IEquatable<TStateB>
		{
			var (v, p, q) = combinedState;
			if (v == 0)
				return first.Category(p);
			else
				return second.Category(q);
		}

		private static ISet<TSymbol> GetAlphabet<TSymbol, TStateA, TStateB, TCat>(Dfa<TSymbol, TCat, TStateA> first, Dfa<TSymbol, TCat, TStateB> second, (int, TStateA, TStateB) combinedState)
			where TSymbol : IEquatable<TSymbol>
			where TStateA : IEquatable<TStateA>
			where TStateB : IEquatable<TStateB>
		{
			var (v, p, q) = combinedState;

			ISet<TSymbol> alphabet;

			if (v == 0)
				alphabet = new HashSet<TSymbol>(first.Transitions(p).Keys);
			else
				alphabet = new HashSet<TSymbol>(second.Transitions(q).Keys);

			return alphabet;

		}

		private static IDictionary<TSymbol, (int, TStateA, TStateB)> GetTransitions<TSymbol, TStateA, TStateB, TCat>
			(Dfa<TSymbol, TCat, TStateA> first, Dfa<TSymbol, TCat, TStateB> second, (int, TStateA, TStateB) state, ISet<TSymbol> alphabet)
			where TSymbol : IEquatable<TSymbol>
			where TStateA : IEquatable<TStateA>
			where TStateB : IEquatable<TStateB>
		{
			var (v, p, q) = state;
			var transitions = new Dictionary<TSymbol, (int, TStateA, TStateB)>();

			if (v == 0)
			{
				var pTransitions = first.Transitions(p);

				foreach (var symbol in alphabet)
				{
					if (pTransitions.ContainsKey(symbol))
						transitions[symbol] = CreateTupleFirst<TStateA, TStateB>(pTransitions[symbol]);
					else
						transitions[symbol] = CreateTupleFirst<TStateA, TStateB>(first.Dead());
				}
			}
			else
			{
				var qTransitions = second.Transitions(q);

				foreach (var symbol in alphabet)
				{
					if (qTransitions.ContainsKey(symbol))
						transitions[symbol] = CreateTupleSecond<TStateA, TStateB>(qTransitions[symbol]);
					else
						transitions[symbol] = CreateTupleSecond<TStateA, TStateB>(second.Dead());
				}
			}

			return transitions;
		}


		public static bool Equivalent<TSymbol, TCat, TStateA, TStateB>(Dfa<TSymbol, TCat, TStateA> first, Dfa<TSymbol, TCat, TStateB> second)
			where TSymbol : IEquatable<TSymbol>
			where TStateA : IEquatable<TStateA>
			where TStateB : IEquatable<TStateB>
		{
			var stateQueue = new Stack<((int, TStateA, TStateB), (int, TStateA, TStateB))>();
			var disjointSet = new DisjointSet<(int, TStateA, TStateB)>();

			var firstHasDeadState = false;
			var secondHasDeadState = false;

			foreach (var stateA in first.TransitionsTable.Keys)
			{
				if (stateA.Equals(first.Dead()))
					firstHasDeadState = true;

				disjointSet.Make(CreateTupleFirst<TStateA, TStateB>(stateA));
			}

			foreach (var stateB in second.TransitionsTable.Keys)
			{
				if (stateB.Equals(second.Dead()))
					secondHasDeadState = true;

				disjointSet.Make(CreateTupleSecond<TStateA, TStateB>(stateB));
			}

			if (!firstHasDeadState)
				disjointSet.Make(CreateTupleFirst<TStateA, TStateB>(first.Dead()));

			if (!secondHasDeadState)
				disjointSet.Make(CreateTupleSecond<TStateA, TStateB>(second.Dead()));

			// todo if dfa will include States() property, we can remove deadState

			var p0 = CreateTupleFirst<TStateA, TStateB>(first.Start());
			var q0 = CreateTupleSecond<TStateA, TStateB>(second.Start());

			disjointSet.Union(p0, q0);

			stateQueue.Push((p0, q0));

			while (stateQueue.Count > 0)
			{
				var (p, q) = stateQueue.Pop();

				var alphabet = GetAlphabet(first, second, p);
				alphabet.UnionWith(GetAlphabet(first, second, q));

				var pTranstitions = GetTransitions(first, second, p, alphabet);
				var qTransititons = GetTransitions(first, second, q, alphabet);

				foreach (var symbol in alphabet)
				{
					var pNext = disjointSet.Find(pTranstitions[symbol]);
					var qNext = disjointSet.Find(qTransititons[symbol]);

					if (!pNext.Equals(qNext))
					{
						disjointSet.Union(pNext, qNext);
						stateQueue.Push((pNext, qNext));
					}
				}
			}

			foreach (var stateA in first.TransitionsTable.Keys)
			{
				var category = first.Category(stateA);
				var setCategory = GetCategory(first, second, disjointSet.Find(CreateTupleFirst<TStateA, TStateB>(stateA)));

				if (category == null && setCategory == null)
					continue;
				if (category == null || setCategory == null || !category.Equals(setCategory))
					return false;
			}

			foreach (var stateB in second.TransitionsTable.Keys)
			{
				var category = second.Category(stateB);
				var setCategory = GetCategory(first, second, disjointSet.Find(CreateTupleSecond<TStateA, TStateB>(stateB)));

				if (category == null && setCategory == null)
					continue;
				if (category == null || setCategory == null || !category.Equals(setCategory))
					return false;
			}

			return true;
		}

		public static Dfa<TSymbol, bool, Regex<TSymbol>> RegexToDfa<TSymbol>(Regex<TSymbol> regex)
			where TSymbol : IEquatable<TSymbol>
		{
			var startState = regex.Normalize();
			return new RegexDfa<TSymbol>(startState);
		}

		// Size of each element on the stack
		public static int ElementSize = 8;

		// Tests equality by going through each field recursively
		// May diverge
		public static bool DeepEquals(object a, object b)
		{
			if (a is null)
			{
				return b is null;
			}

			System.Type type = a.GetType();

			if (b.GetType() != type)
			{
				return false;
			}

			if (a is System.Collections.DictionaryEntry dea) {
				var deb = (System.Collections.DictionaryEntry)b;
				return DeepEquals(dea.Key, deb.Key) && DeepEquals(dea.Value, deb.Value);
			}

			if (type.IsValueType)
			{
				return (dynamic)a == (dynamic)b;
			}

			if (a is System.Collections.IDictionary ia) {
				var ib = (System.Collections.IDictionary)b;
				int count = 0;
				foreach (var av in ia) {
					count++;
					bool anyEquals = false;
					foreach (var bv in ib) {
						anyEquals = anyEquals || DeepEquals(av, bv);
					}
					if (!anyEquals)
						return false;
				}

				foreach (var v in ib) count--;
				return count == 0;
			} else if (a is System.Collections.IEnumerable ia2) {
				var ib = (System.Collections.IEnumerable)b;

				var alst = new List<object>();
				foreach (var av in ia2)
					alst.Add(av);

				int count = 0;
				foreach (var bv in ib) {
					if (count > alst.Count || !DeepEquals(bv, alst[count])) {
						return false;
					}
					count++;
				}

				return true;
			}

			var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
			foreach (var f in fields) {
				if (f.FieldType.IsValueType)
				{
					if ((dynamic)f.GetValue(a) != (dynamic)f.GetValue(b))
					{
						return false;
					}
				} else
				{
					if (!DeepEquals(f.GetValue(a), f.GetValue(b)))
					{
						return false;
					}
				}
			}

			return true;
		}

		public static ControlOperation WriteToMemory(ValueOperation destination, ValueOperation val)
		{
			switch (destination) {
				case RegisterAccess ra:
					return new RegisterWrite(ra.Register, val);
				case MemoryAccess ma:
					return new MemoryWrite(ma.Address, val);
				case null:
					return new Noop();
				default:
					throw new Exception($"Unexpected type {Utils.Dump(destination)}");
			}
		}

		public static ValueOperation SimpleExpressionToOperation(Function origin, SimpleExpression expr)
		{
			ValueOperation rec(Expression e)
			{
				return SimpleExpressionToOperation(origin, (SimpleExpression)e);
			}

			switch (expr) {
				case BoolLiteral x:
					return new ConstValue(x.Value ? 1 : 0);
				case IntLiteral x:
					return new ConstValue(int.Parse(x.Number));
				case Plus x:
					return new Arithmetic.Plus(rec(x.Left), rec(x.Right));
				case Mult x:
					return new Arithmetic.Mult(rec(x.Left), rec(x.Right));
				case Div x:
					return new Arithmetic.Div(rec(x.Left), rec(x.Right));
				case Sub x:
					return new Arithmetic.Sub(rec(x.Left), rec(x.Right));
				case Eq x:
					return new Arithmetic.Eq(rec(x.Left), rec(x.Right));
				case Leq x:
					return new Arithmetic.Leq(rec(x.Left), rec(x.Right));
				case And x:
					return new Arithmetic.And(rec(x.Left), rec(x.Right));
				case Or x:
					return new Arithmetic.Or(rec(x.Left), rec(x.Right));
				case Negation x:
					return new Arithmetic.Negation(rec(x.Operand));
				case VarUse x:
					return origin.GenerateVariableAccess(x.Name.Declaration.Var);
				default:
					throw new Exception($"Unexpected type {Utils.Dump(expr)}");
			}
		}

		public static List<Operation> FunctionCallToOperation(Function origin, FunctionCall x, ValueOperation result)
		{
			var args = x.Arguments.Select(a => SimpleExpressionToOperation(origin, (SimpleExpression)a)).ToList();
			return x.FunctionName.Declaration.Fun.GenerateCall(args, result, origin);
		}

		public static List<Operation> GeneralExpressionToOperation(Function origin, Expression expr)
		{
			if (expr is FunctionCall fc) {
				return FunctionCallToOperation(origin, fc, null);
			} else if (expr is SimpleExpression se) {
				var r = SimpleExpressionToOperation(origin, se);
				return new List<Operation>() { r };
			} else {
				throw new Exception($"Unexpected type {Utils.Dump(expr)}");
			}
		}

		public static ValueOperation GetReturnAddress()
		{
			return new RegisterAccess(HwRegister.RAX);
		}

		public static void PrintCFG(ControlFlowGraph cfg)
		{
			var H = new Dictionary<Label, int>();

			// returns node name
			string loop(Label cur_label)
			{
				var node = cfg.Map[cur_label];

				int index;
				string name() { return $"a{index}"; }
				if (H.TryGetValue(cur_label, out index)) {
					return name();
				} else {
					index = H.Count;
					H.Add(cur_label, index);
				}

				Writer.Write("\n");
				Writer.Write($"\t{name()} [label = \"");
				PrettyNodeVisitor.Print(node.S);
				Writer.Write($"\"];\n");

				void printEdge(Label l, string label)
				{
					if (l != null) {
						var nextName = loop(l);
						Writer.Write($"\t{name()} -> {nextName}");
						if (label != null) {
							Writer.Write($" [label = \"{label}\"]");
						}
						Writer.Write(";\n");
					}
				}

				if (node is CFGNode.Unconditional u) {
					printEdge(u.Next, null);
				} else if (node is CFGNode.Conditional c) {
					printEdge(c.True, "True");
					printEdge(c.False, "False");
				} else {
					throw new Exception("Unreachable");
				}

				return name();
			}

			Writer.Write("digraph G {\n");
			Writer.Write($"\tnode [shape = box];\n");
			loop(cfg.Start);
			Writer.Write("}\n");
		}

		public static IEnumerable<FunctionDeclaration> GetFunctionDeclarations(AST n)
		{
			switch (n) {
				case FunctionDeclaration x:
					foreach (var f in GetFunctionDeclarations(x.Body)) {
						yield return f;
					}
					yield return x;
					break;
				case Program x:
					foreach (var f in x.Declarations.SelectMany(GetFunctionDeclarations)) {
						yield return f;
					}
					break;
				case Group x:
					foreach (var f in x.Statements.SelectMany(GetFunctionDeclarations)) {
						yield return f;
					}
					break;
				default:
					break;
			}
		}

		// Orders vertexes topologically
		public static IEnumerable<T> GraphOrderVertexes<T>(
			IEnumerable<T> start,
			Func<T, IEnumerable<T>> next
			)
		{
			var ret = new List<T>();

			void loop(T current)
			{
				if (ret.Contains(current))
					return;

				ret.Add(current);

				foreach (var v in next(current)) {
					loop(v);
				}
			}

			foreach (var v in start) {
				loop(v);
			}

			return ret;
		}

		// Returns "pred" if "succ" is provided as "next"
		// Returns "succ" if "pred" is provided as "next"
		public static Func<T, IEnumerable<T>> GraphReverseLink<T>(
			IEnumerable<T> start,
			Func<T, IEnumerable<T>> next
			)
		{
			var H = new Dictionary<T, HashSet<T>>();

			void loop(T current)
			{
				if (H.ContainsKey(current))
					return;

				var s = new HashSet<T>();
				H[current] = s;

				foreach (var v in next(current)) {
					loop(v);
					H[v].Add(current);
				}
			}

			foreach (var v in start) {
				loop(v);
			}

			return (k) => H[k];
		}

		public static Dictionary<AsmInstruction, IEnumerable<AsmInstruction>> InstructionListToGraph(IReadOnlyList<(Label, AsmInstruction)> instructionList)
		{
			var g = new Dictionary<Label, AsmInstruction>();
			foreach (var (k, v) in instructionList) {
				g[k] = v;
			}

			var r = new Dictionary<AsmInstruction, IEnumerable<AsmInstruction>>();
			foreach (var (k, v) in instructionList) {
				r[v] = v.PossibleTargets.Select((t) => g[t]);
			}

			return r;
		}
	}

}
