using System.Collections.Generic;
using System.Collections.Immutable;

namespace Cimplified
{
	public class Variable
	{
		private readonly HashSet<Function> _usedInFunctions = new HashSet<Function>();

		public ImmutableList<Function> UsedInFunctions => _usedInFunctions.ToImmutableList();

		public Function Creator { get; }

		public Register Reg;

		public Variable(Function creator)
		{
			Creator = creator;
			Reg = new VirtualRegister();
			AddUsagePlace(Creator);
		}

		// Append "f" to list of places where .this variable was used
		public void AddUsagePlace(Function function)
		{
			_usedInFunctions.Add(function);
		}
	}
}
