using System.Collections.Generic;
using System.Collections.Immutable;
using Cimplified.Parser;
using System;

namespace Cimplified
{
	public static class VariableResolution
	{
		// Assigns .Var fields for appropriate AST nodes
		// and records all usage places of the assigned Variables
		// and returns list of Functions in which they were declared
		public static IList<Function> Process(AST mutableAST)
		{
			var variableDeclarationVisitor = new VariableDeclarationVisitor();
			variableDeclarationVisitor.Visit((Program) mutableAST, new Stack<Function>());
			var variableUsageVisitor = new VariableUsageVisitor();
			variableUsageVisitor.Visit((Program) mutableAST, new Stack<Function>());
			return variableDeclarationVisitor.Functions;
		}
	}

	public class VariableDeclarationVisitor : RecursiveASTNodeVisitor<Stack<Function>>
	{
		private readonly List<Function> _functions;

		public ImmutableList<Function> Functions => _functions.ToImmutableList();

		public VariableDeclarationVisitor()
		{
			_functions = new List<Function>();
		}

		public override void Visit(AST n, Stack<Function> parameter = default)
		{
			return;
		}

		public override void Visit(FunctionDeclaration n, Stack<Function> parameter)
		{
			var fun = new Function(parameter.Count == 0 ? null : parameter.Peek(), n.FunName.Name);
			n.Fun = fun;
			n.FunName.Fun = fun;
			parameter.Push(fun);
			base.Visit(n, parameter);
			parameter.Pop();
			_functions.Add(fun);
		}

		public override void Visit(DeclareAssignment n, Stack<Function> parameter)
		{
			var currentFunction = parameter.Peek();
			n.VarName.Declaration.Var = currentFunction.CreateVariable();
			base.Visit(n, parameter);
		}

		public override void Visit(Argument n, Stack<Function> parameter)
		{
			var currentFunction = parameter.Peek();
			n.VarName.Declaration.Var = currentFunction.CreateArgument();
			base.Visit(n, parameter);
		}
	}

	public class VariableUsageVisitor : RecursiveASTNodeVisitor<Stack<Function>>
	{
		public override void Visit(AST n, Stack<Function> parameter = default)
		{
			return;
		}

		public override void Visit(FunctionDeclaration n, Stack<Function> parameter)
		{
			parameter.Push(n.Fun);
			base.Visit(n, parameter);
			parameter.Pop();
		}

		public override void Visit(VarUse n, Stack<Function> parameter)
		{
			var fun = parameter.Peek();
			n.Name.Declaration.Var.AddUsagePlace(fun);
			base.Visit(n, parameter);
		}

		public override void Visit(Assignment n, Stack<Function> parameter)
		{
			var fun = parameter.Peek();
			n.VarName.Declaration.Var.AddUsagePlace(fun);
			base.Visit(n, parameter);
		}
	}
}
