using System;
using System.Collections.Generic;
using Cimplified.Parser;
using System.Threading;

namespace Cimplified
{

	public class Writer : IDisposable
	{
		private static readonly AsyncLocal<Action<string>> _write = initWriter();

		static readonly Action<string> defaultValue = Console.Write;

		static AsyncLocal<Action<string>> initWriter()
		{
			var ret = new AsyncLocal<Action<string>>();
			ret.Value = defaultValue;
			return ret;
		}

		public static void Write(string str)
		{
			// It seems like this is some C# bug:
			// _write.Value should never be null,
			// but it sometimes is - depending on
			// if C# is feeling racy
			if (_write.Value is null) {
				_write.Value = defaultValue;
			}

			_write.Value(str);
		}

		readonly Action<string> _saved;
		public Writer(Action<string> fn)
		{
			_saved = _write.Value;
			_write.Value = fn;
		}

		public void Dispose()
		{
			_write.Value = _saved;
		}
	}
}

