# Cimplified

Compiler for C-like language.

Example:

```C
Int main() {
	Int x = readInt();
	Int y = readInt();
	writeInt(x + y);
	return 0;
}
```
