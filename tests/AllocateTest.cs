﻿using Cimplified;
using Cimplified.Generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace tests
{
	public class AllocateTest
	{
		private HwRegister r8 = HwRegister.R8;
		private HwRegister r9 = HwRegister.R9;
		private HwRegister r10 = HwRegister.R10;
		private HwRegister r11 = HwRegister.R11;
		private HwRegister r12 = HwRegister.R12;
		private HwRegister r13 = HwRegister.R13;
		private HwRegister r14 = HwRegister.R14;
		private HwRegister r15 = HwRegister.R15;

		private VirtualRegister vr1 = new VirtualRegister();
		private VirtualRegister vr2 = new VirtualRegister();
		private VirtualRegister vr3 = new VirtualRegister();
		private VirtualRegister vr4 = new VirtualRegister();
		private VirtualRegister vr5 = new VirtualRegister();
		private VirtualRegister vr6 = new VirtualRegister();
		private VirtualRegister vr7 = new VirtualRegister();
		private VirtualRegister vr8 = new VirtualRegister();
		private VirtualRegister vr9 = new VirtualRegister();
		private VirtualRegister vr10 = new VirtualRegister();
		private VirtualRegister vr11 = new VirtualRegister();
		private VirtualRegister vr12 = new VirtualRegister();
		private VirtualRegister vr13 = new VirtualRegister();
		private VirtualRegister vr14 = new VirtualRegister();
		private VirtualRegister vr15 = new VirtualRegister();
		private VirtualRegister vr16 = new VirtualRegister();

		/*
		 * Check whether each register is in spilled or a key in allocated, but not in both;
		 * check whether all hardware registers from toAllocate are in allocated.Keys
		 */
		private bool checkHard1(Dictionary<Register, HwRegister> allocated, List<Register> spilled, IEnumerable<Register> toAllocate)
		{
			foreach (var reg in toAllocate)
			{
				if (!allocated.ContainsKey(reg) && !spilled.Contains(reg))
					return false;
				if (allocated.ContainsKey(reg) && spilled.Contains(reg))
					return false;
				if (reg is HwRegister && !allocated.ContainsKey(reg))
					return false;
			}
			return true;
		}

		/*
		 * Check whether for each hardware register hw: allocated[hw] == hw
		 */
		private bool checkHard2(Dictionary<Register, HwRegister> allocated)
		{
			foreach (var reg in allocated.Keys)
			{
				if (reg is HwRegister)
				{
					if (allocated[reg] != reg)
						return false;
				}
			}
			return true;
		}

		/*
		 * Check whether allocated.Values contains only these hw registers present in toAllocate
		 */
		private bool checkHard3(Dictionary<Register, HwRegister> allocated, IEnumerable<Register> toAllocate)
		{
			foreach (var reg in allocated.Values)
			{
				if (!toAllocate.Contains(reg))
					return false;
			}
			return true;
		}

		/*
		 * Check whether for each conflict edge e = (a, b): allocated[a] != allocated[b]
		 */
		private bool checkHard4(Dictionary<Register, HwRegister> allocated, IEnumerable<(Register, Register)> conflicts)
		{
			foreach (var edge in conflicts)
			{
				if (allocated.ContainsKey(edge.Item1) && allocated.ContainsKey(edge.Item2) &&  allocated[edge.Item1] == allocated[edge.Item2])
					return false;
			}
			return true;
		}

		private void checkHardConditions(Dictionary<Register, HwRegister> allocated, List<Register> spilled, IEnumerable<Register> toAllocate, IEnumerable<(Register, Register)> conflicts, IEnumerable<(Register, Register)> copies)
		{
			Assert.True(checkHard1(allocated, spilled, toAllocate));
			Assert.True(checkHard2(allocated));
			Assert.True(checkHard3(allocated, toAllocate));
			Assert.True(checkHard4(allocated, conflicts));
		}

		/*
		 * One vertex with deg equal to colors number
		 */
		[Fact]
		public void Test1()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8,
													r8, r9, r10, r11, r12 };
			var conflicts = new List<(Register, Register)>()
				{ (vr1, vr2), (vr1, vr3), (vr2, vr3), (vr3, vr4), (vr3, vr5), (vr4, vr5), (vr5, vr6), (vr5, vr7), (vr5, vr8),
				(vr2, vr1), (vr3, vr1), (vr3, vr2), (vr4, vr3), (vr5, vr3), (vr5, vr4), (vr6, vr5), (vr7, vr5), (vr8, vr5)};
			var copies = new List<(Register, Register)>();
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		/*
		 * Odd length cycle with 2 colors available
		 */
		[Fact]
		public void Test2()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5,
													r8, r9 };
			var conflicts = new List<(Register, Register)>()
				{ (vr1, vr2), (vr2, vr3), (vr3, vr4), (vr4, vr5), (vr5, vr1),
				(vr2, vr1), (vr3, vr2), (vr4, vr3), (vr5, vr4), (vr1, vr5)};
			var copies = new List<(Register, Register)>();
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		/*
		 * Copy edge between 1 and 2 - we want to merge them
		 */
		[Fact]
		public void Test3()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4,
													r8, r9 };
			var conflicts = new List<(Register, Register)>()
				{ (vr1, vr3), (vr2, vr3), (vr2, vr4), (vr4, vr1),
				(vr3, vr1), (vr3, vr2), (vr4, vr2), (vr1, vr4)};
			var copies = new List<(Register, Register)>() { (vr1, vr2),
															(vr2, vr1)};
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
			// soft condition
			Assert.Empty(spilled);
		}

		/*
		 * Several registers linked with copy edge; 1 should be joined with 2 and 3 with 4
		 */
		[Fact]
		public void Test4()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4,
													r8, r9 };
			var conflicts = new List<(Register, Register)>()
				{ (vr1, vr3),
				(vr3, vr1)};
			var copies = new List<(Register, Register)>() { (vr1, vr2), (vr3, vr4),
															(vr2, vr1), (vr4, vr3)};
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);

			// soft
			Assert.Empty(spilled);
			Assert.Equal(allocated[vr1], allocated[vr2]);
			Assert.Equal(allocated[vr3], allocated[vr4]);
		}

		/*
		 * Only one hw register, but 3 virtual ones linked with copy edges
		 */
		[Fact]
		public void Test5()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3,
													r8 };
			var conflicts = new List<(Register, Register)>();
			var copies = new List<(Register, Register)>() { (vr1, vr3), (vr2, vr3),
															(vr3, vr1), (vr3, vr2)};
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);

			// soft
			Assert.Empty(spilled);
		}

		/*
		 * Copy and conflict edge between 1 and 2 - they have to get different colors
		 */
		[Fact]
		public void Test6()
		{
			var toAllocate = new List<Register>() { vr1, vr2,
													r8, r9 };
			var conflicts = new List<(Register, Register)>()
				{ (vr1, vr2),
				(vr2, vr1) };
			var copies = new List<(Register, Register)>() { (vr1, vr2),
															(vr2, vr1) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		/*
		 * Conflicts between 2 available hw registers and vr1 - vr1 has to be in spilled
		 */
		[Fact]
		public void Test7()
		{
			var toAllocate = new List<Register>() { vr1,
													r8, r9 };
			var conflicts = new List<(Register, Register)>()
				{ (vr1, r8), (vr1, r9),
				(r8, vr1), (r9, vr1) };
			var copies = new List<(Register, Register)>();
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		/*
		 * Several virtual registers not connected with each other, 1 available color
		 */
		[Fact]
		public void Test8()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4,
													r8 };
			var conflicts = new List<(Register, Register)>();
			var copies = new List<(Register, Register)>();
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);

			// soft
			Assert.Empty(spilled);
		}

		/*
		 * Several virtual registers, two of them should be joined
		 */
		[Fact]
		public void Test9()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4,
													r8, r9 };
			var conflicts = new List<(Register, Register)>() { (vr1, vr3), (vr1, vr4), (vr2, vr3), (vr2, vr4),
																(vr3, vr1), (vr4, vr1), (vr3, vr2), (vr4, vr2)};
			var copies = new List<(Register, Register)>() { (vr1, vr2),
															(vr2, vr1) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);

			// soft
			Assert.Empty(spilled);
			Assert.Equal(allocated[vr1], allocated[vr2]);
		}

		/*
		 * 2 available colors, 2 vertices joined with copy edge, but they cannot be joined together
		 */
		[Fact]
		public void Test10()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8,
													r8, r9 };
			var conflicts = new List<(Register, Register)>() { (vr1, vr5), (vr2, vr3), (vr2, vr4), (vr3, vr5), (vr3, vr6), (vr4, vr7), (vr4, vr8),
																(vr5, vr1), (vr3, vr2), (vr4, vr2), (vr5, vr3), (vr6, vr3), (vr7, vr4), (vr8, vr4)};
			var copies = new List<(Register, Register)>() { (vr1, vr2),
															(vr2, vr1) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);

			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);

			// soft
			Assert.Empty(spilled);
			Assert.NotEqual(allocated[vr1], allocated[vr2]);
		}

		/*
		 * Auto generated tests
		 */

		[Fact]
		public void AutoGenerated0()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8,
r8, r9, r10, r11, r12, r13 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r12), (r10, r8), (r10, r9), (r11, r12), (r11, r13), (r11, r8), (r11, r9), (r11, vr2), (r11, vr4), (r11, vr5), (r11, vr6), (r11, vr7), (r12, r10), (r12, r11), (r12, r13), (r12, r8), (r12, r9), (r12, vr2), (r12, vr3), (r12, vr4), (r12, vr5), (r12, vr8), (r13, r11), (r13, r12), (r13, vr2), (r13, vr4), (r13, vr8), (r8, r10), (r8, r11), (r8, r12), (r8, r9), (r8, vr3), (r8, vr4), (r8, vr6), (r8, vr7), (r9, r10), (r9, r11), (r9, r12), (r9, r8), (r9, vr1), (r9, vr3), (r9, vr6), (r9, vr7), (r9, vr8), (vr1, r9), (vr1, vr2), (vr2, r11), (vr2, r12), (vr2, r13), (vr2, vr1), (vr2, vr3), (vr2, vr4), (vr2, vr5), (vr2, vr7), (vr2, vr8), (vr3, r12), (vr3, r8), (vr3, r9), (vr3, vr2), (vr3, vr5), (vr3, vr6), (vr3, vr8), (vr4, r11), (vr4, r12), (vr4, r13), (vr4, r8), (vr4, vr2), (vr4, vr5), (vr4, vr6), (vr5, r11), (vr5, r12), (vr5, vr2), (vr5, vr3), (vr5, vr4), (vr5, vr6), (vr5, vr7), (vr6, r11), (vr6, r8), (vr6, r9), (vr6, vr3), (vr6, vr4), (vr6, vr5), (vr7, r11), (vr7, r8), (vr7, r9), (vr7, vr2), (vr7, vr5), (vr8, r12), (vr8, r13), (vr8, r9), (vr8, vr2), (vr8, vr3) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated1()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10,
r8, r9 };
			var conflicts = new List<(Register, Register)>()
{ (r8, vr1), (r8, vr4), (r8, vr5), (r9, vr3), (r9, vr5), (vr1, r8), (vr1, vr3), (vr1, vr7), (vr10, vr6), (vr10, vr9), (vr2, vr3), (vr2, vr4), (vr3, r9), (vr3, vr1), (vr3, vr2), (vr3, vr6), (vr3, vr7), (vr3, vr8), (vr4, r8), (vr4, vr2), (vr4, vr5), (vr4, vr8), (vr5, r8), (vr5, r9), (vr5, vr4), (vr5, vr6), (vr5, vr8), (vr6, vr10), (vr6, vr3), (vr6, vr5), (vr6, vr8), (vr6, vr9), (vr7, vr1), (vr7, vr3), (vr8, vr3), (vr8, vr4), (vr8, vr5), (vr8, vr6), (vr9, vr10), (vr9, vr6) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated2()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6,
r8, r9, r10, r11 };
			var conflicts = new List<(Register, Register)>()
{ (r10, vr6), (r8, vr1), (r9, vr2), (vr1, r8), (vr1, vr3), (vr1, vr4), (vr1, vr6), (vr2, r9), (vr2, vr4), (vr3, vr1), (vr3, vr6), (vr4, vr1), (vr4, vr2), (vr6, r10), (vr6, vr1), (vr6, vr3) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated3()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13, vr14, vr15,
r8, r9 };
			var conflicts = new List<(Register, Register)>()
{ (r8, r9), (r8, vr10), (r8, vr11), (r8, vr12), (r8, vr15), (r8, vr2), (r8, vr3), (r8, vr7), (r8, vr8), (r9, r8), (r9, vr10), (r9, vr11), (r9, vr14), (r9, vr15), (r9, vr2), (r9, vr3), (r9, vr4), (r9, vr5), (r9, vr6), (r9, vr8), (r9, vr9), (vr1, vr11), (vr1, vr13), (vr1, vr14), (vr1, vr15), (vr1, vr4), (vr1, vr5), (vr1, vr6), (vr1, vr7), (vr1, vr8), (vr10, r8), (vr10, r9), (vr10, vr11), (vr10, vr12), (vr10, vr13), (vr10, vr14), (vr10, vr15), (vr10, vr3), (vr10, vr4), (vr10, vr5), (vr10, vr6), (vr10, vr8), (vr11, r8), (vr11, r9), (vr11, vr1), (vr11, vr10), (vr11, vr13), (vr11, vr14), (vr11, vr7), (vr11, vr8), (vr11, vr9), (vr12, r8), (vr12, vr10), (vr12, vr13), (vr12, vr2), (vr12, vr3), (vr13, vr1), (vr13, vr10), (vr13, vr11), (vr13, vr12), (vr13, vr2), (vr13, vr3), (vr13, vr4), (vr13, vr5), (vr13, vr6), (vr13, vr7), (vr13, vr9), (vr14, r9), (vr14, vr1), (vr14, vr10), (vr14, vr11), (vr14, vr15), (vr14, vr3), (vr14, vr4), (vr14, vr6), (vr14, vr8), (vr14, vr9), (vr15, r8), (vr15, r9), (vr15, vr1), (vr15, vr10), (vr15, vr14), (vr15, vr2), (vr15, vr3), (vr15, vr5), (vr15, vr7), (vr15, vr9), (vr2, r8), (vr2, r9), (vr2, vr12), (vr2, vr13), (vr2, vr15), (vr2, vr3), (vr2, vr5), (vr2, vr6), (vr2, vr8), (vr3, r8), (vr3, r9), (vr3, vr10), (vr3, vr12), (vr3, vr13), (vr3, vr14), (vr3, vr15), (vr3, vr2), (vr3, vr4), (vr3, vr6), (vr3, vr7), (vr4, r9), (vr4, vr1), (vr4, vr10), (vr4, vr13), (vr4, vr14), (vr4, vr3), (vr4, vr5), (vr4, vr7), (vr4, vr8), (vr4, vr9), (vr5, r9), (vr5, vr1), (vr5, vr10), (vr5, vr13), (vr5, vr15), (vr5, vr2), (vr5, vr4), (vr5, vr7), (vr5, vr8), (vr5, vr9), (vr6, r9), (vr6, vr1), (vr6, vr10), (vr6, vr13), (vr6, vr14), (vr6, vr2), (vr6, vr3), (vr6, vr7), (vr6, vr9), (vr7, r8), (vr7, vr1), (vr7, vr11), (vr7, vr13), (vr7, vr15), (vr7, vr3), (vr7, vr4), (vr7, vr5), (vr7, vr6), (vr7, vr8), (vr7, vr9), (vr8, r8), (vr8, r9), (vr8, vr1), (vr8, vr10), (vr8, vr11), (vr8, vr14), (vr8, vr2), (vr8, vr4), (vr8, vr5), (vr8, vr7), (vr8, vr9), (vr9, r9), (vr9, vr11), (vr9, vr13), (vr9, vr14), (vr9, vr15), (vr9, vr4), (vr9, vr5), (vr9, vr6), (vr9, vr7), (vr9, vr8) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated4()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13,
r8, r9, r10, r11, r12 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r11), (r10, r12), (r10, r8), (r10, r9), (r10, vr1), (r10, vr10), (r10, vr12), (r10, vr13), (r10, vr3), (r10, vr5), (r10, vr6), (r10, vr9), (r11, r10), (r11, vr13), (r11, vr2), (r11, vr6), (r11, vr7), (r11, vr9), (r12, r10), (r12, r8), (r12, r9), (r12, vr1), (r12, vr10), (r12, vr2), (r12, vr3), (r12, vr4), (r12, vr5), (r12, vr6), (r12, vr9), (r8, r10), (r8, r12), (r8, r9), (r8, vr1), (r8, vr11), (r8, vr12), (r8, vr13), (r8, vr2), (r8, vr3), (r8, vr7), (r9, r10), (r9, r12), (r9, r8), (r9, vr1), (r9, vr11), (r9, vr2), (r9, vr3), (r9, vr9), (vr1, r10), (vr1, r12), (vr1, r8), (vr1, r9), (vr1, vr10), (vr1, vr11), (vr1, vr2), (vr1, vr3), (vr1, vr6), (vr1, vr7), (vr10, r10), (vr10, r12), (vr10, vr1), (vr10, vr11), (vr10, vr13), (vr10, vr3), (vr10, vr4), (vr10, vr5), (vr10, vr6), (vr10, vr7), (vr11, r8), (vr11, r9), (vr11, vr1), (vr11, vr10), (vr11, vr12), (vr11, vr2), (vr11, vr4), (vr11, vr6), (vr11, vr7), (vr11, vr8), (vr12, r10), (vr12, r8), (vr12, vr11), (vr12, vr13), (vr12, vr2), (vr12, vr4), (vr12, vr9), (vr13, r10), (vr13, r11), (vr13, r8), (vr13, vr10), (vr13, vr12), (vr13, vr2), (vr13, vr3), (vr13, vr5), (vr13, vr6), (vr13, vr7), (vr13, vr8), (vr13, vr9), (vr2, r11), (vr2, r12), (vr2, r8), (vr2, r9), (vr2, vr1), (vr2, vr11), (vr2, vr12), (vr2, vr13), (vr2, vr3), (vr2, vr6), (vr2, vr8), (vr2, vr9), (vr3, r10), (vr3, r12), (vr3, r8), (vr3, r9), (vr3, vr1), (vr3, vr10), (vr3, vr13), (vr3, vr2), (vr3, vr5), (vr3, vr6), (vr3, vr7), (vr3, vr8), (vr3, vr9), (vr4, r12), (vr4, vr10), (vr4, vr11), (vr4, vr12), (vr4, vr5), (vr4, vr6), (vr4, vr8), (vr5, r10), (vr5, r12), (vr5, vr10), (vr5, vr13), (vr5, vr3), (vr5, vr4), (vr5, vr6), (vr6, r10), (vr6, r11), (vr6, r12), (vr6, vr1), (vr6, vr10), (vr6, vr11), (vr6, vr13), (vr6, vr2), (vr6, vr3), (vr6, vr4), (vr6, vr5), (vr7, r11), (vr7, r8), (vr7, vr1), (vr7, vr10), (vr7, vr11), (vr7, vr13), (vr7, vr3), (vr8, vr11), (vr8, vr13), (vr8, vr2), (vr8, vr3), (vr8, vr4), (vr8, vr9), (vr9, r10), (vr9, r11), (vr9, r12), (vr9, r9), (vr9, vr12), (vr9, vr13), (vr9, vr2), (vr9, vr3), (vr9, vr8) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated5()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4,
r8, r9, r10 };
			var conflicts = new List<(Register, Register)>()
{ (r10, vr1), (r10, vr4), (r8, r9), (r9, r8), (r9, vr1), (r9, vr2), (r9, vr3), (r9, vr4), (vr1, r10), (vr1, r9), (vr2, r9), (vr3, r9), (vr4, r10), (vr4, r9) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated6()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4,
r8, r9 };
			var conflicts = new List<(Register, Register)>()
{ (r8, vr3), (r9, vr1), (r9, vr2), (r9, vr4), (vr1, r9), (vr1, vr2), (vr1, vr3), (vr1, vr4), (vr2, r9), (vr2, vr1), (vr3, r8), (vr3, vr1), (vr4, r9), (vr4, vr1) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated7()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12,
r8, r9, r10, r11, r12, r13, r14 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r11), (r10, r12), (r10, r13), (r10, vr1), (r10, vr10), (r10, vr2), (r10, vr4), (r10, vr7), (r10, vr9), (r11, r10), (r11, r12), (r11, r13), (r11, r14), (r11, r8), (r11, vr10), (r11, vr11), (r11, vr12), (r11, vr2), (r11, vr5), (r11, vr8), (r11, vr9), (r12, r10), (r12, r11), (r12, r13), (r12, r14), (r12, r8), (r12, r9), (r12, vr1), (r12, vr10), (r12, vr12), (r12, vr2), (r12, vr3), (r12, vr5), (r12, vr8), (r12, vr9), (r13, r10), (r13, r11), (r13, r12), (r13, r8), (r13, r9), (r13, vr1), (r13, vr10), (r13, vr11), (r13, vr5), (r13, vr8), (r14, r11), (r14, r12), (r14, r9), (r14, vr10), (r14, vr11), (r14, vr5), (r14, vr6), (r14, vr7), (r8, r11), (r8, r12), (r8, r13), (r8, r9), (r8, vr10), (r8, vr12), (r8, vr2), (r8, vr3), (r8, vr5), (r8, vr6), (r8, vr7), (r8, vr8), (r9, r12), (r9, r13), (r9, r14), (r9, r8), (r9, vr1), (r9, vr10), (r9, vr11), (r9, vr12), (r9, vr3), (r9, vr4), (r9, vr5), (r9, vr6), (r9, vr8), (r9, vr9), (vr1, r10), (vr1, r12), (vr1, r13), (vr1, r9), (vr1, vr11), (vr1, vr2), (vr1, vr4), (vr1, vr5), (vr1, vr6), (vr1, vr9), (vr10, r10), (vr10, r11), (vr10, r12), (vr10, r13), (vr10, r14), (vr10, r8), (vr10, r9), (vr10, vr11), (vr10, vr12), (vr10, vr2), (vr10, vr7), (vr10, vr9), (vr11, r11), (vr11, r13), (vr11, r14), (vr11, r9), (vr11, vr1), (vr11, vr10), (vr11, vr12), (vr11, vr5), (vr12, r11), (vr12, r12), (vr12, r8), (vr12, r9), (vr12, vr10), (vr12, vr11), (vr12, vr3), (vr12, vr9), (vr2, r10), (vr2, r11), (vr2, r12), (vr2, r8), (vr2, vr1), (vr2, vr10), (vr2, vr7), (vr2, vr9), (vr3, r12), (vr3, r8), (vr3, r9), (vr3, vr12), (vr3, vr4), (vr3, vr5), (vr3, vr7), (vr3, vr9), (vr4, r10), (vr4, r9), (vr4, vr1), (vr4, vr3), (vr4, vr5), (vr4, vr6), (vr4, vr7), (vr4, vr8), (vr4, vr9), (vr5, r11), (vr5, r12), (vr5, r13), (vr5, r14), (vr5, r8), (vr5, r9), (vr5, vr1), (vr5, vr11), (vr5, vr3), (vr5, vr4), (vr5, vr6), (vr5, vr7), (vr5, vr9), (vr6, r14), (vr6, r8), (vr6, r9), (vr6, vr1), (vr6, vr4), (vr6, vr5), (vr7, r10), (vr7, r14), (vr7, r8), (vr7, vr10), (vr7, vr2), (vr7, vr3), (vr7, vr4), (vr7, vr5), (vr7, vr8), (vr7, vr9), (vr8, r11), (vr8, r12), (vr8, r13), (vr8, r8), (vr8, r9), (vr8, vr4), (vr8, vr7), (vr9, r10), (vr9, r11), (vr9, r12), (vr9, r9), (vr9, vr1), (vr9, vr10), (vr9, vr12), (vr9, vr2), (vr9, vr3), (vr9, vr4), (vr9, vr5), (vr9, vr7) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated8()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13, vr14, vr15,
r8, r9, r10, r11, r12, r13, r14, r15 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r11), (r10, r12), (r10, r8), (r10, r9), (r10, vr12), (r10, vr14), (r10, vr15), (r10, vr3), (r10, vr5), (r10, vr7), (r10, vr8), (r10, vr9), (r11, r10), (r11, r15), (r11, r8), (r11, r9), (r11, vr10), (r11, vr11), (r11, vr14), (r11, vr2), (r11, vr3), (r11, vr5), (r11, vr6), (r11, vr7), (r11, vr9), (r12, r10), (r12, r13), (r12, r15), (r12, r9), (r12, vr1), (r12, vr11), (r12, vr12), (r12, vr13), (r12, vr15), (r12, vr2), (r12, vr8), (r12, vr9), (r13, r12), (r13, r15), (r13, vr10), (r13, vr12), (r13, vr13), (r13, vr15), (r13, vr3), (r13, vr4), (r13, vr9), (r14, r15), (r14, vr12), (r14, vr14), (r14, vr15), (r14, vr4), (r14, vr5), (r14, vr7), (r14, vr9), (r15, r11), (r15, r12), (r15, r13), (r15, r14), (r15, r8), (r15, vr11), (r15, vr12), (r15, vr13), (r15, vr14), (r15, vr15), (r15, vr2), (r15, vr7), (r8, r10), (r8, r11), (r8, r15), (r8, r9), (r8, vr1), (r8, vr12), (r8, vr14), (r8, vr15), (r8, vr2), (r8, vr4), (r8, vr6), (r8, vr7), (r9, r10), (r9, r11), (r9, r12), (r9, r8), (r9, vr1), (r9, vr12), (r9, vr15), (r9, vr3), (r9, vr7), (r9, vr8), (vr1, r12), (vr1, r8), (vr1, r9), (vr1, vr10), (vr1, vr15), (vr1, vr4), (vr1, vr5), (vr1, vr8), (vr10, r11), (vr10, r13), (vr10, vr1), (vr10, vr11), (vr10, vr12), (vr10, vr13), (vr10, vr5), (vr10, vr6), (vr10, vr9), (vr11, r11), (vr11, r12), (vr11, r15), (vr11, vr10), (vr11, vr3), (vr11, vr7), (vr12, r10), (vr12, r12), (vr12, r13), (vr12, r14), (vr12, r15), (vr12, r8), (vr12, r9), (vr12, vr10), (vr12, vr13), (vr12, vr14), (vr12, vr15), (vr12, vr2), (vr12, vr4), (vr12, vr5), (vr12, vr8), (vr13, r12), (vr13, r13), (vr13, r15), (vr13, vr10), (vr13, vr12), (vr13, vr15), (vr13, vr3), (vr13, vr4), (vr13, vr6), (vr13, vr9), (vr14, r10), (vr14, r11), (vr14, r14), (vr14, r15), (vr14, r8), (vr14, vr12), (vr14, vr15), (vr14, vr2), (vr14, vr3), (vr14, vr5), (vr14, vr8), (vr15, r10), (vr15, r12), (vr15, r13), (vr15, r14), (vr15, r15), (vr15, r8), (vr15, r9), (vr15, vr1), (vr15, vr12), (vr15, vr13), (vr15, vr14), (vr15, vr4), (vr15, vr5), (vr15, vr8), (vr2, r11), (vr2, r12), (vr2, r15), (vr2, r8), (vr2, vr12), (vr2, vr14), (vr2, vr3), (vr2, vr4), (vr2, vr5), (vr2, vr7), (vr2, vr9), (vr3, r10), (vr3, r11), (vr3, r13), (vr3, r9), (vr3, vr11), (vr3, vr13), (vr3, vr14), (vr3, vr2), (vr3, vr4), (vr3, vr5), (vr3, vr6), (vr3, vr7), (vr3, vr9), (vr4, r13), (vr4, r14), (vr4, r8), (vr4, vr1), (vr4, vr12), (vr4, vr13), (vr4, vr15), (vr4, vr2), (vr4, vr3), (vr4, vr6), (vr5, r10), (vr5, r11), (vr5, r14), (vr5, vr1), (vr5, vr10), (vr5, vr12), (vr5, vr14), (vr5, vr15), (vr5, vr2), (vr5, vr3), (vr5, vr9), (vr6, r11), (vr6, r8), (vr6, vr10), (vr6, vr13), (vr6, vr3), (vr6, vr4), (vr6, vr7), (vr6, vr8), (vr6, vr9), (vr7, r10), (vr7, r11), (vr7, r14), (vr7, r15), (vr7, r8), (vr7, r9), (vr7, vr11), (vr7, vr2), (vr7, vr3), (vr7, vr6), (vr7, vr8), (vr7, vr9), (vr8, r10), (vr8, r12), (vr8, r9), (vr8, vr1), (vr8, vr12), (vr8, vr14), (vr8, vr15), (vr8, vr6), (vr8, vr7), (vr8, vr9), (vr9, r10), (vr9, r11), (vr9, r12), (vr9, r13), (vr9, r14), (vr9, vr10), (vr9, vr13), (vr9, vr2), (vr9, vr3), (vr9, vr5), (vr9, vr6), (vr9, vr7), (vr9, vr8) };
			var copies = new List<(Register, Register)>() { (vr2, vr4), (vr4, vr2) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated9()
		{
			var toAllocate = new List<Register>() { vr1,
r8, r9 };
			var conflicts = new List<(Register, Register)>()
{ (r9, vr1), (vr1, r9) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated10()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6,
r8, r9 };
			var conflicts = new List<(Register, Register)>()
{ (r8, vr6), (r9, vr1), (r9, vr4), (r9, vr6), (vr1, r9), (vr1, vr2), (vr1, vr5), (vr2, vr1), (vr2, vr6), (vr4, r9), (vr5, vr1), (vr6, r8), (vr6, r9), (vr6, vr2) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated11()
		{
			var toAllocate = new List<Register>() { vr1,
r8, r9, r10 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r9), (r8, vr1), (r9, r10), (vr1, r8) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated12()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13, vr14, vr15, vr16,
r8, r9, r10, r11 };
			var conflicts = new List<(Register, Register)>()
{ (r8, vr4), (vr11, vr16), (vr12, vr15), (vr15, vr12), (vr16, vr11), (vr4, r8), (vr5, vr7), (vr7, vr5), (vr7, vr8), (vr8, vr7) };
			var copies = new List<(Register, Register)>() { (vr1, vr3), (vr1, vr4), (vr2, vr4), (vr3, vr1), (vr4, vr1), (vr4, vr2) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated13()
		{
			var toAllocate = new List<Register>() { vr1, vr2,
r8 };
			var conflicts = new List<(Register, Register)>()
			{ };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated14()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12,
r8, r9, r10, r11, r12, r13 };
			var conflicts = new List<(Register, Register)>()
{ (r10, vr4), (r10, vr9), (r11, vr7), (r12, vr12), (r13, vr11), (r13, vr7), (r8, vr9), (r9, vr1), (vr1, r9), (vr1, vr12), (vr10, vr12), (vr10, vr4), (vr10, vr7), (vr11, r13), (vr11, vr2), (vr12, r12), (vr12, vr1), (vr12, vr10), (vr12, vr2), (vr12, vr3), (vr12, vr5), (vr12, vr8), (vr2, vr11), (vr2, vr12), (vr3, vr12), (vr4, r10), (vr4, vr10), (vr5, vr12), (vr6, vr7), (vr7, r11), (vr7, r13), (vr7, vr10), (vr7, vr6), (vr8, vr12), (vr8, vr9), (vr9, r10), (vr9, r8), (vr9, vr8) };
			var copies = new List<(Register, Register)>() { (vr1, vr3), (vr3, vr1) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated15()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7,
r8, r9, r10, r11 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r8), (r10, vr2), (r10, vr3), (r10, vr5), (r10, vr6), (r10, vr7), (r11, vr3), (r11, vr4), (r11, vr5), (r11, vr6), (r8, r10), (r8, vr2), (r8, vr4), (r8, vr5), (r8, vr7), (r9, vr1), (r9, vr2), (r9, vr4), (r9, vr7), (vr1, r9), (vr1, vr2), (vr1, vr3), (vr1, vr4), (vr1, vr5), (vr2, r10), (vr2, r8), (vr2, r9), (vr2, vr1), (vr2, vr3), (vr2, vr4), (vr2, vr5), (vr2, vr6), (vr2, vr7), (vr3, r10), (vr3, r11), (vr3, vr1), (vr3, vr2), (vr3, vr7), (vr4, r11), (vr4, r8), (vr4, r9), (vr4, vr1), (vr4, vr2), (vr4, vr5), (vr5, r10), (vr5, r11), (vr5, r8), (vr5, vr1), (vr5, vr2), (vr5, vr4), (vr5, vr7), (vr6, r10), (vr6, r11), (vr6, vr2), (vr7, r10), (vr7, r8), (vr7, r9), (vr7, vr2), (vr7, vr3), (vr7, vr5) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated16()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11,
r8, r9, r10, r11, r12, r13 };
			var conflicts = new List<(Register, Register)>()
{ (r10, vr10), (r10, vr4), (r10, vr5), (r10, vr7), (r11, r13), (r11, vr10), (r11, vr11), (r12, vr2), (r12, vr6), (r13, r11), (r13, vr5), (r8, vr9), (r9, vr1), (r9, vr7), (r9, vr8), (vr1, r9), (vr10, r10), (vr10, r11), (vr10, vr3), (vr10, vr5), (vr10, vr9), (vr11, r11), (vr2, r12), (vr3, vr10), (vr3, vr8), (vr4, r10), (vr5, r10), (vr5, r13), (vr5, vr10), (vr6, r12), (vr7, r10), (vr7, r9), (vr7, vr9), (vr8, r9), (vr8, vr3), (vr8, vr9), (vr9, r8), (vr9, vr10), (vr9, vr7), (vr9, vr8) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated17()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13, vr14, vr15, vr16,
r8, r9, r10, r11, r12, r13, r14, r15 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r11), (r10, r14), (r11, r10), (r12, r8), (r12, vr5), (r13, vr16), (r14, r10), (r14, vr14), (r8, r12), (r8, vr3), (r9, vr4), (r9, vr6), (vr12, vr16), (vr12, vr7), (vr13, vr8), (vr14, r14), (vr14, vr9), (vr16, r13), (vr16, vr12), (vr3, r8), (vr4, r9), (vr5, r12), (vr5, vr9), (vr6, r9), (vr7, vr12), (vr8, vr13), (vr9, vr14), (vr9, vr5) };
			var copies = new List<(Register, Register)>() { (vr2, vr3), (vr2, vr4), (vr3, vr2), (vr4, vr2) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated18()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13, vr14,
r8, r9, r10, r11, r12, r13, r14, r15 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r12), (r10, r13), (r10, r14), (r10, r15), (r10, r9), (r10, vr12), (r10, vr13), (r10, vr14), (r10, vr2), (r10, vr3), (r11, r13), (r11, r15), (r11, r8), (r11, r9), (r11, vr11), (r11, vr13), (r11, vr3), (r11, vr7), (r11, vr8), (r12, r10), (r12, r13), (r12, r15), (r12, r8), (r12, vr10), (r12, vr11), (r12, vr12), (r12, vr13), (r12, vr14), (r12, vr2), (r12, vr3), (r12, vr4), (r12, vr9), (r13, r10), (r13, r11), (r13, r12), (r13, r14), (r13, r9), (r13, vr10), (r13, vr2), (r13, vr4), (r13, vr5), (r13, vr6), (r14, r10), (r14, r13), (r14, r15), (r14, r8), (r14, vr12), (r14, vr2), (r14, vr5), (r14, vr7), (r14, vr8), (r14, vr9), (r15, r10), (r15, r11), (r15, r12), (r15, r14), (r15, vr1), (r15, vr10), (r15, vr2), (r15, vr3), (r15, vr5), (r15, vr6), (r15, vr9), (r8, r11), (r8, r12), (r8, r14), (r8, vr11), (r8, vr12), (r8, vr13), (r8, vr4), (r8, vr5), (r8, vr7), (r9, r10), (r9, r11), (r9, r13), (r9, vr1), (r9, vr11), (r9, vr13), (r9, vr3), (r9, vr4), (r9, vr5), (r9, vr6), (r9, vr7), (vr1, r15), (vr1, r9), (vr1, vr10), (vr1, vr12), (vr1, vr13), (vr1, vr14), (vr1, vr2), (vr1, vr3), (vr1, vr5), (vr1, vr6), (vr1, vr9), (vr10, r12), (vr10, r13), (vr10, r15), (vr10, vr1), (vr10, vr11), (vr10, vr12), (vr10, vr13), (vr10, vr14), (vr10, vr2), (vr10, vr3), (vr10, vr4), (vr10, vr5), (vr10, vr6), (vr10, vr9), (vr11, r11), (vr11, r12), (vr11, r8), (vr11, r9), (vr11, vr10), (vr11, vr12), (vr11, vr14), (vr11, vr2), (vr11, vr4), (vr11, vr6), (vr11, vr7), (vr11, vr9), (vr12, r10), (vr12, r12), (vr12, r14), (vr12, r8), (vr12, vr1), (vr12, vr10), (vr12, vr11), (vr12, vr13), (vr12, vr14), (vr12, vr2), (vr12, vr4), (vr13, r10), (vr13, r11), (vr13, r12), (vr13, r8), (vr13, r9), (vr13, vr1), (vr13, vr10), (vr13, vr12), (vr13, vr14), (vr13, vr2), (vr13, vr3), (vr13, vr6), (vr13, vr7), (vr13, vr8), (vr14, r10), (vr14, r12), (vr14, vr1), (vr14, vr10), (vr14, vr11), (vr14, vr12), (vr14, vr13), (vr14, vr2), (vr14, vr4), (vr14, vr5), (vr14, vr7), (vr14, vr9), (vr2, r10), (vr2, r12), (vr2, r13), (vr2, r14), (vr2, r15), (vr2, vr1), (vr2, vr10), (vr2, vr11), (vr2, vr12), (vr2, vr13), (vr2, vr14), (vr2, vr3), (vr2, vr5), (vr2, vr6), (vr2, vr7), (vr2, vr8), (vr3, r10), (vr3, r11), (vr3, r12), (vr3, r15), (vr3, r9), (vr3, vr1), (vr3, vr10), (vr3, vr13), (vr3, vr2), (vr3, vr4), (vr3, vr6), (vr3, vr7), (vr3, vr8), (vr3, vr9), (vr4, r12), (vr4, r13), (vr4, r8), (vr4, r9), (vr4, vr10), (vr4, vr11), (vr4, vr12), (vr4, vr14), (vr4, vr3), (vr4, vr6), (vr4, vr8), (vr4, vr9), (vr5, r13), (vr5, r14), (vr5, r15), (vr5, r8), (vr5, r9), (vr5, vr1), (vr5, vr10), (vr5, vr14), (vr5, vr2), (vr5, vr6), (vr5, vr7), (vr5, vr9), (vr6, r13), (vr6, r15), (vr6, r9), (vr6, vr1), (vr6, vr10), (vr6, vr11), (vr6, vr13), (vr6, vr2), (vr6, vr3), (vr6, vr4), (vr6, vr5), (vr6, vr7), (vr6, vr8), (vr7, r11), (vr7, r14), (vr7, r8), (vr7, r9), (vr7, vr11), (vr7, vr13), (vr7, vr14), (vr7, vr2), (vr7, vr3), (vr7, vr5), (vr7, vr6), (vr7, vr9), (vr8, r11), (vr8, r14), (vr8, vr13), (vr8, vr2), (vr8, vr3), (vr8, vr4), (vr8, vr6), (vr9, r12), (vr9, r14), (vr9, r15), (vr9, vr1), (vr9, vr10), (vr9, vr11), (vr9, vr14), (vr9, vr3), (vr9, vr4), (vr9, vr5), (vr9, vr7) };
			var copies = new List<(Register, Register)>() { (vr1, vr4), (vr4, vr1) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated19()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9,
r8, r9, r10 };
			var conflicts = new List<(Register, Register)>()
			{ };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated20()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13, vr14, vr15, vr16,
r8, r9 };
			var conflicts = new List<(Register, Register)>()
{ (r8, r9), (r8, vr1), (r8, vr10), (r8, vr11), (r8, vr13), (r8, vr16), (r8, vr2), (r8, vr4), (r8, vr5), (r8, vr6), (r9, r8), (r9, vr1), (r9, vr3), (r9, vr7), (r9, vr8), (r9, vr9), (vr1, r8), (vr1, r9), (vr1, vr14), (vr1, vr3), (vr1, vr7), (vr10, r8), (vr10, vr12), (vr10, vr14), (vr10, vr16), (vr10, vr3), (vr10, vr9), (vr11, r8), (vr11, vr12), (vr11, vr14), (vr11, vr15), (vr11, vr2), (vr11, vr3), (vr11, vr8), (vr11, vr9), (vr12, vr10), (vr12, vr11), (vr12, vr13), (vr12, vr14), (vr12, vr15), (vr12, vr16), (vr12, vr2), (vr12, vr4), (vr12, vr8), (vr13, r8), (vr13, vr12), (vr13, vr2), (vr13, vr3), (vr13, vr6), (vr13, vr9), (vr14, vr1), (vr14, vr10), (vr14, vr11), (vr14, vr12), (vr14, vr15), (vr14, vr4), (vr14, vr5), (vr14, vr7), (vr15, vr11), (vr15, vr12), (vr15, vr14), (vr15, vr16), (vr15, vr5), (vr15, vr6), (vr16, r8), (vr16, vr10), (vr16, vr12), (vr16, vr15), (vr16, vr2), (vr16, vr3), (vr16, vr9), (vr2, r8), (vr2, vr11), (vr2, vr12), (vr2, vr13), (vr2, vr16), (vr2, vr3), (vr2, vr5), (vr2, vr6), (vr2, vr8), (vr2, vr9), (vr3, r9), (vr3, vr1), (vr3, vr10), (vr3, vr11), (vr3, vr13), (vr3, vr16), (vr3, vr2), (vr3, vr4), (vr3, vr7), (vr3, vr9), (vr4, r8), (vr4, vr12), (vr4, vr14), (vr4, vr3), (vr4, vr5), (vr4, vr6), (vr4, vr7), (vr5, r8), (vr5, vr14), (vr5, vr15), (vr5, vr2), (vr5, vr4), (vr5, vr6), (vr5, vr7), (vr6, r8), (vr6, vr13), (vr6, vr15), (vr6, vr2), (vr6, vr4), (vr6, vr5), (vr6, vr9), (vr7, r9), (vr7, vr1), (vr7, vr14), (vr7, vr3), (vr7, vr4), (vr7, vr5), (vr7, vr8), (vr8, r9), (vr8, vr11), (vr8, vr12), (vr8, vr2), (vr8, vr7), (vr9, r9), (vr9, vr10), (vr9, vr11), (vr9, vr13), (vr9, vr16), (vr9, vr2), (vr9, vr3), (vr9, vr6) };
			var copies = new List<(Register, Register)>() { (vr1, vr2), (vr2, vr1) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated21()
		{
			var toAllocate = new List<Register>() { vr1,
r8 };
			var conflicts = new List<(Register, Register)>()
			{ };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated22()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10,
r8 };
			var conflicts = new List<(Register, Register)>()
{ (r8, vr1), (r8, vr3), (r8, vr6), (r8, vr7), (r8, vr8), (vr1, r8), (vr1, vr10), (vr1, vr2), (vr10, vr1), (vr10, vr3), (vr10, vr8), (vr10, vr9), (vr2, vr1), (vr2, vr3), (vr2, vr5), (vr2, vr7), (vr3, r8), (vr3, vr10), (vr3, vr2), (vr3, vr8), (vr4, vr9), (vr5, vr2), (vr5, vr8), (vr6, r8), (vr6, vr9), (vr7, r8), (vr7, vr2), (vr8, r8), (vr8, vr10), (vr8, vr3), (vr8, vr5), (vr9, vr10), (vr9, vr4), (vr9, vr6) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated23()
		{
			var toAllocate = new List<Register>() { vr1,
r8, r9, r10, r11, r12, r13, r14, r15 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r11), (r10, r13), (r10, r14), (r10, r15), (r10, r9), (r11, r10), (r11, r9), (r13, r10), (r13, r15), (r14, r10), (r14, r15), (r15, r10), (r15, r13), (r15, r14), (r15, r8), (r15, r9), (r8, r15), (r9, r10), (r9, r11), (r9, r15) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated24()
		{
			var toAllocate = new List<Register>() { vr1, vr2,
r8, r9, r10, r11, r12 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r11), (r10, vr1), (r10, vr2), (r11, r10), (r11, r12), (r11, r8), (r11, vr1), (r11, vr2), (r12, r11), (r12, r9), (r12, vr2), (r8, r11), (r8, r9), (r9, r12), (r9, r8), (vr1, r10), (vr1, r11), (vr1, vr2), (vr2, r10), (vr2, r11), (vr2, r12), (vr2, vr1) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated25()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13,
r8, r9, r10, r11, r12, r13 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r11), (r10, r9), (r10, vr1), (r10, vr11), (r10, vr12), (r10, vr13), (r10, vr3), (r10, vr7), (r11, r10), (r11, r12), (r11, r9), (r11, vr5), (r12, r11), (r12, r13), (r12, r8), (r12, vr1), (r12, vr4), (r12, vr7), (r13, r12), (r13, vr11), (r13, vr13), (r13, vr6), (r13, vr7), (r13, vr8), (r8, r12), (r8, vr1), (r8, vr12), (r8, vr13), (r8, vr4), (r8, vr5), (r8, vr9), (r9, r10), (r9, r11), (r9, vr10), (r9, vr4), (r9, vr9), (vr1, r10), (vr1, r12), (vr1, r8), (vr1, vr13), (vr1, vr5), (vr1, vr7), (vr10, r9), (vr10, vr12), (vr10, vr4), (vr10, vr6), (vr11, r10), (vr11, r13), (vr11, vr2), (vr11, vr6), (vr12, r10), (vr12, r8), (vr12, vr10), (vr12, vr2), (vr12, vr7), (vr13, r10), (vr13, r13), (vr13, r8), (vr13, vr1), (vr13, vr2), (vr13, vr5), (vr13, vr7), (vr2, vr11), (vr2, vr12), (vr2, vr13), (vr2, vr3), (vr2, vr6), (vr3, r10), (vr3, vr2), (vr3, vr6), (vr3, vr9), (vr4, r12), (vr4, r8), (vr4, r9), (vr4, vr10), (vr4, vr7), (vr4, vr8), (vr5, r11), (vr5, r8), (vr5, vr1), (vr5, vr13), (vr5, vr6), (vr5, vr7), (vr5, vr8), (vr5, vr9), (vr6, r13), (vr6, vr10), (vr6, vr11), (vr6, vr2), (vr6, vr3), (vr6, vr5), (vr6, vr9), (vr7, r10), (vr7, r12), (vr7, r13), (vr7, vr1), (vr7, vr12), (vr7, vr13), (vr7, vr4), (vr7, vr5), (vr7, vr8), (vr8, r13), (vr8, vr4), (vr8, vr5), (vr8, vr7), (vr9, r8), (vr9, r9), (vr9, vr3), (vr9, vr5), (vr9, vr6) };
			var copies = new List<(Register, Register)>() { (vr2, vr3), (vr3, vr2) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated26()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12,
r8, r9, r10 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r8), (r10, r9), (r10, vr1), (r10, vr10), (r10, vr11), (r10, vr12), (r10, vr5), (r10, vr6), (r10, vr8), (r10, vr9), (r8, r10), (r8, r9), (r8, vr1), (r8, vr10), (r8, vr11), (r8, vr12), (r8, vr2), (r8, vr3), (r8, vr4), (r8, vr5), (r8, vr7), (r8, vr9), (r9, r10), (r9, r8), (r9, vr10), (r9, vr11), (r9, vr2), (r9, vr5), (r9, vr7), (r9, vr8), (vr1, r10), (vr1, r8), (vr1, vr10), (vr1, vr3), (vr1, vr4), (vr1, vr6), (vr1, vr7), (vr10, r10), (vr10, r8), (vr10, r9), (vr10, vr1), (vr10, vr3), (vr10, vr5), (vr10, vr8), (vr10, vr9), (vr11, r10), (vr11, r8), (vr11, r9), (vr11, vr12), (vr11, vr3), (vr11, vr4), (vr11, vr5), (vr11, vr7), (vr11, vr9), (vr12, r10), (vr12, r8), (vr12, vr11), (vr12, vr2), (vr12, vr3), (vr12, vr6), (vr12, vr7), (vr12, vr8), (vr2, r8), (vr2, r9), (vr2, vr12), (vr2, vr5), (vr2, vr7), (vr2, vr8), (vr2, vr9), (vr3, r8), (vr3, vr1), (vr3, vr10), (vr3, vr11), (vr3, vr12), (vr3, vr4), (vr3, vr5), (vr3, vr8), (vr4, r8), (vr4, vr1), (vr4, vr11), (vr4, vr3), (vr4, vr6), (vr5, r10), (vr5, r8), (vr5, r9), (vr5, vr10), (vr5, vr11), (vr5, vr2), (vr5, vr3), (vr5, vr8), (vr6, r10), (vr6, vr1), (vr6, vr12), (vr6, vr4), (vr7, r8), (vr7, r9), (vr7, vr1), (vr7, vr11), (vr7, vr12), (vr7, vr2), (vr7, vr9), (vr8, r10), (vr8, r9), (vr8, vr10), (vr8, vr12), (vr8, vr2), (vr8, vr3), (vr8, vr5), (vr8, vr9), (vr9, r10), (vr9, r8), (vr9, vr10), (vr9, vr11), (vr9, vr2), (vr9, vr7), (vr9, vr8) };
			var copies = new List<(Register, Register)>() { (vr1, vr2), (vr2, vr1) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated27()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13, vr14, vr15, vr16,
r8, r9, r10, r11, r12, r13, r14 };
			var conflicts = new List<(Register, Register)>()
{ (r10, vr1), (r10, vr16), (r10, vr3), (r10, vr5), (r10, vr6), (r11, vr11), (r11, vr3), (r12, r9), (r12, vr16), (r12, vr5), (r12, vr6), (r13, r9), (r13, vr14), (r13, vr16), (r13, vr2), (r14, vr10), (r14, vr16), (r14, vr4), (r8, vr1), (r8, vr11), (r9, r12), (r9, r13), (r9, vr1), (r9, vr11), (vr1, r10), (vr1, r8), (vr1, r9), (vr1, vr2), (vr10, r14), (vr10, vr12), (vr10, vr8), (vr10, vr9), (vr11, r11), (vr11, r8), (vr11, r9), (vr11, vr9), (vr12, vr10), (vr12, vr4), (vr12, vr7), (vr13, vr15), (vr13, vr2), (vr13, vr5), (vr14, r13), (vr14, vr15), (vr14, vr16), (vr14, vr5), (vr14, vr7), (vr15, vr13), (vr15, vr14), (vr15, vr16), (vr16, r10), (vr16, r12), (vr16, r13), (vr16, r14), (vr16, vr14), (vr16, vr15), (vr16, vr6), (vr2, r13), (vr2, vr1), (vr2, vr13), (vr2, vr5), (vr2, vr8), (vr3, r10), (vr3, r11), (vr3, vr6), (vr3, vr8), (vr4, r14), (vr4, vr12), (vr4, vr5), (vr4, vr8), (vr5, r10), (vr5, r12), (vr5, vr13), (vr5, vr14), (vr5, vr2), (vr5, vr4), (vr5, vr6), (vr5, vr7), (vr5, vr9), (vr6, r10), (vr6, r12), (vr6, vr16), (vr6, vr3), (vr6, vr5), (vr7, vr12), (vr7, vr14), (vr7, vr5), (vr8, vr10), (vr8, vr2), (vr8, vr3), (vr8, vr4), (vr9, vr10), (vr9, vr11), (vr9, vr5) };
			var copies = new List<(Register, Register)>() { (vr1, vr3), (vr1, vr4), (vr2, vr4), (vr3, vr1), (vr4, vr1), (vr4, vr2) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated28()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11, vr12, vr13,
r8, r9, r10 };
			var conflicts = new List<(Register, Register)>()
{ (r10, r9), (r10, vr10), (r10, vr11), (r10, vr13), (r10, vr3), (r10, vr5), (r10, vr7), (r8, vr10), (r8, vr13), (r9, r10), (r9, vr10), (r9, vr11), (r9, vr13), (r9, vr2), (r9, vr4), (r9, vr5), (vr1, vr13), (vr1, vr2), (vr1, vr7), (vr10, r10), (vr10, r8), (vr10, r9), (vr10, vr13), (vr10, vr4), (vr10, vr8), (vr11, r10), (vr11, r9), (vr11, vr6), (vr12, vr2), (vr12, vr7), (vr12, vr9), (vr13, r10), (vr13, r8), (vr13, r9), (vr13, vr1), (vr13, vr10), (vr13, vr4), (vr2, r9), (vr2, vr1), (vr2, vr12), (vr2, vr4), (vr2, vr6), (vr2, vr8), (vr3, r10), (vr3, vr7), (vr4, r9), (vr4, vr10), (vr4, vr13), (vr4, vr2), (vr4, vr9), (vr5, r10), (vr5, r9), (vr5, vr6), (vr6, vr11), (vr6, vr2), (vr6, vr5), (vr7, r10), (vr7, vr1), (vr7, vr12), (vr7, vr3), (vr7, vr9), (vr8, vr10), (vr8, vr2), (vr9, vr12), (vr9, vr4), (vr9, vr7) };
			var copies = new List<(Register, Register)>() { (vr2, vr3), (vr3, vr2) };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}

		[Fact]
		public void AutoGenerated29()
		{
			var toAllocate = new List<Register>() { vr1, vr2, vr3, vr4, vr5, vr6, vr7, vr8, vr9, vr10, vr11,
r8, r9, r10 };
			var conflicts = new List<(Register, Register)>()
{ (r10, vr1), (r10, vr10), (r10, vr11), (r10, vr2), (r10, vr5), (r10, vr8), (r8, vr1), (r8, vr10), (r8, vr11), (r8, vr2), (r8, vr3), (r8, vr4), (r8, vr5), (r8, vr6), (r8, vr7), (r8, vr9), (r9, vr11), (r9, vr2), (r9, vr3), (r9, vr6), (vr1, r10), (vr1, r8), (vr1, vr11), (vr1, vr2), (vr1, vr3), (vr1, vr4), (vr1, vr5), (vr1, vr6), (vr1, vr7), (vr1, vr9), (vr10, r10), (vr10, r8), (vr10, vr11), (vr10, vr3), (vr10, vr4), (vr10, vr5), (vr10, vr7), (vr10, vr8), (vr10, vr9), (vr11, r10), (vr11, r8), (vr11, r9), (vr11, vr1), (vr11, vr10), (vr11, vr4), (vr11, vr5), (vr11, vr7), (vr11, vr9), (vr2, r10), (vr2, r8), (vr2, r9), (vr2, vr1), (vr2, vr4), (vr2, vr6), (vr2, vr8), (vr3, r8), (vr3, r9), (vr3, vr1), (vr3, vr10), (vr3, vr7), (vr3, vr9), (vr4, r8), (vr4, vr1), (vr4, vr10), (vr4, vr11), (vr4, vr2), (vr4, vr5), (vr4, vr8), (vr4, vr9), (vr5, r10), (vr5, r8), (vr5, vr1), (vr5, vr10), (vr5, vr11), (vr5, vr4), (vr5, vr6), (vr5, vr8), (vr6, r8), (vr6, r9), (vr6, vr1), (vr6, vr2), (vr6, vr5), (vr6, vr7), (vr6, vr8), (vr6, vr9), (vr7, r8), (vr7, vr1), (vr7, vr10), (vr7, vr11), (vr7, vr3), (vr7, vr6), (vr7, vr8), (vr7, vr9), (vr8, r10), (vr8, vr10), (vr8, vr2), (vr8, vr4), (vr8, vr5), (vr8, vr6), (vr8, vr7), (vr9, r8), (vr9, vr1), (vr9, vr10), (vr9, vr11), (vr9, vr3), (vr9, vr4), (vr9, vr6), (vr9, vr7) };
			var copies = new List<(Register, Register)>() { };
			var (allocated, spilled) = RegistersAllocation.Allocate(toAllocate, conflicts, copies);
			checkHardConditions(allocated, spilled, toAllocate, conflicts, copies);
		}
	}
}
