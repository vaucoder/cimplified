﻿using System.Collections.Generic;
using Cimplified;
using Cimplified.Parser;
using Xunit;

namespace tests
{
	public class CorrectTypeCheckingTest
	{
		private readonly TypeName intType = new TypeName("Int");
		private readonly TypeName boolType = new TypeName("Bool");
		private readonly TypeName voidType = new TypeName("Void");

		private readonly IntLiteral termNumber0 = new IntLiteral("0");
		private readonly IntLiteral termNumber1 = new IntLiteral("1");
		private readonly IntLiteral termNumber2 = new IntLiteral("2");

		private readonly BoolLiteral termBoolFalse = new BoolLiteral(false);
		private readonly BoolLiteral termBoolTrue = new BoolLiteral(true);

		private readonly FunctionName functionMainName = new FunctionName("main");
		private readonly FunctionName functionName = new FunctionName("function");

		private readonly VarName intXVarName = new VarName("intX");
		private readonly VarName intYVarName = new VarName("intY");
		private readonly VarName boolTrueVarName = new VarName("boolTrue");
		private readonly VarName boolFalseVarName = new VarName("boolFalse");
		private readonly VarName anyVarName = new VarName("var");


		/*
		 * Void main() {
		 * }
		 */
		[Fact]
		public void VoidFunctionTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var functionBlock = new Block(new List<Statement> { });
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(voidType.Type, functionDeclaration.TypeName.Type);
		}

		/*
		 * Int main() {
		 *		return 0;
		 * }
		 */
		[Fact]
		public void IntFunctionTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var returnClause = new ReturnClause(termNumber0);
			var functionBlock = new Block(new List<Statement> {returnClause});
			var functionDeclaration = new FunctionDeclaration(intType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, functionDeclaration.TypeName.Type);
			Assert.Equal(intType.Type, returnClause.ReturnExpression.Type);
		}

		/*
		 * Bool main() {
		 *		return true;
		 * }
		 */
		[Fact]
		public void BoolFunctionTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var returnClause = new ReturnClause(termBoolTrue);
			var functionBlock = new Block(new List<Statement> {returnClause});
			var functionDeclaration = new FunctionDeclaration(boolType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, functionDeclaration.TypeName.Type);
			Assert.Equal(boolType.Type, returnClause.ReturnExpression.Type);
		}


		/*
		 * Void main() {
		 *	Int intX = 0;
		 * }
		 */
		[Fact]
		public void IntDeclarationTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXDeclaration.Value.Type);
		}

		/*
		 * Void main() {
		 *	Bool boolVarTrue = true;
		 * }
		 */
		[Fact]
		public void TrueBoolDeclarationTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var boolVarTrueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, termBoolTrue);
			boolTrueVarName.Declaration = boolVarTrueDeclaration;
			var functionBlock = new Block(new List<Statement> {boolVarTrueDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, boolVarTrueDeclaration.Value.Type);
		}

		/*
		 * Void main() {
		 *	Bool boolVarFalse = false;
		 * }
		 */
		[Fact]
		public void FalseBoolDeclarationTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var boolVarTrueDeclaration = new DeclareAssignment(boolType, boolFalseVarName, termBoolFalse);
			boolFalseVarName.Declaration = boolVarTrueDeclaration;
			var functionBlock = new Block(new List<Statement> {boolVarTrueDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, boolVarTrueDeclaration.Value.Type);
		}

		/*
		 * Void main() {
		 *	Int intX = 0;
		 *	intX = 2;
		 * }
		 */
		[Fact]
		public void IntAssignmentTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var intXReassignment = new Assignment(intXVarName, termNumber2);
			var functionBlock = new Block(new List<Statement> {intXDeclaration, intXReassignment});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXReassignment.Value.Type);
		}

		/*
		 * Void main() {
		 *	Int intX = 0;
		 *  if(true) {
		 *  	intX = 2;
		 *  }
		 *  else {
		 *  }
		 * }
		 */
		[Fact]
		public void IntAssignmentInBlockTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var intXReassignment = new Assignment(intXVarName, termNumber2);
			var ifBlock = new Block(new List<Statement> {intXReassignment});
			var emptyBlock = new Block(new List<Statement>());
			var ifDeclaration = new IfClause(termBoolTrue, ifBlock, new List<ElIfClause>(), emptyBlock);
			var functionBlock = new Block(new List<Statement> {intXDeclaration, ifDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXReassignment.Value.Type);
		}		
		
		/*
		 * Void main() {
		 *	Int anyVar = 0;
		 *  if(true) {
		 *  	Bool anyVar = true;
		 *  }
		 *  else {
		 *  }
		 * }
		 */
		[Fact]
		public void RedeclarationInBlockWithAnotherTypeTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var anyVarDeclaration = new DeclareAssignment(intType, anyVarName, termNumber0);
			anyVarName.Declaration = anyVarDeclaration;			
			var anyVarDeclarationInBlock = new DeclareAssignment(boolType, anyVarName, termBoolFalse);
			anyVarName.Declaration = anyVarDeclarationInBlock;
			
			var ifBlock = new Block(new List<Statement> {anyVarDeclarationInBlock});
			var emptyBlock = new Block(new List<Statement>());
			var ifDeclaration = new IfClause(termBoolTrue, ifBlock, new List<ElIfClause>(), emptyBlock);
			var functionBlock = new Block(new List<Statement> {anyVarDeclaration, ifDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, anyVarDeclaration.Value.Type);
			Assert.Equal(boolType.Type, anyVarDeclarationInBlock.Value.Type);
		}

		/*
		 * Void main() {
		 *	Int intX = 0;
		 *	Int intY = 1;
		 *	intX = intY;
		 * }
		 */
		[Fact]
		public void IntAssignmentTwoVariablesTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var intYDeclaration = new DeclareAssignment(intType, intYVarName, termNumber1);
			intYVarName.Declaration = intYDeclaration;
			var intXReassignment = new Assignment(intXVarName, intYDeclaration.Value);

			var functionBlock = new Block(new List<Statement> {intXDeclaration, intYDeclaration, intXReassignment});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXDeclaration.Value.Type);
			Assert.Equal(intType.Type, intYDeclaration.Value.Type);
			Assert.Equal(intType.Type, intXReassignment.Value.Type);
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = true;
		 *	boolTrue = false;
		 * }
		 */
		[Fact]
		public void BoolTrueToFalseAssignmentTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var boolTrueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, termBoolTrue);
			boolTrueVarName.Declaration = boolTrueDeclaration;
			var boolTrueToFalseReassignment = new Assignment(boolTrueVarName, termBoolFalse);
			var functionBlock = new Block(new List<Statement> {boolTrueDeclaration, boolTrueToFalseReassignment});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, boolTrueToFalseReassignment.Value.Type);
		}

		/*
		 * Void main() {
		 *	Bool boolFalse = false;
		 *	boolFalse = true;
		 * }
		 */
		[Fact]
		public void BoolFalseToTrueAssignmentTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var boolFalseDeclaration = new DeclareAssignment(boolType, boolFalseVarName, termBoolFalse);
			boolFalseVarName.Declaration = boolFalseDeclaration;
			var boolFalseToTrueReassignment = new Assignment(boolFalseVarName, termBoolTrue);
			var functionBlock = new Block(new List<Statement> {boolFalseDeclaration, boolFalseToTrueReassignment});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, boolFalseToTrueReassignment.Value.Type);
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = true;
		 *	Bool boolFalse = false;
		 *	boolTrue = boolFalse;
		 * }
		 */
		[Fact]
		public void BoolAssignmentTwoVariablesTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var trueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, termBoolTrue);
			boolTrueVarName.Declaration = trueDeclaration;
			var falseDeclaration = new DeclareAssignment(boolType, boolFalseVarName, termBoolFalse);
			boolFalseVarName.Declaration = falseDeclaration;
			var trueToFalseAssignment = new Assignment(boolTrueVarName, falseDeclaration.Value);

			var functionBlock =
				new Block(new List<Statement> {trueDeclaration, falseDeclaration, trueToFalseAssignment});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, trueDeclaration.Value.Type);
			Assert.Equal(boolType.Type, falseDeclaration.Value.Type);
			Assert.Equal(boolType.Type, trueToFalseAssignment.Value.Type);
		}

		/*
		 * Void main() {
		 *	while(True) {
		 *	}
		 * }
		 */
		[Fact]
		public void WhileWithTrueTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var whileClause = new WhileClause(termBoolTrue, emptyBlock);
			var functionBlock = new Block(new List<Statement> {whileClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, whileClause.Condition.Type);
		}

		/*
		 * Void main() {
		 *	while(False) {
		 *	}
		 * }
		 */
		[Fact]
		public void WhileWithFalseTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var whileClause = new WhileClause(termBoolFalse, emptyBlock);
			var functionBlock = new Block(new List<Statement> {whileClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, whileClause.Condition.Type);
		}

		/*
		 * Void main() {
		 *	while(True) {
		 *		break 1;
		 *	}
		 * }
		 */
		[Fact]
		public void WhileWithBreakTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var breakStatement = new BreakClause(termNumber1);
			var whileBlock = new Block(new List<Statement> {breakStatement});
			var whileClause = new WhileClause(termBoolTrue, whileBlock);
			var functionBlock = new Block(new List<Statement> {whileClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, breakStatement.N.Type);
		}

		/*
		 * Void main() {
		 *	while(True) {
		 *		continue;
		 *	}
		 * }
		 */
		[Fact]
		public void WhileWithContinueTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var continueClause = new ContinueClause();
			var whileBlock = new Block(new List<Statement> {continueClause});
			var whileClause = new WhileClause(termBoolTrue, whileBlock);
			var functionBlock = new Block(new List<Statement> {whileClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			// cannot check type of ContinueClause, then I test if it pass TypeChecking without errors
		}


		/*
		 * Void main() {
		 *	if(True) {
		 *	}
		 * }
		 */
		[Fact]
		public void IfWithTrueTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var ifClause = new IfClause(termBoolTrue, emptyBlock, new List<ElIfClause>(), emptyBlock);
			var functionBlock = new Block(new List<Statement> {ifClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, ifClause.Condition.Type);
		}


		/*
		 * Void main() {
		 *	if(True) {
		 *	}
		 * }
		 */
		[Fact]
		public void IfWithFalseTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var ifClause = new IfClause(termBoolFalse, emptyBlock, new List<ElIfClause>(), emptyBlock);
			var functionBlock = new Block(new List<Statement> {ifClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, ifClause.Condition.Type);
		}

		/*
		 * Void main() {
		 *	if(True) {
		 *	} elif (True) {
		 * }
		 * }
		 */
		[Fact]
		public void IfWithElifTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var ifClause = new IfClause(termBoolFalse, emptyBlock,
				new List<ElIfClause> {new ElIfClause(termBoolTrue, emptyBlock)}, emptyBlock);
			var functionBlock = new Block(new List<Statement> {ifClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.NotEmpty(ifClause.ElIfs);
			Assert.Equal(boolType.Type, ifClause.ElIfs[0].Condition.Type);
		}

		/*
		 * Void main() {
		 *	Int intX = 1 + 2;
		 * }
		 */
		[Fact]
		public void PlusOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var plusOperator = new Plus {Left = termNumber1, Right = termNumber2};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, plusOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXDeclaration.TypeName.Type);
			Assert.Equal(intType.Type, plusOperator.Type);
		}

		/*
		 * Void main() {
		 *	Int intX = 1 * 2;
		 * }
		 */
		[Fact]
		public void MultOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var multOperator = new Mult {Left = termNumber1, Right = termNumber2};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, multOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXDeclaration.TypeName.Type);
			Assert.Equal(intType.Type, multOperator.Type);
		}

		/*
		 * Void main() {
		 *	Int intX = 2 / 1;
		 * }
		 */
		[Fact]
		public void DivOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var divOperator = new Div {Left = termNumber2, Right = termNumber1};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, divOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXDeclaration.TypeName.Type);
			Assert.Equal(intType.Type, divOperator.Type);
		}

		/*
		 * Void main() {
		 *	Int intX = 2 - 1;
		 * }
		 */
		[Fact]
		public void SubOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var subOperator = new Sub {Left = termNumber2, Right = termNumber1};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, subOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXDeclaration.TypeName.Type);
			Assert.Equal(intType.Type, subOperator.Type);
		}


		/*
		 * Void main() {
		 *	Bool var = 2 == 1;
		 * }
		 */
		[Fact]
		public void EqOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var eqOperator = new Eq {Left = termNumber2, Right = termNumber1};
			var anyVarDeclaration = new DeclareAssignment(boolType, anyVarName, eqOperator);
			anyVarName.Declaration = anyVarDeclaration;
			var functionBlock = new Block(new List<Statement> {anyVarDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, anyVarDeclaration.TypeName.Type);
			Assert.Equal(boolType.Type, eqOperator.Type);
		}

		/*
		 * Void main() {
		 *	Bool var = 2 <= 1;
		 * }
		 */
		[Fact]
		public void LeqOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var leqOperator = new Leq {Left = termNumber2, Right = termNumber1};
			var anyVarDeclaration = new DeclareAssignment(boolType, anyVarName, leqOperator);
			anyVarName.Declaration = anyVarDeclaration;
			var functionBlock = new Block(new List<Statement> {anyVarDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, anyVarDeclaration.TypeName.Type);
			Assert.Equal(boolType.Type, leqOperator.Type);
		}

		/*
		 * Void main() {
		 *	Bool var = true and true;
		 * }
		 */
		[Fact]
		public void AndOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var andOperator = new And {Left = termBoolTrue, Right = termBoolTrue};
			var anyVarDeclaration = new DeclareAssignment(boolType, anyVarName, andOperator);
			anyVarName.Declaration = anyVarDeclaration;
			var functionBlock = new Block(new List<Statement> {anyVarDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, anyVarDeclaration.TypeName.Type);
			Assert.Equal(boolType.Type, andOperator.Type);
		}

		/*
		 * Void main() {
		 *	Bool var = false or true;
		 * }
		 */
		[Fact]
		public void OrOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var orOperator = new Or {Left = termBoolFalse, Right = termBoolTrue};
			var anyVarDeclaration = new DeclareAssignment(boolType, anyVarName, orOperator);
			anyVarName.Declaration = anyVarDeclaration;
			var functionBlock = new Block(new List<Statement> {anyVarDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, anyVarDeclaration.TypeName.Type);
			Assert.Equal(boolType.Type, orOperator.Type);
		}

		/*
		 * Void main() {
		 *	Bool var = not false;
		 * }
		 */
		[Fact]
		public void NegationOperatorTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var negationOperator = new Negation {Operand = termBoolFalse};
			var anyVarDeclaration = new DeclareAssignment(boolType, anyVarName, negationOperator);
			anyVarName.Declaration = anyVarDeclaration;
			var functionBlock = new Block(new List<Statement> {anyVarDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, anyVarDeclaration.TypeName.Type);
			Assert.Equal(boolType.Type, negationOperator.Type);
		}


		/*
		 * Int function1() {
		 *		return 0;
		 * }
		 *
		 * Void main() {
		 *	function1();
		 * }
		 */
		[Fact]
		public void VoidFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();

			var emptyBlock = new Block(new List<Statement>());
			var voidFunctionDeclaration = new FunctionDeclaration(voidType, functionName, args, emptyBlock);
			functionName.Declaration = voidFunctionDeclaration;
			var functionCall = new FunctionCall(functionName, new List<Expression>());
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, args, mainBlock);
			functionMainName.Declaration = mainDeclaration;
			var program = new Program(new List<FunctionDeclaration> {voidFunctionDeclaration, mainDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(voidType.Type, voidFunctionDeclaration.TypeName.Type);
			Assert.Equal(voidType.Type, functionCall.Type);
		}

		/*
		 * Int function1() {
		 *		return 0;
		 * }
		 *
		 * Void main() {
		 *	Int intX = function1();
		 * }
		 */
		[Fact]
		public void IntFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();

			var returnClause = new ReturnClause(termNumber0);
			var functionReturningIntBlock = new Block(new List<Statement> {returnClause});
			var functionReturningIntDeclaration =
				new FunctionDeclaration(intType, functionName, args, functionReturningIntBlock);
			functionName.Declaration = functionReturningIntDeclaration;
			var functionCall = new FunctionCall(functionName, new List<Expression>());
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, functionCall);
			var mainBlock = new Block(new List<Statement> {intXDeclaration});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, args, mainBlock);
			functionMainName.Declaration = mainDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionReturningIntDeclaration, mainDeclaration});

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(intType.Type, intXDeclaration.TypeName.Type);
			Assert.Equal(intType.Type, functionCall.Type);
		}

		/*
		 * Bool function1() {
		 *		return true;
		 * }
		 *
		 * Void main() {
		 *	Bool boolTrue = function1();
		 * }
		 */
		[Fact]
		public void BoolFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();

			var returnClause = new ReturnClause(termBoolTrue);
			var functionReturningIntBlock = new Block(new List<Statement> {returnClause});
			var functionReturningIntDeclaration =
				new FunctionDeclaration(boolType, functionName, args, functionReturningIntBlock);
			functionName.Declaration = functionReturningIntDeclaration;

			var functionCall = new FunctionCall(functionName, new List<Expression>());
			var boolTrueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, functionCall);
			boolTrueVarName.Declaration = boolTrueDeclaration;
			var mainBlock = new Block(new List<Statement> {boolTrueDeclaration});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, args, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionReturningIntDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(boolType.Type, boolTrueDeclaration.TypeName.Type);
			Assert.Equal(boolType.Type, functionCall.Type);
		}

		/*
		 * Void function1(Int anyVar) {
		 * }
		 *
		 * Void main() {
		 *	function1(0);
		 * }
		 */
		[Fact]
		public void SimpleCorrectArgumentWithIntFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument> {new Argument(intType, anyVarName)};
			var functionWithArgumentBlock = new Block(new List<Statement>());
			var functionWithArgumentDeclaration =
				new FunctionDeclaration(voidType, functionName, args, functionWithArgumentBlock);
			functionName.Declaration = functionWithArgumentDeclaration;

			var emptyArgs = new List<Argument>();
			var functionCall = new FunctionCall(functionName, new List<Expression> {termNumber0});
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, emptyArgs, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionWithArgumentDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(voidType.Type, functionCall.Type);
			Assert.Equal(intType.Type, functionCall.Arguments[0].Type);
		}

		/*
		 * Void function1(Bool anyVar) {
		 * }
		 *
		 * Void main() {
		 *	function1(true);
		 * }
		 */
		[Fact]
		public void SimpleCorrectArgumentWithBoolFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument> {new Argument(boolType, anyVarName)};
			var functionWithArgumentBlock = new Block(new List<Statement>());
			var functionWithArgumentDeclaration =
				new FunctionDeclaration(voidType, functionName, args, functionWithArgumentBlock);
			functionName.Declaration = functionWithArgumentDeclaration;

			var emptyArgs = new List<Argument>();
			var functionCall = new FunctionCall(functionName, new List<Expression> {termBoolFalse});
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, emptyArgs, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionWithArgumentDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(voidType.Type, functionCall.Type);
			Assert.Equal(boolType.Type, functionCall.Arguments[0].Type);
		}

		/*
		 * Void function1(Bool anyVar) {
		 *	Bool boolTrue = anyVar;
		 * }
		 *
		 * Void main() {
		 *	function1(true);
		 * }
		 */
		[Fact]
		public void CorrectArgumentWithBoolFunctionCallAndInsideAssignmentTypeCheckingTest()
		{
			// given
			var args = new List<Argument> {new Argument(boolType, anyVarName)};
			anyVarName.Declaration = args[0];
			var argumentUse = new VarUse(anyVarName);
			var boolDeclaration = new DeclareAssignment(boolType, boolTrueVarName, argumentUse);
			boolTrueVarName.Declaration = boolDeclaration;
			var functionWithArgumentBlock = new Block(new List<Statement> {boolDeclaration});
			var functionWithArgumentDeclaration =
				new FunctionDeclaration(voidType, functionName, args, functionWithArgumentBlock);
			functionName.Declaration = functionWithArgumentDeclaration;

			var emptyArgs = new List<Argument>();
			var functionCall = new FunctionCall(functionName, new List<Expression> {termBoolFalse});
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, emptyArgs, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionWithArgumentDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(voidType.Type, functionCall.Type);
			Assert.Equal(boolType.Type, functionCall.Arguments[0].Type);
			Assert.Equal(boolType.Type, boolDeclaration.TypeName.Type);
		}

		/*
		 * Void function1(Int anyVar) {
		 *	Int intX = anyVar;
		 * }
		 *
		 * Void main() {
		 *	function1(0);
		 * }
		 */
		[Fact]
		public void CorrectArgumentWithIntFunctionCallAndInsideAssignmentTypeCheckingTest()
		{
			// given
			var args = new List<Argument> {new Argument(intType, anyVarName)};
			anyVarName.Declaration = args[0];
			var argumentUse = new VarUse(anyVarName);
			var intDeclaration = new DeclareAssignment(intType, intXVarName, argumentUse);
			intXVarName.Declaration = intDeclaration;
			var functionWithArgumentBlock = new Block(new List<Statement> {intDeclaration});
			var functionWithArgumentDeclaration =
				new FunctionDeclaration(voidType, functionName, args, functionWithArgumentBlock);
			functionName.Declaration = functionWithArgumentDeclaration;

			var emptyArgs = new List<Argument>();
			var functionCall = new FunctionCall(functionName, new List<Expression> {termNumber0});
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, emptyArgs, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionWithArgumentDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			TypeChecking.Process(program);

			// then
			Assert.Equal(voidType.Type, functionCall.Type);
			Assert.Equal(intType.Type, functionCall.Arguments[0].Type);
			Assert.Equal(intType.Type, intDeclaration.TypeName.Type);
		}
	}
}
