using Cimplified;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tests
{

	public class DeepEqualsTest
	{

		class A {
			public int x;
			public A(int x)
			{
				this.x = x;
			}
		}

		class B {
			public A x;
			public A y;
			public B(A x, A y)
			{
				this.x = x;
				this.y = y;
			}
		}

		class C { }

		static Dictionary<int, int> dict1 = new Dictionary<int, int>();
		static Dictionary<int, int> dict2 = new Dictionary<int, int>();
		static Dictionary<int, int> dict3 = new Dictionary<int, int>();
		static DeepEqualsTest()
		{
			dict1.Add(0, 1);
			dict2.Add(0, 1);
			dict3.Add(0, 1);

			dict1.Add(2, 7);
			dict2.Add(2, 7);
			dict3.Add(3, 7);
		}

		public static IEnumerable<object[]> Data1 = (new List<(object, object, bool)>() {
				(1, 2, false),
				(1, 1, true),
				(new A(0), new A(0), true),
				(new A(0), new A(1), false),
				(new B(new A(2), new A(3)), new B(new A(2), new A(3)), true),
				(new B(new A(2), new A(3)), new B(new A(2), new A(4)), false),
				(new C(), new C(), true),
				(dict1, dict2, true),
				(dict2, dict1, true),
				(dict1, dict3, false),
				(dict2, dict3, false),
			}).Select(p => new object[] { p.Item1, p.Item2, p.Item3 }) ;

		[Theory]
		[MemberData(nameof(Data1))]
		public void Test1(object a, object b, bool expected)
		{
			Assert.Equal(expected, Utils.DeepEquals(a, b));
		}

	}

}

