﻿using Cimplified;
using Cimplified.Lexer;
using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace tests.Diagnostic
{
	public class AstTests
	{
		[Theory]
		[ClassData(typeof(AstTestExplorer))]
		public void Explore_CimSource_Tests(List<string> fileContent, AST expectedAst)
		{
			var lexer = new Lexer<Terminal>(CimGrammarCreator.CreateTerminalToRegex());
			var parser = new Cimplified.Parser.Parser(CimGrammarCreator.CreateGrammar());

			var source = fileContent;

			var collector = new DiagnosticCollector(source);
			lexer.Emit = collector.Add;
			parser.Emit = collector.Add;
			var tokens = lexer.Process(source);
			var whitespacekind = CimGrammarCreator.Whitespace;
			tokens = tokens.Where(t => t.Kind != whitespacekind);
			List<ParseTree> parseTrees = new List<ParseTree>();
			foreach (var token in tokens)
				parseTrees.Add(new ParseLeaf(token));
			var parseTree = parser.Parse(parseTrees);

			var astEntry = Program.Create(parseTree);

			Assert.True(Utils.DeepEquals(astEntry, expectedAst));
			// Assert.Equal(astEntry, expectedAst);
		}
	}
}
