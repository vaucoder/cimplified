using Cimplified;
using Cimplified.Lexer;
using Cimplified.Parser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace tests.Diagnostic
{
	public class SourceDiagnosticTests
	{
		private List<string> LoadFile(string fileName)
		{
			var lines = System.IO.File.ReadAllLines(fileName);

			return new List<string>(lines);
		}

		[Fact]
		public void TestSimplePrograms()
		{
			var folder = "examples/simple";
			var fileName = "simple2.cim";
			var fileContent = LoadFile($"{Environment.CurrentDirectory}/{folder}/{fileName}");

			var compiler = new Compiler(new Lexer<Terminal>(CimGrammarCreator.CreateTerminalToRegex()), new Cimplified.Parser.Parser(CimGrammarCreator.CreateGrammar()));

			var diagnostic = compiler.Compile(fileContent);

			Assert.Empty(diagnostic.Diagnostics);
		}


		[Theory]
		[ClassData(typeof(DiagnosticTestExplorer))]
		public void Explore_CimSource_Tests(List<string> fileContent, List<TestDiagnostic> diagnostics, string fileName)
		{
			var compiler = new Compiler(new Lexer<Terminal>(CimGrammarCreator.CreateTerminalToRegex()), new Cimplified.Parser.Parser(CimGrammarCreator.CreateGrammar()));

			var diagnostic = compiler.Compile(fileContent);


			if(diagnostic.Diagnostics.Count > 0)
				Assert.Equal("", fileName);

			// TODO: use diagnostics
			Assert.False(diagnostics is null);
		}

		[Fact]
		public void DebugSingleExample()
		{
			// var folder = "examples/if-while-statements/";
			var folder = "examples/syntax/";
			var fileName = "valid-variableNameConsistingOfTypeName.cim";
			var fileContent = LoadFile($"{Environment.CurrentDirectory}/{folder}/{fileName}");

			var compiler = new Compiler(new Lexer<Terminal>(CimGrammarCreator.CreateTerminalToRegex()),
				                        new Cimplified.Parser.Parser(CimGrammarCreator.CreateGrammar()));
			var diagnostic = compiler.Compile(fileContent);
			Assert.Empty(diagnostic.Diagnostics);
		}
	}
}
