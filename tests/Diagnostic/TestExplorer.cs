﻿using Cimplified.Lexer;
using Cimplified.Parser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace tests.Diagnostic
{
	public class TestDiagnostic { 
	
	}

	public class LexerTestDiagnostic : TestDiagnostic
	{
		public Location Loc;
		public string Line;

		public override string ToString()
		{
			return $"Unknown token at {Loc.Line}:{Loc.Pos}";
		}
	}

	public class UnknownTestDiagnostic : TestDiagnostic
	{
		private Symbol _symbol;
		private HashableList<Regex<Symbol>> _state;

		public UnknownTestDiagnostic(Symbol symbol, HashableList<Regex<Symbol>> state)
		{
			_symbol = symbol;
			_state = state;
		}
		public override string ToString()
		{
			return $"No action for given Symbol x State combination." +
						       $"({_symbol}, {_state.ToString()}";
		}
	}

	public class NoExpectedTestDiagnostic : TestDiagnostic
	{
		public NoExpectedTestDiagnostic()
		{

		}
		public override string ToString()
		{
			return "Unexpected end of code.";
		}
	}

	public class TestMetaData {
		public TestMetaData() {
			Skip = false;
			HasAst = false;
			IsDiagnostic = false;
		}

		public bool Valid { get; set; }
		public bool IsDiagnostic { get; set; }
		public bool HasAst { get; set; }
		public bool Skip { get; set; }
		public string ShortFileName { get; set; }
		public string FileName { get; set; }
		public AST Ast { get; set; }

		public List<string> FileContent { get; set; }
		public List<TestDiagnostic> Diagnostics { get; set; }
	}


	class TestExplorer : IEnumerable<TestMetaData>
	{
		private IList<TestMetaData> _tests;
		private static string _baseDir = "/examples";

		public TestExplorer()
		{
			_tests = new List<TestMetaData>();

			if (Directory.Exists($"{Environment.CurrentDirectory}{_baseDir}"))
			{
				ProcessDirectory($"{Environment.CurrentDirectory}{_baseDir}");
			}
		}

		public void HandleFile(string fileName)
		{
			var lines = System.IO.File.ReadAllLines(fileName);

			if (lines.Length > 0)
			{
				if (lines[0].StartsWith("#CIM_TEST"))
				{
					int j = 1;
					string meta = "";
					while (j != lines.Length && !lines[j].StartsWith("#CIM_TEST")) {
						meta += lines[j].Length > 0 ? lines[j].Substring(1) + '\n' : "\n";
						j++;
					}

					if (j == lines.Length)
						return;

					var deserializer = new DeserializerBuilder()
						.WithNamingConvention(CamelCaseNamingConvention.Instance)
						.WithTagMapping("!program", typeof(Program))
						.WithTagMapping("!boolLiteral", typeof(BoolLiteral))
						.WithTagMapping("!intLiteral", typeof(IntLiteral))
						.WithTagMapping("!varName", typeof(VarName))
						.WithTagMapping("!varUse", typeof(VarUse))
						.WithTagMapping("!functionName", typeof(FunctionName))
						.WithTagMapping("!typeName", typeof(TypeName))
						.WithTagMapping("!binaryOperation", typeof(BinaryOperation))
						.WithTagMapping("!plus", typeof(Plus))
						.WithTagMapping("!mult", typeof(Mult))
						.WithTagMapping("!div", typeof(Div))
						.WithTagMapping("!sub", typeof(Sub))
						.WithTagMapping("!eq", typeof(Eq))
						.WithTagMapping("!leq", typeof(Leq))
						.WithTagMapping("!and", typeof(And))
						.WithTagMapping("!or", typeof(Or))
						.WithTagMapping("!unaryOperation", typeof(UnaryOperation))
						.WithTagMapping("!negation", typeof(Negation))
						.WithTagMapping("!functionCall", typeof(FunctionCall))
						.WithTagMapping("!assignment", typeof(Assignment))
						.WithTagMapping("!declareAssignment", typeof(DeclareAssignment))
						.WithTagMapping("!ifClause", typeof(IfClause))
						.WithTagMapping("!elIfClause", typeof(ElIfClause))
						.WithTagMapping("!whileClause", typeof(WhileClause))
						.WithTagMapping("!functionDeclaration", typeof(FunctionDeclaration))
						.WithTagMapping("!argument", typeof(Argument))
						.WithTagMapping("!returnClause", typeof(ReturnClause))
						.WithTagMapping("!block", typeof(Block))
						.WithTagMapping("!breakClause", typeof(BreakClause))
						//diagnostics
						.WithTagMapping("!lexerTestDiagnostic", typeof(LexerTestDiagnostic))
						.WithTagMapping("!unknownTestDiagnostic", typeof(UnknownTestDiagnostic))
						.WithTagMapping("!noExpectedTestDiagnostic", typeof(NoExpectedTestDiagnostic))
						.Build();

					try
					{
						var testMetaData = deserializer.Deserialize<TestMetaData>(meta);

						var fileContent = new List<string>(lines).Skip(j + 1).ToList();

						if (testMetaData.Skip)
							return;

						testMetaData.ShortFileName = Path.GetFileName(fileName);
						testMetaData.FileName = fileName;
						testMetaData.FileContent = fileContent;

						if (testMetaData.Valid)
						{
							testMetaData.Diagnostics = new List<TestDiagnostic>();
							testMetaData.IsDiagnostic = true;

							_tests.Add(testMetaData);
						}
					}
					catch (Exception)
					{
						return;
					}
				}
			}
		}

		public void ProcessDirectory(string targetDirectory)
		{
			string[] fileEntries = Directory.GetFiles(targetDirectory);
			foreach (string fileName in fileEntries)
				HandleFile(fileName);

			// Recurse into subdirectories of this directory.
			string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
			foreach (string subdirectory in subdirectoryEntries)
				ProcessDirectory(subdirectory);
		}


		public IEnumerator<TestMetaData> GetEnumerator()
		{
			return _tests.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	//new object[] { fileContent, new List<Cimplified.Diagnostic>() 

	public class DiagnosticTestExplorer : IEnumerable<object[]>
	{
		private readonly IList<object[]> _tests;
		public DiagnosticTestExplorer()
		{
			var testExplorer = new TestExplorer();

			_tests = testExplorer.Where(test => test.IsDiagnostic).Select(testMetaData => new object[] { testMetaData.FileContent, testMetaData.Diagnostics, testMetaData.ShortFileName }).ToList();
		}
		public IEnumerator<object[]> GetEnumerator()
		{
			return _tests.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	public class AstTestExplorer : IEnumerable<object[]>
	{
		private readonly IList<object[]> _tests;
		public AstTestExplorer()
		{
			var testExplorer = new TestExplorer();

			_tests = testExplorer.Where(test => test.HasAst).Select(testMetaData => new object[] { testMetaData.FileContent, testMetaData.Ast }).ToList();
		}
		public IEnumerator<object[]> GetEnumerator()
		{
			return _tests.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
