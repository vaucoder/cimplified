﻿using Cimplified;
using Cimplified.Parser;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace tests
{
    public class ExtractCFGTest
    {
        private TypeName IntType = new TypeName("Int");
        private TypeName BoolType = new TypeName("Bool");
        private TypeName VoidType = new TypeName("Void");

        private FunctionName mainName = new FunctionName("main");

        private VarName aName = new VarName("a");
        private VarName bName = new VarName("b");

        private IntLiteral number0 = new IntLiteral("0");
        private IntLiteral number1 = new IntLiteral("1");
        private IntLiteral number2 = new IntLiteral("2");
        private IntLiteral number3 = new IntLiteral("3");
        private IntLiteral number4 = new IntLiteral("4");
        private IntLiteral number5 = new IntLiteral("5");

        private BoolLiteral trueCondition = new BoolLiteral(true);
        private BoolLiteral trueCondition2 = new BoolLiteral(true);
        private BoolLiteral trueCondition3 = new BoolLiteral(true);
        private BoolLiteral falseCondition = new BoolLiteral(false);

        static void compare(ControlFlowGraph CFG, string expected)
        {
            var s = "";
            using (new Writer((o) => s += o)) {
                Utils.PrintCFG(CFG);
            }
            Assert.Equal(expected, s);
        }

        [Fact]
        public void TestIf()
        {
            /*
             * Void main()
             * {
             *      Int a = 5;
             *      Int b = 0;
             *      if (True) {
             *          b = 1;
             *      } else {
             *          if (True) {
             *              b = 2;
             *          } else {
             *              if (True) {
             *                  b = 3;
             *              } else {
             *                  b = 4;
             *              }
             *          }
             *     }
             * }
             */
            var aDecl = new DeclareAssignment(IntType, aName, number5);
            var bDecl = new DeclareAssignment(IntType, bName, number0);

            var assignment1 = new Assignment(bName, number1);
            var assignment2 = new Assignment(bName, number2);
            var assignment3 = new Assignment(bName, number3);
            var assignment4 = new Assignment(bName, number4);

            var if3 = new IfClause(new BoolLiteral(true), new Block(assignment3), null, new Block(assignment4));
            var if2 = new IfClause(new BoolLiteral(true), new Block(assignment2), null, new Block(if3));
            var if1 = new IfClause(new BoolLiteral(true), new Block(assignment1), null, new Block(if2));

            var mainBody = new Block(new List<Statement>() { aDecl, bDecl, if1 });
            var main = new FunctionDeclaration(VoidType, mainName, null, mainBody);

            var CFG = ExtractControlFlowGraph.Extract1(mainBody);
            compare(CFG,
                    "digraph G {\n" +
                    "	node [shape = box];\n" +
                    "\n" +
                    "	a0 [label = \"(DeclareAssignment (TypeName Int) (VarName a) 5)\"];\n" +
                    "\n" +
                    "	a1 [label = \"(DeclareAssignment (TypeName Int) (VarName b) 0)\"];\n" +
                    "\n" +
                    "	a2 [label = \"True\"];\n" +
                    "\n" +
                    "	a3 [label = \"(Assignment (VarName b) 1)\"];\n" +
                    "	a2 -> a3 [label = \"True\"];\n" +
                    "\n" +
                    "	a4 [label = \"True\"];\n" +
                    "\n" +
                    "	a5 [label = \"(Assignment (VarName b) 2)\"];\n" +
                    "	a4 -> a5 [label = \"True\"];\n" +
                    "\n" +
                    "	a6 [label = \"True\"];\n" +
                    "\n" +
                    "	a7 [label = \"(Assignment (VarName b) 3)\"];\n" +
                    "	a6 -> a7 [label = \"True\"];\n" +
                    "\n" +
                    "	a8 [label = \"(Assignment (VarName b) 4)\"];\n" +
                    "	a6 -> a8 [label = \"False\"];\n" +
                    "	a4 -> a6 [label = \"False\"];\n" +
                    "	a2 -> a4 [label = \"False\"];\n" +
                    "	a1 -> a2;\n" +
                    "	a0 -> a1;\n" +
                    "}\n");
        }

        [Fact]
        public void TestWhile()
        {
            /*
             * Void main()
             * {
             *      while (True)
             *      {
             *          Int a = 5;
             *      }
             *      Int b = 1;
             * }
             */

            var aDecl = new DeclareAssignment(IntType, aName, number5);
            var bDecl = new DeclareAssignment(IntType, bName, number1);

            var whileBlock = new Block(new List<Statement>() { aDecl });
            var whileClause = new WhileClause(trueCondition, whileBlock);

            var mainBody = new Block(new List<Statement>() { whileClause, bDecl });
            var main = new FunctionDeclaration(VoidType, mainName, null, mainBody);

            var CFG = ExtractControlFlowGraph.Extract1(mainBody);
            compare(CFG,
                    "digraph G {\n" +
                    "	node [shape = box];\n" +
                    "\n" +
                    "	a0 [label = \"True\"];\n" +
                    "\n" +
                    "	a1 [label = \"(DeclareAssignment (TypeName Int) (VarName a) 5)\"];\n" +
                    "	a1 -> a0;\n" +
                    "	a0 -> a1 [label = \"True\"];\n" +
                    "\n" +
                    "	a2 [label = \"(DeclareAssignment (TypeName Int) (VarName b) 1)\"];\n" +
                    "	a0 -> a2 [label = \"False\"];\n" +
                    "}\n");
        }

        [Fact]
        public void TestReturn()
        {
            /*
             * Void main()
             * {
             *      if (True)
             *          return;
             *      Int a = 5;
             *      Int b = 1;
             * }
             */

            var aDecl = new DeclareAssignment(IntType, aName, number5);
            var bDecl = new DeclareAssignment(IntType, bName, number1);

            var returnClause = new ReturnClause(null);
            var ifBody = new Block(new List<Statement>() { returnClause });
            var ifClause = new IfClause(trueCondition, ifBody, null, null);

            var mainBody = new Block(new List<Statement>() { ifClause, aDecl, bDecl });
            var main = new FunctionDeclaration(VoidType, mainName, null, mainBody);

            var CFG = ExtractControlFlowGraph.Extract1(mainBody);
            compare(CFG,
                    "digraph G {\n" +
                    "	node [shape = box];\n" +
                    "\n" +
                    "	a0 [label = \"True\"];\n" +
                    "\n" +
                    "	a1 [label = \"(ReturnClause)\"];\n" +
                    "	a0 -> a1 [label = \"True\"];\n" +
                    "\n" +
                    "	a2 [label = \"(DeclareAssignment (TypeName Int) (VarName a) 5)\"];\n" +
                    "\n" +
                    "	a3 [label = \"(DeclareAssignment (TypeName Int) (VarName b) 1)\"];\n" +
                    "	a2 -> a3;\n" +
                    "	a0 -> a2 [label = \"False\"];\n" +
                    "}\n");
        }

        [Fact]
        public void TestContinue()
        {
            /*
             * Void main()
             * {
             *      while (True)
             *      {
             *          Int a = 5;
             *          if (True)
             *              continue;
             *          Int b = 1;
             *      }
             * }
             */
            var aDecl = new DeclareAssignment(IntType, aName, number5);
            var bDecl = new DeclareAssignment(IntType, bName, number1);

            var continueClause = new ContinueClause();

            var ifBody = new Block(new List<Statement>() { continueClause });
            var ifClause = new IfClause(trueCondition, ifBody, null, null);

            var whileBlock = new Block(new List<Statement>() { aDecl, ifClause, bDecl });
            var whileClause = new WhileClause(trueCondition, whileBlock);

            var mainBody = new Block(new List<Statement>() { whileClause });
            var main = new FunctionDeclaration(VoidType, mainName, null, mainBody);

            var CFG = ExtractControlFlowGraph.Extract1(mainBody);
            compare(CFG,
                    "digraph G {\n" +
                    "	node [shape = box];\n" +
                    "\n" +
                    "	a0 [label = \"True\"];\n" +
                    "\n" +
                    "	a1 [label = \"(DeclareAssignment (TypeName Int) (VarName a) 5)\"];\n" +
                    "\n" +
                    "	a2 [label = \"True\"];\n" +
                    "\n" +
                    "	a3 [label = \"(ContinueClause)\"];\n" +
                    "	a3 -> a0;\n" +
                    "	a2 -> a3 [label = \"True\"];\n" +
                    "\n" +
                    "	a4 [label = \"(DeclareAssignment (TypeName Int) (VarName b) 1)\"];\n" +
                    "	a4 -> a0;\n" +
                    "	a2 -> a4 [label = \"False\"];\n" +
                    "	a1 -> a2;\n" +
                    "	a0 -> a1 [label = \"True\"];\n" +
                    "}\n");
        }

        [Fact]
        public void TestBreak()
        {
            /*
             * Void main()
             * {
             *      while (True)
             *      {
             *          while (True)
             *          {
             *              if (True)
             *                  break;
             *              else
             *                  break 2;
             *          }
             *          Int a = 5;
             *      }
             *      Int b = 1;
             * }
             */

            var aDecl = new DeclareAssignment(IntType, aName, number5);
            var bDecl = new DeclareAssignment(IntType, bName, number1);

            var breakClause1 = new BreakClause(number1);
            var breakClause2 = new BreakClause(number2);

            var elseBody = new Block(new List<Statement>() { breakClause2 });
            var ifBody = new Block(new List<Statement>() { breakClause1 });
            var ifClause = new IfClause(trueCondition, ifBody, null, elseBody);

            var whileInternalBody = new Block(new List<Statement>() { ifClause });
            var whileInternalClause = new WhileClause(trueCondition, whileInternalBody);

            var whileExternalBody = new Block(new List<Statement>() { whileInternalClause, aDecl });
            var whileExternalClause = new WhileClause(trueCondition, whileExternalBody);

            var mainBody = new Block(new List<Statement>() { whileExternalClause, bDecl });
            var main = new FunctionDeclaration(VoidType, mainName, null, mainBody);

            var CFG = ExtractControlFlowGraph.Extract1(mainBody);
            compare(CFG,
                    "digraph G {\n" +
                    "	node [shape = box];\n" +
                    "\n" +
                    "	a0 [label = \"True\"];\n" +
                    "\n" +
                    "	a1 [label = \"True\"];\n" +
                    "\n" +
                    "	a2 [label = \"True\"];\n" +
                    "\n" +
                    "	a3 [label = \"(BreakClause 1)\"];\n" +
                    "	a3 -> a1;\n" +
                    "	a2 -> a3 [label = \"True\"];\n" +
                    "\n" +
                    "	a4 [label = \"(BreakClause 2)\"];\n" +
                    "	a4 -> a0;\n" +
                    "	a2 -> a4 [label = \"False\"];\n" +
                    "	a1 -> a2 [label = \"True\"];\n" +
                    "\n" +
                    "	a5 [label = \"(DeclareAssignment (TypeName Int) (VarName a) 5)\"];\n" +
                    "	a5 -> a0;\n" +
                    "	a1 -> a5 [label = \"False\"];\n" +
                    "	a0 -> a1 [label = \"True\"];\n" +
                    "\n" +
                    "	a6 [label = \"(DeclareAssignment (TypeName Int) (VarName b) 1)\"];\n" +
                    "	a0 -> a6 [label = \"False\"];\n" +
                    "}\n");
        }

        [Fact]
        public void TestShortCircuit()
		{
            /*
             * Void main()
             * {
             *      Bool b = True || False;
             * }
             */

            var orExpression = new Or() { Left = trueCondition, Right = falseCondition, Type = Type.Bool };
            var bDecl = new DeclareAssignment(BoolType, bName, orExpression);

            var mainBody = new Block(new List<Statement>() { bDecl });
            var main = new FunctionDeclaration(VoidType, mainName, null, mainBody);

            var CFG = ExtractControlFlowGraph.Extract1(mainBody);
            compare(CFG,
                "digraph G {\n" +
                "	node [shape = box];\n" +
                "\n" +
                "	a0 [label = \"(DeclareAssignment (TypeName Bool) (VarName b) (Or True False))\"];\n" +
                "}\n");
        }

        
    }
}
