﻿using Cimplified;
using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

using ExprType = Cimplified.Parser.Type;

namespace tests
{
	[Collection("UsingPrettyPrint")]
	public class ExtractShortCicuitTest
	{
		public static List<(Statement, Statement)> extractionStatements()
		{
			var value = new Or() { Left = new BoolLiteral(false), Right = new VarUse(new VarName("y")) };

			value.Type = ExprType.Bool;

			var boolFalse = new BoolLiteral(false);
			boolFalse.Type = ExprType.Bool;


			// Bool x = false or y 
			// -->
			// Bool x = true
			// if(false or y) x = true;
			// else x = false;
			var first = (
					new DeclareAssignment(new TypeName("Bool"), new VarName("x"), value),
					new FlatGroup(new List<Statement>()
					{
						new DeclareAssignment(new TypeName("Bool"), new VarName("x"), new BoolLiteral(true)),
						new IfClause(value,
							new Block(new List<Statement>() {
								new Assignment(new VarName("x"), new BoolLiteral(true))
							}),
							new List<ElIfClause>(),
							new Block(new List<Statement>() {
								new Assignment(new VarName("x"), new BoolLiteral(false))
							})
						)
					})
				);

			var secondExpr = new And() { Left = new BoolLiteral(true), Right = new VarUse(new VarName("y")) };
			secondExpr.Type = ExprType.Bool;

			// foo(true and y) 
			// -->
			// Bool $s_c0 = true
			//
			// if(true and y) $s_c0 = true;
			// else $s_c0 = false;
			//
			// foo($s_c0);
			var second = (
				new FunctionCall(new FunctionName("foo"), new List<Expression>() { secondExpr }),
				new FlatGroup(new List<Statement>() {
					new DeclareAssignment(new TypeName("Bool"), VarName.FreshTemporary("$s_c0"), new BoolLiteral(true)),
					new IfClause(secondExpr,
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c0"), new BoolLiteral(true)) }),
						new List<ElIfClause>(),
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c0"), new BoolLiteral(false)) })
					),
					new FunctionCall(new FunctionName("foo"), new List<Expression>(){new VarUse(VarName.FreshTemporary("$s_c0")) })
				})
			);


			// foo(false) 
			// -->
			// foo(false) 
			var third = (new FunctionCall(new FunctionName("a"), new List<Expression>() { boolFalse }),
				new FunctionCall(new FunctionName("a"), new List<Expression>() { boolFalse })
			);

			var fourthExpr = new And() { Left = new Or() { Left = new VarUse(new VarName("z")), Right = new VarUse(new VarName("w")) }, Right = new VarUse(new VarName("y")) };
			fourthExpr.Type = ExprType.Bool;

			var thirdArg = new IntLiteral("120");
			thirdArg.Type = ExprType.Int;

			// foo(true and y, (z or w) and y, 120) 
			// -->
			// Bool $s_c1 = true
			//
			// if(true and y) $s_c1 = true;
			// else $s_c1 = false;
			//
			// Bool $s_c2 = true
			//
			// if((z or w) and y) $s_c2 = true;
			// else $s_c2 = false;
			//
			// foo($s_c0);
			var fourth = (
				new FunctionCall(new FunctionName("foo"), new List<Expression>() { secondExpr, fourthExpr, thirdArg }),
				new FlatGroup(new List<Statement>() {
					new DeclareAssignment(new TypeName("Bool"), VarName.FreshTemporary("$s_c1"), new BoolLiteral(true)),
					new IfClause(secondExpr,
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c1"), new BoolLiteral(true)) }),
						new List<ElIfClause>(),
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c1"), new BoolLiteral(false)) })
					),
					new DeclareAssignment(new TypeName("Bool"), VarName.FreshTemporary("$s_c2"), new BoolLiteral(true)),
					new IfClause(fourthExpr,
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c2"), new BoolLiteral(true)) }),
						new List<ElIfClause>(),
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c2"), new BoolLiteral(false)) })
					),
					new FunctionCall(new FunctionName("foo"), new List<Expression>(){new VarUse(VarName.FreshTemporary("$s_c1")), new VarUse(VarName.FreshTemporary("$s_c2")), thirdArg })
				})
			);


			// return true and y;
			// -->
			// Bool $s_c3 = true
			//
			// if(true and y) $s_c3 = true;
			// else $s_c3 = false;
			//
			// return $s_c3;
			var fifth = (
				new ReturnClause(secondExpr),
				new FlatGroup(new List<Statement>() {
					new DeclareAssignment(new TypeName("Bool"), VarName.FreshTemporary("$s_c3"), new BoolLiteral(true)),
					new IfClause(secondExpr,
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c3"), new BoolLiteral(true)) }),
						new List<ElIfClause>(),
						new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c3"), new BoolLiteral(false)) })
					),
					new ReturnClause(new VarUse(VarName.FreshTemporary("$s_c3")))
				})
			);
			// {
			//	foo(true and y, (z or w) and y, 120) 
			//	return true and y;
			// }
			// -->
			// {
			//	Bool $s_c1 = true
			//
			//	if(true and y) $s_c1 = true;
			//	else $s_c1 = false;
			//
			//	Bool $s_c2 = true
			//
			//	if((z or w) and y) $s_c2 = true;
			//	else $s_c2 = false;
			//
			//	foo($s_c0);
			// }
			// {
			//	Bool $s_c3 = true
			//
			//	if(true and y) $s_c3 = true;
			//	else $s_c3 = false;
			//
			//	return $s_c3;
			// }
			var sixth = (
				new Block(new List<Statement>() { new FunctionCall(new FunctionName("foo"), new List<Expression>() { secondExpr, fourthExpr, thirdArg }), new ReturnClause(secondExpr)}),
				new FlatGroup(new List<Statement>() {
					new FlatGroup( new List<Statement>() {
						new DeclareAssignment(new TypeName("Bool"), VarName.FreshTemporary("$s_c4"), new BoolLiteral(true)),
						new IfClause(secondExpr,
							new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c4"), new BoolLiteral(true)) }),
							new List<ElIfClause>(),
							new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c4"), new BoolLiteral(false)) })
						),
						new DeclareAssignment(new TypeName("Bool"), VarName.FreshTemporary("$s_c5"), new BoolLiteral(true)),
						new IfClause(fourthExpr,
							new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c5"), new BoolLiteral(true)) }),
							new List<ElIfClause>(),
							new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c5"), new BoolLiteral(false)) })
						),
						new FunctionCall(new FunctionName("foo"), new List<Expression>(){new VarUse(VarName.FreshTemporary("$s_c4")), new VarUse(VarName.FreshTemporary("$s_c5")), thirdArg }),
					}), new FlatGroup( new List<Statement>() {
						new DeclareAssignment(new TypeName("Bool"), VarName.FreshTemporary("$s_c6"), new BoolLiteral(true)),
						new IfClause(secondExpr,
							new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c6"), new BoolLiteral(true)) }),
							new List<ElIfClause>(),
							new Block(new List<Statement> () { new Assignment(VarName.FreshTemporary("$s_c6"), new BoolLiteral(false)) })
						),
						new ReturnClause(new VarUse(VarName.FreshTemporary("$s_c6")))
					})
				})
			);

			return new List<(Statement, Statement)>() {
				first,
				second,
				third,
				fourth,
				fifth,
				sixth
			};
		}
		public static List<Statement> noExtractionStatements()
		{
			var boolFalse = new BoolLiteral(false);
			boolFalse.Type = ExprType.Bool;

			var regularSum = new Plus() { Left = new IntLiteral("123"), Right = new IntLiteral("321") };
			regularSum.Type = ExprType.Int;

			// Bool x = false;
			var first = new DeclareAssignment(new TypeName("Bool"), new VarName("x"), boolFalse);

			// test(false, 123 + 321);
			var second = new FunctionCall(new FunctionName("test"), new List<Expression>() { boolFalse, regularSum });

			// false and y;
			var third = new And() { Left = new BoolLiteral(false), Right = new VarUse(new VarName("y")) };
			third.Type = ExprType.Bool;

			return new List<Statement>(){
				first,
				second,
				third
			};
		}


		public static IEnumerable<object[]> Extractable = extractionStatements().Select(p => new object[] { p.Item1, p.Item2 });
		public static IEnumerable<object[]> NoExtraction = noExtractionStatements().Select(p => new object[] { p });


		[Theory]
		[MemberData(nameof(NoExtraction))]
		public void Should_Not_Extract(Statement input)
		{
			var extracted = ExtractShortCircuits.Extract1(input);

			Assert.Equal(input, extracted);
		}

		[Theory]
		[MemberData(nameof(Extractable))]
		public void Should_Extract_Operation(Statement input, Statement expected)
		{
			var extracted = ExtractShortCircuits.Extract1(input);

			var exptectedStr = "";
			using (new Writer((str) => exptectedStr += str))
			{
				PrettyNodeVisitor.Print(expected, true);
			}

			var actualStr = "";
			using (new Writer((str) => actualStr += str))
			{
				PrettyNodeVisitor.Print(extracted, true);
			}

			Assert.Equal(exptectedStr, actualStr);
		}
	}
}
