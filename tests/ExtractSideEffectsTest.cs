﻿using Cimplified;
using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace tests
{
    public class ExtractSideEffectsTest
    {
        [Fact]
        public void NoFunctionCallTest()
        {
            // x + 1
            var x = new VarUse(new VarName("x"));
            var one = new IntLiteral("0");
            var plusOp = new Plus();
            plusOp.Left = x;
            plusOp.Right = one;

            var (l, e) = ExtractSideEffects.Extract1(plusOp);

            Assert.Empty(l);
            Assert.Equal(plusOp, e);
            Assert.Equal(plusOp.Left, x);
            Assert.Equal(plusOp.Right, one);
        }

        [Fact]
        public void SimpleFunctionCallTest()
        {
            // f(x + 1) + g()
            var x = new VarUse(new VarName("x"));
            var one = new IntLiteral("0");
            var innerPlusOp = new Plus();
            innerPlusOp.Left = x;
            innerPlusOp.Right = one;

            var fDecl = new FunctionDeclaration(null, new FunctionName("f"), new List<Argument>{new Argument(null, new VarName("arg1"))}, new Block(new List<Statement>()));
            fDecl.FunName.Declaration = fDecl;
            var fCall = new FunctionCall(new FunctionName("f"), new List<Expression>{innerPlusOp});
            
            var gDecl = new FunctionDeclaration(null, new FunctionName("g"), new List<Argument>(), new Block(new List<Statement>()));
            gDecl.FunName.Declaration = gDecl;
            var gCall = new FunctionCall(new FunctionName("g"), new List<Expression>());
            
            var outerPlusOp = new Plus();
            outerPlusOp.Left = fCall;
            outerPlusOp.Right = gCall;

            var (l, e) =  ExtractSideEffects.Extract1(outerPlusOp);

            Assert.Equal(2, l.Count);
            var x1Decl = (DeclareAssignment)l[0];
            var x2Decl = (DeclareAssignment)l[1];
            Assert.Equal(fCall, x1Decl.Value);
            Assert.Equal(fCall.Arguments[0], innerPlusOp);
            Assert.Equal(gCall, x2Decl.Value);
            var plusOp = (Plus) e;
            var x1Use = (VarUse)plusOp.Left;
            var x2Use = (VarUse)plusOp.Right;
            Assert.Equal(x1Decl, x1Use.Name.Declaration);
            Assert.Equal(x2Decl, x2Use.Name.Declaration);
        }

        [Fact]
        public void NestedFunctionCallTest()
        {
           // f(x + g())
            var x = new VarUse(new VarName("x"));
            
            var gDecl = new FunctionDeclaration(null, new FunctionName("g"), new List<Argument>(), new Block(new List<Statement>()));
            gDecl.FunName.Declaration = gDecl;
            var gCall = new FunctionCall(new FunctionName("g"), new List<Expression>());
            
            var plusOp = new Plus();
            plusOp.Left = x;
            plusOp.Right = gCall;

            var fDecl = new FunctionDeclaration(null, new FunctionName("f"), new List<Argument>{new Argument(null, new VarName("arg1"))}, new Block(new List<Statement>()));
            fDecl.FunName.Declaration = fDecl;
            var fCall = new FunctionCall(new FunctionName("f"), new List<Expression>{plusOp});

            var (l, e) =  ExtractSideEffects.Extract1(fCall);

            Assert.Equal(2, l.Count);
            var x1Decl = (DeclareAssignment)l[0];
            var x2Decl = (DeclareAssignment)l[1];
            Assert.Equal(gCall, x1Decl.Value);
            Assert.Equal(fCall, x2Decl.Value);
            var x2Use = (VarUse) e;
            Assert.Equal(x2Decl, x2Use.Name.Declaration);
        }

    }
}
