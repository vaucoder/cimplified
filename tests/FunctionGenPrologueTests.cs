﻿using Cimplified;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace tests
{
	public class FunctionGenPrologueTests
	{
		[Fact]
		public void TestGenPrologue()
		{
			var function = new Function(null);

			function.CreateArgument();
			function.CreateArgument();
			function.CreateArgument();

			int i = 10;

			while (i > 0) { 
				function.CreateVariable();
				i--;
			}

			var ops = function.GeneratePrologue();

			Assert.True(ops[0] is MemoryWrite);

			var firstAssign = ops[0] as MemoryWrite;

			Assert.True(firstAssign.Address is Arithmetic.Plus);

			var plus = firstAssign.Address as Arithmetic.Plus;

			Assert.True(plus.Right is ConstValue);
			Assert.True(plus.Left is RegisterAccess);

			Assert.True(((plus.Left as RegisterAccess).Register as HwRegister).Name == "RSP");

			Assert.Equal(-1 * Utils.ElementSize, (plus.Right as ConstValue).Value);


			var third = ops[2] as RegisterWrite;

			var thirdValue = (third.Value as Arithmetic.Plus).Right as ConstValue;

			Assert.Equal(-1 * Utils.ElementSize, thirdValue.Value);
		}

		[Fact]
		public void TestGenPrologue2()
		{
			var function = new Function(null);

			int i = 10;
			while (i > 0)
			{
				function.CreateArgument();
				i--;
			}


			i = 10;
			while (i > 0)
			{
				function.CreateVariable();
				i--;
			}

			var ops = function.GeneratePrologue();

			Assert.True(ops[0] is MemoryWrite);

			var firstAssign = ops[0] as MemoryWrite;

			Assert.True(firstAssign.Address is Arithmetic.Plus);

			var plus = firstAssign.Address as Arithmetic.Plus;

			Assert.True(plus.Right is ConstValue);
			Assert.True(plus.Left is RegisterAccess);

			Assert.True(((plus.Left as RegisterAccess).Register as HwRegister).Name == "RSP");

			Assert.Equal(-1 * Utils.ElementSize, (plus.Right as ConstValue).Value);

			var third = ops[2] as RegisterWrite;

			var thirdValue = (third.Value as Arithmetic.Plus).Right as ConstValue;

			Assert.Equal(-1 * Utils.ElementSize, thirdValue.Value);
		}

		[Fact]
		public void TestGenEpilogue()
		{
			var function = new Function(null);

			function.CreateArgument();
			function.CreateArgument();
			function.CreateArgument();

			int i = 10;

			while (i > 0)
			{
				function.CreateVariable();
				i--;
			}

			var ops = function.GenerateEpilogue();

			var second = ops[1] as RegisterWrite;

			var secondRight = ((second.Value as MemoryAccess).Address as Arithmetic.Plus).Right as ConstValue;

			Assert.Equal(-1 * Utils.ElementSize,secondRight.Value);
		}

		[Fact]
		public void TestGenEpilogue2()
		{
			var function = new Function(null);

			int i = 10;
			while (i > 0)
			{
				function.CreateArgument();
				i--;
			}


			i = 10;
			while (i > 0)
			{
				function.CreateVariable();
				i--;
			}

			var ops = function.GenerateEpilogue();

			var second = ops[1] as RegisterWrite;

			var secondRight = ((second.Value as MemoryAccess).Address as Arithmetic.Plus).Right as ConstValue;

			Assert.Equal(-1 * Utils.ElementSize, secondRight.Value);
		}
	}
}
