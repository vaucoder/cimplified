using Cimplified;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tests
{

	public class GenerateCallTests
	{

		static (Function, Function, List<ValueOperation>, ValueOperation, List<Operation>) case1()
		{
			var f2 = new Function(null, "f2");
			var f3 = new Function(null, "f3");

			var result = (ValueOperation)null;
			var args = new List<ValueOperation>();

			var expected = new List<Operation> () {
				new Call(f2.Label),
				new Noop(), // writing result
			};

			return (f2, f3, args, result, expected);
		}

		static (Function, Function, List<ValueOperation>, ValueOperation, List<Operation>) case2()
		{
			var f1 = new Function(null, "f1");
			var f2 = new Function(null, "f2");
			var f3 = new Function(null, "f3");

			var result = (ValueOperation)null;
			var args = new List<ValueOperation>();

			var expected = new List<Operation> () {
				new Call(f2.Label),
				new Noop(), // writing result
			};

			return (f2, f3, args, result, expected);
		}

		static (Function, Function, List<ValueOperation>, ValueOperation, List<Operation>) case3()
		{
			var f1 = new Function(null, "f1");
			var f2 = new Function(f1, "f2");
			var f3 = new Function(f1, "f3");

			var result = (ValueOperation)null;
			var args = new List<ValueOperation>();

			var expected = new List<Operation> () {
				new Push(
					new MemoryAccess(
						new Arithmetic.Plus(
							new RegisterAccess(HwRegister.RBP),
							new ConstValue(2 * Utils.ElementSize)))), // pushing static link
				new Call(f2.Label),
				new Noop(), // writing result
				new RegisterWrite(
					HwRegister.RSP,
					new Arithmetic.Plus(
						new RegisterAccess(HwRegister.RSP),
						new ConstValue(1 * Utils.ElementSize))), // restoring stack pointer
			};

			return (f2, f3, args, result, expected);
		}

		static (Function, Function, List<ValueOperation>, ValueOperation, List<Operation>) case4()
		{
			var f1 = new Function(null, "f1");
			var f2 = new Function(f1, "f2");
			var f3 = new Function(f1, "f3");
			var f4 = new Function(f3, "f4");
			var f5 = new Function(f4, "f5");

			var result = (ValueOperation)null;
			var args = new List<ValueOperation>();

			var expected = new List<Operation> () {
				new Push(
					new MemoryAccess(
						new Arithmetic.Plus(
							new MemoryAccess(
								new Arithmetic.Plus(
									new MemoryAccess(
										new Arithmetic.Plus(
											new RegisterAccess(HwRegister.RBP),
											new ConstValue(2 * Utils.ElementSize))),
									new ConstValue(2 * Utils.ElementSize))),
							new ConstValue(2 * Utils.ElementSize)))), // pusing static link
				new Call(f2.Label),
				new Noop(), // writing result
				new RegisterWrite(
					HwRegister.RSP,
					new Arithmetic.Plus(
						new RegisterAccess(HwRegister.RSP),
						new ConstValue(1 * Utils.ElementSize))), // restoring stack pointer
			};

			return (f2, f5, args, result, expected);
		}

		static (Function, Function, List<ValueOperation>, ValueOperation, List<Operation>) case5()
		{
			var f2 = new Function(null, "f2");
			var f3 = new Function(null, "f3");

			var result = (ValueOperation)null;
			var args = new List<ValueOperation>();

			for (int i = 0; i < Function.ArgumentRegisters.Length + 2; i++) {
				args.Add(new ConstValue(2 + i));
			}

			var expected = new List<Operation>();

			for (int i = 0; i < Function.ArgumentRegisters.Length; i++) {
				expected.Add(new RegisterWrite(Function.ArgumentRegisters[i], new ConstValue(2 + i)));
			}

			expected.Add(new Push(new ConstValue(2 + Function.ArgumentRegisters.Length + 1)));
			expected.Add(new Push(new ConstValue(2 + Function.ArgumentRegisters.Length + 0)));

			expected.Add(new Call(f2.Label));

			expected.Add(new Noop()); // writing result

			expected.Add(
				new RegisterWrite(
					HwRegister.RSP,
					new Arithmetic.Plus(
						new RegisterAccess(HwRegister.RSP),
						new ConstValue(2 * Utils.ElementSize)))); // restoring rsp

			return (f2, f3, args, result, expected);
		}

		static List<(Function, Function, List<ValueOperation>, ValueOperation, List<Operation>)> data1 = new List<(Function, Function, List<ValueOperation>, ValueOperation, List<Operation>)>() {
			case1(),
			case2(),
			case3(),
			case4(),
			case5(),
		};

		public static IEnumerable<object[]> Data1 = data1.Select(p => new object[] { p.Item1, p.Item2, p.Item3, p.Item4, p.Item5 });

		[Theory]
		[MemberData(nameof(Data1))]
		public void Test1(Function callee, Function caller, List<ValueOperation> args, ValueOperation result, List<Operation> o)
		{
			var g = callee.GenerateCall(args, result, caller);
			Assert.Equal(g.Count, o.Count);
			for (int i = 0; i < g.Count; i++) {
				Assert.True(Utils.DeepEquals(g[i], o[i]));
			}
		}

	}

}
