using Cimplified;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tests
{

	public class GenerateVariableAccessTests
	{

		static (Function, Variable, Operation) case1()
		{
			var f = new Function(null);

			var v = f.CreateArgument();
			var op = new RegisterAccess(v.Reg);
			return (f, v, op);
		}

		static (Function, Variable, Operation) case2()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			var v = f.CreateArgument();
			var op = new RegisterAccess(v.Reg);
			return (f, v, op);
		}

		static (Function, Variable, Operation) case3()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			var v = f1.CreateArgument();
			v.AddUsagePlace(f);

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(-1 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case4()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Arguments < ArgumentRegisters.Length
			f1.CreateArgument().AddUsagePlace(f);
			f1.CreateArgument().AddUsagePlace(f);

			var v = f1.CreateArgument();
			v.AddUsagePlace(f);

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(-3 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case5()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Arguments.Length = ArgumentRegisters.Length + 1
			for (int i = 0; i < Function.ArgumentRegisters.Length; i++)
			{
				f1.CreateArgument().AddUsagePlace(f);
			}

			var v = f1.CreateArgument();
			v.AddUsagePlace(f);

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(2 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case6()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			var v = f1.CreateArgument();
			v.AddUsagePlace(f);

			// Arguments.Length = ArgumentRegisters.Length + 1
			for (int i = 0; i < Function.ArgumentRegisters.Length; i++)
			{
				f1.CreateArgument().AddUsagePlace(f);
			}

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(-1 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case7()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Some offset
			f1.CreateVariable();
			f1.CreateVariable();
			f1.CreateVariable();

			var v = f1.CreateArgument();
			v.AddUsagePlace(f);

			// Arguments.Length = ArgumentRegisters.Length + 1
			for (int i = 0; i < Function.ArgumentRegisters.Length; i++)
			{
				f1.CreateArgument().AddUsagePlace(f);
			}

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(-1 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case8()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Some offset
			f1.CreateVariable();
			f1.CreateVariable();
			f1.CreateVariable();

			var v = f.CreateVariable();

			var op = new RegisterAccess(v.Reg);
			return (f, v, op);
		}

		static (Function, Variable, Operation) case9()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Some offset
			f.CreateVariable();
			f.CreateVariable();
			f.CreateVariable();

			var v = f.CreateVariable();

			var op = new RegisterAccess(v.Reg);
			return (f, v, op);
		}

		static (Function, Variable, Operation) case10()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Some offset
			f1.CreateVariable();
			f1.CreateVariable();
			f1.CreateVariable();

			var v = f1.CreateVariable();
			v.AddUsagePlace(f);

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(-1 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case11()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Some offset
			f1.CreateVariable();
			f1.CreateVariable();
			f1.CreateVariable();

			// Some argument offset
			// Arguments.Length = ArgumentRegisters.Length + 2
			for (int i = 0; i < Function.ArgumentRegisters.Length + 2; i++)
			{
				f1.CreateArgument().AddUsagePlace(f);
			}

			var v = f1.CreateVariable();
			v.AddUsagePlace(f);

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(-7 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case12()
		{
			var f1 = new Function(null);
			var f2 = new Function(f1);
			var f = new Function(f2);

			// Some offset
			f1.CreateVariable();
			f1.CreateVariable();
			f1.CreateVariable();

			var v = f1.CreateArgument();
			v.AddUsagePlace(f);

			var f_link = new RegisterAccess(HwRegister.RBP);
			var f2_link = new MemoryAccess(new Arithmetic.Plus(f_link, new ConstValue(2 * Utils.ElementSize)));
			var f1_link = new MemoryAccess(new Arithmetic.Plus(f2_link, new ConstValue(2 * Utils.ElementSize)));
			var op = new MemoryAccess(new Arithmetic.Plus(f1_link, new ConstValue(-1 * Utils.ElementSize)));
			return (f, v, op);
		}

		static (Function, Variable, Operation) case13()
		{
			var f1 = new Function(null);
			var f = new Function(f1);

			// Arguments.Length = ArgumentRegisters.Length + 1
			for (int i = 0; i < Function.ArgumentRegisters.Length; i++)
			{
				f1.CreateArgument().AddUsagePlace(f);
			}

			var v = f1.CreateArgument();
			v.AddUsagePlace(f);

			var op = new MemoryAccess(new Arithmetic.Plus(new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RBP), new ConstValue(2 * Utils.ElementSize))), new ConstValue(2 * Utils.ElementSize)));
			return (f, v, op);
		}

		static List<(Function, Variable, Operation)> data1 = new List<(Function, Variable, Operation)>() {
			case1(),
			case2(),
			case3(),
			case4(),
			case5(),
			case6(),
			case7(),
			case8(),
			case9(),
			case10(),
			case11(),
			case12(),
			case13(),
		};

		public static IEnumerable<object[]> Data1 = data1.Select(p => new object[] { p.Item1, p.Item2, p.Item3 });

		[Theory]
		[MemberData(nameof(Data1))]
		public void Test1(Function f, Variable v, Operation o)
		{
			var g = f.GenerateVariableAccess(v);
			Assert.True(Utils.DeepEquals(o, g));
		}

	}

}
