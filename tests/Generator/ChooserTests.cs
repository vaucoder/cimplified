using System;
using System.Collections.Generic;
using System.Linq;
using Cimplified;
using Cimplified.Generator;
using Xunit;

namespace tests.Generator
{
	using InstructionFunc = Func<Register, List<IInstructionParameter>, List<AsmInstruction>>;
	
	public class ChooserTest
	{
		private List<AsmInstruction> DummyF(Register reg, List<IInstructionParameter> parameters)
		{
			return new List<AsmInstruction>();
		}

		private List<(Operation, InstructionFunc)> GetDummyFPairs(params Operation[] ops)
		{
			var pairs = new List<(Operation, InstructionFunc)>();
			foreach (var op in ops)
				pairs.Add((op, DummyF));
			return pairs;
		}
		
		private class DummyInstruction : AsmInstruction
		{
			public List<IInstructionParameter> Instructions;

			public override IReadOnlyList<Register> RegistersUsed {
				get {
					var registers = new List<Register>();
					foreach (var instruction in Instructions)
						if (instruction is Register register)
							registers.Add(register);
					return registers;
				}
			}
		
			public override IReadOnlyList<Register> RegistersDefined => null;
			public override IReadOnlyList<Label> PossibleTargets {
				get {
					var labels = new List<Label>();
					foreach (var instruction in Instructions)
						if (instruction is Label label)
							labels.Add(label);
					return labels;
				}
			}


			public string Name;

			public override string ToString()
			{
				return Name;
			}

			public DummyInstruction(string name, params IInstructionParameter[] parameters)
			{
				Name = name;
				Instructions = new List<IInstructionParameter>(parameters);
			}

			public override string ToAsmString()
			{
				return "";
			}
		}

		[Fact]
		public void SortTest()
		{
			var op1 = new RegisterAccess(null);
			var op2 = new MemoryAccess(new RegisterAccess(null));
			var op3p = new Arithmetic.Plus(new RegisterAccess(null),
				                           new RegisterAccess(null));
			var op3o = new Arithmetic.And(new RegisterAccess(null),
				                          new RegisterAccess(null));
			var op4 = new MemoryAccess(new Arithmetic.Sub(new RegisterAccess(null),
				                                            new RegisterAccess(null)));
			(Operation, InstructionFunc) p0 = (op4, DummyF);
			(Operation, InstructionFunc) p1 = (op3p, DummyF);
			(Operation, InstructionFunc) p2 = (op3o, DummyF);
			(Operation, InstructionFunc) p3 = (op2, DummyF);
			(Operation, InstructionFunc) p4 = (op1, DummyF);
			var instructionSet = new InstructionSet() {p3, p1, p4, p0, p2};

			var chooser = new InstructionChooser(instructionSet);
			Assert.Equal(p0, chooser.InstructionSet[0]);
			Assert.True(p1 == chooser.InstructionSet[2] ||
			            p2 == chooser.InstructionSet[2]);
			Assert.True(p1 == chooser.InstructionSet[1] ||
			            p2 == chooser.InstructionSet[1]);
			Assert.Equal(p3, chooser.InstructionSet[3]);
			Assert.Equal(p4, chooser.InstructionSet[4]);
		}

		[Fact]
		public void MatchingTest0()
		{
			bool ok = false;

			List<AsmInstruction> foo(Register register, List<IInstructionParameter> parameters)
			{
				var ret = new List<AsmInstruction>();
				if (parameters.Count != 1)
					return ret;
				if (parameters[0] is Register arg)
					ok = arg == HwRegister.RAX;
				return ret;
			}
			
			var tree = new MemoryAccess(new RegisterAccess(HwRegister.RAX));
			var template = new MemoryAccess(new RegisterAccess(null));
			var instructionSet = new InstructionSet() {(template, foo)};
			var chooser = new InstructionChooser(instructionSet);
			chooser.ChooseInstruction(tree);
			Assert.True(ok);
		}

		[Fact]
		public void MatchingTest1()
		{
			bool ok = false;

			List<AsmInstruction> foo(Register register, List<IInstructionParameter> parameters)
			{
				var ret = new List<AsmInstruction>();
				if (parameters.Count != 2)
					return ret;
				if (parameters[0] is Register && parameters[0] is Register)
					ok = true;
				return ret;
			}

			List<AsmInstruction> bar(Register register, List<IInstructionParameter> parameters)
			{
				return new List<AsmInstruction>();
			}
			
			var tree = new MemoryAccess(new Arithmetic.Plus(new RegisterAccess(HwRegister.RAX),
				                                            new RegisterAccess(HwRegister.RBX)));
			var template = new MemoryAccess(new Arithmetic.Plus(null, null));
			var template1 = new RegisterAccess(null);
			var instructionSet = new InstructionSet() {(template, foo), (template1, bar)};
			var chooser = new InstructionChooser(instructionSet);
			chooser.ChooseInstruction(tree);
			Assert.True(ok);
		}

		[Fact]
		public void UnitOpInstructionsTest0()
		{
			List<AsmInstruction> access(Register res, List<IInstructionParameter> parameters)
			{
				var instructions = new List<AsmInstruction>();
				if (parameters.Count != 1)
					throw new ArgumentException("Too few or too many parameters");
				if (!(parameters[0] is Register))
					throw new ArgumentException("Bad parameter type");
				return new List<AsmInstruction> { new DummyInstruction("mov", res, parameters[0]) };
			}
			
			var template = new RegisterAccess(null);
			var tree = new RegisterAccess(HwRegister.RBX);

			var instructionSet = new InstructionSet() {(template, access)};
			var chooser = new InstructionChooser(instructionSet);
			var code = chooser.ChooseInstruction(tree).ToList();
			Assert.Single(code);
			Assert.Equal("mov", code[0].ToString());
			Assert.Equal("mov", code[0].ToString());
			Assert.Equal(2, code[0].RegistersUsed.Count());
			Assert.Equal(HwRegister.RAX, code[0].RegistersUsed[0]);
			Assert.Equal(HwRegister.RBX, code[0].RegistersUsed[1]);
		}

		[Fact]
		public void UnitOpInstructionsTest1()
		{
			List<AsmInstruction> add(Register res, List<IInstructionParameter> parameters)
			{
				var instructions = new List<AsmInstruction>();
				if (parameters.Count != 2)
					throw new ArgumentException("Too few or too many parameters");
				if (!(parameters[0] is Register) || !(parameters[1] is Register))
					throw new ArgumentException("Bad parameter type");
				return new List<AsmInstruction>
				{
					new DummyInstruction("add", parameters[0], parameters[1]),
					new DummyInstruction("mov", res, parameters[0])
				};
			}
			
			List<AsmInstruction> access(Register res, List<IInstructionParameter> parameters)
			{
				var instructions = new List<AsmInstruction>();
				if (parameters.Count != 1)
					throw new ArgumentException("Too few or too many parameters");
				if (!(parameters[0] is Register))
					throw new ArgumentException("Bad parameter type");
				return new List<AsmInstruction> { new DummyInstruction("mov", res, parameters[0]) };
			}
			
			var templatePlus = new Arithmetic.Plus(null, null);
			var templateAccess = new RegisterAccess(null);
			var tree = new Arithmetic.Plus(new RegisterAccess(HwRegister.R8),
				                           new RegisterAccess(HwRegister.R9));

			var instructionSet = new InstructionSet() {(templatePlus, add), (templateAccess, access)};
			var chooser = new InstructionChooser(instructionSet);
			var code = chooser.ChooseInstruction(tree).ToList();
			// expected:
			// mov vr1 R8
			// mov vr2 R9
			// add vr1 vr2
			// mov rax vr1
			Assert.Equal(4, code.Count());
			Assert.Equal("mov", code[0].ToString());
			Assert.True(code[0].RegistersUsed[0] is VirtualRegister);
			Assert.Equal(HwRegister.R8, code[0].RegistersUsed[1]);
			Assert.Equal("mov", code[1].ToString());
			Assert.True(code[1].RegistersUsed[0] is VirtualRegister);
			Assert.Equal(HwRegister.R9, code[1].RegistersUsed[1]);
			Assert.Equal("add", code[2].ToString());
			Assert.Equal(code[0].RegistersUsed[0], code[2].RegistersUsed[0]);
			Assert.Equal(code[1].RegistersUsed[0], code[2].RegistersUsed[1]);
			Assert.Equal("mov", code[3].ToString());
			Assert.Equal(HwRegister.RAX, code[3].RegistersUsed[0]);
			Assert.Equal(code[0].RegistersUsed[0], code[3].RegistersUsed[1]);
		}

		[Fact]
		public void ConstValueTest()
		{
			List<AsmInstruction> assign(Register res, List<IInstructionParameter> parameters)
			{
				var instructions = new List<AsmInstruction>();
				if (parameters.Count != 2)
					throw new ArgumentException("Too few or too many parameters");
				if (!(parameters[0] is Register) || !(parameters[1] is Constant))
					throw new ArgumentException("Bad parameter type");
				return new List<AsmInstruction>
				{
					new DummyInstruction("mov", parameters[0], parameters[1]),
					new DummyInstruction("mov", res, parameters[0])
				};
			}
			var templateAssign = new RegisterWrite(null, new ConstValue(null));
			var tree = new RegisterWrite(HwRegister.RAX, new ConstValue(5));
			
			var instructionSet = new InstructionSet() {(templateAssign, assign)};
			var chooser = new InstructionChooser(instructionSet);
			var code = chooser.ChooseInstruction(tree).ToList();
			
			Assert.Equal(2, code.Count());
			Assert.Equal("mov", code[0].ToString());
			Assert.Single(code[0].RegistersUsed);
			Assert.Equal(HwRegister.RAX, code[0].RegistersUsed[0]);
			Assert.Equal("mov", code[1].ToString());
			Assert.Equal(HwRegister.RAX, code[1].RegistersUsed[0]);
			Assert.Equal(HwRegister.RAX, code[1].RegistersUsed[1]);
		}
	}
}
