﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cimplified.Lexer;
using Cimplified.Parser;
using Xunit;

namespace tests.Grammar
{
	public class GrammarAnalysisTest
    {
        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest1()
        {
            var nonTerminal_S = new NonTerminal("S"); // Symbols are compared by reference
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");
            var nonTerminal_D = new NonTerminal("D");

            var terminal_a = new Terminal("a");
            var terminal_b = new Terminal("b");

            var terminals = new List<Terminal>() { terminal_a, terminal_b };

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);
            var D = new AtomicRegex<Symbol>(nonTerminal_D);

            var a = new AtomicRegex<Symbol>(terminal_a);
            var b = new AtomicRegex<Symbol>(terminal_b);
            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var S_ABCD = new Production(nonTerminal_S, new ConcatRegex<Symbol>(A, B, C, D)); // S -> ABCD
            var A_a = new Production(nonTerminal_A, a); // A -> a
            var B_a = new Production(nonTerminal_B, b); // B -> b
            var C_D = new Production(nonTerminal_C, D); // C -> D
            var D_eps = new Production(nonTerminal_D, epsilon); // D -> \epsilon

            var productions = new List<Production> { S_ABCD, A_a, B_a, C_D, D_eps };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_C, nonTerminal_D }; // {C, D}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, terminal_a } }, // {S, A, a}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, terminal_a } }, // {A, a}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_C, nonTerminal_D } }, // {C, D}
                { nonTerminal_D, new HashSet<Symbol> { nonTerminal_D } } // {D}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_C, nonTerminal_D } }, // {C, D}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_D } }, // {D}
                { nonTerminal_D, new HashSet<Symbol> { nonTerminal_D } } // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest2()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");
            var nonTerminal_D = new NonTerminal("D");

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);
            var D = new AtomicRegex<Symbol>(nonTerminal_D);

            var terminals = new List<Terminal>();

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var S_ABCD = new Production(nonTerminal_S, new ConcatRegex<Symbol>(A, B, C, D)); // S -> ABCD
            var A_B = new Production(nonTerminal_A, B); // A -> B
            var B_C = new Production(nonTerminal_B, C); // B -> C
            var C_D = new Production(nonTerminal_C, D); // C -> D
            var D_eps = new Production(nonTerminal_D, epsilon); // D -> \epsilon

            var productions = new List<Production> { S_ABCD, A_B, B_C, C_D, D_eps };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_A, nonTerminal_B, nonTerminal_C, nonTerminal_D, nonTerminal_S }; // {A, B, C, D, S}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {S, A, B, C, D}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {A, B, C, D}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {B, C, D}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_C, nonTerminal_D } }, // {C, D}
                { nonTerminal_D, new HashSet<Symbol> { nonTerminal_D } } // {D}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {B, C, D}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {B, C, D}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {B, C, D}
                { nonTerminal_D, new HashSet<Symbol> { nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {B, C, D}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest3()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");

            var terminal_b = new Terminal("b");

            var terminals = new List<Terminal>() { terminal_b };

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var S_AB = new Production(nonTerminal_S, new ConcatRegex<Symbol>(A, B)); // S -> AB
            var A_BC = new Production(nonTerminal_A, new ConcatRegex<Symbol>(new StarRegex<Symbol>(B), C)); // A -> B*C
            var B_b = new Production(nonTerminal_B, new AtomicRegex<Symbol>(terminal_b)); // B -> b
            var C_eps = new Production(nonTerminal_C, epsilon); // C -> \epsilon

            var productions = new List<Production> { S_AB, A_BC, B_b, C_eps };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);
            
            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_A, nonTerminal_C }; // {A, C}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, nonTerminal_B, terminal_b, nonTerminal_C } }, // {S, A, B, b, C}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, nonTerminal_B, terminal_b, nonTerminal_C } }, // {A, B, b, C}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_C } } // {C}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b, C}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b, nonTerminal_C } }, // {B, b, C}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_B, terminal_b } } // {B, b}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);

        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest4()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");
            var nonTerminal_D = new NonTerminal("D");

            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);
            var D = new AtomicRegex<Symbol>(nonTerminal_D);

            var terminals = new List<Terminal>();

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var S_B_CD = new Production(nonTerminal_S, new UnionRegex<Symbol>(B, new ConcatRegex<Symbol>(C, D))); // S -> B + CD
            var B_eps = new Production(nonTerminal_B, epsilon); // B -> \epsilon
            var C_B = new Production(nonTerminal_C, B); // C -> B
            var D_C = new Production(nonTerminal_D, C); // D -> C

            var productions = new List<Production> { S_B_CD, B_eps, C_B, D_C };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);
            
            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_B, nonTerminal_C, nonTerminal_D, nonTerminal_S }; // {B, C, D, S}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_B, nonTerminal_C, nonTerminal_D } }, // {S, B, C, D}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B } }, // {B}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_C, nonTerminal_B } }, // {C, B}
                { nonTerminal_D, new HashSet<Symbol> { nonTerminal_D, nonTerminal_C, nonTerminal_B } } // {D, C, B}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_D, nonTerminal_C, nonTerminal_B } }, // {D, C, B}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_D, nonTerminal_C, nonTerminal_B } }, // {D, C, B}
                { nonTerminal_D, new HashSet<Symbol>()} // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest5()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");

            var terminal_b = new Terminal("b");

            var terminals = new List<Terminal>() { terminal_b };

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);

            var b = new AtomicRegex<Symbol>(terminal_b);

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var S_A_BC = new Production(nonTerminal_S, new UnionRegex<Symbol>(A, new ConcatRegex<Symbol>(B, C))); // S -> A + BC
            var A_eps = new Production(nonTerminal_A, epsilon); // A -> \epsilon
            var B_b = new Production(nonTerminal_B, b); // B -> b
            var C_A = new Production(nonTerminal_C, A); // C -> A

            var productions = new List<Production> { S_A_BC, A_eps, B_b, C_A };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);
            
            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_S, nonTerminal_A, nonTerminal_C }; // {S, A, C}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, nonTerminal_B, terminal_b } }, // {S, A, B, b}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A } }, // {A}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_C, nonTerminal_A } } // {C, A}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol>()}, // {}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_C, nonTerminal_A } },  // {C, A}
                { nonTerminal_C, new HashSet<Symbol>()} // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest6()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");

            var terminal_a = new Terminal("a");
            var terminal_b = new Terminal("b");
            var terminal_c = new Terminal("c");

            var terminals = new List<Terminal>() { terminal_a, terminal_b, terminal_c };

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);

            var a = new AtomicRegex<Symbol>(terminal_a);
            var b = new AtomicRegex<Symbol>(terminal_b);
            var c = new AtomicRegex<Symbol>(terminal_c);

            var S_AB = new Production(nonTerminal_S, new ConcatRegex<Symbol>(new StarRegex<Symbol>(A), new StarRegex<Symbol>(B))); // S -> A*B*
            var A_aB = new Production(nonTerminal_A, new ConcatRegex<Symbol>(a, B)); // A -> aB
            var B_bB_c = new Production(nonTerminal_B, new UnionRegex<Symbol>(new ConcatRegex<Symbol>(b, B), c)); // B -> bB + c

            var productions = new List<Production> { S_AB, A_aB, B_bB_c };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_S }; // {S}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, terminal_a, nonTerminal_B, terminal_b, terminal_c } }, // {S, A, a, B, b, c}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, terminal_a } }, // {A, a}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b, terminal_c } } // {B, b, c}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, terminal_a, nonTerminal_B, terminal_b, terminal_c } }, // {A, a, B, b, c}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_A, terminal_a, nonTerminal_B, terminal_b, terminal_c } }  // {B, b, c}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest7()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");

            var terminal_b = new Terminal("b");

            var terminals = new List<Terminal>() { terminal_b };

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);

            var b = new AtomicRegex<Symbol>(terminal_b);

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var S_A_B = new Production(nonTerminal_S, new UnionRegex<Symbol>(A, B)); // S -> A + B
            var A_eps_B = new Production(nonTerminal_A, new UnionRegex<Symbol>(epsilon, B)); // A -> \epsilon + B
            var B_b_A = new Production(nonTerminal_B, new ConcatRegex<Symbol>(b, new StarRegex<Symbol>(A))); // B -> bA*

            var productions = new List<Production> { S_A_B, A_eps_B, B_b_A };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_S, nonTerminal_A }; // {S, A}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, nonTerminal_B, terminal_b} }, // {S, A, B, b}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, nonTerminal_B, terminal_b } }, // {A, B, b}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } } // {B, b}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_A, nonTerminal_B, terminal_b} }  // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest8()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");

            var terminal_a = new Terminal("a");
            var terminal_b = new Terminal("b");

            var terminals = new List<Terminal>() { terminal_a, terminal_b };

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);

            var a = new AtomicRegex<Symbol>(terminal_a);
            var b = new AtomicRegex<Symbol>(terminal_b);

            var S_A_BC = new Production(nonTerminal_S, new UnionRegex<Symbol>(
                new StarRegex<Symbol>(A), new ConcatRegex<Symbol>(new StarRegex<Symbol>(B), C))); // S -> A* + B*C
            var A_aA = new Production(nonTerminal_A, new ConcatRegex<Symbol>(a, A)); // A -> aA
            var B_bBb = new Production(nonTerminal_B, new ConcatRegex<Symbol>(b, B, b)); // B -> bBb
            var C_A_B = new Production(nonTerminal_C, new ConcatRegex<Symbol>(new StarRegex<Symbol>(A), new StarRegex<Symbol>(B))); // C -> A*B*

            var productions = new List<Production> { S_A_BC, A_aA, B_bBb, C_A_B };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_S, nonTerminal_C }; // {C, S}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, terminal_a, nonTerminal_B, terminal_b, nonTerminal_C } }, // {S, A, a, B, b, C}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, terminal_a } }, // {A, a}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_C, nonTerminal_A, terminal_a, nonTerminal_B, terminal_b } }, // {C, A, a, B, b}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, terminal_a,nonTerminal_B, terminal_b } }, // {A, a}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b, nonTerminal_C, nonTerminal_A, terminal_a } },  // {B, b, C, A, a}
                { nonTerminal_C, new HashSet<Symbol>()}  // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest9()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");

            var terminal_a = new Terminal("a");
            var terminal_b = new Terminal("b");

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);

            var a = new AtomicRegex<Symbol>(terminal_a);
            var b = new AtomicRegex<Symbol>(terminal_b);

            var terminals = new List<Terminal>() { terminal_a, terminal_b };

            var S_AB = new Production(nonTerminal_S, new StarRegex<Symbol>(new ConcatRegex<Symbol>(A, B))); // S -> (AB)*
            var A_a = new Production(nonTerminal_A, a); // A -> a
            var B_b = new Production(nonTerminal_B, b); // B -> b

            var productions = new List<Production> { S_AB, A_a, B_b };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_S }; // {S}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, terminal_a } }, // {S, A, a}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, terminal_a } }, // {A, a}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } } // {B, b}
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_A, terminal_a } }  // {A, a}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest10()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");

            var terminal_b = new Terminal("b");

            var terminals = new List<Terminal>() { terminal_b };

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);

            var b = new AtomicRegex<Symbol>(terminal_b);

            var S_A = new Production(nonTerminal_S, A); // S -> A
            var A_B = new Production(nonTerminal_A, B); // A -> B
            var B_b = new Production(nonTerminal_B, b); // B -> b

            var productions = new List<Production> { S_A, A_B, B_b };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal>(); // {}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, nonTerminal_B, terminal_b } }, // {S, A, B, b}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, nonTerminal_B, terminal_b } }, // {A, B, b}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } } // {B, b}                             
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol>()}, // {}
                { nonTerminal_B, new HashSet<Symbol>()}  // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest11()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");
            var nonTerminal_D = new NonTerminal("D");

            var terminal_a = new Terminal("a");
            var terminal_b = new Terminal("b");
            var terminal_d = new Terminal("d");

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);
            var D = new AtomicRegex<Symbol>(nonTerminal_D);

            var a = new AtomicRegex<Symbol>(terminal_a);
            var b = new AtomicRegex<Symbol>(terminal_b);
            var d = new AtomicRegex<Symbol>(terminal_d);

            var terminals = new List<Terminal>() { terminal_a, terminal_b, terminal_d };

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var S_ABCD = new Production(nonTerminal_S, new ConcatRegex<Symbol>(A, B, C, D)); // S -> ABCD
            var A_a = new Production(nonTerminal_A, a); // A -> a
            var B_b_eps = new Production(nonTerminal_B, new UnionRegex<Symbol>(b, epsilon)); // B -> b + \epsilon
            var C_eps = new Production(nonTerminal_C, epsilon); // C -> \epsilon
            var D_d = new Production(nonTerminal_D, d); // D -> d

            var productions = new List<Production> { S_ABCD, A_a, B_b_eps, C_eps, D_d };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);          

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_B, nonTerminal_C }; // {B, C}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, terminal_a } }, // {S, A, a}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A, terminal_a } }, // {A, a}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_C } }, // {C}
                { nonTerminal_D, new HashSet<Symbol> { nonTerminal_D, terminal_d } } // {D, d}                
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_B, terminal_b, nonTerminal_C, nonTerminal_D, terminal_d } }, // {B, b, C, D, d}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_C, nonTerminal_D, terminal_d } }, // {C, D, d}
                { nonTerminal_C, new HashSet<Symbol> { nonTerminal_D, terminal_d } }, // {D, d}
                { nonTerminal_D, new HashSet<Symbol>()} // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        private Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>> initialiseTest12()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");

            var terminal_b = new Terminal("b");

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);

            var b = new AtomicRegex<Symbol>(terminal_b);

            var terminals = new List<Terminal>() { terminal_b };

            var S_AB = new Production(nonTerminal_S, new ConcatRegex<Symbol>(A, B)); // S -> AB
            var A_eps = new Production(nonTerminal_A, epsilon); // A -> \epsilon
            var B_b = new Production(nonTerminal_B, b); // B -> b

            var productions = new List<Production> { S_AB, A_eps, B_b };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

            var expectedNullable = new HashSet<NonTerminal> { nonTerminal_A }; // {A}

            var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol> { nonTerminal_S, nonTerminal_A, nonTerminal_B, terminal_b } }, // {S, A, B, b}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_A } }, // {A}
                { nonTerminal_B, new HashSet<Symbol> { nonTerminal_B, terminal_b } } // {B, b}              
            };

            var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>() }, // {}
                { nonTerminal_A, new HashSet<Symbol> { nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_B, new HashSet<Symbol>()} // {}
            };

            return new Tuple<GDfas<HashableList<Regex<Symbol>>>, HashSet<NonTerminal>, Dictionary<NonTerminal, HashSet<Symbol>>, Dictionary<NonTerminal, HashSet<Symbol>>>(gdfas, expectedNullable, expectedFirst, expectedFollow);
        }

        [Fact]
        public void NullableTest1()
        {
            var tuple = initialiseTest1();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest1()
        {
            var tuple = initialiseTest1();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest1()
        {
            var tuple = initialiseTest1();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest2()
        {
            var tuple = initialiseTest2();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest2()
        {
            var tuple = initialiseTest2();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest2()
        {
            var tuple = initialiseTest2();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest3()
        {
            var tuple = initialiseTest3();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest3()
        {
            var tuple = initialiseTest3();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest3()
        {
            var tuple = initialiseTest3();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest4()
        {
            var tuple = initialiseTest4();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest4()
        {
            var tuple = initialiseTest4();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest4()
        {
            var tuple = initialiseTest4();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest5()
        {
            var tuple = initialiseTest5();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest5()
        {
            var tuple = initialiseTest5();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest5()
        {
            var tuple = initialiseTest5();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);
            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                        expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest6()
        {
            var tuple = initialiseTest6();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest6()
        {
            var tuple = initialiseTest6();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest6()
        {
            var tuple = initialiseTest6();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest7()
        {
            var tuple = initialiseTest7();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest7()
        {
            var tuple = initialiseTest7();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest7()
        {
            var tuple = initialiseTest7();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest8()
        {
            var tuple = initialiseTest8();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest8()
        {
            var tuple = initialiseTest8();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest8()
        {
            var tuple = initialiseTest8();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                        expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest9()
        {
            var tuple = initialiseTest9();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest9()
        {
            var tuple = initialiseTest9();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest9()
        {
            var tuple = initialiseTest9();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest10()
        {
            var tuple = initialiseTest10();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest10()
        {
            var tuple = initialiseTest10();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest10()
        {
            var tuple = initialiseTest10();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest11()
        {
            var tuple = initialiseTest11();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest11()
        {
            var tuple = initialiseTest11();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest11()
        {
            var tuple = initialiseTest11();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;
            
            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);
            
            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                expectedFollow.Keys.All(key => follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        [Fact]
        public void NullableTest12()
        {
            var tuple = initialiseTest12();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;

            var nullable = GrammarAnalysis.ComputeNullable(gdfas);

            Assert.Equal(expectedNullable, nullable);
        }

        [Fact]
        public void FirstTest12()
        {
            var tuple = initialiseTest12();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;

            var first = GrammarAnalysis.ComputeFirst(gdfas, expectedNullable);

            Assert.True(first.Keys.Count == expectedFirst.Keys.Count &&
                expectedFirst.Keys.All(key => first.ContainsKey(key) && expectedFirst[key].SetEquals(first[key])));
        }

        [Fact]
        public void FollowTest12()
        {
            var tuple = initialiseTest12();
            var gdfas = tuple.Item1;
            var expectedNullable = tuple.Item2;
            var expectedFirst = tuple.Item3;
            var expectedFollow = tuple.Item4;

            var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

            Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
                        expectedFollow.Keys.All(key =>
                            follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
        }

        /*
		 * Start -> FuncDeclaration*
			FuncDeclaration -> Type name Block
			Type -> BaseType  |  voidType
			BaseType -> intType  |  boolType
			Block -> lbracket rbrtacket
			*/
		[Fact]
		public void FollowTestForExampleGrammar()
		{
			var NonTerminalStart = new NonTerminal("Start");
			var NonTerminalFuncDeclaration = new NonTerminal("FuncDeclaration");
			var NonTerminalType = new NonTerminal("Type");
            var NonTerminalBlock = new NonTerminal("Block");
			var NonTerminalBaseType = new NonTerminal("BaseType");
			var TerminalName = new Terminal("name");
			var TerminalVoidType = new Terminal("voidType");
			var TerminalBoolType = new Terminal("boolType");
			var TerminalIntType = new Terminal("intType");
            var TerminalLBracket = new Terminal("lbracket");
			var TerminalRBracket = new Terminal("rbracket");

            var FuncDeclaration = new AtomicRegex<Symbol>(NonTerminalFuncDeclaration);
			var Type = new AtomicRegex<Symbol>(NonTerminalType);
            var Block = new AtomicRegex<Symbol>(NonTerminalBlock);
			var BaseType = new AtomicRegex<Symbol>(NonTerminalBaseType);

			var name = new AtomicRegex<Symbol>(TerminalName);
			var voidType = new AtomicRegex<Symbol>(TerminalVoidType);
			var intType = new AtomicRegex<Symbol>(TerminalIntType);
			var boolType = new AtomicRegex<Symbol>(TerminalBoolType);
            var lBracket = new AtomicRegex<Symbol>(TerminalLBracket);
			var rBracket = new AtomicRegex<Symbol>(TerminalRBracket);

            var terminals = new List<Terminal>
			{
				TerminalName, TerminalVoidType, TerminalBoolType, TerminalIntType, TerminalLBracket, TerminalRBracket
			};

			var StartToFuncDeclaration =
				new Production(NonTerminalStart, new StarRegex<Symbol>(FuncDeclaration)); // Start -> FuncDeclaration*
			var FuncDeclarationProduction = new Production(NonTerminalFuncDeclaration,
				new ConcatRegex<Symbol>(Type, name, Block));
			var TypeProduction = new Production(NonTerminalType, new UnionRegex<Symbol>(BaseType, voidType));
			var BaseTypeProduction = new Production(NonTerminalBaseType, new UnionRegex<Symbol>(intType, boolType));
            var BlockProduction = new Production(NonTerminalBlock,
				new ConcatRegex<Symbol>(lBracket, rBracket));

			var productions = new List<Production>
			{
				StartToFuncDeclaration, FuncDeclarationProduction, TypeProduction, BaseTypeProduction, BlockProduction
			};

			var grammar = new Cimplified.Parser.Grammar(NonTerminalStart, productions, terminals);
			var gdfas = GrammarAnalysis.ComputeGdfas(grammar);

			var expectedNullable = new HashSet<NonTerminal> {NonTerminalStart}; // {Start}

			var expectedFirst = new Dictionary<NonTerminal, HashSet<Symbol>>
			{
				{NonTerminalStart, new HashSet<Symbol> {NonTerminalStart, NonTerminalFuncDeclaration, NonTerminalType, NonTerminalBaseType, TerminalBoolType, TerminalIntType, TerminalVoidType}},
				{NonTerminalFuncDeclaration, new HashSet<Symbol> { NonTerminalFuncDeclaration, NonTerminalType, NonTerminalBaseType, TerminalIntType, TerminalBoolType, TerminalVoidType}},
				{NonTerminalType, new HashSet<Symbol> { NonTerminalType, NonTerminalBaseType, TerminalVoidType, TerminalBoolType, TerminalIntType }},
				{NonTerminalBlock, new HashSet<Symbol> { NonTerminalBlock, TerminalLBracket}},
				{NonTerminalBaseType, new HashSet<Symbol> { NonTerminalBaseType, TerminalIntType, TerminalBoolType}}
			};

			var expectedFollow = new Dictionary<NonTerminal, HashSet<Symbol>>
			{
				{NonTerminalStart, new HashSet<Symbol>()},
				{NonTerminalBlock, new HashSet<Symbol> {NonTerminalFuncDeclaration, NonTerminalType, NonTerminalBaseType, TerminalBoolType, TerminalIntType, TerminalVoidType}},
				{NonTerminalType, new HashSet<Symbol> {TerminalName}},
				{NonTerminalBaseType, new HashSet<Symbol> {TerminalName}},
				{NonTerminalFuncDeclaration, new HashSet<Symbol> {NonTerminalFuncDeclaration, NonTerminalType, NonTerminalBaseType, TerminalBoolType, TerminalIntType, TerminalVoidType}}
			};

			var follow = GrammarAnalysis.ComputeFollow(gdfas, expectedNullable, expectedFirst);

			Assert.True(follow.Keys.Count == expectedFollow.Keys.Count &&
			            expectedFollow.Keys.All(key =>
				            follow.ContainsKey(key) && expectedFollow[key].SetEquals(follow[key])));
		}
    }
}
