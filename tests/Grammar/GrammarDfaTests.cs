using Cimplified.Parser;
using Cimplified.Lexer;
using Cimplified;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace tests.Grammar
{
    public class GrammarDfaTest
    {
        private Production Combine(List<Production> x)
        { 
            return x.Count > 0 ? x.First() : null;   
        }

        private (Cimplified.Parser.Grammar, GDfas<HashableList<Regex<Symbol>>>) GetData1()
        {
            var nt1 = new NonTerminal();
            var rx11 = new AtomicRegex<Symbol>(new Symbol());
            var p11 = new Production(nt1, rx11);
            
            var nt2 = new NonTerminal();
            var rx21 = new AtomicRegex<Symbol>(new Symbol());
            var p21 = new Production(nt2, rx21);
            var rx22 = new AtomicRegex<Symbol>(new Symbol());
            var p22 = new Production(nt2, rx22);
            
            var nt3 = new NonTerminal();
            var rx31 = new AtomicRegex<Symbol>(new Symbol());
            var p31 = new Production(nt3, rx31);
            var rx32 = new AtomicRegex<Symbol>(new Symbol());
            var p32 = new Production(nt3, rx32);
            var rx33 = new AtomicRegex<Symbol>(nt2);
            var p33 = new Production(nt3, rx33);

            var prods = new List<Production> {p11, p21, p22, p31, p32, p33};
            var g = new Cimplified.Parser.Grammar(null, prods, new List<Terminal>());

            var lst1 = new List<(Production, Dfa<Symbol, bool, Regex<Symbol>>)> { (p11, Utils.RegexToDfa(rx11)) };
            var cdfa1 = CombinedDfa.Create(lst1, Combine);
            var lst2 = new List<(Production, Dfa<Symbol, bool, Regex<Symbol>>)> { (p21, Utils.RegexToDfa(rx21)), (p22, Utils.RegexToDfa(rx22)) };
            var cdfa2 = CombinedDfa.Create(lst2, Combine);
            var lst3 = new List<(Production, Dfa<Symbol, bool, Regex<Symbol>>)> { (p31, Utils.RegexToDfa(rx31)), (p32, Utils.RegexToDfa(rx32)), (p33, Utils.RegexToDfa(rx33)) };
            var cdfa3 = CombinedDfa.Create(lst3, Combine);
            
            var gDfas = new GDfas<HashableList<Regex<Symbol>>>();
            gDfas[nt1] = cdfa1;
            gDfas[nt2] = cdfa2;
            gDfas[nt3] = cdfa3;

            return (g, gDfas);
        }
        
        [Fact]
        public void ComputeGDfaTest1()
        {
            var (g, gdfas) = GetData1();
            var x = GrammarAnalysis.ComputeGdfas(g);
            Assert.Equal(gdfas.Count(), x.Count());
            foreach (var ((k1, v1), (k2, v2)) in Enumerable.Zip(x, gdfas, (i, j) => (i, j)))
            {
                Assert.Equal(k1, k2);
                Assert.True(Utils.Equivalent(v1, v2));
            }
        }
    }
}
