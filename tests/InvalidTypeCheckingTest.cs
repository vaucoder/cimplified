﻿using System.Collections.Generic;
using Cimplified;
using Cimplified.Parser;
using Xunit;

namespace tests
{
	public class InvalidTypeCheckingTest
	{
		private readonly TypeName intType = new TypeName("Int");
		private readonly TypeName boolType = new TypeName("Bool");
		private readonly TypeName voidType = new TypeName("Void");

		private readonly IntLiteral termNumber0 = new IntLiteral("0");
		private readonly IntLiteral termNumber1 = new IntLiteral("1");
		private readonly IntLiteral termNumber2 = new IntLiteral("2");

		private readonly BoolLiteral termBoolFalse = new BoolLiteral(false);
		private readonly BoolLiteral termBoolTrue = new BoolLiteral(true);

		private readonly FunctionName functionMainName = new FunctionName("main");
		private readonly FunctionName functionName = new FunctionName("function1");

		private readonly VarName intXVarName = new VarName("intX");
		private readonly VarName intYVarName = new VarName("intY");
		private readonly VarName boolTrueVarName = new VarName("boolTrue");
		private readonly VarName boolFalseVarName = new VarName("boolFalse");
		private readonly VarName anyVarName = new VarName("var");

		/*
		 * Void main() {
		 *	Int intX = true;
		 * }
		 */
		[Fact]
		public void InvalidBoolToIntAssignmentTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termBoolTrue);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = 0;
		 * }
		 */
		[Fact]
		public void InvalidIntToBoolAssignmentTest()
		{
			// given
			var args = new List<Argument>();
			var boolDeclaration = new DeclareAssignment(boolType, boolTrueVarName, termNumber0);
			boolTrueVarName.Declaration = boolDeclaration;
			var functionBlock = new Block(new List<Statement> {boolDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Int main() {
		 *	Int intX = 0;
		 * }
		 */
		[Fact]
		public void MissingIntFunctionReturnTest()
		{
			// given
			var args = new List<Argument>();
			var functionBlock = new Block(new List<Statement> { });
			var functionDeclaration = new FunctionDeclaration(intType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<ReturnInFunctionNodeVisitor.NotAllPathReturnError>(
				delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Int intX = 0;
		 *	Bool boolTrue = true;
		 *	intX = boolTrue;
		 * }
		 */
		[Fact]
		public void IntAssignmentTwoVariablesTypeCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var boolTrueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, termBoolTrue);
			boolTrueVarName.Declaration = boolTrueDeclaration;
			var intXReassignment = new Assignment(intXVarName, boolTrueDeclaration.Value);

			var functionBlock = new Block(new List<Statement> {intXDeclaration, boolTrueDeclaration, intXReassignment});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Bool main() {
		 * }
		 */
		[Fact]
		public void MissingBoolFunctionReturnTest()
		{
			// given
			var args = new List<Argument>();
			var functionBlock = new Block(new List<Statement> { });
			var functionDeclaration = new FunctionDeclaration(boolType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<ReturnInFunctionNodeVisitor.NotAllPathReturnError>(
				delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *  return 0;
		 * }
		 */
		[Fact]
		public void ReturnInVoidFunctionTest()
		{
			// given
			var args = new List<Argument>();
			var returnClause = new ReturnClause(termNumber0);
			var functionBlock = new Block(new List<Statement> {returnClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}


		/*
		 * Void main() {
		 *  while(1) {
		 *  }
		 * }
		 */
		[Fact]
		public void IntInWhileTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var whileClause = new WhileClause(termNumber0, emptyBlock);
			var functionBlock = new Block(new List<Statement> {whileClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *  if(1) {
		 *  }
		 * }
		 */
		[Fact]
		public void IntInIfTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var ifClause = new IfClause(termNumber0, emptyBlock, new List<ElIfClause>(), emptyBlock);
			var functionBlock = new Block(new List<Statement> {ifClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *  if(True) {
		 *  } elif (1) {
		 *	}
		 * }
		 */
		[Fact]
		public void IntInElifTest()
		{
			// given
			var args = new List<Argument>();
			var emptyBlock = new Block(new List<Statement>());
			var ifClause = new IfClause(termBoolTrue, emptyBlock,
				new List<ElIfClause> {new ElIfClause(termNumber0, emptyBlock)}, emptyBlock);
			var functionBlock = new Block(new List<Statement> {ifClause});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Int intX = 1 + false;
		 * }
		 */
		[Fact]
		public void PlusInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var plusOperator = new Plus {Left = termNumber1, Right = termBoolFalse};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, plusOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Int intX = 1 * false;
		 * }
		 */
		[Fact]
		public void MultInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var multOperator = new Mult {Left = termNumber1, Right = termBoolFalse};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, multOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Int intX = 1 / false;
		 * }
		 */
		[Fact]
		public void DivInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var divOperator = new Div {Left = termNumber1, Right = termBoolFalse};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, divOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Int intX = 1 - false;
		 * }
		 */
		[Fact]
		public void SubInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var subOperator = new Sub {Left = termNumber1, Right = termBoolFalse};
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, subOperator);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = 1 == false;
		 * }
		 */
		[Fact]
		public void EqInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var eqOperator = new Eq {Left = termNumber1, Right = termBoolFalse};
			var trueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, eqOperator);
			boolTrueVarName.Declaration = trueDeclaration;
			var functionBlock = new Block(new List<Statement> {trueDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});
			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = 1 <= false;
		 * }
		 */
		[Fact]
		public void LeqInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var leqOperator = new Leq {Left = termNumber1, Right = termBoolFalse};
			var trueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, leqOperator);
			boolTrueVarName.Declaration = trueDeclaration;
			var functionBlock = new Block(new List<Statement> {trueDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});
			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = 1 and false;
		 * }
		 */
		[Fact]
		public void AndInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var andOperator = new And {Left = termNumber1, Right = termBoolFalse};
			var trueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, andOperator);
			boolTrueVarName.Declaration = trueDeclaration;
			var functionBlock = new Block(new List<Statement> {trueDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});
			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = 1 or false;
		 * }
		 */
		[Fact]
		public void OrInvalidOperandsTest()
		{
			// given
			var args = new List<Argument>();
			var orOperator = new Or {Left = termNumber1, Right = termBoolFalse};
			var trueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, orOperator);
			boolTrueVarName.Declaration = trueDeclaration;
			var functionBlock = new Block(new List<Statement> {trueDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});
			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void main() {
		 *	Bool boolTrue = not 1;
		 * }
		 */
		[Fact]
		public void NegationInvalidOperandTest()
		{
			// given
			var args = new List<Argument>();
			var negationOperator = new Negation {Operand = termNumber1};
			var trueDeclaration = new DeclareAssignment(boolType, boolTrueVarName, negationOperator);
			boolTrueVarName.Declaration = trueDeclaration;
			var functionBlock = new Block(new List<Statement> {trueDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});
			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void function1(Int anyVar) {
		 * }
		 *
		 * Void main() {
		 *	function1();
		 * }
		 */
		[Fact]
		public void NotEnoughArgumentsFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument> {new Argument(intType, anyVarName)};
			var functionWithArgumentBlock = new Block(new List<Statement>());
			var functionWithArgumentDeclaration =
				new FunctionDeclaration(voidType, functionName, args, functionWithArgumentBlock);
			functionName.Declaration = functionWithArgumentDeclaration;

			var emptyArgs = new List<Argument>();
			var functionCall = new FunctionCall(functionName, new List<Expression>());
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, emptyArgs, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionWithArgumentDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			Assert.Throws<TypeNodeVisitor.ArgumentNumberError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void function1(Int anyVar) {
		 * }
		 *
		 * Void main() {
		 *	function1(0, 1);
		 * }
		 */
		[Fact]
		public void TooManyArgumentsFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument> {new Argument(intType, anyVarName)};
			var functionWithArgumentBlock = new Block(new List<Statement>());
			var functionWithArgumentDeclaration =
				new FunctionDeclaration(voidType, functionName, args, functionWithArgumentBlock);
			functionName.Declaration = functionWithArgumentDeclaration;

			var emptyArgs = new List<Argument>();
			var functionCall = new FunctionCall(functionName, new List<Expression> {termNumber0, termNumber1});
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, emptyArgs, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionWithArgumentDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			Assert.Throws<TypeNodeVisitor.ArgumentNumberError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Void function1(Int anyVar) {
		 * }
		 *
		 * Void main() {
		 *	function1(true);
		 * }
		 */
		[Fact]
		public void InvalidTypeArgumentsFunctionCallTypeCheckingTest()
		{
			// given
			var args = new List<Argument> {new Argument(intType, anyVarName)};
			var functionWithArgumentBlock = new Block(new List<Statement>());
			var functionWithArgumentDeclaration =
				new FunctionDeclaration(voidType, functionName, args, functionWithArgumentBlock);
			functionName.Declaration = functionWithArgumentDeclaration;

			var emptyArgs = new List<Argument>();
			var functionCall = new FunctionCall(functionName, new List<Expression> {termBoolTrue});
			var mainBlock = new Block(new List<Statement> {functionCall});
			var mainDeclaration = new FunctionDeclaration(voidType, functionMainName, emptyArgs, mainBlock);
			var program = new Program(new List<FunctionDeclaration> {functionWithArgumentDeclaration, mainDeclaration});
			functionMainName.Declaration = mainDeclaration;

			// when
			Assert.Throws<TypeNodeVisitor.TypeMismatchError>(delegate { TypeChecking.Process(program); });

			// then
		}

		/*
		 * Int main() {
		 *	if(True) {
		 *    return 0;
		 *	} else {
		 *  }
		 * }
		 */
		[Fact]
		public void MissingReturnInElseCheckingTest()
		{
			// given
			var args = new List<Argument>();
			var returnClause = new ReturnClause(termNumber0);
			var blockWithReturn = new Block(new List<Statement> {returnClause});
			var emptyBlock = new Block(new List<Statement>());
			var ifClause = new IfClause(termBoolTrue, blockWithReturn, new List<ElIfClause>(), emptyBlock);
			var functionBlock = new Block(new List<Statement> {ifClause});
			var functionDeclaration = new FunctionDeclaration(intType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			Assert.Throws<ReturnInFunctionNodeVisitor.NotAllPathReturnError>(
				delegate { TypeChecking.Process(program); });

			// then
		}
	}
}