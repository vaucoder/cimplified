﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using Cimplified.Lexer;
using Xunit;

namespace Tests.Lexer
{
	public class DfaTestingImplementation : Dfa<char, bool, int>
	{
		private readonly List<int> _acceptingStates;
		private readonly Dictionary<int, Dictionary<char, int>> _statesTransitions;
		private readonly int _startingState;

		public DfaTestingImplementation(Dictionary<int, Dictionary<char, int>> statesTransitions,
			List<int> acceptingStates)
		{
			_statesTransitions = statesTransitions;
			_acceptingStates = acceptingStates;
			_startingState = 0;
		}

		public DfaTestingImplementation(int startingState,
			Dictionary<int, Dictionary<char, int>> statesTransitions,
			List<int> acceptingStates)
		{
			_statesTransitions = statesTransitions;
			_acceptingStates = acceptingStates;
			_startingState = startingState;
		}

		override
		public int Start()
		{
			return _startingState;
		}

		override
		public int Dead()
		{
			return -1;
		}

		override
		public bool Category(int state)
		{
			return _acceptingStates.Contains(state);
		}

		override
		public ImmutableDictionary<char, int> Transitions(int state)
		{
			return _statesTransitions[state].ToImmutableDictionary();
		}

		override
		public bool IsStable(int state)
		{
			var visited = new HashSet<int>();
			var toVisit = new Queue<int>();
			toVisit.Enqueue(state);
			var currentCategory = Category(state);
			while (toVisit.TryDequeue(out var currentState))
			{
				if (currentCategory != Category(currentState))
				{
					return false;
				}

				foreach (var nextState in Transitions(currentState).Values)
				{
					if (!toVisit.Contains(nextState))
					{
						toVisit.Enqueue(nextState);
					}
				}

				visited.Add(currentState);
			}

			return true;
		}
	}

	public static class CombineFunctionFactory<TLabel>
	{
		public static Func<List<TLabel>, bool> AlwaysReturningFalse()
		{
			return list => false;
		}

		public static Func<List<TLabel>, bool> ReturnNotEmpty()
		{
			return list => list.Count > 0;
		}
	}

	public static class DfaTestingImplementationFactory
	{
		public static DfaTestingImplementation Dfa1()
		{
			return new DfaTestingImplementation(new Dictionary<int, Dictionary<char, int>>
			{
				{
					0, new Dictionary<char, int>
					{
						{'a', 1}
					}
				},
				{
					1, new Dictionary<char, int>()
				}
			}, new List<int> {1});
		}

		public static DfaTestingImplementation Dfa2()
		{
			return new DfaTestingImplementation(2, new Dictionary<int, Dictionary<char, int>>
			{
				{
					2, new Dictionary<char, int>
					{
						{'b', 1}
					}
				},
				{
					1, new Dictionary<char, int>()
				}
			}, new List<int> {1});
		}
	}

	public class CombinedDfaTest
	{
		[Fact]
		public void TestEmptyCombinedDfaStart()
		{
			var combinedDfa =
			new CombinedDfa<char, string, int, bool>(new List<(string, Dfa<char, bool, int>)>(),
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Empty(combinedDfa.Start().List);
		}

		[Fact]
		public void TestEmptyCombinedDfaDead()
		{
			var combinedDfa =
			new CombinedDfa<char, string, int, bool>(new List<(string, Dfa<char, bool, int>)>(),
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Empty(combinedDfa.Dead().List);
		}

		[Fact]
		public void TestEmptyCombinedDfaCategory()
		{
			var combinedDfa =
			new CombinedDfa<char, string, int, bool>(new List<(string, Dfa<char, bool, int>)>(),
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.False(combinedDfa.Category(new HashableList<int>()));
		}

		[Fact]
		public void TestEmptyCombinedDfaTransitions()
		{
			var combinedDfa =
			new CombinedDfa<char, string, int, bool>(new List<(string, Dfa<char, bool, int>)>(),
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Empty(combinedDfa.Transitions(new HashableList<int>()));
		}

		[Fact]
		public void TestEmptyCombinedDfaIsStable()
		{
			var combinedDfa =
			new CombinedDfa<char, string, int, bool>(new List<(string, Dfa<char, bool, int>)>(),
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.True(combinedDfa.IsStable(new HashableList<int>()));
		}

		[Fact]
		public void TestSingleCombinedDfaStart()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Equal(new List<int> {0}, combinedDfa.Start().List);
		}

		[Fact]
		public void TestSingleCombinedDfaDead()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Equal(new List<int> {-1}, combinedDfa.Dead().List);
		}

		[Fact]
		public void TestSingleCombinedDfaCategory()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.False(combinedDfa.Category(new HashableList<int>(0)));
		}

		[Fact]
		public void TestSingleCombinedDfaTransitions()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Equal(new Dictionary<char, HashableList<int>>
			{
				{'a', new HashableList<int>(new List<int> {1})}
			}, combinedDfa.Transitions(new HashableList<int>(0)));
		}

		[Fact]
		public void TestSingleCombinedDfaIsStable_False()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.False(combinedDfa.IsStable(new HashableList<int>(0)));
		}

		[Fact]
		public void TestSingleCombinedDfaIsStable_True()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.True(combinedDfa.IsStable(new HashableList<int>(1)));
		}

		[Fact]
		public void TestDoubleCombinedDfaStart()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();
			var dfa2 = DfaTestingImplementationFactory.Dfa2();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1), ("b", dfa2)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Equal(new List<int> {0, 2}, combinedDfa.Start().List);
		}

		[Fact]
		public void TestDoubleCombinedDfaDead()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();
			var dfa2 = DfaTestingImplementationFactory.Dfa2();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1), ("b", dfa2)},
				CombineFunctionFactory<string>.AlwaysReturningFalse());

			Assert.Equal(new List<int> {-1, -1}, combinedDfa.Dead().List);
		}

		[Fact]
		public void TestDoubleCombinedDfaCategory()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();
			var dfa2 = DfaTestingImplementationFactory.Dfa2();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1), ("b", dfa2)},
				CombineFunctionFactory<string>.ReturnNotEmpty());

			Assert.True(combinedDfa.Category(new HashableList<int>(0, 1)));
		}

		[Fact]
		public void TestDoubleCombinedDfaTransitions()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();
			var dfa2 = DfaTestingImplementationFactory.Dfa2();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1), ("b", dfa2)},
				CombineFunctionFactory<string>.ReturnNotEmpty());

			Assert.Equal(new Dictionary<char, HashableList<int>>()
			{
				{
					'a', new HashableList<int>(1, -1)
				},
				{
					'b', new HashableList<int>(-1, 1)
				}
			}, combinedDfa.Transitions(new HashableList<int>(0, 2)));
		}

		[Fact]
		public void TestDoubleCombinedDfaIsStable_True()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();
			var dfa2 = DfaTestingImplementationFactory.Dfa2();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1), ("b", dfa2)},
				CombineFunctionFactory<string>.ReturnNotEmpty());

			Assert.True(combinedDfa.IsStable(new HashableList<int>(1, 1)));
		}

		[Fact]
		public void TestDoubleCombinedDfaIsStable_False()
		{
			var dfa1 = DfaTestingImplementationFactory.Dfa1();
			var dfa2 = DfaTestingImplementationFactory.Dfa2();

			var combinedDfa = new CombinedDfa<char, string, int, bool>(
				new List<(string, Dfa<char, bool, int>)> {("a", dfa1), ("b", dfa2)},
				CombineFunctionFactory<string>.ReturnNotEmpty());

			Assert.False(combinedDfa.IsStable(new HashableList<int>(1, 2)));
		}
	}
}
