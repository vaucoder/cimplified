using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using Cimplified.Lexer;
using Xunit;

namespace Tests.Lexer
{
    class SimpleDfaImplementation : Dfa<char, bool, int>
    {   
        public ImmutableDictionary<int, ImmutableDictionary<char, int>> transitions = null;
        public SimpleDfaImplementation(Dictionary<int, Dictionary<char, int>> transitions)
        {
            this.transitions = transitions.ToImmutableDictionary(e => e.Key, e => e.Value.ToImmutableDictionary());
        }
        public override bool Category(int state)
        {
            return true;
        }

        public override int Start()
        {
            return 0;
        }

        public override int Dead()
        {
            return 3;
        }

        public override bool IsStable(int state)
        {
            return state == 3;
        }

        public override ImmutableDictionary<char, int> Transitions(int state)
        {
            return this.transitions[state];
        }
    }

    class SimpleDfaFactory
    {
        public static SimpleDfaImplementation Dfa1()
        {
            var transitions = new Dictionary<int, Dictionary<char, int>>
            {   
                {0, new Dictionary<char, int>{{'a', 1}, {'b', 2}, {'c', 3}}},
                {1, new Dictionary<char, int>{{'b', 1}, {'c', 2}}},
                {2, new Dictionary<char, int>{{'a', 1}, {'b', 0}}},
                {3, new Dictionary<char, int>()},
            };
            return new SimpleDfaImplementation(transitions);
        }
    }

    public class DfaTransitionsTableTest
    {
        [Fact]
        public void Dfa1Test()
        {
            var dfa = SimpleDfaFactory.Dfa1();
            var expected = dfa.transitions;
            Assert.Equal(expected, dfa.TransitionsTable);
        }
    }

    public class DfaReverseTransitionsTableTest
    {
        [Fact]
        public void Dfa1Test()
        {
            var dfa = SimpleDfaFactory.Dfa1();
            var expected = new Dictionary<int, Dictionary<char, HashSet<int>>>
            {   
                {0, new Dictionary<char, HashSet<int>>{{'b', new HashSet<int>{2}}}},
                {1, new Dictionary<char, HashSet<int>>{{'a', new HashSet<int>{0, 2}}, {'b', new HashSet<int>{1}}}},
                {2, new Dictionary<char, HashSet<int>>{{'b', new HashSet<int>{0}}, {'c', new HashSet<int>{1}}}},
                {3, new Dictionary<char, HashSet<int>>{{'c', new HashSet<int>{0}}}},
            }.ToImmutableDictionary(e => e.Key, e => e.Value.ToImmutableDictionary(x => x.Key, x => x.Value.ToImmutableHashSet()));
            Assert.Equal(expected, dfa.ReverseTransitionsTable);
        }
    }
}
