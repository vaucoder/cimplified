﻿using Cimplified;
using Cimplified.Lexer;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;
using Xunit;

namespace tests.Lexer
{
	public class EquivalenceTest
	{
		public class SimpleDfa<TState> : Dfa<int, bool, TState> where TState: IEquatable<TState>
		{
			private readonly Dictionary<TState, Dictionary<int, TState>> _transitions;
			private readonly HashSet<TState> _endStates;
			private readonly TState _deadState;
			private readonly TState _startState;

			public SimpleDfa(Dictionary<TState, Dictionary<int, TState>> transitions, HashSet<TState> endStates, TState deadState, TState startState)
			{
				_transitions = transitions;
				_endStates = endStates;
				_deadState = deadState;
				_startState = startState;
			}

			public override bool Category(TState state)
			{
				return _endStates.Contains(state);
			}

			public override TState Dead()
			{
				return _deadState;
			}

			public override bool IsStable(TState state)
			{
				return _deadState.Equals(state);
			}

			public override TState Start()
			{
				return _startState;
			}

			public override ImmutableDictionary<int, TState> Transitions(TState state)
			{
				return _transitions[state].ToImmutableDictionary();
			}
		}

		[Fact]
		public void Should_Be_Equivalent_Dfas()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });

			firstTransitions[0].Add(1, 1);
			firstTransitions[0].Add(2, 2);
			firstTransitions[0].Add(3, 3);
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });

			var secondTransitions = new Dictionary<int, Dictionary<int, int>>();

			secondTransitions.Add(0, new Dictionary<int, int>() { });

			secondTransitions[0].Add(1, 1);
			secondTransitions[0].Add(2, 1);
			secondTransitions[0].Add(3, 1);
			secondTransitions.Add(1, new Dictionary<int, int>() { });
			secondTransitions.Add(2, new Dictionary<int, int>() { });

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 1, 2, 3 }, 4, 0);
			var equivalentTestDfa = new SimpleDfa<int>(secondTransitions, new HashSet<int>() { 1 }, 2, 0);

			Assert.True(Utils.Equivalent(testDfa, equivalentTestDfa));
		}

		[Fact]
		public void ShouldNotBe_Equivalent_Dfas()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });

			firstTransitions[0].Add(1, 1);
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions[1].Add(2, 2);
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });

			var secondTransitions = new Dictionary<int, Dictionary<int, int>>();

			secondTransitions.Add(0, new Dictionary<int, int>() { });

			secondTransitions[0].Add(1, 1);
			secondTransitions[0].Add(2, 1);
			secondTransitions.Add(1, new Dictionary<int, int>() { });
			secondTransitions[1].Add(2, 2);
			secondTransitions.Add(2, new Dictionary<int, int>() { });
			secondTransitions.Add(3, new Dictionary<int, int>() { });

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 2 }, 3, 0);
			var nonEquivalentTestDfa = new SimpleDfa<int>(secondTransitions, new HashSet<int>() { 2 }, 3, 0);

			Assert.False(Utils.Equivalent(testDfa, nonEquivalentTestDfa));
		}

		[Fact]
		public void Should_Be_Equivalent_Dfas2()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });

			firstTransitions[0].Add(0, 3);
			firstTransitions[0].Add(1, 1);

			firstTransitions[1].Add(0, 2);
			firstTransitions[1].Add(1, 5);

			firstTransitions[2].Add(0, 2);
			firstTransitions[2].Add(1, 5);

			firstTransitions[3].Add(0, 0);
			firstTransitions[3].Add(1, 4);

			firstTransitions[4].Add(0, 2);
			firstTransitions[4].Add(1, 5);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 1, 4, 2 }, 5, 0);

			var secondTransitions = new Dictionary<int, Dictionary<int, int>>();

			secondTransitions.Add(0, new Dictionary<int, int>() { });
			secondTransitions.Add(1, new Dictionary<int, int>() { });
			secondTransitions.Add(2, new Dictionary<int, int>() { });

			secondTransitions[0].Add(0, 0);
			secondTransitions[0].Add(1, 1);

			secondTransitions[1].Add(0, 1);
			secondTransitions[1].Add(1, 2);

			var equivalentTestDfa = new SimpleDfa<int>(secondTransitions, new HashSet<int>() { 1 }, 2, 0);

			Assert.True(Utils.Equivalent(testDfa, equivalentTestDfa));
		}

		[Fact]
		public void Should_Be_Equivalent_Dfas_With_Diffrent_States()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });

			firstTransitions[0].Add(0, 3);
			firstTransitions[0].Add(1, 1);

			firstTransitions[1].Add(0, 2);
			firstTransitions[1].Add(1, 5);

			firstTransitions[2].Add(0, 2);
			firstTransitions[2].Add(1, 5);

			firstTransitions[3].Add(0, 0);
			firstTransitions[3].Add(1, 4);

			firstTransitions[4].Add(0, 2);
			firstTransitions[4].Add(1, 5);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 1, 4, 2 }, 5, 0);

			var secondTransitions = new Dictionary<char, Dictionary<int, char>>();

			secondTransitions.Add('0', new Dictionary<int, char>() { });
			secondTransitions.Add('1', new Dictionary<int, char>() { });
			secondTransitions.Add('2', new Dictionary<int, char>() { });

			secondTransitions['0'].Add(0, '0');
			secondTransitions['0'].Add(1, '1');

			secondTransitions['1'].Add(0, '1');
			secondTransitions['1'].Add(1, '2');

			var equivalentTestDfa = new SimpleDfa<char>(secondTransitions, new HashSet<char>() { '1' }, '2', '0');

			Assert.True(Utils.Equivalent(testDfa, equivalentTestDfa));
		}

		[Fact]
		public void Should_Be_Equivalent_Dfas3()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });

			firstTransitions[0].Add(0, 1);
			firstTransitions[0].Add(1, 2);

			firstTransitions[1].Add(0, 1);
			firstTransitions[1].Add(1, 3);

			firstTransitions[2].Add(0, 1);
			firstTransitions[2].Add(1, 2);

			firstTransitions[3].Add(0, 1);
			firstTransitions[3].Add(1, 4);

			firstTransitions[4].Add(0, 1);
			firstTransitions[4].Add(1, 2);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 4 }, 5, 0);

			var secondTransitions = new Dictionary<int, Dictionary<int, int>>();

			secondTransitions.Add(0, new Dictionary<int, int>() { });
			secondTransitions.Add(1, new Dictionary<int, int>() { });
			secondTransitions.Add(2, new Dictionary<int, int>() { });
			secondTransitions.Add(3, new Dictionary<int, int>() { });
			secondTransitions.Add(4, new Dictionary<int, int>() { });

			secondTransitions[0].Add(0, 1);
			secondTransitions[0].Add(1, 0);

			secondTransitions[1].Add(0, 1);
			secondTransitions[1].Add(1, 2);

			secondTransitions[2].Add(0, 1);
			secondTransitions[2].Add(1, 3);

			secondTransitions[3].Add(0, 1);
			secondTransitions[3].Add(1, 0);

			var equivalentTestDfa = new SimpleDfa<int>(secondTransitions, new HashSet<int>() { 3 }, 4, 0);

			Assert.True(Utils.Equivalent(testDfa, equivalentTestDfa));
		}

		[Fact]
		public void ShouldNotBe_Equivalent_Dfas2()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });

			firstTransitions[0].Add(0, 1);
			firstTransitions[0].Add(1, 2);

			firstTransitions[1].Add(0, 1);
			firstTransitions[1].Add(1, 3);

			firstTransitions[2].Add(0, 1);
			firstTransitions[2].Add(1, 2);

			firstTransitions[3].Add(0, 1);
			firstTransitions[3].Add(1, 4);

			firstTransitions[4].Add(0, 1);
			firstTransitions[4].Add(1, 2);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 4 }, 5, 0);

			var secondTransitions = new Dictionary<char, Dictionary<int, char>>();

			secondTransitions.Add('0', new Dictionary<int, char>() { });
			secondTransitions.Add('1', new Dictionary<int, char>() { });
			secondTransitions.Add('2', new Dictionary<int, char>() { });

			secondTransitions['0'].Add(0, '0');
			secondTransitions['0'].Add(1, '1');

			secondTransitions['1'].Add(0, '1');
			secondTransitions['1'].Add(1, '2');

			var nonEquivalentTestDfa = new SimpleDfa<char>(secondTransitions, new HashSet<char>() { '1' }, '2', '0');

			Assert.False(Utils.Equivalent(testDfa, nonEquivalentTestDfa));
		}

		[Fact]
		public void ShouldBe_Equivalent_Dfas4()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });
			firstTransitions.Add(6, new Dictionary<int, int>() { });
			firstTransitions.Add(7, new Dictionary<int, int>() { });
			firstTransitions.Add(8, new Dictionary<int, int>() { });

			firstTransitions[0].Add(0, 1);
			firstTransitions[0].Add(1, 5);

			firstTransitions[1].Add(0, 6);
			firstTransitions[1].Add(1, 2);

			firstTransitions[2].Add(0, 0);
			firstTransitions[2].Add(1, 2);

			firstTransitions[3].Add(0, 2);
			firstTransitions[3].Add(1, 6);

			firstTransitions[4].Add(0, 7);
			firstTransitions[4].Add(1, 5);

			firstTransitions[5].Add(0, 2);
			firstTransitions[5].Add(1, 6);

			firstTransitions[6].Add(0, 6);
			firstTransitions[6].Add(1, 4);

			firstTransitions[7].Add(0, 6);
			firstTransitions[7].Add(1, 2);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 2 }, 8, 0);

			var secondTransitions = new Dictionary<char, Dictionary<int, char>>();

			secondTransitions.Add('a', new Dictionary<int, char>() { });
			secondTransitions.Add('b', new Dictionary<int, char>() { });
			secondTransitions.Add('c', new Dictionary<int, char>() { });
			secondTransitions.Add('d', new Dictionary<int, char>() { });
			secondTransitions.Add('e', new Dictionary<int, char>() { });
			secondTransitions.Add('f', new Dictionary<int, char>() { });

			secondTransitions['a'].Add(0, 'b');
			secondTransitions['a'].Add(1, 'd');

			secondTransitions['b'].Add(0, 'c');
			secondTransitions['b'].Add(1, 'e');

			secondTransitions['c'].Add(0, 'c');
			secondTransitions['c'].Add(1, 'a');

			secondTransitions['d'].Add(0, 'e');
			secondTransitions['d'].Add(1, 'c');

			secondTransitions['e'].Add(0, 'a');
			secondTransitions['e'].Add(1, 'e');

			var equivalentTestDfa = new SimpleDfa<char>(secondTransitions, new HashSet<char>() { 'e' }, 'f', 'a');

			Assert.True(Utils.Equivalent(testDfa, equivalentTestDfa));
		}

		[Fact]
		public void ShouldNotBe_Equivalent_Dfas3()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });
			firstTransitions.Add(6, new Dictionary<int, int>() { });
			firstTransitions.Add(7, new Dictionary<int, int>() { });
			firstTransitions.Add(8, new Dictionary<int, int>() { });

			firstTransitions[0].Add(0, 1);
			firstTransitions[0].Add(1, 5);

			firstTransitions[1].Add(0, 2);
			firstTransitions[1].Add(1, 6);

			firstTransitions[2].Add(0, 0);
			firstTransitions[2].Add(1, 2);

			firstTransitions[3].Add(0, 2);
			firstTransitions[3].Add(1, 6);

			firstTransitions[4].Add(0, 7);
			firstTransitions[4].Add(1, 5);

			firstTransitions[5].Add(0, 2);
			firstTransitions[5].Add(1, 6);

			firstTransitions[6].Add(0, 6);
			firstTransitions[6].Add(1, 4);

			firstTransitions[7].Add(0, 6);
			firstTransitions[7].Add(1, 2);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 2 }, 8, 0);

			var secondTransitions = new Dictionary<char, Dictionary<int, char>>();

			secondTransitions.Add('a', new Dictionary<int, char>() { });
			secondTransitions.Add('b', new Dictionary<int, char>() { });
			secondTransitions.Add('c', new Dictionary<int, char>() { });
			secondTransitions.Add('d', new Dictionary<int, char>() { });
			secondTransitions.Add('e', new Dictionary<int, char>() { });
			secondTransitions.Add('f', new Dictionary<int, char>() { });

			secondTransitions['a'].Add(0, 'b');
			secondTransitions['a'].Add(1, 'd');

			secondTransitions['b'].Add(0, 'c');
			secondTransitions['b'].Add(1, 'e');

			secondTransitions['c'].Add(0, 'c');
			secondTransitions['c'].Add(1, 'a');

			secondTransitions['d'].Add(0, 'e');
			secondTransitions['d'].Add(1, 'c');

			secondTransitions['e'].Add(0, 'a');
			secondTransitions['e'].Add(1, 'e');

			var nonEquivalentTestDfa = new SimpleDfa<char>(secondTransitions, new HashSet<char>() { 'e' }, 'f', 'a');

			Assert.False(Utils.Equivalent(testDfa, nonEquivalentTestDfa));
		}
	}
}
