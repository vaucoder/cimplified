using Cimplified.Lexer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tests.Lexer
{
	public class LexerTests
	{
		static List<(int, Regex<char>)> makeDict(List<Regex<char>> regexes)
		{
			var ret = new List<(int, Regex<char>)>();
			foreach (var r in regexes) {
				ret.Add((regexes.IndexOf(r), r));
			}
			return ret;
		}

		static Cimplified.Lexer.Lexer<int> makeLexer(List<Regex<char>> regexes)
		{
			return new Cimplified.Lexer.Lexer<int>(makeDict(regexes));
		}

		[Theory]
		[InlineData("a")]
		[InlineData("b")]
		[InlineData("abab")]
		[InlineData("ab\nab")]
		[InlineData("\n\n\na\n\n")]
		public void Test_Positive_0(string text)
		{
			var lines = text.Split(Environment.NewLine.ToCharArray()).ToList();
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('\n');
			var L = makeLexer(new List<Regex<char>> {a, b, c});

			L.Process(lines).ToList();
		}

		[Theory]
		[InlineData("")]
		[InlineData("ac")]
		[InlineData("ababc")]
		public void Test_Negative_0(string text)
		{
			var lines = text.Split(Environment.NewLine.ToCharArray()).ToList();
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('\n');
			var L = makeLexer(new List<Regex<char>> {a, b, c});

			Assert.Throws<UnknownTokenError>(() => L.Process(lines).ToList());
		}

		[Theory]
		[InlineData("ab\nab")]
		[InlineData("\n\n\na\n\n")]
		public void Test_Negative_NoNewlineRegex(string text)
		{
			var lines = text.Split(Environment.NewLine.ToCharArray()).ToList();
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var L = makeLexer(new List<Regex<char>> {a, b});

			Assert.Throws<UnknownTokenError>(() => L.Process(lines).ToList());
		}

		[Theory]
		[InlineData("")]
		[InlineData("a")]
		[InlineData("b")]
		[InlineData("abab")]
		public void Test_Positive_Empty(string text)
		{
			var lines = text.Split(Environment.NewLine.ToCharArray()).ToList();
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new StarRegex<char>(a);
			var L = makeLexer(new List<Regex<char>> {a, b, c});

			L.Process(lines).ToList();
		}

		static void check_content(string text, params (int, string, int, int)[] answer_tuple)
		{
			var lines = text.Split(Environment.NewLine.ToCharArray()).ToList();
			var answer = answer_tuple.Select(p => new Token<int>
				{
					Kind = p.Item1,
					Text = p.Item2,
					Loc = new Location { Line = p.Item3, Pos = p.Item4 },
				}).ToList();

			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('\n');
			var d = new ConcatRegex<char>(a, b);
			var L = makeLexer(new List<Regex<char>> {d, a, b, c});
			var result = L.Process(lines).ToList();

			Assert.Equal(answer, result);
		}

		[Fact]
		public void Test_Positive_Content_1()
		{
			check_content("a", (1, "a", 0, 0));
		}

		[Fact]
		public void Test_Positive_Content_2()
		{
			check_content("b", (2, "b", 0, 0));
		}

		[Fact]
		public void Test_Positive_Content_3()
		{
			check_content("abab", (0, "ab", 0, 0), (0, "ab", 0, 2));
		}

		[Fact]
		public void Test_Positive_Content_4()
		{
			check_content("aabaabb", (1, "a", 0, 0), (0, "ab", 0, 1), (1, "a", 0, 3), (0, "ab", 0, 4), (2, "b", 0, 6));
		}

		[Fact]
		public void Test_Positive_Content_5()
		{
			check_content("aabaab\nb", (1, "a", 0, 0), (0, "ab", 0, 1), (1, "a", 0, 3), (0, "ab", 0, 4), (3, "\n", 0, 6), (2, "b", 1, 0));
		}
	}
}
