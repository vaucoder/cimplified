﻿using Cimplified.Lexer;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests.Lexer
{
	public class RegexEqualsTest
	{
		[Fact]
		public void TestEqualsAtomic()
		{
			var reg1 = new AtomicRegex<char>('a');
			var reg2 = new AtomicRegex<char>('b');
			var reg3 = new AtomicRegex<char>('a');
			Assert.True(reg1.Equals(reg3));
			Assert.False(reg1.Equals(reg2));
		}

		[Fact]
		public void TestEqualsUnion()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('b');
			var d = new AtomicRegex<char>('d');
			var reg1 = new UnionRegex<char>(a, b);
			var reg2 = new UnionRegex<char>(a, c);
			var reg3 = new UnionRegex<char>(a, d);
			Assert.True(reg1.Equals(reg2));
			Assert.False(reg1.Equals(reg3));
			Assert.False(reg3.Equals(reg1));
		}

		[Fact]
		public void TestEqualsConcat()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('b');
			var d = new AtomicRegex<char>('d');
			var reg1 = new ConcatRegex<char>(a, b, c);
			var reg2 = new ConcatRegex<char>(a, b, b);
			var reg3 = new ConcatRegex<char>(b);
			var reg4 = new ConcatRegex<char>(c);
			var reg5 = new ConcatRegex<char>(d, c);
			var reg6 = new ConcatRegex<char>(c, d);
			Assert.True(reg1.Equals(reg2));
			Assert.True(reg3.Equals(reg3));
			Assert.False(reg5.Equals(reg6));
		}

		[Fact]
		public void TestEqualsStar()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('b');
			var reg1 = new StarRegex<char>(a);
			var reg2 = new StarRegex<char>(b);
			var reg3 = new StarRegex<char>(c);
			Assert.True(reg2.Equals(reg3));
			Assert.True(reg3.Equals(reg2));
			Assert.False(reg1.Equals(reg2));
			Assert.False(reg1.Equals(a));
		}

		[Fact]
		public void TestEqualsEmpty()
		{
			var reg1 = new EmptyRegex<char>();
			var reg2 = new EmptyRegex<char>();
			var reg3 = new UnionRegex<char>();
			var reg4 = new ConcatRegex<char>();
			Assert.True(reg1.Equals(reg2));
			Assert.False(reg2.Equals(reg3));
			Assert.False(reg2.Equals(reg4));
		}

		[Fact]
		public void TestEqualsDifferentClasses()
		{
			var reg1 = new EmptyRegex<char>();
			var reg2 = new UnionRegex<char>();
			var reg3 = new ConcatRegex<char>();
			var reg4 = new StarRegex<char>(reg1);
			Assert.False(reg1.Equals(reg2));
			Assert.False(reg1.Equals(reg3));
			Assert.False(reg1.Equals(reg4));
			Assert.False(reg2.Equals(reg3));
			Assert.False(reg2.Equals(reg4));
			Assert.False(reg3.Equals(reg4));
		}
	}

	public class RegexNormalizeTest
	{
		[Fact]
		public void NormalizeEmptyTest()
		{
			var reg1 = new EmptyRegex<char>();
			var reg2 = new UnionRegex<char>();
			var reg3 = new ConcatRegex<char>();
			Assert.True(reg1.Equals(reg2.Normalize()));
			Assert.True(reg1.Equals(reg3.Normalize()));
			Assert.True(reg2.Normalize().Equals(reg3.Normalize()));
			Assert.True(reg1.Equals(reg1.Normalize()));
		}

		[Fact]
		public void NormalizeAtomicTest()
		{
			var reg1 = new AtomicRegex<char>('a');
			var reg2 = new AtomicRegex<char>('b');
			Assert.True(reg1.Normalize().Equals(reg1));
			Assert.False(reg1.Normalize().Equals(reg2));
		}

		[Fact]
		public void NormalizeUnionTest()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('a');
			var c = new AtomicRegex<char>('b');
			var d = new AtomicRegex<char>('d');
			var e = new AtomicRegex<char>('e');
			var reg1 = new UnionRegex<char>(a, c, b);
			var reg2 = new UnionRegex<char>(b, c);
			Assert.True(reg1.Equals(reg2));
			Assert.True(reg1.Normalize().Equals(reg2.Normalize()));
			var reg3 = new UnionRegex<char>(a, d);
			var reg4 = new UnionRegex<char>(reg3, c);
			var reg5 = new UnionRegex<char>(a, c, d);
			Assert.False(reg4.Equals(reg5));
			Assert.True(reg4.Normalize().Equals(reg5.Normalize()));
			var reg6 = new UnionRegex<char>(c, e);
			var reg7 = new UnionRegex<char>(reg3, reg6);
			var reg8 = new UnionRegex<char>(a, b, c, d, e);
			Assert.True(reg7.Normalize().Equals(reg8.Normalize()));
			Assert.False(reg7.Equals(reg8));
			var reg9 = new UnionRegex<char>(a);
			Assert.True(reg9.Normalize().Equals(a));
			Assert.False(reg9.Equals(a.Normalize()));
		}

		[Fact]
		public void NormalizeConcatTest()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('a');
			var c = new AtomicRegex<char>('b');
			var d = new AtomicRegex<char>('d');
			var e = new AtomicRegex<char>('e');
			var reg1 = new ConcatRegex<char>(a, b, c);
			var reg2 = new ConcatRegex<char>(b, a, c);
			var reg3 = new ConcatRegex<char>(d, e);
			var reg4 = new ConcatRegex<char>(e, d);
			Assert.False(reg3.Equals(reg4));
			Assert.False(reg3.Normalize().Equals(reg4.Normalize()));
			Assert.True(reg1.Normalize().Equals(reg2.Normalize()));
			var reg5 = new ConcatRegex<char>(reg1, reg3);
			var reg6 = new ConcatRegex<char>(reg1, reg4);
			Assert.False(reg5.Normalize().Equals(reg6.Normalize()));
			var reg7 = new ConcatRegex<char>(a, a, c, d, e);
			Assert.False(reg7.Equals(reg5));
			Assert.True(reg7.Normalize().Equals(reg5.Normalize()));
		}

		[Fact]
		public void NormalizeStar()
		{
			var a = new AtomicRegex<char>('a');
			var reg1 = new StarRegex<char>(a);
			var reg2 = new StarRegex<char>(reg1);
			Assert.False(reg1.Equals(reg2));
			Assert.True(reg1.Normalize().Equals(reg2.Normalize()));
			var reg3 = new StarRegex<char>(new EmptyRegex<char>()); // \epsilon
			var reg4 = new StarRegex<char>(reg3);
			Assert.True(reg3.Normalize().Equals(reg4.Normalize()));
			Assert.False(reg3.Equals(reg4));
		}
	}
}
