using System;
using Xunit;
using Cimplified.Lexer;
using System.Collections.Generic;

namespace Tests.Lexer
{
	public class RegexIsEmptyTest
	{
		[Fact]
		public void TestAtomicRegex()
		{
			var regex = new AtomicRegex<char>('a');
			Assert.False(regex.IsEmpty());
		}

		[Fact]
		public void TestStarRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var empty = new UnionRegex<char>();
			StarRegex<char> regex = null;

			regex = new StarRegex<char>(a);
			Assert.False(regex.IsEmpty());

			regex = new StarRegex<char>(empty);
			Assert.False(regex.IsEmpty());

			regex = new StarRegex<char>(new UnionRegex<char>(a, b));
			Assert.False(regex.IsEmpty());

			regex = new StarRegex<char>(new ConcatRegex<char>(a, b));
			Assert.False(regex.IsEmpty());

			regex = new StarRegex<char>(new StarRegex<char>(a));
			Assert.False(regex.IsEmpty());
		}

		[Fact]
		public void TestUnionRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			UnionRegex<char> regex = null;

			regex = new UnionRegex<char>();
			Assert.True(regex.IsEmpty());

			regex = new UnionRegex<char>(empty);
			Assert.True(regex.IsEmpty());

			regex = new UnionRegex<char>(a);
			Assert.False(regex.IsEmpty());

			regex = new UnionRegex<char>(a, empty);
			Assert.False(regex.IsEmpty());

			regex = new UnionRegex<char>(new StarRegex<char>(a), b);
			Assert.False(regex.IsEmpty());

			regex = new UnionRegex<char>(new ConcatRegex<char>(a, b), c);
			Assert.False(regex.IsEmpty());
		}

		[Fact]
		public void TestConcatRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			ConcatRegex<char> regex = null;

			regex = new ConcatRegex<char>();
			Assert.True(regex.IsEmpty());

			regex = new ConcatRegex<char>(empty);
			Assert.True(regex.IsEmpty());

			regex = new ConcatRegex<char>(empty, empty);
			Assert.True(regex.IsEmpty());

			regex = new ConcatRegex<char>(a, empty);
			Assert.True(regex.IsEmpty());

			regex = new ConcatRegex<char>(a, b);
			Assert.False(regex.IsEmpty());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), b);
			Assert.False(regex.IsEmpty());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), empty, b);
			Assert.True(regex.IsEmpty());

			regex = new ConcatRegex<char>(new UnionRegex<char>(a, b), c);
			Assert.False(regex.IsEmpty());
		}
	}

	public class RegexContainsEpsilonTest
	{
		[Fact]
		public void TestAtomicRegex()
		{
			var regex = new AtomicRegex<char>('a');
			Assert.False(regex.ContainsEpsilon());
		}

		[Fact]
		public void TestStarRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var empty = new UnionRegex<char>();
			StarRegex<char> regex = null;

			regex = new StarRegex<char>(a);
			Assert.True(regex.ContainsEpsilon());

			regex = new StarRegex<char>(empty);
			Assert.True(regex.ContainsEpsilon());

			regex = new StarRegex<char>(new UnionRegex<char>(a, b));
			Assert.True(regex.ContainsEpsilon());

			regex = new StarRegex<char>(new ConcatRegex<char>(a, b));
			Assert.True(regex.ContainsEpsilon());

			regex = new StarRegex<char>(new StarRegex<char>(a));
			Assert.True(regex.ContainsEpsilon());
		}

		[Fact]
		public void TestUnionRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			UnionRegex<char> regex = null;

			regex = new UnionRegex<char>();
			Assert.False(regex.ContainsEpsilon());

			regex = new UnionRegex<char>(empty);
			Assert.False(regex.ContainsEpsilon());

			regex = new UnionRegex<char>(a);
			Assert.False(regex.ContainsEpsilon());

			regex = new UnionRegex<char>(new StarRegex<char>(a), b);
			Assert.True(regex.ContainsEpsilon());

			regex = new UnionRegex<char>(new ConcatRegex<char>(a, b), c);
			Assert.False(regex.ContainsEpsilon());
		}

		[Fact]
		public void TestConcatRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			ConcatRegex<char> regex = null;

			regex = new ConcatRegex<char>();
			Assert.False(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(empty);
			Assert.False(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(new StarRegex<char>(empty));
			Assert.True(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(new StarRegex<char>(empty), empty);
			Assert.False(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(a, b);
			Assert.False(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), b);
			Assert.False(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), new StarRegex<char>(b));
			Assert.True(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), empty, new StarRegex<char>(b));
			Assert.False(regex.ContainsEpsilon());

			regex = new ConcatRegex<char>(new UnionRegex<char>(a, b), c);
			Assert.False(regex.ContainsEpsilon());
		}
	}

	public class RegexHeadsTest
	{
		[Fact]
		public void TestAtomicRegex()
		{
			var regex = new AtomicRegex<char>('a');
			var expected = new HashSet<char> {'a'};
			Assert.Equal(expected, regex.Heads());
		}

		[Fact]
		public void TestStarRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var empty = new UnionRegex<char>();
			StarRegex<char> regex = null;
			HashSet<char> expected = null;

			regex = new StarRegex<char>(a);
			expected = new HashSet<char> {'a'};
			Assert.Equal(expected, regex.Heads());

			regex = new StarRegex<char>(empty);
			expected = new HashSet<char> { };
			Assert.Equal(expected, regex.Heads());

			regex = new StarRegex<char>(new UnionRegex<char>(a, b));
			expected = new HashSet<char> {'a', 'b'};
			Assert.Equal(expected, regex.Heads());

			regex = new StarRegex<char>(new ConcatRegex<char>(a, b));
			expected = new HashSet<char> {'a'};
			Assert.Equal(expected, regex.Heads());

			regex = new StarRegex<char>(new StarRegex<char>(a));
			expected = new HashSet<char> {'a'};
			Assert.Equal(expected, regex.Heads());
		}

		[Fact]
		public void TestUnionRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			UnionRegex<char> regex = null;
			HashSet<char> expected = null;

			regex = new UnionRegex<char>();
			expected = new HashSet<char>();
			Assert.Equal(expected, regex.Heads());

			regex = new UnionRegex<char>(empty);
			expected = new HashSet<char>();
			Assert.Equal(expected, regex.Heads());

			regex = new UnionRegex<char>(a);
			expected = new HashSet<char> {'a'};
			Assert.Equal(expected, regex.Heads());

			regex = new UnionRegex<char>(a, b, empty);
			expected = new HashSet<char> {'a', 'b'};
			Assert.Equal(expected, regex.Heads());

			regex = new UnionRegex<char>(new StarRegex<char>(a), b);
			expected = new HashSet<char> {'a', 'b'};
			Assert.Equal(expected, regex.Heads());

			regex = new UnionRegex<char>(new ConcatRegex<char>(a, b), c);
			expected = new HashSet<char> {'a', 'c'};
			Assert.Equal(expected, regex.Heads());

		}

		[Fact]
		public void TestConcatRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			ConcatRegex<char> regex = null;
			HashSet<char> expected = null;

			regex = new ConcatRegex<char>();
			expected = new HashSet<char>();
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(empty);
			expected = new HashSet<char>();
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(new StarRegex<char>(empty));
			expected = new HashSet<char> { };
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(new StarRegex<char>(empty), empty);
			expected = new HashSet<char>();
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(a, b);
			expected = new HashSet<char> {'a'};
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), b);
			expected = new HashSet<char> {'a', 'b'};
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), new StarRegex<char>(b));
			expected = new HashSet<char> {'a', 'b'};
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), empty, new StarRegex<char>(b));
			expected = new HashSet<char>();
			Assert.Equal(expected, regex.Heads());

			regex = new ConcatRegex<char>(new UnionRegex<char>(a, b), c);
			expected = new HashSet<char> {'a', 'b'};
			Assert.Equal(expected, regex.Heads());
		}
	}

	public class RegexDerivativeTest
	{

		[Fact]
		public void TestAtomicRegex()
		{
			AtomicRegex<char> regex = null;
			Regex<char> derivative = null;
			Regex<char> expected = null;

			regex = new AtomicRegex<char>('a');
			derivative = regex.Derivative('a');
			expected = new StarRegex<char>(new UnionRegex<char>());
			Assert.Equal(expected, derivative);

			regex = new AtomicRegex<char>('a');
			derivative = regex.Derivative('b');
			expected = new UnionRegex<char>();
			Assert.Equal(expected, derivative);
		}

		[Fact]
		public void TestStarRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var empty = new UnionRegex<char>();
			StarRegex<char> regex = null;
			Regex<char> derivative = null;
			Regex<char> expected = null;

			regex = new StarRegex<char>(a);
			derivative = regex.Derivative('a');
			expected = new StarRegex<char>(a);
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new StarRegex<char>(a);
			derivative = regex.Derivative('b');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new StarRegex<char>(empty);
			derivative = regex.Derivative('b');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new StarRegex<char>(new UnionRegex<char>(a, b));
			derivative = regex.Derivative('a');
			expected = new StarRegex<char>(new UnionRegex<char>(a, b));
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new StarRegex<char>(new ConcatRegex<char>(a, b));
			derivative = regex.Derivative('a');
			expected = new ConcatRegex<char>(b, new StarRegex<char>(new ConcatRegex<char>(a, b)));
			Assert.Equal(expected.Normalize(), derivative.Normalize());
		}

		[Fact]
		public void TestUnionRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			UnionRegex<char> regex = null;
			Regex<char> derivative = null;
			Regex<char> expected = null;

			regex = new UnionRegex<char>();
			derivative = regex.Derivative('a');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new UnionRegex<char>(empty);
			derivative = regex.Derivative('a');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new UnionRegex<char>(a);
			derivative = regex.Derivative('a');
			expected = new StarRegex<char>(empty);
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new UnionRegex<char>(a, b, empty);
			derivative = regex.Derivative('a');
			expected = new StarRegex<char>(empty);
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new UnionRegex<char>(new StarRegex<char>(a), b);
			derivative = regex.Derivative('a');
			expected = new StarRegex<char>(a);
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new UnionRegex<char>(new ConcatRegex<char>(a, b), c);
			derivative = regex.Derivative('a');
			expected = new AtomicRegex<char>('b');
			Assert.Equal(expected.Normalize(), derivative.Normalize());
		}

		[Fact]
		public void TestConcatRegex()
		{
			var a = new AtomicRegex<char>('a');
			var b = new AtomicRegex<char>('b');
			var c = new AtomicRegex<char>('c');
			var empty = new UnionRegex<char>();
			ConcatRegex<char> regex = null;
			Regex<char> derivative = null;
			Regex<char> expected = null;

			regex = new ConcatRegex<char>();
			derivative = regex.Derivative('a');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new ConcatRegex<char>(empty);
			derivative = regex.Derivative('a');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new ConcatRegex<char>(new StarRegex<char>(empty));
			derivative = regex.Derivative('a');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new ConcatRegex<char>(new StarRegex<char>(empty), a);
			derivative = regex.Derivative('a');
			expected = new StarRegex<char>(empty);
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new ConcatRegex<char>(a, b);
			derivative = regex.Derivative('a');
			expected = new AtomicRegex<char>('b');
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new ConcatRegex<char>(a, b);
			derivative = regex.Derivative('b');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new ConcatRegex<char>(new StarRegex<char>(a), empty, new StarRegex<char>(a));
			derivative = regex.Derivative('a');
			expected = new UnionRegex<char>();
			Assert.Equal(expected.Normalize(), derivative.Normalize());

			regex = new ConcatRegex<char>(new UnionRegex<char>(a, b), c);
			derivative = regex.Derivative('a');
			expected = new AtomicRegex<char>('c');
			Assert.Equal(expected.Normalize(), derivative.Normalize());
		}
	}

}
