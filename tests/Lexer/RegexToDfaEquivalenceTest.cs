﻿using Cimplified.Lexer;
using Cimplified;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using static tests.Lexer.EquivalenceTest;

namespace tests.Lexer
{
	public class RegexToDfaEquivalenceTest
	{
		[Fact]
		public void Should_Be_Equivalent_RegexDfa()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });

			firstTransitions[0].Add(1, 1);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 1 }, 2, 0);

			var regexDfa = Utils.RegexToDfa<int>(new AtomicRegex<int>(1));

			Assert.True(Utils.Equivalent(testDfa, regexDfa));
		}

		[Fact]
		public void Should_Be_Equivalent_RegexDfa2()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });

			firstTransitions[0].Add(9, 1);

			firstTransitions[1].Add(1, 1);
			firstTransitions[1].Add(2, 1);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 1 }, 2, 0);

			var regexDfa = Utils.RegexToDfa<int>(
				new ConcatRegex<int>(
					new Regex<int>[] {
						new AtomicRegex<int>(9),
						new StarRegex<int>(
							new UnionRegex<int>(new Regex<int>[] {
								new ConcatRegex<int>(new Regex<int>[] {
									new AtomicRegex<int>(1),
							}),
								new ConcatRegex<int>(new Regex<int>[] {
									new AtomicRegex<int>(2),
							})
							})
							)
					}));

			Assert.True(Utils.Equivalent(testDfa, regexDfa));
		}

		private class StringRegex : ConcatRegex<int>
		{
			public StringRegex(List<int> symbols) : base(symbols.Select(x => new AtomicRegex<int>(x)).ToArray())
			{

			}
		}



		[Fact]
		public void Should_Be_Equivalent_RegexDfa3()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });
			firstTransitions.Add(6, new Dictionary<int, int>() { });
			firstTransitions.Add(7, new Dictionary<int, int>() { });

			firstTransitions[0].Add(9, 1);

			firstTransitions[1].Add(1, 2);

			firstTransitions[2].Add(2, 3);

			firstTransitions[3].Add(3, 4);

			firstTransitions[4].Add(4, 5);
			firstTransitions[4].Add(1, 2);

			firstTransitions[5].Add(5, 6);

			firstTransitions[6].Add(1, 2);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 1, 4, 6 }, 7, 0);

			var regexDfa = Utils.RegexToDfa<int>(
				new ConcatRegex<int>(
					new Regex<int>[] {
						new AtomicRegex<int>(9),
						new StarRegex<int>(
							new UnionRegex<int>(new Regex<int>[] {
								new StringRegex(new List<int>() { 1,2,3,4,5}),
								new StringRegex(new List<int>() { 1,2,3,})
							})
							)
					}));

			Assert.True(Utils.Equivalent(testDfa, regexDfa));
		}

		[Fact]
		public void Should_Be_Equivalent_RegexDfa4()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });
			firstTransitions.Add(6, new Dictionary<int, int>() { });
			firstTransitions.Add(7, new Dictionary<int, int>() { });

			firstTransitions[1].Add(1, 2);

			firstTransitions[2].Add(2, 3);

			firstTransitions[3].Add(2, 3);
			firstTransitions[3].Add(3, 4);

			firstTransitions[4].Add(2, 3);
			firstTransitions[4].Add(3, 5);
			firstTransitions[4].Add(5, 6);

			firstTransitions[5].Add(2, 3);
			firstTransitions[5].Add(3, 5);
			firstTransitions[5].Add(4, 4);
			firstTransitions[5].Add(5, 6);

			firstTransitions[6].Add(1, 4);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 2, 3, 4, 5 }, 7, 1);

			var regexDfa = Utils.RegexToDfa<int>(
				new ConcatRegex<int>(
					new Regex<int>[] {
						new AtomicRegex<int>(1),
						new StarRegex<int>(
							new ConcatRegex<int>(new Regex<int>[] {
								new AtomicRegex<int>(2),
								new StarRegex<int>(
									new ConcatRegex<int>(new Regex<int>[] {
										new AtomicRegex<int>(3),
										new StarRegex<int>(
											new UnionRegex<int>(new Regex<int>[] {
												new StringRegex(new List<int>() { 3, 4}),
												new StringRegex(new List<int>() { 5, 1})
											})
										)
									})
								)
							})
						)
					}));

			Assert.True(Utils.Equivalent(testDfa, regexDfa));
		}

		[Fact]
		public void Should_Be_Equivalent_RegexDfa5()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });

			firstTransitions[1].Add(1, 2);
			firstTransitions[2].Add(2, 1);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 1 }, 3, 1);

			var regexDfa = Utils.RegexToDfa<int>(new StarRegex<int>(new StringRegex(new List<int>() { 1, 2 })));
			Assert.True(Utils.Equivalent(testDfa, regexDfa));
		}

		[Fact]
		public void Should_Be_Equivalent_RegexDfa6()
		{
			var firstTransitions = new Dictionary<int, Dictionary<int, int>>();

			firstTransitions.Add(0, new Dictionary<int, int>() { });
			firstTransitions.Add(1, new Dictionary<int, int>() { });
			firstTransitions.Add(2, new Dictionary<int, int>() { });
			firstTransitions.Add(3, new Dictionary<int, int>() { });
			firstTransitions.Add(4, new Dictionary<int, int>() { });
			firstTransitions.Add(5, new Dictionary<int, int>() { });
			firstTransitions.Add(6, new Dictionary<int, int>() { });
			firstTransitions.Add(7, new Dictionary<int, int>() { });
			firstTransitions.Add(8, new Dictionary<int, int>() { });
			firstTransitions.Add(9, new Dictionary<int, int>() { });

			firstTransitions[0].Add(3, 2);
			firstTransitions[0].Add(1, 1);

			firstTransitions[2].Add(4, 3);

			firstTransitions[1].Add(2, 4);

			firstTransitions[3].Add(1, 5);
			firstTransitions[3].Add(3, 7);

			firstTransitions[4].Add(1, 1);
			firstTransitions[4].Add(3, 7);

			firstTransitions[5].Add(2, 6);

			firstTransitions[7].Add(4, 8);

			firstTransitions[8].Add(3, 7);

			var testDfa = new SimpleDfa<int>(firstTransitions, new HashSet<int>() { 0, 3, 4, 6, 8 }, 9, 0);

			var regexDfa = Utils.RegexToDfa(
				new ConcatRegex<int>(
					new[] { 
						new UnionRegex<int>(new Regex<int>[] { 
							new StarRegex<int>(new StringRegex(new List<int>(){ 1, 2 })),
							new StringRegex(new List<int>(){ 3, 4 })
						}),
						new UnionRegex<int>(new Regex<int>[] {
							new StringRegex(new List<int>(){ 1, 2 }),
							new StarRegex<int>(new StringRegex(new List<int>(){ 3, 4 }))
						}),
					}
				)
			);

			Assert.True(Utils.Equivalent(testDfa, regexDfa));
		}

	}
}
