﻿using Cimplified.Lexer;
using Cimplified;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tests.Lexer
{
	public class RegexToDfaTests
	{
		private Regex<char> WalkDfa(Dfa<char, bool, Regex<char>> dfa, string text)
		{
			var stuck = false;
			var currState = dfa.Start();
			int i = 0;

			while (!stuck && i != text.Length)
			{
				var transitions = dfa.Transitions(currState);

				if (transitions.ContainsKey(text[i]))
				{

					var nextState = transitions[text[i]];
					i++;

					currState = nextState;
				} else
				{
					stuck = true;
					currState = dfa.Dead();
				}
			}

			return currState;
		}

		[Theory]
		[InlineData("")]
		[InlineData("a")]
		[InlineData("aaaaaaaaaaa")]
		public void Should_CreateDfa_Accepting_StarRegex_of_a(string text)
		{
			var regex = new StarRegex<char>(new AtomicRegex<char>('a'));

			var dfa = Utils.RegexToDfa(regex);

			var endRegex = WalkDfa(dfa, text);

			Assert.True(dfa.Category(endRegex));
		}

		[Theory]
		[InlineData("")]
		[InlineData("abaab")]
		[InlineData("abababababab")]
		[InlineData("aaaaab")]
		public void Should_CreateDfa_Accepting_StarRegex_of_SymbolUnion_of_ab(string text)
		{
			var regex = new StarRegex<char>(new UnionRegex<char>(new[]
			{new AtomicRegex<char>('a'), new AtomicRegex<char>('b')}));

			var dfa = Utils.RegexToDfa(regex);

			var endRegex = WalkDfa(dfa, text);

			Assert.True(dfa.Category(endRegex));
		}

		[Theory]
		[InlineData("a")]
		[InlineData("abaab")]
		[InlineData("ababababababa")]
		[InlineData("aab")]
		public void Should_CreateDfa_NotAccepting_StarRegex_of_Concat_of_ab(string text)
		{
			var regex = new StarRegex<char>(new ConcatRegex<char>(
				new[] {new AtomicRegex<char>('a'), new AtomicRegex<char>('b')}));

			var dfa = Utils.RegexToDfa(regex);

			var endRegex = WalkDfa(dfa, text);

			Assert.False(dfa.Category(endRegex));
		}

		[Theory]
		[InlineData("if")]
		[InlineData("while")]
		[InlineData("true")]
		[InlineData("false")]
		public void Should_CreateDfa_Accepting_Keyword(string keyword)
		{
			var regex = new StarRegex<char>(new ConcatRegex<char>(
				keyword.ToCharArray().Select(c => new AtomicRegex<char>(c)).ToArray()));

			var dfa = Utils.RegexToDfa(regex);

			var endRegex = WalkDfa(dfa, keyword);

			Assert.True(dfa.Category(endRegex));
		}

		private Regex<char> DigitRegex() => new UnionRegex<char>(
			new[]
			{
				new AtomicRegex<char>('0'),
				new AtomicRegex<char>('1'),
				new AtomicRegex<char>('2'),
				new AtomicRegex<char>('3'),
				new AtomicRegex<char>('4'),
				new AtomicRegex<char>('5'),
				new AtomicRegex<char>('6'),
				new AtomicRegex<char>('7'),
				new AtomicRegex<char>('8'),
				new AtomicRegex<char>('9')
			});

		private Regex<char> NumberRegex() => new ConcatRegex<char>(
			new[] {DigitRegex(), new StarRegex<char>(DigitRegex())});

		[Theory]
		[InlineData("123")]
		[InlineData("19021")]
		[InlineData("0")]
		[InlineData("9999")]
		[InlineData("00001")]
		public void Should_CreateDfa_Accepting_Numbers(string number)
		{
			var dfa = Utils.RegexToDfa(NumberRegex());

			var endRegex = WalkDfa(dfa, number);

			Assert.True(dfa.Category(endRegex));
		}

		[Theory]
		[InlineData("123a")]
		[InlineData("abc")]
		[InlineData("")]
		[InlineData("9999.0")]
		public void Should_CreateDfa_Rejecting_IncorrectNumbers(string number)
		{
			var dfa = Utils.RegexToDfa(NumberRegex());

			var endRegex = WalkDfa(dfa, number);

			Assert.False(dfa.Category(endRegex));
		}

		private Regex<char> KeywordRegex() => new StarRegex<char>(new UnionRegex<char>(
			new Regex<char>[]
			{
				new ConcatRegex<char>(new[]
				{
					new AtomicRegex<char>('w'),
					new AtomicRegex<char>('h'),
					new AtomicRegex<char>('i'),
					new AtomicRegex<char>('l'),
					new AtomicRegex<char>('e')
				}),
				new ConcatRegex<char>(new[]
				{
					new AtomicRegex<char>('i'),
					new AtomicRegex<char>('f')
				}),
				new ConcatRegex<char>(new[]
				{
					new AtomicRegex<char>('e'),
					new AtomicRegex<char>('l'),
					new AtomicRegex<char>('s'),
					new AtomicRegex<char>('e')
				}),
				new AtomicRegex<char>('{'),
				new AtomicRegex<char>('}'),
				new AtomicRegex<char>('('),
				new AtomicRegex<char>(')'),
				new ConcatRegex<char>(new[]
				{
					new AtomicRegex<char>('t'),
					new AtomicRegex<char>('r'),
					new AtomicRegex<char>('u'),
					new AtomicRegex<char>('e')
				}),
				new ConcatRegex<char>(new[]
				{
					new AtomicRegex<char>('f'),
					new AtomicRegex<char>('a'),
					new AtomicRegex<char>('l'),
					new AtomicRegex<char>('s'),
					new AtomicRegex<char>('e')
				}),
			}));

		[Fact]
		public void Should_CreateDfa_Accepting_While()
		{
			var dfa = Utils.RegexToDfa(new ConcatRegex<char>(new[]
			{
				new AtomicRegex<char>('w'),
				new AtomicRegex<char>('h'),
				new AtomicRegex<char>('i'),
				new AtomicRegex<char>('l'),
				new AtomicRegex<char>('e')
			}));

			var endRegex = WalkDfa(dfa, "while");

			Assert.True(dfa.Category(endRegex));
		}

		[Theory]
		[InlineData("while(true){}")]
		[InlineData("if(true){}else{}")]
		[InlineData("if(false)")]
		[InlineData("if(false){if(true)}")]
		public void Should_CreateDfa_Accepting_Sequence_of_Keywords(string keywords)
		{
			var dfa = Utils.RegexToDfa(KeywordRegex());

			var endRegex = WalkDfa(dfa, keywords);

			Assert.True(dfa.Category(endRegex));
		}

		[Theory]
		[InlineData("while-if(){}")]
		[InlineData("if(variable)")]
		[InlineData("else-if()")]
		[InlineData("if(1)")]
		public void Should_CreateDfa_Rejecting__WrongSequence_of_Keywords(string keywords)
		{
			var dfa = Utils.RegexToDfa(KeywordRegex());

			var endRegex = WalkDfa(dfa, keywords);

			Assert.False(dfa.Category(endRegex));
		}
	}
}
