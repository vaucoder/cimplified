using Cimplified;
using Cimplified.Generator;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace tests
{
	public class LivenessTests
	{
		public class TestRegister : Register
		{
			public int Id;
			public TestRegister(int id)
			{
				Id = id;
			}

			public override bool Equals(object other)
			{
				return other is TestRegister o && this.Id == o.Id;
			}

			public override int GetHashCode()
			{
				return this.Id;
			}

			public override string ToString()
			{
				return $"T{Id}";
			}
		}

		public class TestInstruction : AsmInstruction
		{
			public override IReadOnlyList<Register> RegistersUsed { get { return usedLst; } }
			public override IReadOnlyList<Register> RegistersDefined { get { return defLst; } }
			public override IReadOnlyList<Label> PossibleTargets { get { return targets; } }

			List<Register> usedLst;
			List<Register> defLst;
			List<Label> targets;

			public int Lab;

			public TestInstruction(int lab, Register used, Register defined, IEnumerable<Label> targetsParams)
			{
				targets = targetsParams.ToList();
				Lab = lab;

				usedLst = new List<Register>();
				if (used is object) {
					usedLst.Add(used);
				}

				defLst = new List<Register>();
				if (defined is object) {
					defLst.Add(defined);
				}
			}

			public override string ToString()
			{
				return $"<Inst {Lab}>";
			}

			public override string ToAsmString()
			{
				return "";
			}
		}

		static void add(List<(Label, AsmInstruction)> lst, int? registerUsed, int? registerDefined, params int[] goTo)
		{
			lst.Add((new NamedLabel(lst.Count.ToString()),
				new TestInstruction(lst.Count,
					registerUsed is null ? null : new TestRegister(registerUsed.Value),
					registerDefined is null ? null : new TestRegister(registerDefined.Value),
					goTo.Select((v) => new NamedLabel(v.ToString())))));
		}

		static void test(List<(Label, AsmInstruction)> lst, params (int, int)[] expected)
		{
			var result = LivenessAnalysis.Analyse(lst);

			var H = new HashSet<(Register, Register)>();
			foreach (var (x, y) in expected) {
				H.Add((new TestRegister(x), new TestRegister(y)));
				H.Add((new TestRegister(y), new TestRegister(x)));
			}

			Assert.True(result.SetEquals(H));
		}

		[Fact]
		public void Test1()
		{
			var L = new List<(Label, AsmInstruction)>();
			add(L, 1, null);

			test(L);
		}

		[Fact]
		public void Test2()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, 1, null, ++i);
			add(L, null, 2, ++i);
			add(L, 2, null, ++i);
			add(L, 3, null);

			test(L, (1, 3), (2, 3));
		}

		[Fact]
		public void Test3()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, null, ++i);
			add(L, 1, null, ++i);
			add(L, null, 2, ++i);
			add(L, 2, null, ++i);
			add(L, 3, null);

			test(L, (1, 3), (2, 3));
		}

		[Fact]
		public void Test6()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, 1, null, ++i);
			add(L, 2, null, ++i);
			add(L, 3, null);

			test(L, (1, 3), (2, 3), (1, 2));
		}

		[Fact]
		public void Test7()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, null, ++i);
			add(L, 1, null, ++i);
			add(L, 2, null, ++i);
			add(L, 3, null);

			test(L, (1, 3), (2, 3), (1, 2));
		}

		[Fact]
		public void Test4()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, null, ++i);
			add(L, 1, null, ++i);
			add(L, null, null, ++i);
			add(L, null, null, ++i);
			add(L, null, null, ++i);
			add(L, null, 2, ++i);
			add(L, 2, null, ++i);
			add(L, 3, null);

			test(L, (1, 3), (2, 3));
		}

		[Fact]
		public void Test9()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, 1, ++i);     // a := 0
			add(L, null, 3, ++i);     // c := 0
			add(L, 1, 2, ++i);        // b := a + 1
			add(L, 3, 3, ++i);        // c := c + 1
			add(L, 2, 1, ++i);        // a := b * 2
			add(L, 1, null, 2, ++i);  // a < 9 ? goto 2 : goto next
			add(L, 3, null);          // return c

			test(L, (1, 3), (2, 3));
		}

		[Fact]
		public void Test10()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, 1, ++i);     // a := 0
			add(L, null, 3, ++i);     // c := 0
			add(L, 1, 2, ++i);        // b := a + 1
			add(L, 3, 2, ++i);        // b := b + 1
			add(L, 2, 1, ++i);        // a := b * 2
			add(L, 1, null, 2, ++i);  // a < 9 ? goto 2 : goto next
			add(L, 3, null);          // return c

			test(L, (1, 3), (2, 3));
		}

		[Fact]
		public void Test11()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, 1, ++i);     // a := 0
			add(L, null, 3, ++i);     // c := 0
			add(L, 1, 2, ++i);        // b := a + 1
			add(L, 3, 2, ++i);        // b := c + 1
			add(L, 2, 1, ++i);        // a := b * 2
			add(L, 1, null, 3, ++i);  // a < 9 ? goto 3 : goto next
			add(L, 3, null);          // return c

			test(L, (1, 3), (2, 3));
		}

		[Fact]
		public void Test12()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, 1, ++i);     // a := 0
			add(L, null, 3, ++i);     // c := 0
			add(L, 1, 2, ++i);        // b := a + 1
			add(L, 3, 3, ++i);        // c := c + 1
			add(L, 2, 1, ++i);        // a := b * 2
			add(L, 1, null, 3, ++i);  // a < 9 ? goto 3 : goto next
			add(L, 3, null);          // return c

			test(L, (1, 3), (2, 3), (1, 2));
		}

		[Fact]
		public void Test13()
		{
			var L = new List<(Label, AsmInstruction)>();
			int i = 0;
			add(L, null, 1, ++i);     // a := 0
			add(L, null, 3, ++i);     // c := 0
			add(L, 1, 2, ++i);        // b := a + 1
			add(L, 2, 3, ++i);        // c := b + 1
			add(L, 2, 1, ++i);        // a := b * 2
			add(L, 1, null, 3, ++i);  // a < 9 ? goto 3 : goto next
			add(L, 3, null);          // return c

			test(L, (1, 3), (2, 3), (1, 2));
		}
	}
}
