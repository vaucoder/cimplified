﻿using Cimplified;
using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace tests
{
    public class NameResolutionTest
    {
        private TypeName IntType = new TypeName("Int");
        private TypeName BoolType = new TypeName("Bool");
        private TypeName VoidType = new TypeName("Void");
        private FunctionName fun1 = new FunctionName("fun1");
        private FunctionName fun1_2 = new FunctionName("fun1");
        private FunctionName main = new FunctionName("main");
        private VarName var1_0 = new VarName("var1");
        private VarName var1_1 = new VarName("var1");
        private VarName var1_2 = new VarName("var1");
        private VarName var1_3 = new VarName("var1");
        private VarName var1_4 = new VarName("var1");
        private IntLiteral number0 = new IntLiteral("0");
        private IntLiteral number1 = new IntLiteral("1");
        private IntLiteral number2 = new IntLiteral("2");
        private IntLiteral number3 = new IntLiteral("3");
        private BoolLiteral trueCondition = new BoolLiteral(true);

        [Fact]
        public void NRTest1()
        {
            /*
             * Int fun1()
             * {
             *      Int var1 = 0;
             *      if (True)
             *      {
             *          var1 = 2;
             *          Int var1 = 1;
             *          var1 = 3;
             *      }
             *      return 0;
             * }
             */

            var args = new List<Argument>();
            var var1Decl1 = new DeclareAssignment(IntType, var1_0, number0);
            var var1Assignment2 = new Assignment(var1_2, number2);
            var var1Decl2 = new DeclareAssignment(IntType, var1_1, number1);
            var var1Assignment3 = new Assignment(var1_3, number3);
            var if1Body = new Block(new List<Statement>() { var1Assignment2, var1Decl2, var1Assignment3 });
            var if1 = new IfClause(trueCondition, if1Body, new List<ElIfClause>(), null);
            var returnClause = new ReturnClause(number0);
            Block returnBlock = new Block(new List<Statement>() { var1Decl1, if1, returnClause});
            FunctionDeclaration functionDeclaration = new FunctionDeclaration(IntType, fun1, args, returnBlock);
            var program = new Program(new List<FunctionDeclaration>() { functionDeclaration });           
            NameResolution.Process(program);

            Assert.Equal(var1Decl1, var1Decl1.VarName.Declaration);
            Assert.Equal(var1Decl1, var1Assignment2.VarName.Declaration);
            Assert.Equal(var1Decl2, var1Decl2.VarName.Declaration);
            Assert.Equal(var1Decl2, var1Assignment3.VarName.Declaration);
        }

        [Fact]
        public void NRTest2()
        {
            /*
             * Int fun1(Bool var1)
             * {
             *      var1 = True;
             *      Int var1 = 0;
             *      while (True)
             *      {
             *          var1 = 2;
             *          Bool var1 = True;
             *      }
             *      return 0;
             * }
             */
            var args = new List<Argument>() { new Argument(BoolType, var1_0) };
            var var1Decl1 = new DeclareAssignment(IntType, var1_1, number0);
            var var1Decl2 = new DeclareAssignment(BoolType, var1_2, trueCondition);
            var var1AssignmentTrue = new Assignment(var1_3, trueCondition);
            var var1Assignment2 = new Assignment(var1_4, number2);

            var whileBody = new Block(new List<Statement>() { var1Assignment2, var1Decl2 });
            var while1 = new WhileClause(trueCondition, whileBody);
            var returnClause = new ReturnClause(number0);
            Block returnBlock = new Block(new List<Statement>() { var1AssignmentTrue, var1Decl1, while1, returnClause});
            FunctionDeclaration functionDeclaration = new FunctionDeclaration(IntType, fun1, args, returnBlock);
            var program = new Program(new List<FunctionDeclaration>() { functionDeclaration });
            NameResolution.Process(program);

            Assert.Equal(functionDeclaration, functionDeclaration.FunName.Declaration);
            Assert.Equal(functionDeclaration.ArgumentList[0], functionDeclaration.ArgumentList[0].VarName.Declaration);
            Assert.Equal(functionDeclaration.ArgumentList[0], var1AssignmentTrue.VarName.Declaration);            
            Assert.Equal(var1Decl1, var1Decl1.VarName.Declaration);
            Assert.Equal(var1Decl1, var1Assignment2.VarName.Declaration);
            Assert.Equal(var1Decl2, var1Decl2.VarName.Declaration);
        }

        [Fact]
        public void NRTest3()
        {
         /*
		 * Bool fun1() 
		 * {
		 *		return true;
		 * }
		 *
		 * Void main() 
		 * {
		 *      Bool var1 = fun1();
		 * }
		 */

            var args = new List<Argument>();

            var returnClause = new ReturnClause(trueCondition);
            var fun1Block = new Block(new List<Statement> { returnClause });
            var fun1Decl = new FunctionDeclaration(BoolType, fun1, args, fun1Block);

            var functionCall = new FunctionCall(fun1_2, new List<Expression>());
            var boolTrueDeclaration = new DeclareAssignment(BoolType, var1_0, functionCall);

            var mainBlock = new Block(new List<Statement> { boolTrueDeclaration });
            var mainDeclaration = new FunctionDeclaration(VoidType, main, args, mainBlock);
            var program = new Program(new List<FunctionDeclaration> { fun1Decl, mainDeclaration });

            NameResolution.Process(program);

            Assert.Equal(fun1Decl, fun1.Declaration);
            Assert.Equal(boolTrueDeclaration, var1_0.Declaration);
            Assert.Equal(mainDeclaration, main.Declaration);
            Assert.Equal(fun1Decl, functionCall.FunctionName.Declaration);
        }
    }
}
