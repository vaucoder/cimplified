using Cimplified;
using Cimplified.Parser;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using System;

namespace tests
{
	public class OpInterpreterTests
	{

		static (Program, Int64) Case1()
		{
			/*
			 * Bool f(Bool x) {
			 *     return (not x);
			 * }
			 *
			 * Int main()
			 * {
			 *     Int a = 1;
			 *     while (f(f(a < 5 or a < 5))) {
			 *         a = a + 2;
			 *     }
			 *     return a;
			 * }
			 */

			var xName = "x";
			var xArg = new Argument(new TypeName("Bool"), new VarName(xName));
			var negf = new Negation() { Operand = new VarUse(new VarName(xName)) };
			var retf = new ReturnClause(negf);
			var fblock = new Block(new List<Statement>() { retf });
			var fargs = new List<Argument>() { xArg };
			var fdecl = new FunctionDeclaration(new TypeName("Bool"), new FunctionName("f"), fargs, fblock);

			var aName = "a";
			var aDecl = new DeclareAssignment(new TypeName("Int"), new VarName(aName), new IntLiteral("1"));
			var useA = new VarUse(new VarName(aName));
			var num1 = new IntLiteral("2");
			var plus = new Plus() { Left = useA, Right = num1 };
			var aAss = new Assignment(new VarName(aName), plus);

			var comp = new Leq() { Left = new VarUse(new VarName(aName)), Right = new IntLiteral("5") };
			var boolTerm = new BoolLiteral(false);
			var orTerm = new Or() { Left = comp, Right = comp };
			var fnCall0 = new FunctionCall(new FunctionName("f"), new List<Expression>() { orTerm } );
			var fnCall = new FunctionCall(new FunctionName("f"), new List<Expression>() { fnCall0 } );
			var whileCond = fnCall;
			var whileBlock = new Block(new List<Statement>() { aAss });
			var whileClause = new WhileClause(whileCond, whileBlock);

			var ret = new ReturnClause(new VarUse(new VarName(aName)));

			var mainBody = new Block(new List<Statement>() { aDecl, whileClause, ret });
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { fdecl, main });
			return (program0, 5);
		}

		static (Program, Int64) Case2()
		{
			/*
			 * Int main()
			 * {
			 *     Int a = 1;
			 *     while (a < 5) {
			 *         a = a + 2;
			 *     }
			 *     return a;
			 * }
			 */

			var aName = "a";
			var aDecl = new DeclareAssignment(new TypeName("Int"), new VarName(aName), new IntLiteral("1"));
			var useA = new VarUse(new VarName(aName));
			var num1 = new IntLiteral("2");
			var plus = new Plus() { Left = useA, Right = num1 };
			var aAss = new Assignment(new VarName(aName), plus);

			var comp = new Leq() { Left = new VarUse(new VarName(aName)), Right = new IntLiteral("5") };
			var whileCond = comp;
			var whileBlock = new Block(new List<Statement>() { aAss });
			var whileClause = new WhileClause(whileCond, whileBlock);

			var ret = new ReturnClause(new VarUse(new VarName(aName)));

			var mainBody = new Block(new List<Statement>() { aDecl, whileClause, ret });
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 5);
		}



		static (Program, Int64) CaseFact1()
		{
			/*
			 * Int main()
			 * {
			 *     Int a = 10;
			 *     Int n = 1;
			 *     while (0 < a) {
			 *         n = a * n;
			 *         a = a - 1;
			 *     }
			 *     return n;
			 * }
			 */

			var aName = "a";
			var nName = "n";
			var aDecl = new DeclareAssignment(null, new VarName(aName), new IntLiteral("10"));
			var nDecl = new DeclareAssignment(null, new VarName(nName), new IntLiteral("1"));

			var comp = new Leq() { Left = new IntLiteral("0"), Right = new VarUse(new VarName(aName)) };
			var whileCond = comp;
			var whileBlock = new Block(new List<Statement>() {
				new Assignment(new VarName(nName), new Mult() { Left = new VarUse(new VarName(aName)), Right = new VarUse(new VarName(nName)) }),
				new Assignment(new VarName(aName), new Sub() { Left = new VarUse(new VarName(aName)), Right = new IntLiteral("1") }),
				});
			var whileClause = new WhileClause(whileCond, whileBlock);

			var ret = new ReturnClause(new VarUse(new VarName(nName)));

			var mainBody = new Block(new List<Statement>() { aDecl, nDecl, whileClause, ret });
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 3628800);
		}


		static (Program, Int64) CaseNested1()
		{
			/*
			 * Int main()
			 * {
			 *     Bool f(Bool x) {
			 *         return (not x);
			 *     }
			 *
			 *     Int a = 1;
			 *     while (f(f(a < 5 or a < 5))) {
			 *         a = a + 2;
			 *     }
			 *     return a;
			 * }
			 */

			var xName = "x";
			var xArg = new Argument(new TypeName("Bool"), new VarName(xName));
			var negf = new Negation() { Operand = new VarUse(new VarName(xName)) };
			var retf = new ReturnClause(negf);
			var fblock = new Block(new List<Statement>() { retf });
			var fargs = new List<Argument>() { xArg };
			var fdecl = new FunctionDeclaration(new TypeName("Bool"), new FunctionName("f"), fargs, fblock);

			var aName = "a";
			var aDecl = new DeclareAssignment(new TypeName("Int"), new VarName(aName), new IntLiteral("1"));
			var useA = new VarUse(new VarName(aName));
			var num1 = new IntLiteral("2");
			var plus = new Plus() { Left = useA, Right = num1 };
			var aAss = new Assignment(new VarName(aName), plus);

			var comp = new Leq() { Left = new VarUse(new VarName(aName)), Right = new IntLiteral("5") };
			var boolTerm = new BoolLiteral(false);
			var orTerm = new Or() { Left = comp, Right = comp };
			var fnCall0 = new FunctionCall(new FunctionName("f"), new List<Expression>() { orTerm } );
			var fnCall = new FunctionCall(new FunctionName("f"), new List<Expression>() { fnCall0 } );
			var whileCond = fnCall;
			var whileBlock = new Block(new List<Statement>() { aAss });
			var whileClause = new WhileClause(whileCond, whileBlock);

			var ret = new ReturnClause(new VarUse(new VarName(aName)));

			var mainBody = new Block(new List<Statement>() { fdecl, aDecl, whileClause, ret });
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 5);
		}


		static (Program, Int64) CaseNested2()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 0;
			 *     Int f() {
			 *         y = y + 3;
			 *         return y;
			 *     }
			 *
			 *     Int a = 1;
			 *     while (a < 5) {
			 *         a = a + 2;
			 *         f();
			 *     }
			 *
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("0"));

			var fSt1 = new Assignment(new VarName(yName), new Plus() { Left = new VarUse(new VarName(yName)), Right = new IntLiteral("3") });
			var fSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var fBlock = new Block(fSt1, fSt2);
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName("f"), new List<Argument>(), fBlock);

			var aName = "a";
			var aDecl = new DeclareAssignment(new TypeName("Int"), new VarName(aName), new IntLiteral("1"));
			var useA = new VarUse(new VarName(aName));
			var num1 = new IntLiteral("2");
			var plus = new Plus() { Left = useA, Right = num1 };
			var aAss = new Assignment(new VarName(aName), plus);

			var comp = new Leq() { Left = new VarUse(new VarName(aName)), Right = new IntLiteral("5") };
			var whileCond = comp;
			var fnCall0 = new FunctionCall(new FunctionName("f"), new List<Expression>());
			var whileBlock = new Block(new List<Statement>() { aAss, fnCall0 });
			var whileClause = new WhileClause(whileCond, whileBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName("f"), new List<Expression>()));

			var mainBody = new Block(yDecl, fDecl, aDecl, whileClause, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 9);
		}


		static (Program, Int64) CaseNested3()
		{
			/*
			 * Int main()
			 * {
			 *     Int z = 0;
			 *     Int y = 0;
			 *     Int f() {
			 *         y = y + 3;
			 *         return y;
			 *     }
			 *
			 *     Int a = 1;
			 *     while (a < 5) {
			 *         a = a + 2;
			 *         f();
			 *     }
			 *
			 *     return f();
			 * }
			 */

			var zName = "z";
			var zDecl = new DeclareAssignment(new TypeName("Int"), new VarName(zName), new IntLiteral("0"));

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("0"));

			var fSt1 = new Assignment(new VarName(yName), new Plus() { Left = new VarUse(new VarName(yName)), Right = new IntLiteral("3") });
			var fSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var fBlock = new Block(fSt1, fSt2);
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName("f"), new List<Argument>(), fBlock);

			var aName = "a";
			var aDecl = new DeclareAssignment(new TypeName("Int"), new VarName(aName), new IntLiteral("1"));
			var useA = new VarUse(new VarName(aName));
			var num1 = new IntLiteral("2");
			var plus = new Plus() { Left = useA, Right = num1 };
			var aAss = new Assignment(new VarName(aName), plus);

			var comp = new Leq() { Left = new VarUse(new VarName(aName)), Right = new IntLiteral("5") };
			var whileCond = comp;
			var fnCall0 = new FunctionCall(new FunctionName("f"), new List<Expression>());
			var whileBlock = new Block(new List<Statement>() { aAss, fnCall0 });
			var whileClause = new WhileClause(whileCond, whileBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName("f"), new List<Expression>()));

			var mainBody = new Block(zDecl, yDecl, fDecl, aDecl, whileClause, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 9);
		}


		static (Program, Int64) CaseNested4()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int f() {
			 *         Int g() {
			 *             return y;
			 *         }
			 *         return g();
			 *     }
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var fSt1 = gDecl;
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(gName), new List<Expression>()));
			var fBlock = new Block(fSt1, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(yDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 11);
		}


		static (Program, Int64) CaseNested5()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int f() {
			 *         Int g() {
			 *             y = y + 3;
			 *             return y;
			 *         }
			 *         return g();
			 *     }
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt1 = new Assignment(new VarName(yName), new Plus() { Left = new VarUse(new VarName(yName)), Right = new IntLiteral("3") });
			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt1, gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var fSt1 = gDecl;
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(gName), new List<Expression>()));
			var fBlock = new Block(fSt1, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(yDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 14);
		}


		static (Program, Int64) CaseNested6()
		{
			/*
			 * Int main()
			 * {
			 *     Int z = 77;
			 *     Int y = 23;
			 *     Int f() {
			 *         Int g() {
			 *             y = y + 3;
			 *             return y;
			 *         }
			 *         return g();
			 *     }
			 *     return f();
			 * }
			 */

			var zName = "z";
			var zDecl = new DeclareAssignment(new TypeName("Int"), new VarName(zName), new IntLiteral("77"));

			var yName = "y";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("23"));

			var gSt1 = new Assignment(new VarName(yName), new Plus() { Left = new VarUse(new VarName(yName)), Right = new IntLiteral("3") });
			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt1, gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var fSt1 = gDecl;
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(gName), new List<Expression>()));
			var fBlock = new Block(fSt1, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(zDecl, yDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 26);
		}


		static (Program, Int64) CaseNested7()
		{
			/*
			 * Int main()
			 * {
			 *     Int z = 0;
			 *     Int y = 0;
			 *     Int f() {
			 *         Int g() {
			 *             y = y + 3;
			 *             return y;
			 *         }
			 *         return g();
			 *     }
			 *
			 *     Int a = 1;
			 *     while (a < 5) {
			 *         a = a + 1;
			 *         f();
			 *     }
			 *
			 *     return f();
			 * }
			 */

			var zName = "z";
			var zDecl = new DeclareAssignment(new TypeName("Int"), new VarName(zName), new IntLiteral("0"));

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("0"));

			var gSt1 = new Assignment(new VarName(yName), new Plus() { Left = new VarUse(new VarName(yName)), Right = new IntLiteral("3") });
			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt1, gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var fSt1 = gDecl;
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(gName), new List<Expression>()));
			var fBlock = new Block(fSt1, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var aName = "a";
			var aDecl = new DeclareAssignment(new TypeName("Int"), new VarName(aName), new IntLiteral("1"));
			var useA = new VarUse(new VarName(aName));
			var num1 = new IntLiteral("1");
			var plus = new Plus() { Left = useA, Right = num1 };
			var aAss = new Assignment(new VarName(aName), plus);

			var comp = new Leq() { Left = new VarUse(new VarName(aName)), Right = new IntLiteral("5") };
			var whileCond = comp;
			var fnCall0 = new FunctionCall(new FunctionName(fName), new List<Expression>());
			var whileBlock = new Block(new List<Statement>() { aAss, fnCall0 });
			var whileClause = new WhileClause(whileCond, whileBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(zDecl, yDecl, fDecl, aDecl, whileClause, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 15);
		}


		static (Program, Int64) CaseNestedParallel1()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f() {
			 *         return g();
			 *     }
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(gName), new List<Expression>()));
			var fBlock = new Block(fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 11);
		}


		static (Program, Int64) CaseNestedParallel2()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f() {
			 *         Int h() {
			 *             return g();
			 *         }
			 *         return h();
			 *     }
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hSt2 = new ReturnClause(new FunctionCall(new FunctionName(gName), new List<Expression>()));
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), new List<Argument>(), hBlock);

			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(hName), new List<Expression>()));
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 11);
		}


		static (Program, Int64) CaseNestedParallelArgs1()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f() {
			 *         Int h(Int x1, Int x2, Int x3, Int x4, Int x5, Int x6, Int x7, Int x8, Int x9) {
			 *             return g();
			 *         }
			 *         return h(1, 2, 3, 4, 5, 6, 7, 8, 9);
			 *     }
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hSt2 = new ReturnClause(new FunctionCall(new FunctionName(gName), new List<Expression>()));
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hArgs = new List<Argument>();
			for (int i = 0; i < 9; i++) {
				hArgs.Add(new Argument(new TypeName("Int"), new VarName($"x{i}")));
			}
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), hArgs, hBlock);

			var fhArgs = new List<Expression>();
			for (int i = 0; i < hArgs.Count; i++) {
				fhArgs.Add(new IntLiteral($"{i}"));
			}
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(hName), fhArgs));
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 11);
		}


		static (Program, Int64) CaseNestedParallelArgs2()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f() {
			 *         Int h(Int x1, Int x2, Int x3, Int x4, Int x5, Int x6, Int x7, Int x8, Int x9) {
			 *             return x8 * g();
			 *         }
			 *         return h(1, 2, 3, 4, 5, 6, 7, 8, 9);
			 *     }
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hgCall = new FunctionCall(new FunctionName(gName), new List<Expression>());
			var hRetExpr = new Mult() { Left = new VarUse(new VarName("x8")), Right = hgCall };
			var hSt2 = new ReturnClause(hRetExpr);
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				hArgs.Add(new Argument(new TypeName("Int"), new VarName($"x{i}")));
			}
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), hArgs, hBlock);

			var fhArgs = new List<Expression>();
			for (int i = 0; i < hArgs.Count; i++) {
				fhArgs.Add(new IntLiteral($"{i + 1}"));
			}
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(hName), fhArgs));
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 88);
		}


		static (Program, Int64) CaseNestedParallelArgs3()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f() {
			 *         Int h(Int x1, Int x2, Int x3, Int x4, Int x5, Int x6, Int x7, Int x8, Int x9) {
			 *             return x2 + (x8 * g());
			 *         }
			 *         return h(1, 2, 3, 4, 5, 6, 7, 8, 9);
			 *     }
			 *     return f();
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hgCall = new FunctionCall(new FunctionName(gName), new List<Expression>());
			var hRetMult = new Mult() { Left = new VarUse(new VarName("x8")), Right = hgCall };
			var hRetExpr = new Plus() { Left = new VarUse(new VarName("x2")), Right = hRetMult };
			var hSt2 = new ReturnClause(hRetExpr);
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				hArgs.Add(new Argument(new TypeName("Int"), new VarName($"x{i}")));
			}
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), hArgs, hBlock);

			var fhArgs = new List<Expression>();
			for (int i = 0; i < hArgs.Count; i++) {
				fhArgs.Add(new IntLiteral($"{i + 1}"));
			}
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(hName), fhArgs));
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), new List<Argument>(), fBlock);

			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), new List<Expression>()));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 90);
		}


		static (Program, Int64) CaseNestedParallelArgs4()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f(Int z1, Int z2, Int z3, Int z4, Int z5, Int z6, Int z7, Int z8, Int z9) {
			 *         Int h(Int x1, Int x2, Int x3, Int x4, Int x5, Int x6, Int x7, Int x8, Int x9) {
			 *             return x2 + (x8 * g());
			 *         }
			 *         return h(1, 2, 3, 4, 5, 6, 7, 8, 9);
			 *     }
			 *     return f(10, 20, 30, 40, 50, 60, 70, 80, 90);
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hgCall = new FunctionCall(new FunctionName(gName), new List<Expression>());
			var hRetMult = new Mult() { Left = new VarUse(new VarName("x8")), Right = hgCall };
			var hRetExpr = new Plus() { Left = new VarUse(new VarName("x2")), Right = hRetMult };
			var hSt2 = new ReturnClause(hRetExpr);
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				hArgs.Add(new Argument(new TypeName("Int"), new VarName($"x{i}")));
			}
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), hArgs, hBlock);

			var fhArgs = new List<Expression>();
			for (int i = 0; i < hArgs.Count; i++) {
				fhArgs.Add(new IntLiteral($"{i + 1}"));
			}
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(hName), fhArgs));
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				fArgs.Add(new Argument(new TypeName("Int"), new VarName($"z{i}")));
			}
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), fArgs, fBlock);

			var fInArgs = new List<Expression>();
			for (int i = 0; i < fArgs.Count; i++) {
				fInArgs.Add(new IntLiteral($"{(i + 1) * 10}"));
			}
			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), fInArgs));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 90);
		}


		static (Program, Int64) CaseNestedParallelArgs5()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f(Int z1, Int z2, Int z3, Int z4, Int z5, Int z6, Int z7, Int z8, Int z9) {
			 *         Int h(Int x1, Int x2, Int x3, Int x4, Int x5, Int x6, Int x7, Int x8, Int x9) {
			 *             # 0 * (2 + 8 * 11) = 30 * 90 = 31 * 90 = 2700
			 *             return z3 * (x2 + (x8 * g()));
			 *         }
			 *         return h(1, 2, 3, 4, 5, 6, 7, 8, 9);
			 *     }
			 *     return f(10, 20, 30, 40, 50, 60, 70, 80, 90);
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hgCall = new FunctionCall(new FunctionName(gName), new List<Expression>());
			var hRetMult = new Mult() { Left = new VarUse(new VarName("x8")), Right = hgCall };
			var hRetAdd1 = new Plus() { Left = new VarUse(new VarName("x2")), Right = hRetMult };
			var hRetMult2 = new Mult() { Left = new VarUse(new VarName("z3")), Right = hRetAdd1 };
			var hSt2 = new ReturnClause(hRetMult2);
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				hArgs.Add(new Argument(new TypeName("Int"), new VarName($"x{i}")));
			}
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), hArgs, hBlock);

			var fhArgs = new List<Expression>();
			for (int i = 0; i < hArgs.Count; i++) {
				fhArgs.Add(new IntLiteral($"{i + 1}"));
			}
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(hName), fhArgs));
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				fArgs.Add(new Argument(new TypeName("Int"), new VarName($"z{i}")));
			}
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), fArgs, fBlock);

			var fInArgs = new List<Expression>();
			for (int i = 0; i < fArgs.Count; i++) {
				fInArgs.Add(new IntLiteral($"{(i + 1) * 10}"));
			}
			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), fInArgs));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 2700);
		}


		static (Program, Int64) CaseNestedParallelArgs6()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f(Int z1, Int z2, Int z3, Int z4, Int z5, Int z6, Int z7, Int z8, Int z9) {
			 *         Int h(Int x1, Int x2, Int x3, Int x4, Int x5, Int x6, Int x7, Int x8, Int x9) {
			 *             # 90 + 30 * (2 + 8 * 11) = 90 + 30 * 90 = 31 * 90 = 2790
			 *             return z9 + (z3 * (x2 + (x8 * g())));
			 *         }
			 *         return h(1, 2, 3, 4, 5, 6, 7, 8, 9);
			 *     }
			 *     return f(10, 20, 30, 40, 50, 60, 70, 80, 90);
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hgCall = new FunctionCall(new FunctionName(gName), new List<Expression>());
			var hRetMult = new Mult() { Left = new VarUse(new VarName("x8")), Right = hgCall };
			var hRetAdd1 = new Plus() { Left = new VarUse(new VarName("x2")), Right = hRetMult };
			var hRetMult2 = new Mult() { Left = new VarUse(new VarName("z3")), Right = hRetAdd1 };
			var hRetAdd2 = new Plus() { Left = new VarUse(new VarName("z9")), Right = hRetMult2 };
			var hSt2 = new ReturnClause(hRetAdd2);
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				hArgs.Add(new Argument(new TypeName("Int"), new VarName($"x{i}")));
			}
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), hArgs, hBlock);

			var fhArgs = new List<Expression>();
			for (int i = 0; i < hArgs.Count; i++) {
				fhArgs.Add(new IntLiteral($"{i + 1}"));
			}
			var fSt2 = new ReturnClause(new FunctionCall(new FunctionName(hName), fhArgs));
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				fArgs.Add(new Argument(new TypeName("Int"), new VarName($"z{i}")));
			}
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), fArgs, fBlock);

			var fInArgs = new List<Expression>();
			for (int i = 0; i < fArgs.Count; i++) {
				fInArgs.Add(new IntLiteral($"{(i + 1) * 10}"));
			}
			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), fInArgs));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 2790);
		}


		static (Program, Int64) CaseNestedParallelArgs7()
		{
			/*
			 * Int main()
			 * {
			 *     Int y = 11;
			 *     Int g() {
			 *         return y;
			 *     }
			 *     Int f(Int z1, Int z2, Int z3, Int z4, Int z5, Int z6, Int z7, Int z8, Int z9) {
			 *         Int h(Int x1, Int x2, Int x3, Int x4, Int x5, Int x6, Int x7, Int x8, Int x9) {
			 *             # 90 + 30 * (2 + 8 * 11) = 90 + 30 * 90 = 31 * 90 = 2790
			 *             return z9 + (z3 * (x2 + (x8 * g())));
			 *         }
			 *         # 70 + 10 * 2790 = 70 + 27900 = 27970
			 *         return z7 + (z1 * h(1, 2, 3, 4, 5, 6, 7, 8, 9));
			 *     }
			 *     return f(10, 20, 30, 40, 50, 60, 70, 80, 90);
			 * }
			 */

			var yName = "x";
			var yDecl = new DeclareAssignment(new TypeName("Int"), new VarName(yName), new IntLiteral("11"));

			var gSt2 = new ReturnClause(new VarUse(new VarName(yName)));
			var gBlock = new Block(gSt2);
			var gName = "g";
			var gDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(gName), new List<Argument>(), gBlock);

			var hgCall = new FunctionCall(new FunctionName(gName), new List<Expression>());
			var hRetMult = new Mult() { Left = new VarUse(new VarName("x8")), Right = hgCall };
			var hRetAdd1 = new Plus() { Left = new VarUse(new VarName("x2")), Right = hRetMult };
			var hRetMult2 = new Mult() { Left = new VarUse(new VarName("z3")), Right = hRetAdd1 };
			var hRetAdd2 = new Plus() { Left = new VarUse(new VarName("z9")), Right = hRetMult2 };
			var hSt2 = new ReturnClause(hRetAdd2);
			var hBlock = new Block(hSt2);
			var hName = "h";
			var hArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				hArgs.Add(new Argument(new TypeName("Int"), new VarName($"x{i}")));
			}
			var hDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(hName), hArgs, hBlock);

			var fhArgs = new List<Expression>();
			for (int i = 0; i < hArgs.Count; i++) {
				fhArgs.Add(new IntLiteral($"{i + 1}"));
			}
			var fHCall = new FunctionCall(new FunctionName(hName), fhArgs);
			var fMult = new Mult() { Left = new VarUse(new VarName("z1")), Right = fHCall };
			var fRetExpr = new Plus() { Left = new VarUse(new VarName("z7")), Right = fMult };
			var fSt2 = new ReturnClause(fRetExpr);
			var fBlock = new Block(hDecl, fSt2);
			var fName = "f";
			var fArgs = new List<Argument>();
			for (int i = 1; i <= 9; i++) {
				fArgs.Add(new Argument(new TypeName("Int"), new VarName($"z{i}")));
			}
			var fDecl = new FunctionDeclaration(new TypeName("Int"), new FunctionName(fName), fArgs, fBlock);

			var fInArgs = new List<Expression>();
			for (int i = 0; i < fArgs.Count; i++) {
				fInArgs.Add(new IntLiteral($"{(i + 1) * 10}"));
			}
			var ret = new ReturnClause(new FunctionCall(new FunctionName(fName), fInArgs));

			var mainBody = new Block(yDecl, gDecl, fDecl, ret);
			var main = new FunctionDeclaration(new TypeName("Int"), new FunctionName("main"), new List<Argument>(), mainBody);
			var program0 = new Program(new List<FunctionDeclaration>() { main });
			return (program0, 27970);
		}

		static List<(Program, Int64)> data1 = new List<(Program, Int64)>() {
			Case1(),
			Case2(),
			CaseFact1(),
			CaseNested1(),
			CaseNested2(),
			CaseNested3(),
			CaseNested4(),
			CaseNested5(),
			CaseNested6(),
			CaseNested7(),
			CaseNestedParallel1(),
			CaseNestedParallel2(),
			CaseNestedParallelArgs1(),
			CaseNestedParallelArgs2(),
			CaseNestedParallelArgs3(),
			CaseNestedParallelArgs4(),
			CaseNestedParallelArgs5(),
			CaseNestedParallelArgs6(),
			CaseNestedParallelArgs7(),
		};

		public static IEnumerable<object[]> Data1 = data1.Select(p => new object[] { p.Item1, p.Item2 });

		[Theory]
		[MemberData(nameof(Data1))]
		public void CaseAll(Program program0, Int64 expected)
		{
			NameResolution.Process(program0);
			TypeChecking.Process(program0);
			var program = (Program)SimplifyAST.Simplify(program0);
			VariableResolution.Process(program);

			var functions = Utils.GetFunctionDeclarations(program);
			var r = OpInterpreter.InterpretFunctions(functions.ToList());
			Assert.Equal(expected, r);
		}

	}
}
