using System.Linq;
using Cimplified.Lexer;
using Cimplified.Parser;
using Xunit;

namespace tests.Parser
{
	public class ASTTests
	{
		private ParseTree GetExampleExpression1(string intLiteral)
		{
			return new ParseBranch(CimGrammarCreator.expToExpPrime, new[]
			{
				new ParseBranch(CimGrammarCreator.expPrimeToLiteral, new[]
				{
					new ParseBranch(CimGrammarCreator.literalToBoolOrInt, new[]
					{
						new ParseLeaf(new Token<Terminal>(CimGrammarCreator.intLiteral, intLiteral))
					})
				})
			});
		}

		private ParseTree GetExampleExpression2(string boolLiteral)
		{
			return new ParseBranch(CimGrammarCreator.expToExpPrime, new[]
			{
				new ParseBranch(CimGrammarCreator.expPrimeToLiteral, new[]
				{
					new ParseBranch(CimGrammarCreator.literalToBoolOrInt, new[]
					{
						new ParseLeaf(new Token<Terminal>(CimGrammarCreator.boolLiteral, boolLiteral))
					})
				})
			});
		}

		private ParseTree GetEmptyFunction(string fName)
		{
			ParseTree type = new ParseBranch(CimGrammarCreator.typeToBaseTypesOrVoid,
				new[] {new ParseLeaf(new Token<Terminal>(CimGrammarCreator.voidType))});
			ParseTree name = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.name, fName));
			ParseTree args = new ParseBranch(CimGrammarCreator.parametersListToNonEmpty, new[]
			{
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.lparen)),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.rparen)),
			});
			ParseTree block = new ParseBranch(CimGrammarCreator.blockToStatements, new[]
			{
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.lbracket)),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.rbracket)),
			});
			ParseTree foo = new ParseBranch(CimGrammarCreator.funcDeclProd,
				new[] {type, name, args, block});
			return foo;
		}

		[Fact]
		public void SimpleProgram()
		{
			ParseTree tree = new ParseBranch(CimGrammarCreator.startToFuncDecl, new[]
			{
				GetEmptyFunction("foo"),
				GetEmptyFunction("bar"),
			});

			var program = Program.Create(tree);
			Assert.Equal(2, program.Declarations.Count());
			Assert.Equal("foo", program.Declarations[0].FunName.Name);
			Assert.Equal("bar", program.Declarations[1].FunName.Name);
		}

		[Fact]
		public void EmptyFunction()
		{
			var foo = GetEmptyFunction("foo");
			FunctionDeclaration ast = FunctionDeclaration.Create(foo);
			Assert.Equal(3, ast.Children.Count());
			Assert.Equal("voidType", ast.TypeName.Name);
			Assert.Equal("foo", ast.FunName.Name);
			Assert.Equal(0, ast.ArgumentList.Count);
			Assert.Equal(0, ast.Body.Statements.Count);
		}
		
		[Fact]
		public void FunctionEmptyBlock() 
		{
			ParseTree type = new ParseBranch(CimGrammarCreator.typeToBaseTypesOrVoid,
				new[] {new ParseLeaf(new Token<Terminal>(CimGrammarCreator.voidType))});
			ParseTree name = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.name, "foo"));
			ParseTree args = new ParseBranch(CimGrammarCreator.parametersListToNonEmpty, new ParseTree[]
			{
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.lparen)),
				new ParseBranch(CimGrammarCreator.typeToBaseTypesOrVoid, new []{ new ParseBranch(CimGrammarCreator.baseTypesToIntOrBool, new []{new ParseLeaf(new Token<Terminal>(CimGrammarCreator.intType))})}),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.name, "intArg")),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.comma)),
				new ParseBranch(CimGrammarCreator.typeToBaseTypesOrVoid, new []{ new ParseBranch(CimGrammarCreator.baseTypesToIntOrBool, new []{new ParseLeaf(new Token<Terminal>(CimGrammarCreator.boolType))})}),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.name, "boolArg")),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.rparen)),
			});
			ParseTree block = new ParseBranch(CimGrammarCreator.blockToStatements, new[]
			{
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.lbracket)),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.rbracket)),
			});
			ParseTree foo = new ParseBranch(CimGrammarCreator.funcDeclProd,
				new[] {type, name, args, block});

			FunctionDeclaration ast = FunctionDeclaration.Create(foo);
			
			Assert.Equal(5, ast.Children.Count());
			Assert.Equal("voidType", ast.TypeName.Name);
			Assert.Equal("foo", ast.FunName.Name);
			Assert.Equal(2, ast.ArgumentList.Count);
			Assert.Equal("intType", ast.ArgumentList[0].TypeName.Name);
			Assert.Equal("intArg", ast.ArgumentList[0].VarName.Name);
			Assert.Equal("boolType", ast.ArgumentList[1].TypeName.Name);
			Assert.Equal("boolArg", ast.ArgumentList[1].VarName.Name);
			Assert.Equal(0, ast.Body.Statements.Count);
		}

		[Fact]
		public void StatementToFunction()
		{
			ParseTree type = new ParseBranch(CimGrammarCreator.typeToBaseTypesOrVoid,
				new[] {new ParseLeaf(new Token<Terminal>(CimGrammarCreator.voidType))});
			ParseTree name = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.name, "foo"));
			ParseTree args = new ParseBranch(CimGrammarCreator.parametersListToNonEmpty, new[]
			{
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.lparen)),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.rparen)),
			});
			ParseTree block = new ParseBranch(CimGrammarCreator.blockToStatements, new[]
			{
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.lbracket)),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.rbracket)),
			});
			ParseTree foo = new ParseBranch(CimGrammarCreator.declarationToVFunc,
				new[] {type, name, args, block});
			ParseTree statement = new ParseBranch(CimGrammarCreator.statementToDeclaration, new[] {foo});
		
			Assert.Equal(((ParseBranch)statement).Prod, CimGrammarCreator.statementToDeclaration);
			Statement sFDecl = Statement.Create(statement);
			Assert.True(sFDecl is FunctionDeclaration);
			var fDecl = (FunctionDeclaration)sFDecl;
			Assert.Equal(3, fDecl.Children.Count());
			Assert.Equal("voidType", fDecl.TypeName.Name);
			Assert.Equal("foo", fDecl.FunName.Name);
			Assert.Equal(0, fDecl.ArgumentList.Count);
			Assert.Equal(0, fDecl.Body.Statements.Count);
		}

		[Fact]
		public void StatementEmptyReturn()
		{
			ParseTree returnK = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.returnKeyword));
			ParseTree sc = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.semicolon));
			ParseTree returnS = new ParseBranch(CimGrammarCreator.returnClauseProd, new[] {returnK});
			ParseTree tree = new ParseBranch(CimGrammarCreator.statementToReturn, new[] {returnS, sc});

			Statement statement = Statement.Create(tree);
			Assert.True(statement is ReturnClause);
			Assert.Null(statement.Children.First());
		}

		[Fact]
		public void StatementEmptyBreak()
		{
			ParseTree breakK = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.breakKeyword));
			ParseTree sc = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.semicolon));
			ParseTree breakS = new ParseBranch(CimGrammarCreator.breakClauseProd, new[] {breakK});
			ParseTree tree = new ParseBranch(CimGrammarCreator.statementToBreak, new[] {breakS, sc});

			Statement statement = Statement.Create(tree);
			Assert.True(statement is BreakClause );
			Assert.Null(statement.Children.First());
		}

		[Fact]
		public void StatementBreakInt()
		{
			ParseTree breakK = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.breakKeyword));
			ParseTree intL = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.intLiteral, "5"));
			ParseTree sc = new ParseLeaf(new Token<Terminal>(CimGrammarCreator.semicolon));
			ParseTree breakS = new ParseBranch(CimGrammarCreator.breakClauseProd, new[] {breakK, intL});
			ParseTree tree = new ParseBranch(CimGrammarCreator.statementToBreak, new[] {breakS, sc});

			Statement statement = Statement.Create(tree);
			Assert.True(statement is BreakClause);
			Assert.Equal(((ParseLeaf)intL).Token.Text, ((BreakClause)statement).N.Number);
		}

		[Fact]
		public void ExpressionLiteralInt()
		{
			ParseTree tree = new ParseBranch(CimGrammarCreator.expToExpPrime, new[]
			{
				new ParseBranch(CimGrammarCreator.expPrimeToLiteral, new[]
				{
					new ParseBranch(CimGrammarCreator.literalToBoolOrInt, new[]
					{
						new ParseLeaf(new Token<Terminal>(CimGrammarCreator.intLiteral, "5"))
					})
				})
			});
			Expression expr = Expression.Create(tree);
			Assert.True(expr is IntLiteral);
			Assert.Equal("5", ((IntLiteral)expr).Number);
		}

		[Fact]
		public void ExpressionLiteralBool()
		{
			ParseTree tree = new ParseBranch(CimGrammarCreator.expToExpPrime, new[]
			{
				new ParseBranch(CimGrammarCreator.expPrimeToLiteral, new[]
				{
					new ParseBranch(CimGrammarCreator.literalToBoolOrInt, new[]
					{
						new ParseLeaf(new Token<Terminal>(CimGrammarCreator.boolLiteral, "true"))
					})
				})
			});
			Expression expr = Expression.Create(tree);
			Assert.True(expr is BoolLiteral);
			Assert.True(((BoolLiteral)expr).Value);

			tree = new ParseBranch(CimGrammarCreator.expToExpPrime, new[]
			{
				new ParseBranch(CimGrammarCreator.expPrimeToLiteral, new[]
				{
					new ParseBranch(CimGrammarCreator.literalToBoolOrInt, new[]
					{
						new ParseLeaf(new Token<Terminal>(CimGrammarCreator.boolLiteral, "false"))
					})
				})
			});
			expr = Expression.Create(tree);
			Assert.True(expr is BoolLiteral);
			Assert.False(((BoolLiteral)expr).Value);
		}

		// [Fact]
		// public void ExpressionName()
		// {
		// 	ParseTree tree = new ParseBranch(CimGrammarCreator.expToExpPrime, new[]
		// 	{
		// 		new ParseBranch(CimGrammarCreator.expPrimeToName, new[]
		// 		{
		// 			new ParseLeaf(new Token<Terminal>(CimGrammarCreator.name, "bar"))
		// 		})
		// 	});
		// 	Expression expr = Expression.Create(tree);
		// 	Assert.True(expr is VarUse);
		// 	Assert.Equal("bar", ((VarUse)expr).Name.Name);
		// }
		//
		// [Fact]
		// public void ExpressionExpParented()
		// {
		// 	ParseTree tree = new ParseBranch(CimGrammarCreator.expToExpPrime, new[]
		// 	{
		// 		new ParseBranch(CimGrammarCreator.expPrimeToParentedExp, new[]
		// 		{
		// 			new ParseBranch(CimGrammarCreator.parentedExpProd, new ParseTree[]
		// 			{
		// 			new ParseLeaf(new Token<Terminal>(CimGrammarCreator.lparen)),
		// 				new ParseBranch(CimGrammarCreator.expPrimeToName, new[]
		// 				{
		// 					new ParseLeaf(new Token<Terminal>(CimGrammarCreator.name, "bar"))
		// 				}),
		// 			new ParseLeaf(new Token<Terminal>(CimGrammarCreator.rparen)),
		// 			})
		// 		})
		// 	});
		// 	Expression expr = Expression.Create(tree);
		// 	Assert.True(expr is VarUse);
		// 	Assert.Equal("bar", ((VarUse)expr).Name.Name);
		// }

		[Fact]
		public void ExpressionBinaryOp()
		{
			ParseTree tree = new ParseBranch(CimGrammarCreator.expToBinaryOp, new[]
			{
				GetExampleExpression1("3"),
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.binaryOp, "+")),
				GetExampleExpression1("4"),
			});
			Expression expr = Expression.Create(tree);
			Assert.True(expr is Plus);
			Assert.True(((Plus)expr).Left is IntLiteral);
			Assert.True(((Plus)expr).Right is IntLiteral);
		}

		[Fact]
		public void ExpressionUnaryOp()
		{
			ParseTree tree = new ParseBranch(CimGrammarCreator.expToUnaryOp, new[]
			{
				new ParseLeaf(new Token<Terminal>(CimGrammarCreator.unaryOp, "not")),
				GetExampleExpression2("false"),
			});
			Expression expr = Expression.Create(tree);
			Assert.True(expr is Negation);
			Assert.True(((Negation)expr).Operand is BoolLiteral);
		}

	}
}
