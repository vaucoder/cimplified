﻿using Cimplified.Lexer;
using Cimplified.Parser;
using System.Collections.Generic;
using Xunit;
using static Cimplified.Parser.Parser;
using Action = Cimplified.Parser.Parser.Action;

namespace tests.Parser
{
    public class ParserConstructorTest
    {
        [Fact]
        public void TestConstructor()
        {
            var nonTerminal_S = new NonTerminal("S");
            var nonTerminal_A = new NonTerminal("A");
            var nonTerminal_B = new NonTerminal("B");
            var nonTerminal_C = new NonTerminal("C");

            var terminal_a = new Terminal("a");
            var terminal_b = new Terminal("b");
            var terminal_c = new Terminal("c");

            var epsilon = new StarRegex<Symbol>(new EmptyRegex<Symbol>());

            var A = new AtomicRegex<Symbol>(nonTerminal_A);
            var B = new AtomicRegex<Symbol>(nonTerminal_B);
            var C = new AtomicRegex<Symbol>(nonTerminal_C);

            var a = new AtomicRegex<Symbol>(terminal_a);
            var b = new AtomicRegex<Symbol>(terminal_b);
            var c = new AtomicRegex<Symbol>(terminal_c);

            var S_ABC = new Production(nonTerminal_S, new ConcatRegex<Symbol>(new StarRegex<Symbol>(A), B, C)); // S -> A*BC
            var A_a = new Production(nonTerminal_A, a); // A -> a
            var B_b_eps = new Production(nonTerminal_B, new UnionRegex<Symbol>(b, epsilon)); // B -> b + \epsilon
            var C_c = new Production(nonTerminal_C, c); // C -> c

            var terminals = new List<Terminal>() { terminal_a, terminal_b, terminal_c };
            var productions = new List<Production>() { S_ABC, A_a, B_b_eps, C_c };

            var grammar = new Cimplified.Parser.Grammar(nonTerminal_S, productions, terminals);
            var nullable = new HashSet<NonTerminal>() { nonTerminal_B };
            var first = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>(){ nonTerminal_S, nonTerminal_A, terminal_a, nonTerminal_B, terminal_b, nonTerminal_C, terminal_c } }, // {S, A, a, B, b, C, c}
                { nonTerminal_A, new HashSet<Symbol>(){ nonTerminal_A, terminal_a } }, // {A, a}
                { nonTerminal_B, new HashSet<Symbol>(){ nonTerminal_B, terminal_b } }, // {B, b}
                { nonTerminal_C, new HashSet<Symbol>(){ nonTerminal_C, terminal_c } } // {C, c}
            };

            var follow = new Dictionary<NonTerminal, HashSet<Symbol>>
            {
                { nonTerminal_S, new HashSet<Symbol>(){ } }, // {}
                { nonTerminal_A, new HashSet<Symbol>(){ nonTerminal_A, terminal_a, nonTerminal_B, terminal_b, nonTerminal_C, terminal_c } }, // {A, a, B, b, C, c}
                { nonTerminal_B, new HashSet<Symbol>(){ nonTerminal_C, terminal_c } },  // {C, c}
                { nonTerminal_C, new HashSet<Symbol>(){ } }
            };

            var gdfas = GrammarAnalysis.ComputeGdfas(grammar);
            List<HashableList<Regex<Symbol>>> mapping = new List<HashableList<Regex<Symbol>>>();

            foreach (var gdfa in gdfas.Values)
            {
                var transitions = gdfa.TransitionsTable;                
                Queue<HashableList<Regex<Symbol>>> states = new Queue<HashableList<Regex<Symbol>>>();
                states.Enqueue(gdfa.Start());
                HashSet<HashableList<Regex<Symbol>>> visited = new HashSet<HashableList<Regex<Symbol>>>();
                while (states.Count > 0)
                {
                    var currentState = states.Dequeue();
                    if (visited.Contains(currentState))
                        continue;
                    visited.Add(currentState);
                    mapping.Add(currentState);
                    foreach (var state in transitions[currentState].Values)               
                        states.Enqueue(state);
                }
            }

            var ActionTable = new Dictionary<(NonTerminal, HashableList<Regex<Symbol>>, Symbol), Action>();
            ActionTable[(nonTerminal_S, mapping[0], terminal_a)] = new Parser<HashableList<Regex<Symbol>>>.Call(mapping[0], mapping[3], nonTerminal_A);
            ActionTable[(nonTerminal_S, mapping[0], terminal_b)] = new Parser<HashableList<Regex<Symbol>>>.Call(mapping[1], mapping[5], nonTerminal_B);
            ActionTable[(nonTerminal_S, mapping[0], terminal_c)] = new Parser<HashableList<Regex<Symbol>>>.Call(mapping[1], mapping[5], nonTerminal_B);


            ActionTable[(nonTerminal_S, mapping[1], terminal_c)] = new Parser<HashableList<Regex<Symbol>>>.Call(mapping[2], mapping[7], nonTerminal_C);

            ActionTable[(nonTerminal_S, mapping[2], null)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(S_ABC);

            ActionTable[(nonTerminal_A, mapping[3], terminal_a)] = new Parser<HashableList<Regex<Symbol>>>.Shift(mapping[4]);

            ActionTable[(nonTerminal_A, mapping[4], null)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(A_a);
            ActionTable[(nonTerminal_A, mapping[4], terminal_a)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(A_a);
            ActionTable[(nonTerminal_A, mapping[4], terminal_b)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(A_a);
            ActionTable[(nonTerminal_A, mapping[4], terminal_c)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(A_a);

            ActionTable[(nonTerminal_B, mapping[5], null)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(B_b_eps);
            ActionTable[(nonTerminal_B, mapping[5], terminal_b)] = new Parser<HashableList<Regex<Symbol>>>.Shift(mapping[6]);   
            ActionTable[(nonTerminal_B, mapping[5], terminal_c)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(B_b_eps);

            ActionTable[(nonTerminal_B, mapping[6], null)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(B_b_eps);
            ActionTable[(nonTerminal_B, mapping[6], terminal_c)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(B_b_eps);

            ActionTable[(nonTerminal_C, mapping[7], terminal_c)] = new Parser<HashableList<Regex<Symbol>>>.Shift(mapping[8]);

            ActionTable[(nonTerminal_C, mapping[8], null)] = new Parser<HashableList<Regex<Symbol>>>.Reduce(C_c);

            var actionTable = new Cimplified.Parser.Parser(grammar).ActionTable;

            var expected = new List<KeyValuePair<(NonTerminal, HashableList<Regex<Symbol>>, Symbol), Action>>();
            var computed = new List<KeyValuePair<(NonTerminal, HashableList<Regex<Symbol>>, Symbol), Action>>();

            foreach (var v in ActionTable)
                expected.Add(v);

            foreach (var v in actionTable)
                computed.Add(v);

            for (int i = 0; i < expected.Count; i++)
                Assert.Equal(expected[i].GetType(), computed[i].GetType());
        }
    }
}
