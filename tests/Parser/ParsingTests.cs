using Cimplified.Parser;
using Cimplified.Lexer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xunit;

namespace tests.Parser
{
	public class ParsingTests
	{
		private Parser<int> NewParser()
		{
			var parser = new Parser<int>();
			NonTerminal startNT = new NonTerminal("start");
			parser.ActionTable = new Dictionary<(NonTerminal, int, Symbol), Parser<int>.Action>();
			parser.StartState = (startNT, 0);
			return parser;
		}

		private ParseLeaf NewLeaf(Terminal s)
		{
			return new ParseLeaf(new Token<Terminal>(s));
		}

		private List<ParseTree> CreateInput(params Terminal[] str)
		{
			var input = new List<ParseTree>();
			foreach (var s in str)
				input.Add(new ParseLeaf(new Token<Terminal>(s)));
			return input;
		}

		private static bool CompareTrees(ParseTree t1, ParseTree t2)
		{
			if (t1 is ParseLeaf && t2 is ParseLeaf)
			{
				return t1.Symbol.Equals(t2.Symbol);
			}
			if (t1 is ParseBranch && t2 is ParseBranch)
			{
				ParseBranch b1 = (ParseBranch)t1;
				ParseBranch b2 = (ParseBranch)t2;
				if (!b1.Prod.Lhs.Equals(b2.Prod.Lhs) || !b1.Prod.Rhs.Equals(b2.Prod.Rhs))
					return false;
				if (b1.Children.Count() != b2.Children.Count())
					return false;
				foreach (var (child1, child2) in Enumerable.Zip(b1.Children, b2.Children, (i, j) => (i, j)))
				{
					if (!CompareTrees(child1, child2))
						return false;
				}
				return true;
			}
			return false;
		}

		[Fact]
		public void TestComparison0()
		{
			var a = new Terminal();
			ParseTree l11 = NewLeaf(a);
			ParseTree l12 = NewLeaf(a);
			Assert.True(CompareTrees(l11, l12));
		}
		
		[Fact]
		public void TestComparison1()
		{
			var a = new Terminal();
			var nt1 = new NonTerminal();
			var nt2 = new NonTerminal();
			
			Regex<Symbol> r1 = new AtomicRegex<Symbol>(a);
			var l11 = NewLeaf(a);
			var l12 = NewLeaf(a);
			Regex<Symbol> r11 = new ConcatRegex<Symbol>(new[] { r1, r1 });
			var p11 = new Production(nt1, r1);
			var b11 = new ParseBranch(p11, new List<ParseTree> {l11, l12});
			var l13 = NewLeaf(a);
			Regex<Symbol> r12 = new ConcatRegex<Symbol>(new[] { r1, r11 });
			var p12 = new Production(nt2, r12);
			var b12 = new ParseBranch(p12, new List<ParseTree> {l13, b11});
			
			Regex<Symbol> r2 = new AtomicRegex<Symbol>(a);
			var l21 = NewLeaf(a);
			var l22 = NewLeaf(a);
			Regex<Symbol> r21 = new ConcatRegex<Symbol>(new[] { r2, r2 });
			var p21 = new Production(nt1, r2);
			var b21 = new ParseBranch(p21, new List<ParseTree> {l21, l22});
			var l23 = NewLeaf(a);
			Regex<Symbol> r22 = new ConcatRegex<Symbol>(new[] { r2, r21 });
			var p22 = new Production(nt2, r22);
			var b22 = new ParseBranch(p22, new List<ParseTree> {l23, b21});
			
			Assert.True(CompareTrees(b12, b22));
		}
		
		[Fact]
		public void TestComparison2()
		{
			var a = new Terminal();
			var nt1 = new NonTerminal();
			var nt2 = new NonTerminal();
			
			Regex<Symbol> r1 = new AtomicRegex<Symbol>(a);
			var l11 = NewLeaf(a);
			var l12 = NewLeaf(a);
			Regex<Symbol> r11 = new ConcatRegex<Symbol>(new[] { r1, r1 });
			var p11 = new Production(nt1, r1);
			var b11 = new ParseBranch(p11, new List<ParseTree> {l11, l12});
			var l13 = NewLeaf(a);
			Regex<Symbol> r12 = new ConcatRegex<Symbol>(new[] { r1, r11 });
			var p12 = new Production(nt2, r12);
			var b12 = new ParseBranch(p12, new List<ParseTree> {l13, b11});
			
			Regex<Symbol> r2 = new AtomicRegex<Symbol>(a);
			var l21 = NewLeaf(a);
			var l22 = NewLeaf(a);
			Regex<Symbol> r21 = new ConcatRegex<Symbol>(new[] { r2 });
			var p21 = new Production(nt1, r2);
			var b21 = new ParseBranch(p21, new List<ParseTree> {l21, l22});
			var l23 = NewLeaf(a);
			Regex<Symbol> r22 = new ConcatRegex<Symbol>(new[] { r2, r21 });
			var p22 = new Production(nt2, r22);
			var b22 = new ParseBranch(p22, new List<ParseTree> {l23, b21});
			
			Assert.False(CompareTrees(b12, b22));
		}
		
		[Fact]
		public void TestAStar()
		{
			var parser = NewParser();
			var a = new Terminal();
			NonTerminal startNT = parser.StartState.Item1;
			
			Regex<Symbol> regex = new StarRegex<Symbol>(new AtomicRegex<Symbol>(a));
			var production = new Production(new NonTerminal(), regex);

			parser.ActionTable[(startNT, 0, a)] = new Parser<int>.Shift(0);
			parser.ActionTable[(startNT, 0, null)] = new Parser<int>.Reduce(production);
			
			var input = new List<ParseTree> { NewLeaf(a), NewLeaf(a), NewLeaf(a) };
			var expected = new ParseBranch(production, input);

			var tree = parser.Parse(input);
			Assert.True(CompareTrees(tree, expected));
		}
		
		[Fact]
		public void TestPseudofunction1()
		{
			var parser = NewParser();
			
			// S -> VlVBr
			// B -> (cV)*
			// V -> tn
			var s = parser.StartState.Item1;
			var v = new NonTerminal("V");
			var b = new NonTerminal("B");
			var t = new Terminal("t");
			var n = new Terminal("n");
			var lp = new Terminal("lp");
			var rp = new Terminal("rp");
			var c = new Terminal("c");

			Regex<Symbol> r_t = new AtomicRegex<Symbol>(t);
			Regex<Symbol> r_n = new AtomicRegex<Symbol>(n);
			Regex<Symbol> r_lp = new AtomicRegex<Symbol>(lp);
			Regex<Symbol> r_rp = new AtomicRegex<Symbol>(rp);
			Regex<Symbol> r_c = new AtomicRegex<Symbol>(c);
			Regex<Symbol> r_v = new ConcatRegex<Symbol>(new [] {r_t, r_n});
			Regex<Symbol> r_b = new StarRegex<Symbol>(new ConcatRegex<Symbol>(new [] {r_c, r_v}));
			Regex<Symbol> r_s = new ConcatRegex<Symbol>(new [] {r_v, r_lp, r_v, r_b, r_rp});

			var p_s = new Production(s, r_s);
			var p_b = new Production(b, r_b);
			var p_v = new Production(v, r_v);

			parser.ActionTable[(s, 0, t)] = new Parser<int>.Call(1, 0, v);
			parser.ActionTable[(v, 0, t)] = new Parser<int>.Shift(1);
			parser.ActionTable[(v, 1, n)] = new Parser<int>.Shift(2);
			parser.ActionTable[(v, 2, lp)] = new Parser<int>.Reduce(p_v);
			parser.ActionTable[(v, 2, rp)] = new Parser<int>.Reduce(p_v);
			parser.ActionTable[(v, 2, c)] = new Parser<int>.Reduce(p_v);
			parser.ActionTable[(s, 1, lp)] = new Parser<int>.Shift(2);
			parser.ActionTable[(s, 2, t)] = new Parser<int>.Call(3, 0, v);
			parser.ActionTable[(s, 3, rp)] = new Parser<int>.Call(4, 0, b);
			parser.ActionTable[(s, 4, rp)] = new Parser<int>.Shift(5);
			parser.ActionTable[(s, 3, c)] = new Parser<int>.Call(4, 0, b);
			parser.ActionTable[(b, 0, c)] = new Parser<int>.Shift(1);
			parser.ActionTable[(b, 1, t)] = new Parser<int>.Call(0, 0, v);
			parser.ActionTable[(b, 0, rp)] = new Parser<int>.Reduce(p_b);
			parser.ActionTable[(s, 5, null)] = new Parser<int>.Reduce(p_s);

			var input = CreateInput(new [] {t, n, lp, t, n, rp});
			var tnBranch = new ParseBranch(p_v, CreateInput(t, n));
			var expected = new ParseBranch(p_s, new List<ParseTree>
			{
				tnBranch,
				new ParseLeaf(new Token<Terminal>(lp)),
				tnBranch,
				new ParseBranch(p_b, new List<ParseTree>()),
				new ParseLeaf(new Token<Terminal>(rp)),
			});
			var output = parser.Parse(input);
			Assert.True(CompareTrees(output, expected));
		}
		
		[Fact]
	 	public void TestPseudofunction2()
	 	{
			var parser = NewParser();
			
			// S -> VlVBr
			// B -> (cV)*
			// V -> tn
			var s = parser.StartState.Item1;
			var v = new NonTerminal("V");
			var b = new NonTerminal("B");
			var t = new Terminal("t");
			var n = new Terminal("n");
			var lp = new Terminal("lp");
			var rp = new Terminal("rp");
			var c = new Terminal("c");

			Regex<Symbol> r_t = new AtomicRegex<Symbol>(t);
			Regex<Symbol> r_n = new AtomicRegex<Symbol>(n);
			Regex<Symbol> r_lp = new AtomicRegex<Symbol>(lp);
			Regex<Symbol> r_rp = new AtomicRegex<Symbol>(rp);
			Regex<Symbol> r_c = new AtomicRegex<Symbol>(c);
			Regex<Symbol> r_v = new ConcatRegex<Symbol>(new [] {r_t, r_n});
			Regex<Symbol> r_b = new StarRegex<Symbol>(new ConcatRegex<Symbol>(new [] {r_c, r_v}));
			Regex<Symbol> r_s = new ConcatRegex<Symbol>(new [] {r_v, r_lp, r_v, r_b, r_rp});

			var p_s = new Production(s, r_s);
			var p_b = new Production(b, r_b);
			var p_v = new Production(v, r_v);

			parser.ActionTable[(s, 0, t)] = new Parser<int>.Call(1, 0, v);
			parser.ActionTable[(v, 0, t)] = new Parser<int>.Shift(1);
			parser.ActionTable[(v, 1, n)] = new Parser<int>.Shift(2);
			parser.ActionTable[(v, 2, lp)] = new Parser<int>.Reduce(p_v);
			parser.ActionTable[(v, 2, rp)] = new Parser<int>.Reduce(p_v);
			parser.ActionTable[(v, 2, c)] = new Parser<int>.Reduce(p_v);
			parser.ActionTable[(s, 1, lp)] = new Parser<int>.Shift(2);
			parser.ActionTable[(s, 2, t)] = new Parser<int>.Call(3, 0, v);
			parser.ActionTable[(s, 3, rp)] = new Parser<int>.Call(4, 0, b);
			parser.ActionTable[(s, 4, rp)] = new Parser<int>.Shift(5);
			parser.ActionTable[(s, 3, c)] = new Parser<int>.Call(4, 0, b);
			parser.ActionTable[(b, 0, c)] = new Parser<int>.Shift(1);
			parser.ActionTable[(b, 1, t)] = new Parser<int>.Call(0, 0, v);
			parser.ActionTable[(b, 0, rp)] = new Parser<int>.Reduce(p_b);
			parser.ActionTable[(s, 5, null)] = new Parser<int>.Reduce(p_s);
	
	 		var input = CreateInput(new [] {t, n, lp, t, n, c, t, n, c, t, n, rp});
	 		var vBranch = new ParseBranch(p_v, CreateInput(t, n));
	 		var bBranch = new ParseBranch(p_b, new List<ParseTree>
	 		{
	 			new ParseLeaf(new Token<Terminal>(c)),
	 			vBranch,
	 			new ParseLeaf(new Token<Terminal>(c)),
	 			vBranch
	 		});
	 		var expected = new ParseBranch(p_s, new List<ParseTree>
	 		{
	 			vBranch,
	 			new ParseLeaf(new Token<Terminal>(lp)),
	 			vBranch,
	 			bBranch,
	 			new ParseLeaf(new Token<Terminal>(rp))
	 		});
	 		var output = parser.Parse(input);
	 		Assert.True(CompareTrees(output, expected));
	 	}
	
		[Fact]
		public void TestRecursiveParentheses1()
		{
			var parser = NewParser();
			// S -> lAr
			// A -> (x*lAr)* | x*
			var s = parser.StartState.Item1;
			var a = new NonTerminal("A");
			var l = new Terminal("l");
			var r = new Terminal("r");
			var x = new Terminal("x");
			
			Regex<Symbol> r_l = new AtomicRegex<Symbol>(l);
			Regex<Symbol> r_r = new AtomicRegex<Symbol>(r);
			Regex<Symbol> r_x = new AtomicRegex<Symbol>(x);
			Regex<Symbol> r_a = new AtomicRegex<Symbol>(a);
			Regex<Symbol> r_s = new ConcatRegex<Symbol>(new [] {r_l, r_a, r_r});
			Regex<Symbol> r_a1 = new StarRegex<Symbol>(r_x);
			Regex<Symbol> r_a2 = new StarRegex<Symbol>(new ConcatRegex<Symbol>(new []
			{
				new StarRegex<Symbol>(r_x),
				r_l, r_a, r_r
			}));
	
			var p_s = new Production(s, r_s);
			var p_a1 = new Production(a, r_a1);
			var p_a2 = new Production(a, r_a2);
			
			parser.ActionTable[(s, 0, l)] = new Parser<int>.Shift(1);
			parser.ActionTable[(s, 1, x)] = new Parser<int>.Call(2, 0, a);
			parser.ActionTable[(s, 1, l)] = new Parser<int>.Call(2, 0, a);
			parser.ActionTable[(s, 1, r)] = new Parser<int>.Call(2, 0, a);
			parser.ActionTable[(s, 2, r)] = new Parser<int>.Shift(3);
			parser.ActionTable[(s, 3, null)] = new Parser<int>.Reduce(p_s);
			parser.ActionTable[(a, 0, x)] = new Parser<int>.Shift(0);
			parser.ActionTable[(a, 0, l)] = new Parser<int>.Shift(2);
			parser.ActionTable[(a, 0, r)] = new Parser<int>.Reduce(p_a1);
			parser.ActionTable[(a, 2, l)] = new Parser<int>.Call(3, 0, a);
			parser.ActionTable[(a, 2, x)] = new Parser<int>.Call(3, 0, a);
			parser.ActionTable[(a, 2, r)] = new Parser<int>.Call(3, 0, a);
			parser.ActionTable[(a, 3, r)] = new Parser<int>.Shift(4);
			parser.ActionTable[(a, 4, x)] = new Parser<int>.Shift(1);
			parser.ActionTable[(a, 4, l)] = new Parser<int>.Shift(2);
			parser.ActionTable[(a, 1, x)] = new Parser<int>.Shift(1);
			parser.ActionTable[(a, 1, l)] = new Parser<int>.Shift(2);
			parser.ActionTable[(a, 4, r)] = new Parser<int>.Reduce(p_a2);
	
			var input = CreateInput(new[] {l, x, x, l, x, r, l, r, l, l, l, x, x, r, r, r, x, l, r, r});
			var expected = new ParseBranch(p_s, new List<ParseTree>
			{
				new ParseLeaf(new Token<Terminal>(l)),
				new ParseBranch(p_a2, new List<ParseTree>
				{
					new ParseLeaf(new Token<Terminal>(x)),
					new ParseLeaf(new Token<Terminal>(x)),
					new ParseLeaf(new Token<Terminal>(l)),
					new ParseBranch(p_a1, new List<ParseTree>
					{
						new ParseLeaf(new Token<Terminal>(x))
					}),
					new ParseLeaf(new Token<Terminal>(r)),
					new ParseLeaf(new Token<Terminal>(l)),
					new ParseBranch(p_a1, new List<ParseTree>{}),
					new ParseLeaf(new Token<Terminal>(r)),
					new ParseLeaf(new Token<Terminal>(l)),
					new ParseBranch(p_a2, new List<ParseTree>
					{
						new ParseLeaf(new Token<Terminal>(l)),
						new ParseBranch(p_a2, new List<ParseTree>
						{
							new ParseLeaf(new Token<Terminal>(l)),
							new ParseBranch(p_a1, new List<ParseTree>
							{
								new ParseLeaf(new Token<Terminal>(x)),
								new ParseLeaf(new Token<Terminal>(x))
							}),
							new ParseLeaf(new Token<Terminal>(r)),
						}),
						new ParseLeaf(new Token<Terminal>(r)),
					}),
					new ParseLeaf(new Token<Terminal>(r)),
					new ParseLeaf(new Token<Terminal>(x)),
					new ParseLeaf(new Token<Terminal>(l)),
					new ParseBranch(p_a1, new List<ParseTree>{}),
					new ParseLeaf(new Token<Terminal>(r)),
				}),
				new ParseLeaf(new Token<Terminal>(r)),
			});
			var output = parser.Parse(input);
			Assert.True(CompareTrees(output, expected));
		}
	}
}
