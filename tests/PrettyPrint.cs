using Cimplified;
using Cimplified.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Tests.Lexer
{
	[Collection("UsingPrettyPrint")]
	public class PrettyPrintTests
	{
		static List<(string, Action)> data1 = new List<(string, Action)>() {
			("(Plus 5 3)", () => new PrettyNodeVisitor().Visit(new Plus { Left = new IntLiteral("5"), Right = new IntLiteral("3") })),
			("(Plus 5 True)", () => new PrettyNodeVisitor().Visit(new Plus { Left = new IntLiteral("5"), Right = new BoolLiteral(true) })),
			("(Plus 5 (Or 3 True))", () => new PrettyNodeVisitor().Visit(new Plus { Left = new IntLiteral("5"), Right = new Or { Left = new IntLiteral("3"), Right = new BoolLiteral(true) } })),
		};
		public static IEnumerable<object[]> Data1 = data1.Select(p => new object[] { p.Item1, p.Item2 });

		[Theory]
		[MemberData(nameof(Data1))]
		public void Test1(string expected, Action action)
		{
			var result = "";
			using (new Writer((str) => result += str)) {
				action();
			}
			Assert.Equal(expected, result);
		}
	}
}
