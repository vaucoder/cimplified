﻿using System.Collections.Generic;
using System.Linq;
using Cimplified;
using Cimplified.Parser;
using Xunit;

namespace tests
{
	public class VariableResolutionTest
	{
		private readonly TypeName intType = new TypeName("Int");
		private readonly TypeName boolType = new TypeName("Bool");
		private readonly TypeName voidType = new TypeName("Void");

		private readonly IntLiteral termNumber0 = new IntLiteral("0");
		private readonly IntLiteral termNumber1 = new IntLiteral("1");
		private readonly IntLiteral termNumber2 = new IntLiteral("2");

		private readonly BoolLiteral termBoolFalse = new BoolLiteral(false);
		private readonly BoolLiteral termBoolTrue = new BoolLiteral(true);

		private readonly FunctionName functionMainName = new FunctionName("main");
		private readonly FunctionName functionName = new FunctionName("function");

		private readonly VarName intXVarName = new VarName("intX");
		private readonly VarName intYVarName = new VarName("intY");
		private readonly VarName boolTrueVarName = new VarName("boolTrue");
		private readonly VarName boolFalseVarName = new VarName("boolFalse");

		/*
		 * Void main() {
		 * }
		 */
		[Fact]
		public void OneFunctionNoVariableResolutionTest()
		{
			// given
			var args = new List<Argument>();
			var functionBlock = new Block(new List<Statement> { });
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			Assert.Single(functions);
			Assert.Empty(functions[0].Arguments);
			Assert.Empty(functions[0].Variables);
		}

		/*
         * Void main() {
         *	Int intX = 0;
         * }
         */
		[Fact]
		public void OneFunctionOneVariableResolutionTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			Assert.Single(functions);
			Assert.Empty(functions[0].Arguments);
			Assert.Single(functions[0].Variables);
		}

		/*
         * Void main() {
         *	Int intX = 0;
		 *	Int intY = 1;
         * }
         */
		[Fact]
		public void OneFunctionTwoVariableResolutionTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var intYDeclaration = new DeclareAssignment(intType, intYVarName, termNumber1);
			intYVarName.Declaration = intYDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration, intYDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			Assert.Single(functions);
			Assert.Empty(functions[0].Arguments);
			Assert.Equal(2, functions[0].Variables.Count);
			Assert.Equal(functions[0], functions[0].Variables[0].Creator);
			Assert.Contains(functions[0], functions[0].Variables[0].UsedInFunctions);
			Assert.Equal(functions[0], functions[0].Variables[1].Creator);
			Assert.Contains(functions[0], functions[0].Variables[1].UsedInFunctions);
		}
		
		/*
         * Void main() {
         *	Int intX = 0;
		 *	intX = 1;
         * }
         */
		[Fact]
		public void OneFunctionVariableReassignmentResolutionTest()
		{
			// given
			var args = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var intXReassignment = new Assignment(intXVarName, termNumber1);
			var functionBlock = new Block(new List<Statement> {intXDeclaration, intXReassignment});
			var functionDeclaration = new FunctionDeclaration(voidType, functionMainName, args, functionBlock);
			functionMainName.Declaration = functionDeclaration;
			var program = new Program(new List<FunctionDeclaration> {functionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			Assert.Single(functions);
			Assert.Empty(functions[0].Arguments);
			Assert.Single(functions[0].Variables);
			Assert.Equal(functions[0], functions[0].Variables[0].Creator);
			Assert.Contains(functions[0], functions[0].Variables[0].UsedInFunctions);
		}

		/*
		 * Void fun() {
		 *	Int intY = 1;
		 * }
		 * 
         * Void main() {
         *	Int intX = 0;
         * }
         */
		[Fact]
		public void TwoFunctionsOneVariableResolutionTest()
		{
			// given
			var functionArgs = new List<Argument>();
			var intYDeclaration = new DeclareAssignment(intType, intYVarName, termNumber1);
			intYVarName.Declaration = intYDeclaration;
			var functionBlock = new Block(new List<Statement> {intYDeclaration});
			var functionDeclaration = new FunctionDeclaration(voidType, functionName, functionArgs, functionBlock);
			functionName.Declaration = functionDeclaration;

			var mainArgs = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var mainFunctionBlock = new Block(new List<Statement> {intXDeclaration});
			var mainFunctionDeclaration =
				new FunctionDeclaration(voidType, functionMainName, mainArgs, mainFunctionBlock);
			functionMainName.Declaration = mainFunctionDeclaration;

			var program = new Program(new List<FunctionDeclaration> {functionDeclaration, mainFunctionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			Assert.Equal(2, functions.Count);
			Assert.Empty(functions[0].Arguments);
			Assert.Empty(functions[1].Arguments);
			Assert.Single(functions[0].Variables);
			Assert.Single(functions[1].Variables);
			Assert.Equal(functions[0], functions[0].Variables[0].Creator);
			Assert.Equal(functions[1], functions[1].Variables[0].Creator);
			Assert.Equal(intYVarName.Declaration.Var, functions[0].Variables[0]);
			Assert.Equal(intXVarName.Declaration.Var, functions[1].Variables[0]);
			Assert.Contains(functions[0], functions[0].Variables[0].UsedInFunctions);
			Assert.Contains(functions[1], functions[1].Variables[0].UsedInFunctions);
		}

		/*
         * Void main(Int any) {
         * }
         */
		[Fact]
		public void OneFunctionOneArgumentResolutionTest()
		{
			// given
			var anyVarName = new VarName("var");
			var anyVarArg = new Argument(intType, anyVarName);
			anyVarName.Declaration = anyVarArg;

			var mainArgs = new List<Argument> { anyVarArg };
			var mainFunctionBlock = new Block(new List<Statement>());
			var mainFunctionDeclaration =
				new FunctionDeclaration(voidType, functionMainName, mainArgs, mainFunctionBlock);
			functionMainName.Declaration = mainFunctionDeclaration;

			var program = new Program(new List<FunctionDeclaration> {mainFunctionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			Assert.Equal(1, functions.Count);
			Assert.Single(functions[0].Arguments);
			Assert.Empty(functions[0].Variables);
			Assert.Contains(functions[0], functions[0].Arguments[0].UsedInFunctions);
			Assert.Equal(anyVarName.Declaration.Var, functions[0].Arguments[0]);
		}

		/*
         * Void main(Int intX, Bool boolTrue) {
         * }
         */
		[Fact]
		public void OneFunctionTwoArgumentsResolutionTest()
		{
			// given
			var intXVarName = new VarName("intX");
			var intXVarNameArg =  new Argument(intType, intXVarName);
			intXVarName.Declaration = intXVarNameArg;

			var boolTrueVarName = new VarName("boolTrue");
			var boolTrueVarNameArg =  new Argument(boolType, boolTrueVarName);
			boolTrueVarName.Declaration = boolTrueVarNameArg;

			var mainArgs = new List<Argument>() { intXVarNameArg, boolTrueVarNameArg };
			var mainFunctionBlock = new Block(new List<Statement>());
			var mainFunctionDeclaration =
				new FunctionDeclaration(voidType, functionMainName, mainArgs, mainFunctionBlock);
			functionMainName.Declaration = mainFunctionDeclaration;

			var program = new Program(new List<FunctionDeclaration> {mainFunctionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			Assert.Equal(1, functions.Count);
			Assert.Equal(2, functions[0].Arguments.Count);
			Assert.Empty(functions[0].Variables);
			Assert.Contains(functions[0], functions[0].Arguments[0].UsedInFunctions);
			Assert.Equal(intXVarName.Declaration.Var, functions[0].Arguments[0]);
			Assert.Equal(boolTrueVarName.Declaration.Var, functions[0].Arguments[1]);
		}

		/*
		 * Void main() {
		 *	Int intX = 0;
		 *	Void function() {
		 *		intX = 1;
		 *	}
		 * }
		 */
		[Fact]
		public void TwoFunctionsNestedUsingVarResolutionTest()
		{
			// given
			var intXVarName = new VarName("intX");
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;

			var mainArgs = new List<Argument>();
			var intXUsage = new Assignment(intXVarName, termNumber1);
			var functionBlock = new Block(new List<Statement> {intXUsage});
			var functionDeclaration =
				new FunctionDeclaration(voidType, functionName, new List<Argument>(), functionBlock);
			var mainFunctionBlock = new Block(new List<Statement> {intXDeclaration, functionDeclaration});
			var mainFunctionDeclaration =
				new FunctionDeclaration(voidType, functionMainName, mainArgs, mainFunctionBlock);
			functionMainName.Declaration = mainFunctionDeclaration;

			var program = new Program(new List<FunctionDeclaration> {mainFunctionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			// functions[1] is 'main' function
			// functions[0] is 'function'
			Assert.Equal(2, functions.Count);
			Assert.Empty(functions[0].Variables);
			Assert.Single(functions[1].Variables);
			Assert.Empty(functions[0].Arguments);
			Assert.Empty(functions[1].Arguments);
			Assert.Equal(intXVarName.Declaration.Var, functions[1].Variables[0]);
			Assert.Contains(functions[0], intXVarName.Declaration.Var.UsedInFunctions);
			Assert.Contains(functions[1], intXVarName.Declaration.Var.UsedInFunctions);
		}


		/*
		 * Void main(Int intX) {
		 *	Void function() {
		 *		intX = 1;
		 *	}
		 * }
		 */
		[Fact]
		public void TwoFunctionsNestedUsingArgumentsResolutionTest()
		{
			// given
			var intXVarName = new VarName("intX");
			var mainArgs = new List<Argument> {new Argument(intType, intXVarName)};
			intXVarName.Declaration = mainArgs[0];
			var intXUsage = new Assignment(intXVarName, termNumber1);
			var functionBlock = new Block(new List<Statement> {intXUsage});
			var functionDeclaration =
				new FunctionDeclaration(voidType, functionName, new List<Argument>(), functionBlock);
			var mainFunctionBlock = new Block(new List<Statement> {functionDeclaration});
			var mainFunctionDeclaration =
				new FunctionDeclaration(voidType, functionMainName, mainArgs, mainFunctionBlock);
			functionMainName.Declaration = mainFunctionDeclaration;

			var program = new Program(new List<FunctionDeclaration> {mainFunctionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			// functions[1] is 'main' function
			// functions[0] is 'function'
			Assert.Equal(2, functions.Count);
			Assert.Empty(functions[0].Variables);
			Assert.Empty(functions[1].Variables);
			Assert.Empty(functions[0].Arguments);
			Assert.Single(functions[1].Arguments);
			Assert.Equal(intXVarName.Declaration.Var, functions[1].Arguments[0]);
			Assert.Contains(functions[0], intXVarName.Declaration.Var.UsedInFunctions);
			Assert.Contains(functions[1], intXVarName.Declaration.Var.UsedInFunctions);
		}

		/*
		 * Void main() {
		 *	Void function() {
		 *		Int intX = 1;
		 *	}
		 * }
		 */
		[Fact]
		public void TwoFunctionsNestedVariableInNestedResolutionTest()
		{
			// given
			var intXVarName = new VarName("intX");
			var mainArgs = new List<Argument>();
			var intXDeclaration = new DeclareAssignment(intType, intXVarName, termNumber0);
			intXVarName.Declaration = intXDeclaration;
			var functionBlock = new Block(new List<Statement> {intXDeclaration});
			var functionDeclaration =
				new FunctionDeclaration(voidType, functionName, new List<Argument>(), functionBlock);
			var mainFunctionBlock = new Block(new List<Statement> {functionDeclaration});
			var mainFunctionDeclaration =
				new FunctionDeclaration(voidType, functionMainName, mainArgs, mainFunctionBlock);
			functionMainName.Declaration = mainFunctionDeclaration;

			var program = new Program(new List<FunctionDeclaration> {mainFunctionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			// functions[1] is 'main' function
			// functions[0] is 'function'
			Assert.Equal(2, functions.Count);
			Assert.Single(functions[0].Variables);
			Assert.Empty(functions[1].Variables);
			Assert.Empty(functions[0].Arguments);
			Assert.Empty(functions[1].Arguments);
			Assert.Equal(intXVarName.Declaration.Var, functions[0].Variables[0]);
			Assert.Contains(functions[0], intXVarName.Declaration.Var.UsedInFunctions);
			Assert.DoesNotContain(functions[1], intXVarName.Declaration.Var.UsedInFunctions);
		}		
		
		
		/*
		 * Void main() {
		 *	Void function(Int intX) {
		 *	}
		 * }
		 */
		[Fact]
		public void TwoFunctionsNestedArgumentInNestedResolutionTest()
		{
			// given
			var intXVarName = new VarName("intX");
			var functionName = new FunctionName("function");
			var mainArgs = new List<Argument>();
			var functionBlock = new Block(new List<Statement> {});
			var intXVarNameArg = new Argument(intType, intXVarName);
			intXVarName.Declaration = intXVarNameArg;
			var functionDeclaration =
				new FunctionDeclaration(voidType, functionName, new List<Argument>{intXVarNameArg}, functionBlock);
			functionName.Declaration = functionDeclaration;
			var mainFunctionBlock = new Block(new List<Statement> {functionDeclaration});
			var mainFunctionDeclaration =
				new FunctionDeclaration(voidType, functionMainName, mainArgs, mainFunctionBlock);
			functionMainName.Declaration = mainFunctionDeclaration;

			var program = new Program(new List<FunctionDeclaration> {mainFunctionDeclaration});

			// when
			var functions = VariableResolution.Process(program);

			// then
			// functions[1] is 'main' function
			// functions[0] is 'function'
			Assert.Equal(2, functions.Count);
			Assert.Empty(functions[0].Variables);
			Assert.Empty(functions[1].Variables);
			Assert.Single(functions[0].Arguments);
			Assert.Empty(functions[1].Arguments);
			Assert.Equal(intXVarName.Declaration.Var, functions[0].Arguments[0]);
			Assert.Contains(functions[0], intXVarName.Declaration.Var.UsedInFunctions);
			Assert.DoesNotContain(functions[1], intXVarName.Declaration.Var.UsedInFunctions);
		}
	}
}
